//
//  UIFont+Additions.swift
//  Tawseel
//
//  Created by Omar Al tawashi on 8/30/19.
//  Copyright © 2019 Omar Al tawashi. All rights reserved.
//

import Foundation
import UIKit


extension UIFont {
 
    
    static func FFShamelFamilySemiRoundBook(ofSize: CGFloat) -> UIFont {
  
        return UIFont(name: "FFShamelFamilySemiRoundBook-S", size: ofSize)!
    }
    
    static func FFShamelFamilySemiRoundMedium(ofSize: CGFloat) -> UIFont {
    
          return UIFont(name: "FFShamelFamily-SemiRoundMedium", size: ofSize)!
      }
  
}


