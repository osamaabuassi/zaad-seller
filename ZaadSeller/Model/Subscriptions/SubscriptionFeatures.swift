//
//  Features.swift
//
//  Created by  Ahmed’s MacBook Pro on 9/24/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct SubscriptionFeatures: Codable {
    
    enum CodingKeys: String, CodingKey {
        case title
        case type
        case id
        case descriptionValue = "description"
        case slug
        case value
    }
    
    var title: String?
    var type: String?
    var id: Int?
    var descriptionValue: String?
    var slug: String?
    var value: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
        if let value = try? container.decode(Int.self, forKey:.type) {
            type = String(value)
        }else if let value = try? container.decode(String.self, forKey:.type) {
            type = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.descriptionValue) {
            descriptionValue = String(value)
        }else if let value = try? container.decode(String.self, forKey:.descriptionValue) {
            descriptionValue = value
        }
        if let value = try? container.decode(Int.self, forKey:.slug) {
            slug = String(value)
        }else if let value = try? container.decode(String.self, forKey:.slug) {
            slug = value
        }
        if let valuer = try? container.decode(Int.self, forKey:.value) {
            value = String(valuer)
        } else if let valuer = try? container.decode(String.self, forKey:.value) {
            value = valuer
        }
    }
    
}
