//
//  Plan.swift
//
//  Created by  Ahmed’s MacBook Pro on 9/25/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct SubscrbtionPlan: Codable {

  enum CodingKeys: String, CodingKey {
    case title
    case price
    case planId = "plan_id"
    case planPriceId = "plan_price_id"
    case salePrice = "sale_price"
  }

  var title: String?
  var price: Int?
  var planId: Int?
  var planPriceId: Int?
  var salePrice: Int?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.title) {                       
title = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.title) {
 title = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.price) {
 price = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.price) {
price = value 
}
    if let value = try? container.decode(String.self, forKey:.planId) {
 planId = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.planId) {
planId = value 
}
    if let value = try? container.decode(String.self, forKey:.planPriceId) {
 planPriceId = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.planPriceId) {
planPriceId = value 
}
    if let value = try? container.decode(String.self, forKey:.salePrice) {
 salePrice = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.salePrice) {
salePrice = value 
}
  }

}
