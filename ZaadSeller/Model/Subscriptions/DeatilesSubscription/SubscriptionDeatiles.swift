//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 9/25/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct SubscriptionDeatiles: Codable {
    
    enum CodingKeys: String, CodingKey {
           case cancelsAt = "cancels_at"
           case active
           case startsAt = "starts_at"
           case canceled
           case endsAt = "ends_at"
           case createdAt = "created_at"
           case plan
           case paymentDate = "payment_date"
           case title
           case ended
           case trial
           case id
       }
       
       var cancelsAt: String?
       var active: Bool?
       var startsAt: String?
       var canceled: Bool?
       var endsAt: String?
       var createdAt: String?
       var plan: SubscrbtionPlan?
       var paymentDate: String?
       var title: String?
       var ended: Bool?
       var trial: Bool?
       var id: Int?
       
       
       
       init(from decoder: Decoder) throws {
           let container = try decoder.container(keyedBy: CodingKeys.self)
           if let value = try? container.decode(Int.self, forKey:.cancelsAt) {
               cancelsAt = String(value)
           }else if let value = try? container.decode(String.self, forKey:.cancelsAt) {
               cancelsAt = value
           }
           active = try container.decodeIfPresent(Bool.self, forKey: .active)
           if let value = try? container.decode(Int.self, forKey:.startsAt) {
               startsAt = String(value)
           }else if let value = try? container.decode(String.self, forKey:.startsAt) {
               startsAt = value
           }
           canceled = try container.decodeIfPresent(Bool.self, forKey: .canceled)
           if let value = try? container.decode(Int.self, forKey:.endsAt) {
               endsAt = String(value)
           }else if let value = try? container.decode(String.self, forKey:.endsAt) {
               endsAt = value
           }
           if let value = try? container.decode(Int.self, forKey:.createdAt) {
               createdAt = String(value)
           }else if let value = try? container.decode(String.self, forKey:.createdAt) {
               createdAt = value
           }
           plan = try container.decodeIfPresent(SubscrbtionPlan.self, forKey: .plan)
           if let value = try? container.decode(Int.self, forKey:.paymentDate) {
               paymentDate = String(value)
           }else if let value = try? container.decode(String.self, forKey:.paymentDate) {
               paymentDate = value
           }
           if let value = try? container.decode(Int.self, forKey:.title) {
               title = String(value)
           }else if let value = try? container.decode(String.self, forKey:.title) {
               title = value
           }
           ended = try container.decodeIfPresent(Bool.self, forKey: .ended)
           trial = try container.decodeIfPresent(Bool.self, forKey: .trial)
           if let value = try? container.decode(String.self, forKey:.id) {
               id = Int(value)
           } else if let value = try? container.decode(Int.self, forKey:.id) {
               id = value
           }
       }
       
}
