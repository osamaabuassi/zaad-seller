//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 9/24/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct SubscriptionData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case title
        case id
        case prices
        case features
    }
    
    var title: String?
    var id: Int?
    var prices: [Prices]?
    var features: [SubscriptionFeatures]?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        prices = try container.decodeIfPresent([Prices].self, forKey: .prices)
        features = try container.decodeIfPresent([SubscriptionFeatures].self, forKey: .features)
    }
    
}
