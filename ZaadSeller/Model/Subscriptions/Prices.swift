//
//  Prices.swift
//
//  Created by  Ahmed’s MacBook Pro on 9/24/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Prices: Codable {
    
    enum CodingKeys: String, CodingKey {
        case plan
        case invoicePeriod = "invoice_period"
        case canUpgrade = "can_upgrade"
        case regularPrice = "regular_price"
        case canDowngrade = "can_downgrade"
        case appleProductId = "apple_product_id"
        
        case invoiceInterval = "invoice_interval"
        case id
        case current
        case price
        case title
        case mode
        case salePrice = "sale_price"
        case canSubscribe = "can_subscribe"
        case unit
        case quantity
        case canRenew = "can_renew"
        case sortOrder = "sort_order"
    }
    
    var plan: String?
    var invoicePeriod: Int?
    var canUpgrade: Bool?
    var regularPrice: Int?
    var canDowngrade: Bool?
    var invoiceInterval: String?
    var appleProductId: String?
    var id: Int?
    var current: Bool?
    var price: Int?
    var title: String?
    var mode: String?
    var salePrice: Int?
    var canSubscribe: Bool?
    var unit: String?
    var quantity: Int?
    var canRenew: Bool?
    var sortOrder: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.plan) {
            plan = String(value)
        }else if let value = try? container.decode(String.self, forKey:.plan) {
            plan = value
        }
        
        if let value = try? container.decode(Int.self, forKey:.appleProductId) {
            appleProductId = String(value)
        }else if let value = try? container.decode(String.self, forKey:.appleProductId) {
            appleProductId = value
        }
        if let value = try? container.decode(String.self, forKey:.invoicePeriod) {
            invoicePeriod = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.invoicePeriod) {
            invoicePeriod = value
        }
        canUpgrade = try container.decodeIfPresent(Bool.self, forKey: .canUpgrade)
        if let value = try? container.decode(String.self, forKey:.regularPrice) {
            regularPrice = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.regularPrice) {
            regularPrice = value
        }
        canDowngrade = try container.decodeIfPresent(Bool.self, forKey: .canDowngrade)
        if let value = try? container.decode(Int.self, forKey:.invoiceInterval) {
            invoiceInterval = String(value)
        }else if let value = try? container.decode(String.self, forKey:.invoiceInterval) {
            invoiceInterval = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        current = try container.decodeIfPresent(Bool.self, forKey: .current)
        if let value = try? container.decode(String.self, forKey:.price) {
            price = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.price) {
            price = value
        }
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
        if let value = try? container.decode(Int.self, forKey:.mode) {
            mode = String(value)
        }else if let value = try? container.decode(String.self, forKey:.mode) {
            mode = value
        }
        if let value = try? container.decode(String.self, forKey:.salePrice) {
            salePrice = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.salePrice) {
            salePrice = value
        }
        canSubscribe = try container.decodeIfPresent(Bool.self, forKey: .canSubscribe)
        if let value = try? container.decode(Int.self, forKey:.unit) {
            unit = String(value)
        }else if let value = try? container.decode(String.self, forKey:.unit) {
            unit = value
        }
        if let value = try? container.decode(String.self, forKey:.quantity) {
            quantity = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.quantity) {
            quantity = value
        }
        canRenew = try container.decodeIfPresent(Bool.self, forKey: .canRenew)
        if let value = try? container.decode(String.self, forKey:.sortOrder) {
            sortOrder = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.sortOrder) {
            sortOrder = value
        }
    }
    
}
