//
//  Plan.swift
//
//  Created by  Ahmed’s MacBook Pro on 9/27/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct BuyPlan: Codable {
    
    enum CodingKeys: String, CodingKey {
        case title
        case planId = "plan_id"
        case salePrice = "sale_price"
        case price
        case planPriceId = "plan_price_id"
    }
    
    var title: String?
    var planId: Int?
    var salePrice: Int?
    var price: Int?
    var planPriceId: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
        if let value = try? container.decode(String.self, forKey:.planId) {
            planId = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.planId) {
            planId = value
        }
        if let value = try? container.decode(String.self, forKey:.salePrice) {
            salePrice = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.salePrice) {
            salePrice = value
        }
        if let value = try? container.decode(String.self, forKey:.price) {
            price = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.price) {
            price = value
        }
        if let value = try? container.decode(String.self, forKey:.planPriceId) {
            planPriceId = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.planPriceId) {
            planPriceId = value
        }
    }
    
}
