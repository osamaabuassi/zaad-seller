//
//  PaymentRequest.swift
//
//  Created by  Ahmed’s MacBook Pro on 9/27/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct PaymentRequest: Codable {
    
    enum CodingKeys: String, CodingKey {
        case paymentId = "payment_id"
        case returnurl
        case code
        case descriptionValue = "description"
    }
    
    var paymentId: String?
    var returnurl: String?
    var code: String?
    var descriptionValue: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.paymentId) {
            paymentId = String(value)
        }else if let value = try? container.decode(String.self, forKey:.paymentId) {
            paymentId = value
        }
        if let value = try? container.decode(Int.self, forKey:.returnurl) {
            returnurl = String(value)
        }else if let value = try? container.decode(String.self, forKey:.returnurl) {
            returnurl = value
        }
        if let value = try? container.decode(Int.self, forKey:.code) {                       
            code = String(value)
        }else if let value = try? container.decode(String.self, forKey:.code) {
            code = value
        }
        if let value = try? container.decode(Int.self, forKey:.descriptionValue) {
            descriptionValue = String(value)
        }else if let value = try? container.decode(String.self, forKey:.descriptionValue) {
            descriptionValue = value
        }
    }
    
}
