//
//  BaseClass.swift
//
//  Created by  Ahmed’s MacBook Pro on 9/27/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ResourceSubscrip: Codable {
    
    enum CodingKeys: String, CodingKey {
        case resource
        case returnUrl = "return_url"
        case paymentRequest
    }
    
    var resource: BuyResource?
    var returnUrl: String?
    var paymentRequest: PaymentRequest?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        resource = try container.decodeIfPresent(BuyResource.self, forKey: .resource)
        if let value = try? container.decode(Int.self, forKey:.returnUrl) {
            returnUrl = String(value)
        }else if let value = try? container.decode(String.self, forKey:.returnUrl) {
            returnUrl = value
        }
        paymentRequest = try container.decodeIfPresent(PaymentRequest.self, forKey: .paymentRequest)
    }
    
}
