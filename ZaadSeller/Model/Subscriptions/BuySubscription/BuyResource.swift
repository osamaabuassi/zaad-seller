//
//  Resource.swift
//
//  Created by  Ahmed’s MacBook Pro on 9/27/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct BuyResource: Codable {
    
    enum CodingKeys: String, CodingKey {
        case startsAt = "starts_at"
        case plan
        case canceled
        case title
        case endsAt = "ends_at"
        case createdAt = "created_at"
        case trial
        case id
        case ended
        case active
    }
    
    var startsAt: String?
    var plan:BuyPlan?
    var canceled: Bool?
    var title: String?
    var endsAt: String?
    var createdAt: String?
    var trial: Bool?
    var id: Int?
    var ended: Bool?
    var active: Bool?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.startsAt) {
            startsAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.startsAt) {
            startsAt = value
        }
        plan = try container.decodeIfPresent(BuyPlan.self, forKey: .plan)
        canceled = try container.decodeIfPresent(Bool.self, forKey: .canceled)
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
        if let value = try? container.decode(Int.self, forKey:.endsAt) {
            endsAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.endsAt) {
            endsAt = value
        }
        if let value = try? container.decode(Int.self, forKey:.createdAt) {
            createdAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.createdAt) {
            createdAt = value
        }
        trial = try container.decodeIfPresent(Bool.self, forKey: .trial)
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        ended = try container.decodeIfPresent(Bool.self, forKey: .ended)
        active = try container.decodeIfPresent(Bool.self, forKey: .active)
    }
    
}
