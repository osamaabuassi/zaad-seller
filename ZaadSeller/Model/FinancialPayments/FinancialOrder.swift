//
//  Order.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/14/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct FinancialOrder: Codable {
    
    enum CodingKeys: String, CodingKey {
        case total
        case id
        case purchaseDate = "purchase_date"
        case status
        case orderNo = "order_no"
        case shippingPrice = "shipping_price"
    }
    
    var total: Int?
    var id: Int?
    var purchaseDate: String?
    var status: Status?
    var orderNo: String?
    var shippingPrice: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.total) {
            total = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.total) {
            total = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.purchaseDate) {
            purchaseDate = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.purchaseDate) {
            purchaseDate = value
        }
        status = try container.decodeIfPresent(Status.self, forKey: .status)
        if let value = try? container.decode(Int.self, forKey:.orderNo) {
            orderNo = String(value)
        }else if let value = try? container.decode(String.self, forKey:.orderNo) {
            orderNo = value
        }
        if let value = try? container.decode(String.self, forKey:.shippingPrice) {
            shippingPrice = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.shippingPrice) {
            shippingPrice = value
        }
    }
    
}
