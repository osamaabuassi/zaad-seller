//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/14/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct FinancialPaymentsData: Codable {

  enum CodingKeys: String, CodingKey {
    case firstPageUrl = "first_page_url"
    case total
    case currentPageUrl = "current_page_url"
    case resources
    case lastPageUrl = "last_page_url"
    case currentPage = "current_page"
    case lastPage = "last_page"
    case perPage = "per_page"
  }

  var firstPageUrl: String?
  var total: Int?
  var currentPageUrl: String?
  var resources: [FinancialResources]?
  var lastPageUrl: String?
  var currentPage: Int?
  var lastPage: Int?
  var perPage: Int?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.firstPageUrl) {                       
firstPageUrl = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.firstPageUrl) {
 firstPageUrl = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.total) {
 total = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.total) {
total = value 
}
    if let value = try? container.decode(Int.self, forKey:.currentPageUrl) {                       
currentPageUrl = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.currentPageUrl) {
 currentPageUrl = value                                                                                     
}
    resources = try container.decodeIfPresent([FinancialResources].self, forKey: .resources)
    if let value = try? container.decode(Int.self, forKey:.lastPageUrl) {                       
lastPageUrl = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.lastPageUrl) {
 lastPageUrl = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.currentPage) {
 currentPage = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.currentPage) {
currentPage = value 
}
    if let value = try? container.decode(String.self, forKey:.lastPage) {
 lastPage = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.lastPage) {
lastPage = value 
}
    if let value = try? container.decode(String.self, forKey:.perPage) {
 perPage = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.perPage) {
perPage = value 
}
  }

}
