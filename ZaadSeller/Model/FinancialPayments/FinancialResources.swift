//
//  Resources.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/14/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct FinancialResources: Codable {

  enum CodingKeys: String, CodingKey {
    case id
    case paymentDate = "payment_date"
    case document
    case customer
    case transactionNo = "transaction_no"
    case order
    case status
  }

  var id: Int?
  var paymentDate: String?
  var document: String?
  var customer: Customer?
  var transactionNo: String?
  var order: FinancialOrder?
  var status: Status?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
    if let value = try? container.decode(Int.self, forKey:.paymentDate) {                       
paymentDate = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.paymentDate) {
 paymentDate = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.document) {                       
document = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.document) {
 document = value                                                                                     
}
    customer = try container.decodeIfPresent(Customer.self, forKey: .customer)
    if let value = try? container.decode(Int.self, forKey:.transactionNo) {                       
transactionNo = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.transactionNo) {
 transactionNo = value                                                                                     
}
    order = try container.decodeIfPresent(FinancialOrder.self, forKey: .order)
    status = try container.decodeIfPresent(Status.self, forKey: .status)
  }

}
