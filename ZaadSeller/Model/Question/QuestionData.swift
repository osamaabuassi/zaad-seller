//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/15/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct QuestionData: Codable {

  enum CodingKeys: String, CodingKey {
    case questions
    case title
  }

  var questions: [Questions]?
  var title: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    questions = try container.decodeIfPresent([Questions].self, forKey: .questions)
    if let value = try? container.decode(Int.self, forKey:.title) {                       
title = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.title) {
 title = value                                                                                     
}
  }

}
