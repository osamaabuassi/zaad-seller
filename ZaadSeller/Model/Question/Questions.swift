//
//  Questions.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/15/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Questions: Codable {

  enum CodingKeys: String, CodingKey {
    case categoryName = "category_name"
    case priority
    case question
    case id
    case answer
  }

  var categoryName: String?
  var priority: Int?
  var question: String?
  var id: Int?
  var answer: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.categoryName) {                       
categoryName = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.categoryName) {
 categoryName = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.priority) {
 priority = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.priority) {
priority = value 
}
    if let value = try? container.decode(Int.self, forKey:.question) {                       
question = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.question) {
 question = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
    if let value = try? container.decode(Int.self, forKey:.answer) {                       
answer = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.answer) {
 answer = value                                                                                     
}
  }

}
