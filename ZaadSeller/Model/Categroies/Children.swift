//
//  Children.swift
//
//  Created by osamaaassi on 1/23/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Children: Codable {
    
    enum CodingKeys: String, CodingKey {
        case title
        case id
        case children
    }
    var id: Int?
    var title: String?
    var children: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
        
        if let value = try? container.decode(Int.self, forKey:.children) {
                   children = String(value)
               }else if let value = try? container.decode(String.self, forKey:.children) {
                   children = value
               }
        
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
    }
    
}
