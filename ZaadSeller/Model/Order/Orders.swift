//
//  Resources.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/13/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Orders: Codable {

  enum CodingKeys: String, CodingKey {
    case buyerEmail = "buyer_email"
    case status
    case buyerName = "buyer_name"
    case total
    case purchaseDate = "purchase_date"
    case id
    case orderNo = "order_no"
    case paymentMethod = "payment_method"
  }

  var buyerEmail: String?
  var status: Status?
  var buyerName: String?
  var total: Int?
  var purchaseDate: String?
  var id: Int?
  var orderNo: String?
  var paymentMethod: PaymentMethod?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.buyerEmail) {                       
buyerEmail = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.buyerEmail) {
 buyerEmail = value                                                                                     
}
    status = try container.decodeIfPresent(Status.self, forKey: .status)
    if let value = try? container.decode(Int.self, forKey:.buyerName) {                       
buyerName = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.buyerName) {
 buyerName = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.total) {
 total = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.total) {
total = value 
}
    if let value = try? container.decode(Int.self, forKey:.purchaseDate) {                       
purchaseDate = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.purchaseDate) {
 purchaseDate = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
    if let value = try? container.decode(Int.self, forKey:.orderNo) {                       
orderNo = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.orderNo) {
 orderNo = value                                                                                     
}
    paymentMethod = try container.decodeIfPresent(PaymentMethod.self, forKey: .paymentMethod)
  }

}
