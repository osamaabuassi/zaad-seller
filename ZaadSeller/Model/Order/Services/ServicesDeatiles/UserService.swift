//
//  User.swift
//
//  Created by osamaaassi on 1/21/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct UserService: Codable {

  enum CodingKeys: String, CodingKey {
    case title
    case id
  }

  var title: String?
  var id: Int?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.title) {                       
title = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.title) {
 title = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
  }

}
