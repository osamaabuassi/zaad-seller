//
//  Data.swift
//
//  Created by osamaaassi on 1/21/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct DataServices: Codable {
    
    enum CodingKeys: String, CodingKey {
        case items
        case total
        case addressLine3 = "address_line_3"
        case subtotal
        case email
        case status
        case descriptionValue = "description"
        case city
        case user
        case postalCode = "postal_code"
        case createdAt = "created_at"
        case toTime = "to_time"
        case orderNo = "order_no"
        case id
        
        case addressLine1 = "address_line_1"
        case requiredDate = "required_date"
        case paymentStatus = "payment_status"
        case addressLine2 = "address_line_2"
        case fromTime = "from_time"
        
        case county
        case phone
        case mobile
        
        case note
    }
    
   
    var id: Int?
    var orderNo: String?
    var descriptionValue: String?
    var status: StatusServices?
    var paymentStatus: PaymentStatus?
    var user: UserService?
    var createdAt: String?
    var requiredDate: String?
    var fromTime: String?
    var toTime: String?
    var items: [ItemsServices]?
    
   
    var addressLine1: String?
    var addressLine2: String?
    var addressLine3: String?
    var county: String?
    var city: String?
    var postalCode: String?
    var phone: String?
    var mobile: String?
    var email: String?
    var subtotal: Int?
    var total: Int?
    var note: String?
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
         items = try container.decodeIfPresent([ItemsServices].self, forKey: .items)
        if let value = try? container.decode(Int.self, forKey:.county) {
            county = String(value)
        }else if let value = try? container.decode(String.self, forKey:.county) {
            county = value
        }
        
        if let value = try? container.decode(Int.self, forKey:.phone) {
            phone = String(value)
        }else if let value = try? container.decode(String.self, forKey:.phone) {
            phone = value
        }
        
        if let value = try? container.decode(Int.self, forKey:.mobile) {
            mobile = String(value)
        }else if let value = try? container.decode(String.self, forKey:.mobile) {
            mobile = value
        }
        
        
        if let value = try? container.decode(String.self, forKey:.total) {
            total = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.total) {
            total = value
        }
        if let value = try? container.decode(Int.self, forKey:.addressLine3) {
            addressLine3 = String(value)
        }else if let value = try? container.decode(String.self, forKey:.addressLine3) {
            addressLine3 = value
        }
        if let value = try? container.decode(String.self, forKey:.subtotal) {
            subtotal = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.subtotal) {
            subtotal = value
        }
        if let value = try? container.decode(Int.self, forKey:.email) {
            email = String(value)
        }else if let value = try? container.decode(String.self, forKey:.email) {
            email = value
        }
        status = try container.decodeIfPresent(StatusServices.self, forKey: .status)
        if let value = try? container.decode(Int.self, forKey:.descriptionValue) {
            descriptionValue = String(value)
        }else if let value = try? container.decode(String.self, forKey:.descriptionValue) {
            descriptionValue = value
        }
        if let value = try? container.decode(Int.self, forKey:.city) {
            city = String(value)
        }else if let value = try? container.decode(String.self, forKey:.city) {
            city = value
        }
        user = try container.decodeIfPresent(UserService.self, forKey: .user)
        if let value = try? container.decode(Int.self, forKey:.postalCode) {
            postalCode = String(value)
        }else if let value = try? container.decode(String.self, forKey:.postalCode) {
            postalCode = value
        }
        if let value = try? container.decode(Int.self, forKey:.createdAt) {
            createdAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.createdAt) {
            createdAt = value
        }
        
        if let value = try? container.decode(Int.self, forKey:.orderNo) {
            orderNo = String(value)
        }else if let value = try? container.decode(String.self, forKey:.orderNo) {
            orderNo = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
     
        if let value = try? container.decode(Int.self, forKey:.requiredDate) {
            requiredDate = String(value)
        }else if let value = try? container.decode(String.self, forKey:.requiredDate) {
            requiredDate = value
        }
        paymentStatus = try container.decodeIfPresent(PaymentStatus.self, forKey: .paymentStatus)
        if let value = try? container.decode(Int.self, forKey:.addressLine2) {
            addressLine2 = String(value)
        }else if let value = try? container.decode(String.self, forKey:.addressLine2) {
            addressLine2 = value
        }
        if let value = try? container.decode(Int.self, forKey:.fromTime) {
            fromTime = String(value)
        }else if let value = try? container.decode(String.self, forKey:.fromTime) {
            fromTime = value
        }
        
        if let value = try? container.decode(Int.self, forKey:.note) {
            note = String(value)
        }else if let value = try? container.decode(String.self, forKey:.note) {
            note = value
        }
    }
    
}
