//
//  Items.swift
//
//  Created by osamaaassi on 1/31/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ItemsServices: Codable {

  enum CodingKeys: String, CodingKey {
    case note
    case payable
    case quantity
    case sellerDeliveryDate = "seller_delivery_date"
    case coupon
    case paymentStatus = "payment_status"
    case status
    case id
    case regularPrice = "regular_price"
    case discount
    case title
  }

  var note: String?
  var payable: Int?
  var quantity: Int?
  var sellerDeliveryDate: String?
  var coupon: String?
  var paymentStatus: PaymentStatus?
  var status: StatusServices?
  var id: Int?
  var regularPrice: Int?
  var discount: Int?
  var title: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.note) {                       
note = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.note) {
 note = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.payable) {
 payable = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.payable) {
payable = value 
}
    if let value = try? container.decode(String.self, forKey:.quantity) {
 quantity = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.quantity) {
quantity = value 
}
    if let value = try? container.decode(Int.self, forKey:.sellerDeliveryDate) {                       
sellerDeliveryDate = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.sellerDeliveryDate) {
 sellerDeliveryDate = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.coupon) {                       
coupon = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.coupon) {
 coupon = value                                                                                     
}
    paymentStatus = try container.decodeIfPresent(PaymentStatus.self, forKey: .paymentStatus)
    status = try container.decodeIfPresent(StatusServices.self, forKey: .status)
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
    if let value = try? container.decode(String.self, forKey:.regularPrice) {
 regularPrice = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.regularPrice) {
regularPrice = value 
}
    if let value = try? container.decode(String.self, forKey:.discount) {
 discount = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.discount) {
discount = value 
}
    if let value = try? container.decode(Int.self, forKey:.title) {                       
title = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.title) {
 title = value                                                                                     
}
  }

}
