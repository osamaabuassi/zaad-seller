//
//  ItemsServices.swift
//  ZaadSeller
//
//  Created by osamaaassi on 27/02/2021.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import Foundation
struct ItemsServicesOrder: Codable {

  enum CodingKeys: String, CodingKey {
    case items
   
  }

  var items: [PaymentStatus]?




  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    items = try container.decodeIfPresent([PaymentStatus].self, forKey: .items)


  }

}
