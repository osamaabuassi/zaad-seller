//
//  Data.swift
//
//  Created by osamaaassi on 1/16/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ServicesData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case resources
        case currentPageUrl = "current_page_url"
        case firstPageUrl = "first_page_url"
        case prevPageUrl = "prev_page_url"
        case nextPageUrl = "next_page_url"
        case lastPageUrl = "last_page_url"
        case perPage = "per_page"
        case currentPage = "current_page"
        case lastPage = "last_page"
        case total
    }
    
    var resources: [Resources]?
    var currentPageUrl: String?
    var firstPageUrl: String?
    var prevPageUrl: String?
    var nextPageUrl: String?
    var lastPageUrl: String?
    var perPage: Int?
    var currentPage: Int?
    var lastPage: Int?
    var total: Int?
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.lastPage) {
            lastPage = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.lastPage) {
            lastPage = value
        }
        if let value = try? container.decode(String.self, forKey:.total) {
            total = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.total) {
            total = value
        }
        if let value = try? container.decode(Int.self, forKey:.firstPageUrl) {
            firstPageUrl = String(value)
        }else if let value = try? container.decode(String.self, forKey:.firstPageUrl) {
            firstPageUrl = value
        }
        if let value = try? container.decode(Int.self, forKey:.prevPageUrl) {
            prevPageUrl = String(value)
        }else if let value = try? container.decode(String.self, forKey:.prevPageUrl) {
            prevPageUrl = value
        }
        
        
        if let value = try? container.decode(Int.self, forKey:.currentPageUrl) {
            currentPageUrl = String(value)
        }else if let value = try? container.decode(String.self, forKey:.currentPageUrl) {
            currentPageUrl = value
        }
        if let value = try? container.decode(String.self, forKey:.perPage) {
            perPage = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.perPage) {
            perPage = value
        }
        resources = try container.decodeIfPresent([Resources].self, forKey: .resources)
        if let value = try? container.decode(Int.self, forKey:.lastPageUrl) {
            lastPageUrl = String(value)
        }else if let value = try? container.decode(String.self, forKey:.lastPageUrl) {
            lastPageUrl = value
        }
        if let value = try? container.decode(String.self, forKey:.currentPage) {
            currentPage = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.currentPage) {
            currentPage = value
        }
        if let value = try? container.decode(Int.self, forKey:.nextPageUrl) {
            nextPageUrl = String(value)
        }else if let value = try? container.decode(String.self, forKey:.nextPageUrl) {
            nextPageUrl = value
        }
    }
    
}
