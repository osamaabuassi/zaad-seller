//
//  BaseClass.swift
//
//  Created by osamaaassi on 1/16/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ServicesOrders: Codable {
    
    enum CodingKeys: String, CodingKey {
        case status
        case data
        case code
        case locale
        case message
    }
    
    var status: Int?
    var data: ServicesData?
    var code: Int?
    var locale: String?
    var message: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.status) {
            status = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.status) {
            status = value
        }
        data = try container.decodeIfPresent(ServicesData.self, forKey: .data)
        if let value = try? container.decode(String.self, forKey:.code) {
            code = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.code) {
            code = value
        }
        if let value = try? container.decode(Int.self, forKey:.locale) {
            locale = String(value)
        }else if let value = try? container.decode(String.self, forKey:.locale) {
            locale = value
        }
        if let value = try? container.decode(Int.self, forKey:.message) {                       
            message = String(value)
        }else if let value = try? container.decode(String.self, forKey:.message) {
            message = value
        }
    }
    
}
