//
//  Resources.swift
//
//  Created by osamaaassi on 1/16/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Resources: Codable {
    
    enum CodingKeys: String, CodingKey {
        case id
        case orderNo = "order_no"
        case service
        case status
        case paymentStatus = "payment_status"
        case createdAt = "created_at"
        
        case subtotal
        case total
        case couponValue = "coupon_value"
        case nextStatus = "next_status"
        
    }
    
    
    var id: Int?
    var orderNo: String?
    var service: Activity?
    var status: StatusServices?
    var paymentStatus: PaymentStatus?
    var createdAt: String?
    
    var subtotal: Int?
    var total: Int?
    var couponValue: Int?
    var nextStatus: [StatusServices]?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.orderNo) {
            orderNo = String(value)
        }else if let value = try? container.decode(String.self, forKey:.orderNo) {
            orderNo = value
        }
        if let value = try? container.decode(Int.self, forKey:.createdAt) {
            createdAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.createdAt) {
            createdAt = value
        }
       
        if let value = try? container.decode(String.self, forKey:.subtotal) {
            subtotal = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.subtotal) {
            subtotal = value
        }
        if let value = try? container.decode(String.self, forKey:.total) {
            total = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.total) {
            total = value
        }
        if let value = try? container.decode(String.self, forKey:.couponValue) {
            couponValue = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.couponValue) {
            couponValue = value
        }
        service = try container.decodeIfPresent(Activity.self, forKey: .service)
        status = try container.decodeIfPresent(StatusServices.self, forKey: .status)
        paymentStatus = try container.decodeIfPresent(PaymentStatus.self, forKey: .paymentStatus)
        nextStatus = try container.decodeIfPresent([StatusServices].self, forKey: .nextStatus)
    }
    
}
