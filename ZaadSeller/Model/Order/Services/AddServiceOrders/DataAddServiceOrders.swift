//
//  Data.swift
//
//  Created by osamaaassi on 03/03/2021
//  Copyright (c) . All rights reserved.
//

import Foundation

struct DataAddServiceOrders: Codable {

  enum CodingKeys: String, CodingKey {
    case item
  }

  var item: DataServices?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    item = try container.decodeIfPresent(DataServices.self, forKey: .item)
  }

}
