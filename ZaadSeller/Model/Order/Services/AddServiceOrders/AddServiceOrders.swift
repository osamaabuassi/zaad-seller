//
//  AddServiceOrders.swift
//
//  Created by osamaaassi on 03/03/2021
//  Copyright (c) . All rights reserved.
//

import Foundation

struct AddServiceOrders: Codable {

  enum CodingKeys: String, CodingKey {
    case data
    case message
    case locale
    case code
    case status
  }

  var data: DataAddServiceOrders?
  var message: String?
  var locale: String?
  var code: Int?
  var status: Int?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    data = try container.decodeIfPresent(DataAddServiceOrders.self, forKey: .data)
    if let value = try? container.decode(Int.self, forKey:.message) {                       
message = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.message) {
 message = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.locale) {                       
locale = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.locale) {
 locale = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.code) {
 code = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.code) {
code = value 
}
    if let value = try? container.decode(String.self, forKey:.status) {
 status = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.status) {
status = value 
}
  }

}
