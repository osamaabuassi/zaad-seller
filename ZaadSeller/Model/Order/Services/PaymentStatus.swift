//
//  PaymentStatus.swift
//
//  Created by osamaaassi on 1/16/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct PaymentStatus: Codable {

  enum CodingKeys: String, CodingKey {
    case name
    case id
  }

  var name: String?
  var id: Int?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.name) {                       
name = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.name) {
 name = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
  }

}
