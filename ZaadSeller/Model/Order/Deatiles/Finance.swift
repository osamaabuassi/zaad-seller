//
//  Finance.swift
//
//  Created by  Ahmed’s MacBook Pro on 8/8/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Finance: Codable {
    
    enum CodingKeys: String, CodingKey {
        case discount
        case shippingDiscount = "shipping_discount"
        case total
        case net
        case coupon
        case couponDiscount = "coupon_discount"
    }
    
    var discount: Int?
    var shippingDiscount: Int?
    var total: Int?
    var net: Int?
    var couponDiscount: Int?
    var coupon:Coupon?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.discount) {
            discount = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.discount) {
            discount = value
        }
        if let value = try? container.decode(String.self, forKey:.shippingDiscount) {
            shippingDiscount = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.shippingDiscount) {
            shippingDiscount = value
        }
        if let value = try? container.decode(String.self, forKey:.total) {
            total = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.total) {
            total = value
        }
        if let value = try? container.decode(String.self, forKey:.net) {
            net = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.net) {
            net = value
        }
        if let value = try? container.decode(String.self, forKey:.couponDiscount) {
            couponDiscount = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.couponDiscount) {
            couponDiscount = value
        }
        
        coupon = try container.decodeIfPresent(Coupon.self, forKey: .coupon)

    }
    
}
