//
//  Main.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/13/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Main: Codable {

  enum CodingKeys: String, CodingKey {
    case buyerName = "buyer_name"
    case paymentMethod = "payment_method"
    case status
    case id
    case orderNo = "order_no"
    case buyerEmail = "buyer_email"
    case purchaseDate = "purchase_date"
  }

  var buyerName: String?
  var paymentMethod: PaymentMethod?
  var status: Status?
  var id: Int?
  var orderNo: String?
  var buyerEmail: String?
  var purchaseDate: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.buyerName) {                       
buyerName = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.buyerName) {
 buyerName = value                                                                                     
}
    paymentMethod = try container.decodeIfPresent(PaymentMethod.self, forKey: .paymentMethod)
    status = try container.decodeIfPresent(Status.self, forKey: .status)
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
    if let value = try? container.decode(Int.self, forKey:.orderNo) {                       
orderNo = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.orderNo) {
 orderNo = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.buyerEmail) {                       
buyerEmail = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.buyerEmail) {
 buyerEmail = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.purchaseDate) {                       
purchaseDate = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.purchaseDate) {
 purchaseDate = value                                                                                     
}
  }

}
