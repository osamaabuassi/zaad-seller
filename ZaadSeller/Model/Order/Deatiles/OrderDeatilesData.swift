//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/13/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct OrderDeatilesData: Codable {

  enum CodingKeys: String, CodingKey {
    case main
    case shipping
    case items
    case finance
  }

  var main: Main?
  var shipping: Shipping?
  var items: [Items]?
  var finance: Finance?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    main = try container.decodeIfPresent(Main.self, forKey: .main)
    shipping = try container.decodeIfPresent(Shipping.self, forKey: .shipping)
    items = try container.decodeIfPresent([Items].self, forKey: .items)
    finance = try container.decodeIfPresent(Finance.self, forKey: .finance)
  }

}
