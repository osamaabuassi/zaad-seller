//
//  Items.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/13/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Items: Codable {

  enum CodingKeys: String, CodingKey {
    case id
    case title
    case shippingPrice = "shipping_price"
    case lineTotal = "line_total"
    case shippingService = "shipping_service"
    case total
    case unitPrice = "unit_price"
    case itemId = "item_id"
    case quantity
  }

  var id: Int?
  var title: String?
  var shippingPrice: Int?
  var lineTotal: Int?
  var shippingService: String?
  var total: Int?
  var unitPrice: Int?
  var itemId: Int?
  var quantity: Int?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
    if let value = try? container.decode(Int.self, forKey:.title) {                       
title = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.title) {
 title = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.shippingPrice) {
 shippingPrice = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.shippingPrice) {
shippingPrice = value 
}
    if let value = try? container.decode(String.self, forKey:.lineTotal) {
 lineTotal = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.lineTotal) {
lineTotal = value 
}
    if let value = try? container.decode(Int.self, forKey:.shippingService) {                       
shippingService = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.shippingService) {
 shippingService = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.total) {
 total = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.total) {
total = value 
}
    if let value = try? container.decode(String.self, forKey:.unitPrice) {
 unitPrice = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.unitPrice) {
unitPrice = value 
}
    if let value = try? container.decode(String.self, forKey:.itemId) {
 itemId = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.itemId) {
itemId = value 
}
    if let value = try? container.decode(String.self, forKey:.quantity) {
 quantity = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.quantity) {
quantity = value 
}
  }

}
