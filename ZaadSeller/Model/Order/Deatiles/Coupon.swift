//
//  Coupon.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/13/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Coupon: Codable {
    
    enum CodingKeys: String, CodingKey {
        case id
        case couponDiscount = "coupon_discount"
        case orderDiscount = "order_discount"
        case code
        case discountType = "discount_type"
        case amount
    }
    
    var id: Int?
    var couponDiscount: String?
    var orderDiscount: String?
    var code: String?
    var discountType: String?
    var amount: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.couponDiscount) {
            couponDiscount = String(value)
        }else if let value = try? container.decode(String.self, forKey:.couponDiscount) {
            couponDiscount = value
        }
        if let value = try? container.decode(Int.self, forKey:.orderDiscount) {
            orderDiscount = String(value)
        }else if let value = try? container.decode(String.self, forKey:.orderDiscount) {
            orderDiscount = value
        }
        if let value = try? container.decode(Int.self, forKey:.code) {
            code = String(value)
        }else if let value = try? container.decode(String.self, forKey:.code) {
            code = value
        }
        if let value = try? container.decode(Int.self, forKey:.discountType) {
            discountType = String(value)
        }else if let value = try? container.decode(String.self, forKey:.discountType) {
            discountType = value
        }
        if let value = try? container.decode(String.self, forKey:.amount) {
            amount = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.amount) {
            amount = value
        }
    }
    
}
