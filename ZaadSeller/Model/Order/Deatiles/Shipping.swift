//
//  Shipping.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/13/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Shipping: Codable {

  enum CodingKeys: String, CodingKey {
    case postalCode = "postal_code"
    case name
    case addressLine3 = "address_line_3"
    case city
    case addressLine2 = "address_line_2"
    case phone
    case mobile
    case country
    case email
    case addressLine1 = "address_line_1"
  }

  var postalCode: String?
  var name: String?
  var addressLine3: String?
  var city: City?
  var addressLine2: String?
  var phone: String?
  var mobile: String?
  var country: Country?
  var email: String?
  var addressLine1: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.postalCode) {                       
postalCode = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.postalCode) {
 postalCode = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.name) {                       
name = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.name) {
 name = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.addressLine3) {                       
addressLine3 = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.addressLine3) {
 addressLine3 = value                                                                                     
}
    city = try container.decodeIfPresent(City.self, forKey: .city)
    if let value = try? container.decode(Int.self, forKey:.addressLine2) {                       
addressLine2 = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.addressLine2) {
 addressLine2 = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.phone) {                       
phone = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.phone) {
 phone = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.mobile) {                       
mobile = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.mobile) {
 mobile = value                                                                                     
}
    country = try container.decodeIfPresent(Country.self, forKey: .country)
    if let value = try? container.decode(Int.self, forKey:.email) {                       
email = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.email) {
 email = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.addressLine1) {                       
addressLine1 = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.addressLine1) {
 addressLine1 = value                                                                                     
}
  }

}
