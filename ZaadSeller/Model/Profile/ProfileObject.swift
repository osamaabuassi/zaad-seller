//
//  BaseClass.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/10/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ProfileObject: Codable {
    
    enum CodingKeys: String, CodingKey {
        case data
        case locale
        case message
        case code
        case status
    }
    
    var data: ProfileData?
    var locale: String?
    var message: String?
    var code: Int?
    var status: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        data = try container.decodeIfPresent(ProfileData.self, forKey: .data)
        if let value = try? container.decode(Int.self, forKey:.locale) {
            locale = String(value)
        }else if let value = try? container.decode(String.self, forKey:.locale) {
            locale = value
        }
        if let value = try? container.decode(Int.self, forKey:.message) {
            message = String(value)
        }else if let value = try? container.decode(String.self, forKey:.message) {
            message = value
        }
        if let value = try? container.decode(String.self, forKey:.code) {
            code = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.code) {
            code = value
        }
        if let value = try? container.decode(String.self, forKey:.status) {
            status = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.status) {
            status = value
        }
    }
    
}
