//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/10/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ProfileData: Codable {

  enum CodingKeys: String, CodingKey {
    case name
    case country
    case status
    case city
    case nationality
    case paymentMethod = "payment_method"
    case username
    case email
    case blocked
    case phone
    case address
    case id
    case emailVerified = "email_verified"
    case mobile
    case isActive = "is_active"
    case createdAt = "created_at"
    case idCard = "id_card"
    case commercialRegistration = "commercial_registration"
    
  }

  var name: String?
  var country: Country?
  var status: Int?
  var city: City?
  var nationality: Nationality?
  var paymentMethod: Int?
  var username: String?
  var email: String?
  var blocked: Int?
  var phone: String?
  var address: String?
  var id: Int?
  var emailVerified: Int?
  var mobile: String?
  var isActive: Int?
  var createdAt: String?
  var idCard: String?
  var commercialRegistration: String?

  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    
    if let value = try? container.decode(Int.self, forKey:.idCard) {
        idCard = String(value)
}else if let value = try? container.decode(String.self, forKey:.idCard) {
    idCard = value
}
    if let value = try? container.decode(Int.self, forKey:.commercialRegistration) {
        commercialRegistration = String(value)
}else if let value = try? container.decode(String.self, forKey:.commercialRegistration) {
    commercialRegistration = value
}
    
    if let value = try? container.decode(Int.self, forKey:.name) {                       
name = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.name) {
 name = value                                                                                     
}
    country = try container.decodeIfPresent(Country.self, forKey: .country)
    if let value = try? container.decode(String.self, forKey:.status) {
 status = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.status) {
status = value 
}
    city = try container.decodeIfPresent(City.self, forKey: .city)
    nationality = try container.decodeIfPresent(Nationality.self, forKey: .nationality)
    if let value = try? container.decode(String.self, forKey:.paymentMethod) {
 paymentMethod = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.paymentMethod) {
paymentMethod = value 
}
    if let value = try? container.decode(Int.self, forKey:.username) {                       
username = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.username) {
 username = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.email) {                       
email = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.email) {
 email = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.blocked) {
 blocked = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.blocked) {
blocked = value 
}
    if let value = try? container.decode(Int.self, forKey:.phone) {                       
phone = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.phone) {
 phone = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.address) {                       
address = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.address) {
 address = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
    if let value = try? container.decode(String.self, forKey:.emailVerified) {
 emailVerified = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.emailVerified) {
emailVerified = value 
}
    if let value = try? container.decode(Int.self, forKey:.mobile) {                       
mobile = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.mobile) {
 mobile = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.isActive) {
 isActive = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.isActive) {
isActive = value 
}
    if let value = try? container.decode(Int.self, forKey:.createdAt) {                       
createdAt = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.createdAt) {
 createdAt = value                                                                                     
}
  }

}
