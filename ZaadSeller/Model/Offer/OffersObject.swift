//
//  BaseClass.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/11/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct OffersObject: Codable {

  enum CodingKeys: String, CodingKey {
    case message
    case data
    case code
    case status
    case locale
  }

  var message: String?
  var data: OffersData?
  var code: Int?
  var status: Int?
  var locale: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.message) {                       
message = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.message) {
 message = value                                                                                     
}
    data = try container.decodeIfPresent(OffersData.self, forKey: .data)
    if let value = try? container.decode(String.self, forKey:.code) {
 code = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.code) {
code = value 
}
    if let value = try? container.decode(String.self, forKey:.status) {
 status = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.status) {
status = value 
}
    if let value = try? container.decode(Int.self, forKey:.locale) {                       
locale = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.locale) {
 locale = value                                                                                     
}
  }

}
