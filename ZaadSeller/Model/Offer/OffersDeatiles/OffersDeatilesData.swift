//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/11/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct OffersDeatilesData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case startDate = "start_date"
        case regularPrice = "regular_price"
        case status
        case id
        case title
        case amountLabel = "amount_label"
        case discountType = "discount_type"
        case amount
        case endDate = "end_date"
        case salePrice = "sale_price"
    }
    
    var startDate: String?
    var regularPrice: Int?
    var status: Status?
    var id: Int?
    var title: String?
    var amountLabel: String?
    var discountType: DiscountType?
    var amount: Int?
    var endDate: String?
    var salePrice: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.startDate) {
            startDate = String(value)
        }else if let value = try? container.decode(String.self, forKey:.startDate) {
            startDate = value
        }
        if let value = try? container.decode(String.self, forKey:.regularPrice) {
            regularPrice = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.regularPrice) {
            regularPrice = value
        }else{
            let value = try? container.decode(Double.self, forKey:.regularPrice)
            regularPrice = Int(value!)
        }
        status = try container.decodeIfPresent(Status.self, forKey: .status)
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
        if let value = try? container.decode(Int.self, forKey:.amountLabel) {
            amountLabel = String(value)
        }else if let value = try? container.decode(String.self, forKey:.amountLabel) {
            amountLabel = value
        }
        discountType = try container.decodeIfPresent(DiscountType.self, forKey: .discountType)
        if let value = try? container.decode(String.self, forKey:.amount) {
            amount = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.amount) {
            amount = value
        }
        if let value = try? container.decode(Int.self, forKey:.endDate) {
            endDate = String(value)
        }else if let value = try? container.decode(String.self, forKey:.endDate) {
            endDate = value
        }
        if let value = try? container.decode(String.self, forKey:.salePrice) {
            salePrice = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.salePrice) {
            salePrice = value
        }else{
            let value = try? container.decode(Double.self, forKey:.salePrice)
            salePrice = Int(value!)
        }
    }
    
}
