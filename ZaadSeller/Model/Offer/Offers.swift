//
//  Resources.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/11/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Offers: Codable {
    
    enum CodingKeys: String, CodingKey {
        case status
        case regularPrice = "regular_price"
        case title
        case id
        case amountLabel = "amount_label"
        case startDate = "start_date"
        case salePrice = "sale_price"
        case endDate = "end_date"
    }
    
    var status: Status?
    var regularPrice: Int?
    var title: String?
    var id: Int?
    var amountLabel: String?
    var startDate: String?
    var salePrice: Int?
    var endDate: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        status = try container.decodeIfPresent(Status.self, forKey: .status)
        if let value = try? container.decode(String.self, forKey:.regularPrice) {
            regularPrice = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.regularPrice) {
            regularPrice = value
        }else{
            let value = try? container.decode(Double.self, forKey:.regularPrice)
            regularPrice = Int(value!)
        }
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.amountLabel) {
            amountLabel = String(value)
        }else if let value = try? container.decode(String.self, forKey:.amountLabel) {
            amountLabel = value
        }
        if let value = try? container.decode(Int.self, forKey:.startDate) {
            startDate = String(value)
        }else if let value = try? container.decode(String.self, forKey:.startDate) {
            startDate = value
        }
        if let value = try? container.decode(String.self, forKey:.salePrice) {
            salePrice = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.salePrice) {
            salePrice = value
        }else{
            let value = try? container.decode(Double.self, forKey:.salePrice)
            salePrice = Int(value!)
        }
        if let value = try? container.decode(Int.self, forKey:.endDate) {
            endDate = String(value)
        }else if let value = try? container.decode(String.self, forKey:.endDate) {
            endDate = value
        }
    }
    
}
