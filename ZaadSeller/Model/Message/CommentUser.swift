//
//  comments.swift
//  ZaadSeller
//
//  Created by osamaaassi on 1/14/21.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import Foundation

struct CommentServes: Codable {
    let status, code: Int
    let locale, message: String
    let data: DataClass
}

// MARK: - CommentUser
struct DataClass: Codable {
    let items: [CommentChat]
}

// MARK: - Item
struct CommentChat: Codable {

    // var name, comment, type, date: String

    enum CodingKeys: String, CodingKey {
        case userID = "user_id"
        case name
        case comment
        case type
        case date

    }

    var userID: Int?
    var name: String?
    var comment: String?
    var type: String?
    var date: String?

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        if let value = try? container.decode(String.self, forKey:.userID) {
            userID = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.userID) {
            userID = value
        }
        if let value = try? container.decode(Int.self, forKey:.name) {
            name = String(value)
        }else if let value = try? container.decode(String.self, forKey:.name) {
            name = value
        }

        if let value = try? container.decode(Int.self, forKey:.comment) {
            comment = String(value)
        }else if let value = try? container.decode(String.self, forKey:.comment) {
            comment = value
        }
        if let value = try? container.decode(Int.self, forKey:.type) {
            type = String(value)
        }else if let value = try? container.decode(String.self, forKey:.type) {
            type = value
        }
        if let value = try? container.decode(Int.self, forKey:.date) {
            date = String(value)
        }else if let value = try? container.decode(String.self, forKey:.date) {
            date = value
        }

    }
}
