//
//  Messages.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/14/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Messages: Codable {
    
    enum CodingKeys: String, CodingKey {
        case type
        case sender
        case createdAt = "created_at"
        case message
        case id
    }
    
    var type: Int?
    var sender: String?
    var createdAt: String?
    var message: String?
    var id: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.type) {
            type = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.type) {
            type = value
        }
        if let value = try? container.decode(Int.self, forKey:.sender) {
            sender = String(value)
        }else if let value = try? container.decode(String.self, forKey:.sender) {
            sender = value
        }
        if let value = try? container.decode(Int.self, forKey:.createdAt) {
            createdAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.createdAt) {
            createdAt = value
        }
        if let value = try? container.decode(Int.self, forKey:.message) {
            message = String(value)
        }else if let value = try? container.decode(String.self, forKey:.message) {
            message = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
    }
    
}
