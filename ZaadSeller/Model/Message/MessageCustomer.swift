//
//  Customer.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/14/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct MessageCustomer: Codable {
    
    enum CodingKeys: String, CodingKey {
        case mobile
        case email
        case image
        case name
    }
    
    var mobile: String?
    var email: String?
    var image: String?
    var name: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.mobile) {
            mobile = String(value)
        }else if let value = try? container.decode(String.self, forKey:.mobile) {
            mobile = value
        }
        if let value = try? container.decode(Int.self, forKey:.email) {
            email = String(value)
        }else if let value = try? container.decode(String.self, forKey:.email) {
            email = value
        }
        if let value = try? container.decode(Int.self, forKey:.image) {
            image = String(value)
        }else if let value = try? container.decode(String.self, forKey:.image) {
            image = value
        }
        if let value = try? container.decode(Int.self, forKey:.name) {
            name = String(value)
        }else if let value = try? container.decode(String.self, forKey:.name) {
            name = value
        }
    }
    
}
