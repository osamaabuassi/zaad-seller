//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/14/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct MessageData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case customerId = "customer_id"
        case customer
    }
    
    var customerId: Int?
    var customer: MessageCustomer?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.customerId) {
            customerId = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.customerId) {
            customerId = value
        }
        customer = try container.decodeIfPresent(MessageCustomer.self, forKey: .customer)
    }
    
}
