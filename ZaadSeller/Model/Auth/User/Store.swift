//
//  Store.swift
//
//  Created by  Ahmed’s MacBook Pro on 8/19/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Store: Codable {
    
    enum CodingKeys: String, CodingKey {
        case phone
        case mobile
        case name
        case expireDate = "expire_date"
        case createdAt = "created_at"
        case status
        case owner
        case email
        case size
        case businessRecordNo = "business_record_no"
        case logo
        case businessName = "business_name"
        case activity
        case id
        case activityType = "activity_type"
        case inventoryNo = "inventory_no"
        case taxNo = "tax_no"
        case address
        case addressMap = "address_map"
        
    }
    
    var id: Int?
    var name: String?
    var phone: String?
    var mobile: String?
    var email: String?
    var owner: String?
    var logo: String?
    var size: Int?
    var expireDate: String?
    var status: Int?
    var inventoryNo: Int?
    var taxNo: Int?
    var address: String?
    
    var businessRecordNo: String?
    var businessName: String?
    var createdAt: String?
    var activityType: ActivityType?
    var activity: Activity?
    var addressMap: AddressMap?
    
    
    
    //inventory_no
    //tax_no
    //address
    //address_map
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if let value = try? container.decode(String.self, forKey:.inventoryNo) {
            inventoryNo = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.inventoryNo) {
            inventoryNo = value
        }
        if let value = try? container.decode(String.self, forKey:.taxNo) {
            taxNo = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.taxNo) {
            taxNo = value
        }
        if let value = try? container.decode(Int.self, forKey:.phone) {
            phone = String(value)
        }else if let value = try? container.decode(String.self, forKey:.phone) {
            phone = value
        }
        
        if let value = try? container.decode(Int.self, forKey:.address) {
            address = String(value)
        }else if let value = try? container.decode(String.self, forKey:.address) {
            address = value
        }
        
        
        if let value = try? container.decode(Int.self, forKey:.mobile) {
            mobile = String(value)
        }else if let value = try? container.decode(String.self, forKey:.mobile) {
            mobile = value
        }
        if let value = try? container.decode(Int.self, forKey:.name) {
            name = String(value)
        }else if let value = try? container.decode(String.self, forKey:.name) {
            name = value
        }
        if let value = try? container.decode(Int.self, forKey:.expireDate) {
            expireDate = String(value)
        }else if let value = try? container.decode(String.self, forKey:.expireDate) {
            expireDate = value
        }
        if let value = try? container.decode(Int.self, forKey:.createdAt) {
            createdAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.createdAt) {
            createdAt = value
        }
        if let value = try? container.decode(String.self, forKey:.status) {
            status = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.status) {
            status = value
        }
        if let value = try? container.decode(Int.self, forKey:.owner) {
            owner = String(value)
        }else if let value = try? container.decode(String.self, forKey:.owner) {
            owner = value
        }
        if let value = try? container.decode(Int.self, forKey:.email) {
            email = String(value)
        }else if let value = try? container.decode(String.self, forKey:.email) {
            email = value
        }
        if let value = try? container.decode(String.self, forKey:.size) {
            size = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.size) {
            size = value
        }
        if let value = try? container.decode(Int.self, forKey:.businessRecordNo) {
            businessRecordNo = String(value)
        }else if let value = try? container.decode(String.self, forKey:.businessRecordNo) {
            businessRecordNo = value
        }
        if let value = try? container.decode(Int.self, forKey:.logo) {
            logo = String(value)
        }else if let value = try? container.decode(String.self, forKey:.logo) {
            logo = value
        }
        if let value = try? container.decode(Int.self, forKey:.businessName) {
            businessName = String(value)
        }else if let value = try? container.decode(String.self, forKey:.businessName) {
            businessName = value
        }
        activity = try container.decodeIfPresent(Activity.self, forKey: .activity)
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        activityType = try container.decodeIfPresent(ActivityType.self, forKey: .activityType)
        
        addressMap = try container.decodeIfPresent(AddressMap.self, forKey: .addressMap)
    }
    
}
