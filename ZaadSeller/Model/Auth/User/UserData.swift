//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/10/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct UserData: Codable {

  enum CodingKeys: String, CodingKey {
    case auth
    case user
  }

  var auth: Auth?
  var user: UserStruct?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    auth = try container.decodeIfPresent(Auth.self, forKey: .auth)
    user = try container.decodeIfPresent(UserStruct.self, forKey: .user)
  }

}
