//
//  Auth.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/10/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Auth: Codable {

  enum CodingKeys: String, CodingKey {
    case expiresIn = "expires_in"
    case accessToken = "access_token"
    case tokenType = "token_type"
  }

  var expiresIn: Int?
  var accessToken: String?
  var tokenType: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(String.self, forKey:.expiresIn) {
 expiresIn = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.expiresIn) {
expiresIn = value 
}
    if let value = try? container.decode(Int.self, forKey:.accessToken) {                       
accessToken = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.accessToken) {
 accessToken = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.tokenType) {                       
tokenType = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.tokenType) {
 tokenType = value                                                                                     
}
  }

}
