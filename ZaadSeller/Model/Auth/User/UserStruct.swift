//
//  User.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/10/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct UserStruct: Codable {
    
    enum CodingKeys: String, CodingKey {
        case store
        case blocked
        case nationality
        case country
        case city
        case address
        case name
        case username
        case mobile
        case emailVerified = "email_verified"
        case phone
        case id
        case isActive = "is_active"
        case createdAt = "created_at"
        case status
        case paymentMethod = "payment_method"
        case email
    }
    
    var store: Store?
    var blocked: Int?
    var nationality: Nationality?
    var country: Country?
    var city: City?
    var address: String?
    var name: String?
    var username: String?
    var mobile: String?
    var emailVerified: Int?
    var phone: String?
    var id: Int?
    var isActive: Int?
    var createdAt: String?
    var status: Int?
    var paymentMethod: Int?
    var email: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        store = try container.decodeIfPresent(Store.self, forKey: .store)
        if let value = try? container.decode(String.self, forKey:.blocked) {
            blocked = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.blocked) {
            blocked = value
        }
        nationality = try container.decodeIfPresent(Nationality.self, forKey: .nationality)
        country = try container.decodeIfPresent(Country.self, forKey: .country)
        city = try container.decodeIfPresent(City.self, forKey: .city)
        if let value = try? container.decode(Int.self, forKey:.address) {
            address = String(value)
        }else if let value = try? container.decode(String.self, forKey:.address) {
            address = value
        }
        if let value = try? container.decode(Int.self, forKey:.name) {
            name = String(value)
        }else if let value = try? container.decode(String.self, forKey:.name) {
            name = value
        }
        if let value = try? container.decode(Int.self, forKey:.username) {
            username = String(value)
        }else if let value = try? container.decode(String.self, forKey:.username) {
            username = value
        }
        if let value = try? container.decode(Int.self, forKey:.mobile) {
            mobile = String(value)
        }else if let value = try? container.decode(String.self, forKey:.mobile) {
            mobile = value
        }
        if let value = try? container.decode(String.self, forKey:.emailVerified) {
            emailVerified = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.emailVerified) {
            emailVerified = value
        }
        if let value = try? container.decode(Int.self, forKey:.phone) {
            phone = String(value)
        }else if let value = try? container.decode(String.self, forKey:.phone) {
            phone = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(String.self, forKey:.isActive) {
            isActive = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.isActive) {
            isActive = value
        }
        if let value = try? container.decode(Int.self, forKey:.createdAt) {
            createdAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.createdAt) {
            createdAt = value
        }
        if let value = try? container.decode(String.self, forKey:.status) {
            status = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.status) {
            status = value
        }
        if let value = try? container.decode(String.self, forKey:.paymentMethod) {
            paymentMethod = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.paymentMethod) {
            paymentMethod = value
        }
        if let value = try? container.decode(Int.self, forKey:.email) {
            email = String(value)
        }else if let value = try? container.decode(String.self, forKey:.email) {
            email = value
        }
    }
    
}
