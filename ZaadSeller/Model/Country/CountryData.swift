//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/10/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct CountryData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case currentPage = "current_page"
        case resources
        case lastPageUrl = "last_page_url"
        case currentPageUrl = "current_page_url"
        case total
        case lastPage = "last_page"
        case firstPageUrl = "first_page_url"
        case perPage = "per_page"
    }
    
    var currentPage: Int?
    var resources: [Country]?
    var lastPageUrl: String?
    var currentPageUrl: String?
    var total: Int?
    var lastPage: Int?
    var firstPageUrl: String?
    var perPage: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.currentPage) {
            currentPage = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.currentPage) {
            currentPage = value
        }
        resources = try container.decodeIfPresent([Country].self, forKey: .resources)
        if let value = try? container.decode(Int.self, forKey:.lastPageUrl) {
            lastPageUrl = String(value)
        }else if let value = try? container.decode(String.self, forKey:.lastPageUrl) {
            lastPageUrl = value
        }
        if let value = try? container.decode(Int.self, forKey:.currentPageUrl) {
            currentPageUrl = String(value)
        }else if let value = try? container.decode(String.self, forKey:.currentPageUrl) {
            currentPageUrl = value
        }
        if let value = try? container.decode(String.self, forKey:.total) {
            total = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.total) {
            total = value
        }
        if let value = try? container.decode(String.self, forKey:.lastPage) {
            lastPage = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.lastPage) {
            lastPage = value
        }
        if let value = try? container.decode(Int.self, forKey:.firstPageUrl) {
            firstPageUrl = String(value)
        }else if let value = try? container.decode(String.self, forKey:.firstPageUrl) {
            firstPageUrl = value
        }
        if let value = try? container.decode(String.self, forKey:.perPage) {
            perPage = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.perPage) {
            perPage = value
        }
    }
    
}
