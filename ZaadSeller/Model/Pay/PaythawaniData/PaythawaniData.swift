//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/10/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct PaythawaniData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case publicKey = "public_key"
        case id
        case apiKey = "api_key"
    }
    
    var publicKey: String?
    var id: Int?
    var apiKey: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.publicKey) {
            publicKey = String(value)
        }else if let value = try? container.decode(String.self, forKey:.publicKey) {
            publicKey = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.apiKey) {
            apiKey = String(value)
        }else if let value = try? container.decode(String.self, forKey:.apiKey) {
            apiKey = value
        }
    }
    
}
