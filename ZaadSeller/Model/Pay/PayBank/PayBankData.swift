//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/10/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct PayBankData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case branch
        case id
        case accountNo = "account_no"
        case bankName = "bank_name"
        case name
    }
    
    var branch: String?
    var id: Int?
    var accountNo: String?
    var bankName: String?
    var name: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.branch) {
            branch = String(value)
        }else if let value = try? container.decode(String.self, forKey:.branch) {
            branch = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.accountNo) {
            accountNo = String(value)
        }else if let value = try? container.decode(String.self, forKey:.accountNo) {
            accountNo = value
        }
        if let value = try? container.decode(Int.self, forKey:.bankName) {
            bankName = String(value)
        }else if let value = try? container.decode(String.self, forKey:.bankName) {
            bankName = value
        }
        if let value = try? container.decode(Int.self, forKey:.name) {
            name = String(value)
        }else if let value = try? container.decode(String.self, forKey:.name) {
            name = value
        }
    }
    
}
