//
//  BaseClass.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/10/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct PayBankObject: Codable {
    
    enum CodingKeys: String, CodingKey {
        case code
        case data
        case locale
        case message
        case status
    }
    
    var code: Int?
    var data: PayBankData?
    var locale: String?
    var message: String?
    var status: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.code) {
            code = Int(value)                                                                                     
        } else if let value = try? container.decode(Int.self, forKey:.code) {
            code = value 
        }
        data = try container.decodeIfPresent(PayBankData.self, forKey: .data)
        if let value = try? container.decode(Int.self, forKey:.locale) {                       
            locale = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.locale) {
            locale = value                                                                                     
        }
        if let value = try? container.decode(Int.self, forKey:.message) {                       
            message = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.message) {
            message = value                                                                                     
        }
        if let value = try? container.decode(String.self, forKey:.status) {
            status = Int(value)                                                                                     
        } else if let value = try? container.decode(Int.self, forKey:.status) {
            status = value 
        }
    }
    
}
