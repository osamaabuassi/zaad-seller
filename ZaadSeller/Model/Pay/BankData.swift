//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/10/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct BankData: Codable {

  enum CodingKeys: String, CodingKey {
    case lastPage = "last_page"
    case total
    case currentPage = "current_page"
    case firstPageUrl = "first_page_url"
    case currentPageUrl = "current_page_url"
    case resources
    case perPage = "per_page"
    case lastPageUrl = "last_page_url"
    case nextPageUrl = "next_page_url"
  }

  var lastPage: Int?
  var total: Int?
  var currentPage: Int?
  var firstPageUrl: String?
  var currentPageUrl: String?
  var resources: [Bank]?
  var perPage: Int?
  var lastPageUrl: String?
  var nextPageUrl: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(String.self, forKey:.lastPage) {
 lastPage = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.lastPage) {
lastPage = value 
}
    if let value = try? container.decode(String.self, forKey:.total) {
 total = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.total) {
total = value 
}
    if let value = try? container.decode(String.self, forKey:.currentPage) {
 currentPage = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.currentPage) {
currentPage = value 
}
    if let value = try? container.decode(Int.self, forKey:.firstPageUrl) {                       
firstPageUrl = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.firstPageUrl) {
 firstPageUrl = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.currentPageUrl) {                       
currentPageUrl = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.currentPageUrl) {
 currentPageUrl = value                                                                                     
}
    resources = try container.decodeIfPresent([Bank].self, forKey: .resources)
    if let value = try? container.decode(String.self, forKey:.perPage) {
 perPage = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.perPage) {
perPage = value 
}
    if let value = try? container.decode(Int.self, forKey:.lastPageUrl) {                       
lastPageUrl = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.lastPageUrl) {
 lastPageUrl = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.nextPageUrl) {                       
nextPageUrl = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.nextPageUrl) {
 nextPageUrl = value                                                                                     
}
  }

}
