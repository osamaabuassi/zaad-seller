//
//  Data.swift
//
//  Created by osamaaassi on 2/1/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct StoresData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case businessRecordNo = "business_record_no"
        case status
        case taxNo = "tax_no"
        case createdAt = "created_at"
        case name
        case email
        case phone
        case inventoryNo = "inventory_no"
        case activity
        case logo
        case owner
        case addressMap = "address_map"
        case businessName = "business_name"
        case size
        case activityType = "activity_type"
        case expireDate = "expire_date"
        case mobile
        case address
        case id
    }
    
    var businessRecordNo: String?
    var status: Int?
    var taxNo: Int?
    var createdAt: String?
    var name: String?
    var email: String?
    var phone: String?
    var inventoryNo: Int?
    var activity: ActivityStore?
    var logo: String?
    var owner: String?
    var addressMap: AddressMap?
    var businessName: String?
    var size: Int?
    var activityType: ActivityTypeStore?
    var expireDate: String?
    var mobile: String?
    var address: String?
    var id: Int?
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.businessRecordNo) {
            businessRecordNo = String(value)
        }else if let value = try? container.decode(String.self, forKey:.businessRecordNo) {
            businessRecordNo = value
        }
        if let value = try? container.decode(String.self, forKey:.status) {
            status = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.status) {
            status = value
        }
        if let value = try? container.decode(String.self, forKey:.taxNo) {
            taxNo = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.taxNo) {
            taxNo = value
        }
        if let value = try? container.decode(Int.self, forKey:.createdAt) {
            createdAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.createdAt) {
            createdAt = value
        }
        if let value = try? container.decode(Int.self, forKey:.name) {
            name = String(value)
        }else if let value = try? container.decode(String.self, forKey:.name) {
            name = value
        }
        if let value = try? container.decode(Int.self, forKey:.email) {
            email = String(value)
        }else if let value = try? container.decode(String.self, forKey:.email) {
            email = value
        }
        if let value = try? container.decode(Int.self, forKey:.phone) {
            phone = String(value)
        }else if let value = try? container.decode(String.self, forKey:.phone) {
            phone = value
        }
        if let value = try? container.decode(String.self, forKey:.inventoryNo) {
            inventoryNo = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.inventoryNo) {
            inventoryNo = value
        }
        activity = try container.decodeIfPresent(ActivityStore.self, forKey: .activity)
        if let value = try? container.decode(Int.self, forKey:.logo) {
            logo = String(value)
        }else if let value = try? container.decode(String.self, forKey:.logo) {
            logo = value
        }
        if let value = try? container.decode(Int.self, forKey:.owner) {
            owner = String(value)
        }else if let value = try? container.decode(String.self, forKey:.owner) {
            owner = value
        }
        addressMap = try container.decodeIfPresent(AddressMap.self, forKey: .addressMap)
        if let value = try? container.decode(Int.self, forKey:.businessName) {
            businessName = String(value)
        }else if let value = try? container.decode(String.self, forKey:.businessName) {
            businessName = value
        }
        if let value = try? container.decode(String.self, forKey:.size) {
            size = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.size) {
            size = value
        }
        activityType = try container.decodeIfPresent(ActivityTypeStore.self, forKey: .activityType)
        if let value = try? container.decode(Int.self, forKey:.expireDate) {
            expireDate = String(value)
        }else if let value = try? container.decode(String.self, forKey:.expireDate) {
            expireDate = value
        }
        if let value = try? container.decode(Int.self, forKey:.mobile) {
            mobile = String(value)
        }else if let value = try? container.decode(String.self, forKey:.mobile) {
            mobile = value
        }
        if let value = try? container.decode(Int.self, forKey:.address) {
            address = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.address) {
            address = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
    }
    
}
