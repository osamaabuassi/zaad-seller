//
//  Children.swift
//
//  Created by osamaaassi on 2/1/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ChildrenStore: Codable {
    
    enum CodingKeys: String, CodingKey {
        case id
        case title
        case children
        case open
    }
    
    var id: Int?
    var title: String?
    var children: [ChildrenCatogre]?
    var open:Bool?
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
         children = try container.decodeIfPresent([ChildrenCatogre].self, forKey: .children)
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
    }
    
}
