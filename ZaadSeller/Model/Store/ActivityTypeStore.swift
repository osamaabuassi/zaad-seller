//
//  ActivityType.swift
//
//  Created by osamaaassi on 2/1/21
//  Copyright (c) . All rights reserved.
//

import Foundation

class ActivityTypeStore: Codable {
    
    enum CodingKeys: String, CodingKey {
        case children
        case title
        case id
    }
    
    var children: [ActivityTypeStore]?
    var title: String?
    var id: Int?
    
    
    var isSelected :Bool = false
    
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        children = try container.decodeIfPresent([ActivityTypeStore].self, forKey: .children)
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value                                                                                     
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
    }
    
}
