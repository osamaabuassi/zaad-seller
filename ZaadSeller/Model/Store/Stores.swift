//
//  Stores.swift
//
//  Created by osamaaassi on 2/1/21
//  Copyright (c) . All rights reserved.
//

import Foundation
enum ExpandbleType{
    case sub1,sub2,sub3
}


struct ExpandableStore{
    var activityTypeStore :ActivityTypeStore
    var type :ExpandbleType
}



struct Stores: Codable {
    
    enum CodingKeys: String, CodingKey {
        case data
        case message
        case code
        case locale
        case status
    }
    
    var data: [StoresData]?
    var message: String?
    var code: Int?
    var locale: String?
    var status: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        data = try container.decodeIfPresent([StoresData].self, forKey: .data)
        if let value = try? container.decode(Int.self, forKey:.message) {
            message = String(value)
        }else if let value = try? container.decode(String.self, forKey:.message) {
            message = value
        }
        if let value = try? container.decode(String.self, forKey:.code) {
            code = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.code) {
            code = value
        }
        if let value = try? container.decode(Int.self, forKey:.locale) {
            locale = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.locale) {
            locale = value
        }
        if let value = try? container.decode(String.self, forKey:.status) {
            status = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.status) {
            status = value
        }
    }
    
}
