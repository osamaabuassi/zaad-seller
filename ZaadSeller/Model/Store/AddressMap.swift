//
//  AddressMap.swift
//
//  Created by osamaaassi on 2/1/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct AddressMap: Codable {
    
    enum CodingKeys: String, CodingKey {
        case lat
        case lng
    }
    
    var lat: String?
    var lng: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.lat) {
            lat = String(value)
        }else if let value = try? container.decode(String.self, forKey:.lat) {
            lat = value                                                                                     
        }
        if let value = try? container.decode(Int.self, forKey:.lng) {
            lng = String(value)
        }else if let value = try? container.decode(String.self, forKey:.lng) {
            lng = value
        }
    }
    
}
