//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 8/16/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct LinkData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case link
    }
    
    var link: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.link) {                       
            link = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.link) {
            link = value                                                                                     
        }
    }
    
}
