//
//  Notifications.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/12/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Notifications: Codable {
    
    enum CodingKeys: String, CodingKey {
        case descriptionValue = "description"
        case isRead = "is_read"
        case createdAt = "created_at"
        case id
        case title
    }
    
    var descriptionValue: String?
    var isRead: Bool?
    var createdAt: String?
    var id: Int?
    var title: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.descriptionValue) {
            descriptionValue = String(value)
        }else if let value = try? container.decode(String.self, forKey:.descriptionValue) {
            descriptionValue = value
        }
        isRead = try container.decodeIfPresent(Bool.self, forKey: .isRead)
        if let value = try? container.decode(Int.self, forKey:.createdAt) {
            createdAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.createdAt) {
            createdAt = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
    }
    
}
