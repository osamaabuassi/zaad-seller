//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/12/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct NotificationData: Codable {

  enum CodingKeys: String, CodingKey {
    case currentPageUrl = "current_page_url"
    case lastPage = "last_page"
    case nextPageUrl = "next_page_url"
    case notifications
    case total
    case firstPageUrl = "first_page_url"
    case currentPage = "current_page"
    case lastPageUrl = "last_page_url"
    case perPage = "per_page"
  }

  var currentPageUrl: String?
  var lastPage: Int?
  var nextPageUrl: String?
  var notifications: [Notifications]?
  var total: Int?
  var firstPageUrl: String?
  var currentPage: Int?
  var lastPageUrl: String?
  var perPage: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.currentPageUrl) {                       
currentPageUrl = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.currentPageUrl) {
 currentPageUrl = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.lastPage) {
 lastPage = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.lastPage) {
lastPage = value 
}
    if let value = try? container.decode(Int.self, forKey:.nextPageUrl) {                       
nextPageUrl = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.nextPageUrl) {
 nextPageUrl = value                                                                                     
}
    notifications = try container.decodeIfPresent([Notifications].self, forKey: .notifications)
    if let value = try? container.decode(String.self, forKey:.total) {
 total = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.total) {
total = value 
}
    if let value = try? container.decode(Int.self, forKey:.firstPageUrl) {                       
firstPageUrl = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.firstPageUrl) {
 firstPageUrl = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.currentPage) {
 currentPage = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.currentPage) {
currentPage = value 
}
    if let value = try? container.decode(Int.self, forKey:.lastPageUrl) {                       
lastPageUrl = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.lastPageUrl) {
 lastPageUrl = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.perPage) {                       
perPage = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.perPage) {
 perPage = value                                                                                     
}
  }

}
