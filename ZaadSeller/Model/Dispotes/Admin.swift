//
//  Admin.swift
//
//  Created by  Ahmed’s MacBook Pro on 9/5/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Admin: Codable {

  enum CodingKeys: String, CodingKey {
    case name
    case image
  }

  var name: String?
  var image: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.name) {                       
name = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.name) {
 name = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.image) {                       
image = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.image) {
 image = value                                                                                     
}
  }

}
