//
//  Comments.swift
//
//  Created by  Ahmed’s MacBook Pro on 9/5/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct DisputesDeatilesComments: Codable {

  enum CodingKeys: String, CodingKey {
    case comment
    case senderType = "sender_type"
    case date
    case senderId = "sender_id"
    case senderName = "sender_name"
  }

  var comment: String?
  var senderType: String?
  var date: String?
  var senderId: Int?
  var senderName: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.comment) {                       
comment = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.comment) {
 comment = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.senderType) {                       
senderType = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.senderType) {
 senderType = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.date) {                       
date = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.date) {
 date = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.senderId) {
 senderId = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.senderId) {
senderId = value 
}
    if let value = try? container.decode(Int.self, forKey:.senderName) {                       
senderName = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.senderName) {
 senderName = value                                                                                     
}
  }

}
