//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 9/5/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct DisputesDeatilesData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case adminInformed
        case admin
        case comments
        case createdAt = "created_at"
        case orderNo = "order_no"
        case user
        case sellerAskedForClose = "seller_asked_for_close"
        case disputeCode = "dispute_code"
        case id
        case seller
        case status
        case item
        case customerConfirmedClose = "customer_confirmed_close"
        case problem
        case videoUrl = "video_url"
        case attachment
        
        
    }
    
    var adminInformed: Bool?
    var admin: Admin?
    var comments: [DisputesDeatilesComments]?
    var createdAt: String?
    var orderNo: String?
    var user: DisputesDeatilesUser?
    var sellerAskedForClose: Bool?
    var disputeCode: String?
    var id: Int?
    var seller: DisputesSeller?
    var status: DisputesDeatilesStatus?
    var customerConfirmedClose: Bool?
    var problem: String?
    var item: String?
    var videoUrl: String?
    var attachment: String?
    
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        adminInformed = try container.decodeIfPresent(Bool.self, forKey: .adminInformed)
        admin = try container.decodeIfPresent(Admin.self, forKey: .admin)
        comments = try container.decodeIfPresent([DisputesDeatilesComments].self, forKey: .comments)
        if let value = try? container.decode(Int.self, forKey:.createdAt) {
            createdAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.createdAt) {
            createdAt = value
        }
        if let value = try? container.decode(Int.self, forKey:.orderNo) {
            orderNo = String(value)
        }else if let value = try? container.decode(String.self, forKey:.orderNo) {
            orderNo = value
        }
        user = try container.decodeIfPresent(DisputesDeatilesUser.self, forKey: .user)
        sellerAskedForClose = try container.decodeIfPresent(Bool.self, forKey: .sellerAskedForClose)
        if let value = try? container.decode(Int.self, forKey:.disputeCode) {
            disputeCode = String(value)
        }else if let value = try? container.decode(String.self, forKey:.disputeCode) {
            disputeCode = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        seller = try container.decodeIfPresent(DisputesSeller.self, forKey: .seller)
        status = try container.decodeIfPresent(DisputesDeatilesStatus.self, forKey: .status)
        customerConfirmedClose = try container.decodeIfPresent(Bool.self, forKey: .customerConfirmedClose)
        if let value = try? container.decode(Int.self, forKey:.problem) {
            problem = String(value)
        }else if let value = try? container.decode(String.self, forKey:.problem) {
            problem = value
        }
        if let value = try? container.decode(Int.self, forKey:.item) {
            item = String(value)
        }else if let value = try? container.decode(String.self, forKey:.item) {
            item = value
        }
        if let value = try? container.decode(Int.self, forKey:.videoUrl) {
            videoUrl = String(value)
        }else if let value = try? container.decode(String.self, forKey:.videoUrl) {
            videoUrl = value
        }
        if let value = try? container.decode(Int.self, forKey:.attachment) {
            attachment = String(value)
        }else if let value = try? container.decode(String.self, forKey:.attachment) {
            attachment = value
        }
        
    }
    
}
