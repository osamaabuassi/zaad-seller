//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/10/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct CapounsDeatilesData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case descriptionValue = "description"
        case id
        case status
        case amount
        case minAmount = "min_amount"
        case usedAmount = "used_amount"
        case discountType = "discount_type"
        case allowed
        case amountLabel = "amount_label"
        case code
        case endDate = "end_date"
        case startDate = "start_date"
        case usage
    }
    
    var descriptionValue: String?
    var id: Int?
    var status: Status?
    var amount: Int?
    var usedAmount: Int?
    var minAmount: Int?
    var discountType: DiscountType?
    var allowed: Int?
    var amountLabel: String?
    var code: String?
    var endDate: String?
    var startDate: String?
    var usage: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.descriptionValue) {
            descriptionValue = String(value)
        }else if let value = try? container.decode(String.self, forKey:.descriptionValue) {
            descriptionValue = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        status = try container.decodeIfPresent(Status.self, forKey: .status)
        if let value = try? container.decode(String.self, forKey:.amount) {
            amount = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.amount) {
            amount = value
        }
        if let value = try? container.decode(String.self, forKey:.usedAmount) {
            usedAmount = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.usedAmount) {
            usedAmount = value
        }
        discountType = try container.decodeIfPresent(DiscountType.self, forKey: .discountType)
        if let value = try? container.decode(String.self, forKey:.allowed) {
            allowed = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.allowed) {
            allowed = value
        }
        if let value = try? container.decode(Int.self, forKey:.amountLabel) {
            amountLabel = String(value)
        }else if let value = try? container.decode(String.self, forKey:.amountLabel) {
            amountLabel = value
        }
        if let value = try? container.decode(Int.self, forKey:.code) {
            code = String(value)
        }else if let value = try? container.decode(String.self, forKey:.code) {
            code = value
        }
        if let value = try? container.decode(Int.self, forKey:.endDate) {
            endDate = String(value)
        }else if let value = try? container.decode(String.self, forKey:.endDate) {
            endDate = value
        }
        if let value = try? container.decode(Int.self, forKey:.startDate) {
            startDate = String(value)
        }else if let value = try? container.decode(String.self, forKey:.startDate) {
            startDate = value
        }
        if let value = try? container.decode(String.self, forKey:.usage) {
            usage = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.usage) {
            usage = value
        }
        if let value = try? container.decode(String.self, forKey:.minAmount) {
            minAmount = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.minAmount) {
            minAmount = value
        }
    }
    
}
