//
//  DiscountType.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/10/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct DiscountType: Codable {
    
    enum CodingKeys: String, CodingKey {
        case id
        case title
    }
    
    var id: String?
    var title: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.id) {
            id = String(value)
        }else if let value = try? container.decode(String.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
    }
    
}
