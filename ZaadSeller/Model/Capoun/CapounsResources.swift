//
//  Resources.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/10/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct CapounsResources: Codable {
    
    enum CodingKeys: String, CodingKey {
        case descriptionValue = "description"
        case createdAt = "created_at"
        case allowed
        case startDate = "start_date"
        case endDate = "end_date"
        case amountLabel = "amount_label"
        case id
        case status
        case code
    }
    
    var descriptionValue: String?
    var createdAt: String?
    var allowed: Int?
    var startDate: String?
    var endDate: String?
    var amountLabel: String?
    var id: Int?
    var status: Status?
    var code: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.descriptionValue) {
            descriptionValue = String(value)
        }else if let value = try? container.decode(String.self, forKey:.descriptionValue) {
            descriptionValue = value
        }
        if let value = try? container.decode(Int.self, forKey:.createdAt) {
            createdAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.createdAt) {
            createdAt = value
        }
        if let value = try? container.decode(String.self, forKey:.allowed) {
            allowed = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.allowed) {
            allowed = value
        }
        if let value = try? container.decode(Int.self, forKey:.startDate) {
            startDate = String(value)
        }else if let value = try? container.decode(String.self, forKey:.startDate) {
            startDate = value
        }
        if let value = try? container.decode(Int.self, forKey:.endDate) {
            endDate = String(value)
        }else if let value = try? container.decode(String.self, forKey:.endDate) {
            endDate = value
        }
        if let value = try? container.decode(Int.self, forKey:.amountLabel) {
            amountLabel = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.amountLabel) {
            amountLabel = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        status = try container.decodeIfPresent(Status.self, forKey: .status)
        if let value = try? container.decode(Int.self, forKey:.code) {
            code = String(value)
        }else if let value = try? container.decode(String.self, forKey:.code) {
            code = value
        }
    }
    
}
