//
//  BaseClass.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/10/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct CapounsObject: Codable {
    
    enum CodingKeys: String, CodingKey {
        case code
        case data
        case message
        case status
        case locale
    }
    
    var code: Int?
    var data: CapounsData?
    var message: String?
    var status: Int?
    var locale: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.code) {
            code = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.code) {
            code = value
        }
        data = try container.decodeIfPresent(CapounsData.self, forKey: .data)
        if let value = try? container.decode(Int.self, forKey:.message) {
            message = String(value)
        }else if let value = try? container.decode(String.self, forKey:.message) {
            message = value
        }
        if let value = try? container.decode(String.self, forKey:.status) {
            status = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.status) {
            status = value
        }
        if let value = try? container.decode(Int.self, forKey:.locale) {
            locale = String(value)
        }else if let value = try? container.decode(String.self, forKey:.locale) {
            locale = value
        }
    }
    
}
