//
//  AddressMap.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/21/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct AddressMap: Codable {
    
    enum CodingKeys: String, CodingKey {
        case lng
        case lat
    }
    
    var lng: String?
    var lat: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.lng) {
            lng = String(value)
        }else if let value = try? container.decode(String.self, forKey:.lng) {
            lng = value
        }
        if let value = try? container.decode(Int.self, forKey:.lat) {
            lat = String(value)
        }else if let value = try? container.decode(String.self, forKey:.lat) {
            lat = value
        }
    }
    
}
