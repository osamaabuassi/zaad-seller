//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/21/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct StoreData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case expireDate = "expire_date"
        case name
        case taxNo = "tax_no"
        case createdAt = "created_at"
        case businessRecordNo = "business_record_no"
        case id
        case inventoryNo = "inventory_no"
        case size
        case status
        case logo
        case address
        case activityType = "activity_type"
        case activity
        case owner
        case addressMap = "address_map"
        case businessName = "business_name"
    }
    
    var expireDate: String?
    var name: String?
    var taxNo: Int?
    var createdAt: String?
    var businessRecordNo: String?
    var id: Int?
    var inventoryNo: Int?
    var size: Int?
    var status: Int?
    var logo: String?
    var address: String?
    var activityType: ActivityType?
    var activity: Activity?
    var owner: String?
    var addressMap: AddressMap?
    var businessName: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.expireDate) {
            expireDate = String(value)
        }else if let value = try? container.decode(String.self, forKey:.expireDate) {
            expireDate = value
        }
        if let value = try? container.decode(Int.self, forKey:.name) {
            name = String(value)
        }else if let value = try? container.decode(String.self, forKey:.name) {
            name = value
        }
        if let value = try? container.decode(String.self, forKey:.taxNo) {
            taxNo = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.taxNo) {
            taxNo = value
        }
        if let value = try? container.decode(Int.self, forKey:.createdAt) {
            createdAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.createdAt) {
            createdAt = value
        }
        if let value = try? container.decode(Int.self, forKey:.businessRecordNo) {
            businessRecordNo = String(value)
        }else if let value = try? container.decode(String.self, forKey:.businessRecordNo) {
            businessRecordNo = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(String.self, forKey:.inventoryNo) {
            inventoryNo = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.inventoryNo) {
            inventoryNo = value
        }
        if let value = try? container.decode(String.self, forKey:.size) {
            size = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.size) {
            size = value
        }
        if let value = try? container.decode(String.self, forKey:.status) {
            status = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.status) {
            status = value
        }
        if let value = try? container.decode(Int.self, forKey:.logo) {
            logo = String(value)
        }else if let value = try? container.decode(String.self, forKey:.logo) {
            logo = value
        }
        if let value = try? container.decode(Int.self, forKey:.address) {
            address = String(value)
        }else if let value = try? container.decode(String.self, forKey:.address) {
            address = value
        }
        activityType = try container.decodeIfPresent(ActivityType.self, forKey: .activityType)
        activity = try container.decodeIfPresent(Activity.self, forKey: .activity)
        if let value = try? container.decode(Int.self, forKey:.owner) {
            owner = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.owner) {
            owner = value
        }
        addressMap = try container.decodeIfPresent(AddressMap.self, forKey: .addressMap)
        if let value = try? container.decode(Int.self, forKey:.businessName) {
            businessName = String(value)
        }else if let value = try? container.decode(String.self, forKey:.businessName) {
            businessName = value
        }
    }
    
}
