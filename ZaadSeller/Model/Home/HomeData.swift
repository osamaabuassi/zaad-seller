//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/18/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct HomeData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case orderPerStatus = "order_per_status"
        case sales
        case orderPerPaymentMethod = "order_per_payment_method"
        case income
        case itemPerStatus = "item_per_status"
        case ordersSum = "orders_sum"
        case ordersCount = "orders_count"
    }
    
    var orderPerStatus: [OrderPerStatus]?
    var sales: [Sales]?
    var orderPerPaymentMethod: [OrderPerPaymentMethod]?
    var income: [Income]?
    var itemPerStatus: [ItemPerStatus]?
    var ordersSum: Int?
    var ordersCount: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        orderPerStatus = try container.decodeIfPresent([OrderPerStatus].self, forKey: .orderPerStatus)
        sales = try container.decodeIfPresent([Sales].self, forKey: .sales)
        orderPerPaymentMethod = try container.decodeIfPresent([OrderPerPaymentMethod].self, forKey: .orderPerPaymentMethod)
        income = try container.decodeIfPresent([Income].self, forKey: .income)
        itemPerStatus = try container.decodeIfPresent([ItemPerStatus].self, forKey: .itemPerStatus)
        if let value = try? container.decode(String.self, forKey:.ordersCount) {
            ordersCount = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.ordersCount) {
            ordersCount = value
        }else{
            let value = try? container.decode(Double.self, forKey:.ordersCount)
            ordersCount = Int(value ?? 0)
        }
        
        if let value = try? container.decode(String.self, forKey:.ordersSum) {
            ordersSum = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.ordersSum) {
            ordersSum = value
        }else{
            let value = try? container.decode(Double.self, forKey:.ordersSum)
            ordersSum = Int(value ?? 0)
        }
        
    }
    
}
