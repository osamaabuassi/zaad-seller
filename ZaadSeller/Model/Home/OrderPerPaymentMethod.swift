//
//  OrderPerPaymentMethod.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/18/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct OrderPerPaymentMethod: Codable {
    
    enum CodingKeys: String, CodingKey {
        case count
        case paymentMethod = "payment_method"
    }
    
    var count: Int?
    var paymentMethod: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.count) {
            count = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.count) {
            count = value
        }else{
            let value = try? container.decode(Double.self, forKey:.count)
            count = Int(value!)
        }
        if let value = try? container.decode(Int.self, forKey:.paymentMethod) {
            paymentMethod = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.paymentMethod) {
            paymentMethod = value
        }
    }
    
}
