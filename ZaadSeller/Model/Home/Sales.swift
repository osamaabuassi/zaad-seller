//
//  Sales.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/18/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Sales: Codable {
    
    enum CodingKeys: String, CodingKey {
        case status
        case sum
    }
    
    var status: String?
    var sum: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.status) {
            status = String(value)
        }else if let value = try? container.decode(String.self, forKey:.status) {
            status = value
        }
        if let value = try? container.decode(String.self, forKey:.sum) {
            sum = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.sum) {
            sum = value
        }else{
            let value = try? container.decode(Double.self, forKey:.sum)
            sum = Int(value ?? 0)
        }
    }
    
}
