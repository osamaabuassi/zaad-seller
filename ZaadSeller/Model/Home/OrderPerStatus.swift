//
//  OrderPerStatus.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/18/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct OrderPerStatus: Codable {
    
    enum CodingKeys: String, CodingKey {
        case count
        case status
    }
    
    var count: Int?
    var status: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.count) {
            count = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.count) {
            count = value
        }else{
            let value = try? container.decode(Double.self, forKey:.count)
            count = Int(value!)
        }
        if let value = try? container.decode(Int.self, forKey:.status) {
            status = String(value)
        }else if let value = try? container.decode(String.self, forKey:.status) {
            status = value
        }
    }
    
}
