//
//  Items.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/11/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Producat: Codable {
    
    enum CodingKeys: String, CodingKey {
        case regularPrice = "regular_price"
        case updatedAt = "updated_at"
        case id
        case salePrice = "sale_price"
        case createdAt = "created_at"
        case title
        case status
        case image
        case category
        case published
    }
    
    var regularPrice: Int?
    var updatedAt: String?
    var id: Int?
    var salePrice: Int?
    var createdAt: String?
    var title: String?
    var status: Status?
    var image: String?
    var category: Category?
    var published: Published?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        published = try container.decodeIfPresent(Published.self, forKey: .published)
        if let value = try? container.decode(String.self, forKey:.regularPrice) {
            regularPrice = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.regularPrice) {
            regularPrice = value
        }
        if let value = try? container.decode(Int.self, forKey:.updatedAt) {
            updatedAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.updatedAt) {
            updatedAt = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(String.self, forKey:.salePrice) {
            salePrice = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.salePrice) {
            salePrice = value
        }
        if let value = try? container.decode(Int.self, forKey:.createdAt) {
            createdAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.createdAt) {
            createdAt = value
        }
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
        status = try container.decodeIfPresent(Status.self, forKey: .status)
        if let value = try? container.decode(Int.self, forKey:.image) {
            image = String(value)
        }else if let value = try? container.decode(String.self, forKey:.image) {
            image = value
        }
        category = try container.decodeIfPresent(Category.self, forKey: .category)
    }
}
