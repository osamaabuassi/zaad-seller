//
//  Resources.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/11/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Images: Codable {
    
    enum CodingKeys: String, CodingKey {
        case src
        case id
        case title
        case alt
    }
    
    var src: String?
    var id: Int?
    var title: String?
    var alt: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.src) {
            src = String(value)
        }else if let value = try? container.decode(String.self, forKey:.src) {
            src = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
        if let value = try? container.decode(Int.self, forKey:.alt) {
            alt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.alt) {
            alt = value
        }
    }
    
}
