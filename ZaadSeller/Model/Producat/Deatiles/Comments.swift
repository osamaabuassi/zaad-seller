//
//  Comments.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/12/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Comments: Codable {

  enum CodingKeys: String, CodingKey {
    case date
    case comment
    case user
  }

  var date: String?
  var comment: String?
  var user: User?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.date) {                       
date = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.date) {
 date = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.comment) {                       
comment = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.comment) {
 comment = value                                                                                     
}
    user = try container.decodeIfPresent(User.self, forKey: .user)
  }

}
