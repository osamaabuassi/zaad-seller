//
//  Rates.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/12/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Rates: Codable {

  enum CodingKeys: String, CodingKey {
    case details
  }

  var details: [ProducatDetails]?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    details = try container.decodeIfPresent([ProducatDetails].self, forKey: .details)
  }

}
