//
//  BaseClass.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/12/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ProductaDeatilesObject: Codable {
    
    enum CodingKeys: String, CodingKey {
        case status
        case code
        case locale
        case message
        case data
    }
    
    var status: Int?
    var code: Int?
    var locale: String?
    var message: String?
    var data: ProducatInfo?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.status) {
            status = Int(value)                                                                                     
        } else if let value = try? container.decode(Int.self, forKey:.status) {
            status = value 
        }
        if let value = try? container.decode(String.self, forKey:.code) {
            code = Int(value)                                                                                     
        } else if let value = try? container.decode(Int.self, forKey:.code) {
            code = value 
        }
        if let value = try? container.decode(Int.self, forKey:.locale) {                       
            locale = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.locale) {
            locale = value                                                                                     
        }
        if let value = try? container.decode(Int.self, forKey:.message) {                       
            message = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.message) {
            message = value                                                                                     
        }
        data = try container.decodeIfPresent(ProducatInfo.self, forKey: .data)
    }
    
}
