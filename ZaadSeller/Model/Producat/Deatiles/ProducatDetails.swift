//
//  Details.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/12/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ProducatDetails: Codable {
    
    enum CodingKeys: String, CodingKey {
        case date
        case rate
        case user
        case note
    }
    
    var date: String?
    var rate: Int?
    var user: User?
    var note: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.date) {
            date = String(value)
        }else if let value = try? container.decode(String.self, forKey:.date) {
            date = value
        }
        if let value = try? container.decode(String.self, forKey:.rate) {
            rate = Int(value)                                                                                     
        } else if let value = try? container.decode(Int.self, forKey:.rate) {
            rate = value
        }
        user = try container.decodeIfPresent(User.self, forKey: .user)
        if let value = try? container.decode(Int.self, forKey:.note) {
            note = String(value)
        }else if let value = try? container.decode(String.self, forKey:.note) {
            note = value
        }
    }
    
}
