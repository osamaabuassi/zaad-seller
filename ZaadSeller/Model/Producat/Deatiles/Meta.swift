//
//  Meta.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/12/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Meta: Codable {

  enum CodingKeys: String, CodingKey {
    case name
    case value
  }

  var name: String?
  var value: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.name) {                       
name = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.name) {
 name = value                                                                                     
}
    if let valuer = try? container.decode(Int.self, forKey:.value) {
value = String(valuer)
}else if let valuer = try? container.decode(String.self, forKey:.value) {
 value = valuer
}
  }

}
