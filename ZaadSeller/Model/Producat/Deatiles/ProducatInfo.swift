//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/12/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ProducatInfo: Codable {
    
    enum CodingKeys: String, CodingKey {
        case weight
        case meta
        case status
        case id
        case discount
        case comments
        case image
        case title
        case createdAt = "created_at"
        case descriptionValue = "description"
        case category
        case rates
        case brand
        case quantity
        case shortDescription = "short_description"
        case regularPrice = "regular_price"
        case images
        case salePrice = "sale_price"
        case updatedAt = "updated_at"
    }
    
    var weight: Int?
    var meta: [Meta]?
    var status: Status?
    var id: Int?
    var discount: Int?
    var comments: [Comments]?
    var image: String?
    var title: String?
    var createdAt: String?
    var descriptionValue: String?
    var category: Category?
    var rates: Rates?
    var quantity: Int?
    var brand: String?
    var shortDescription: String?
    var regularPrice: Int?
    var images: [Images]?
    var salePrice: Int?
    var updatedAt: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.weight) {
            weight = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.weight) {
            weight = value
        }
        meta = try container.decodeIfPresent([Meta].self, forKey: .meta)
        status = try container.decodeIfPresent(Status.self, forKey: .status)
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(String.self, forKey:.discount) {
            discount = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.discount) {
            discount = value
        }
        comments = try container.decodeIfPresent([Comments].self, forKey: .comments)
        if let value = try? container.decode(Int.self, forKey:.image) {
            image = String(value)
        }else if let value = try? container.decode(String.self, forKey:.image) {
            image = value
        }
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
        if let value = try? container.decode(Int.self, forKey:.createdAt) {
            createdAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.createdAt) {
            createdAt = value
        }
        if let value = try? container.decode(Int.self, forKey:.descriptionValue) {
            descriptionValue = String(value)
        }else if let value = try? container.decode(String.self, forKey:.descriptionValue) {
            descriptionValue = value
        }
        category = try container.decodeIfPresent(Category.self, forKey: .category)
        rates = try container.decodeIfPresent(Rates.self, forKey: .rates)
        if let value = try? container.decode(String.self, forKey:.quantity) {
            quantity = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.quantity) {
            quantity = value
        }
        if let value = try? container.decode(Int.self, forKey:.shortDescription) {
            shortDescription = String(value)
        }else if let value = try? container.decode(String.self, forKey:.shortDescription) {
            shortDescription = value
        }
        if let value = try? container.decode(String.self, forKey:.regularPrice) {
            regularPrice = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.regularPrice) {
            regularPrice = value
        }
        images = try container.decodeIfPresent([Images].self, forKey: .images)
        if let value = try? container.decode(String.self, forKey:.salePrice) {
            salePrice = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.salePrice) {
            salePrice = value
        }
        if let value = try? container.decode(Int.self, forKey:.updatedAt) {
            updatedAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.updatedAt) {
            updatedAt = value
        }
        if let value = try? container.decode(Int.self, forKey:.brand) {
            brand = String(value)
        }else if let value = try? container.decode(String.self, forKey:.brand) {
            brand = value
        }
    }
    
}
