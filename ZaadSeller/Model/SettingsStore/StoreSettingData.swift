//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 8/28/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct StoreSettingData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case status
        case phone
        case activity
        case logo
        case inventoryNo = "inventory_no"
        case email
        case businessName = "business_name"
        case taxNo = "tax_no"
        case id
        case size
        case activityType = "activity_type"
        case address
        case addressMap = "address_map"
        case owner
        case expireDate = "expire_date"
        case businessRecordNo = "business_record_no"
        case mobile
        case name
        case createdAt = "created_at"
    }
    
    var status: Int?
    var phone: String?
    var activity: Activity?
    var logo: String?
    var inventoryNo: Int?
    var email: String?
    var businessName: String?
    var taxNo: Int?
    var id: Int?
    var size: Int?
    var activityType: ActivityType?
    var address: String?
    var owner: String?
    var expireDate: String?
    var businessRecordNo: String?
    var mobile: String?
    var addressMap: AddressMapStore?
    var name: String?
    var createdAt: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        addressMap = try container.decodeIfPresent(AddressMapStore.self, forKey: .addressMap)
        if let value = try? container.decode(String.self, forKey:.status) {
            status = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.status) {
            status = value
        }
        if let value = try? container.decode(Int.self, forKey:.phone) {
            phone = String(value)
        }else if let value = try? container.decode(String.self, forKey:.phone) {
            phone = value
        }
        activity = try container.decodeIfPresent(Activity.self, forKey: .activity)
        if let value = try? container.decode(Int.self, forKey:.logo) {
            logo = String(value)
        }else if let value = try? container.decode(String.self, forKey:.logo) {
            logo = value
        }
        if let value = try? container.decode(String.self, forKey:.inventoryNo) {
            inventoryNo = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.inventoryNo) {
            inventoryNo = value
        }
        if let value = try? container.decode(Int.self, forKey:.email) {
            email = String(value)
        }else if let value = try? container.decode(String.self, forKey:.email) {
            email = value
        }
        if let value = try? container.decode(Int.self, forKey:.businessName) {
            businessName = String(value)
        }else if let value = try? container.decode(String.self, forKey:.businessName) {
            businessName = value
        }
        if let value = try? container.decode(String.self, forKey:.taxNo) {
            taxNo = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.taxNo) {
            taxNo = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(String.self, forKey:.size) {
            size = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.size) {
            size = value
        }
        activityType = try container.decodeIfPresent(ActivityType.self, forKey: .activityType)
        if let value = try? container.decode(Int.self, forKey:.address) {
            address = String(value)
        }else if let value = try? container.decode(String.self, forKey:.address) {
            address = value
        }
        if let value = try? container.decode(Int.self, forKey:.owner) {
            owner = String(value)
        }else if let value = try? container.decode(String.self, forKey:.owner) {
            owner = value
        }
        if let value = try? container.decode(Int.self, forKey:.expireDate) {
            expireDate = String(value)
        }else if let value = try? container.decode(String.self, forKey:.expireDate) {
            expireDate = value
        }
        if let value = try? container.decode(Int.self, forKey:.businessRecordNo) {
            businessRecordNo = String(value)
        }else if let value = try? container.decode(String.self, forKey:.businessRecordNo) {
            businessRecordNo = value
        }
        if let value = try? container.decode(Int.self, forKey:.mobile) {
            mobile = String(value)
        }else if let value = try? container.decode(String.self, forKey:.mobile) {
            mobile = value
        }
        if let value = try? container.decode(Int.self, forKey:.name) {
            name = String(value)
        }else if let value = try? container.decode(String.self, forKey:.name) {
            name = value
        }
        if let value = try? container.decode(Int.self, forKey:.createdAt) {
            createdAt = String(value)
        }else if let value = try? container.decode(String.self, forKey:.createdAt) {
            createdAt = value
        }
    }
    
}
