//
//  BaseClass.swift
//
//  Created by  Ahmed’s MacBook Pro on 8/28/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct StoreSettingObject: Codable {

  enum CodingKeys: String, CodingKey {
    case status
    case data
    case message
    case locale
    case code
  }

  var status: Int?
  var data: StoreSettingData?
  var message: String?
  var locale: String?
  var code: Int?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(String.self, forKey:.status) {
 status = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.status) {
status = value 
}
    data = try container.decodeIfPresent(StoreSettingData.self, forKey: .data)
    if let value = try? container.decode(Int.self, forKey:.message) {                       
message = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.message) {
 message = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.locale) {                       
locale = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.locale) {
 locale = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.code) {
 code = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.code) {
code = value 
}
  }

}
