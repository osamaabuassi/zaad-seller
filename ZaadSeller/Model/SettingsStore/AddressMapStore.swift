//
//  AddressMap.swift
//
//  Created by  Ahmed’s MacBook Pro on 9/13/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct AddressMapStore: Codable {
    
    enum CodingKeys: String, CodingKey {
        case lng
        case lat
    }
    
    var lng: Double?
    var lat: Double?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.lng) {
            lng = Double(value)
        }else if let value = try? container.decode(Double.self, forKey:.lng) {
            lng = value
        }else{
            if let value = try? container.decode(String.self, forKey:.lng) {
                lng = Double(value)
            }
        }
        if let value = try? container.decode(Int.self, forKey:.lat) {
            lat = Double(value)
        }else if let value = try? container.decode(Double.self, forKey:.lat) {
            lat = value
        }else{
            if let value = try? container.decode(String.self, forKey:.lat) {
               lat = Double(value)
            }
        }
    }
    
}
