//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/11/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct InfoDeatiles: Codable {
    
    enum CodingKeys: String, CodingKey {
        case id
        case title
        case html
    }
    
    var id: Int?
    var title: String?
    var html: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
        if let value = try? container.decode(Int.self, forKey:.html) {
            html = String(value)
        }else if let value = try? container.decode(String.self, forKey:.html) {
            html = value
        }
    }
    
}
