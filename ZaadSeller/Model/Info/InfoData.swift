//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 7/11/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct InfoData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case resources
        case firstPageUrl = "first_page_url"
        case lastPageUrl = "last_page_url"
        case total
        case currentPage = "current_page"
        case currentPageUrl = "current_page_url"
        case perPage = "per_page"
        case lastPage = "last_page"
    }
    
    var resources: [Info]?
    var firstPageUrl: String?
    var lastPageUrl: String?
    var total: Int?
    var currentPage: Int?
    var currentPageUrl: String?
    var perPage: Int?
    var lastPage: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        resources = try container.decodeIfPresent([Info].self, forKey: .resources)
        if let value = try? container.decode(Int.self, forKey:.firstPageUrl) {
            firstPageUrl = String(value)
        }else if let value = try? container.decode(String.self, forKey:.firstPageUrl) {
            firstPageUrl = value
        }
        if let value = try? container.decode(Int.self, forKey:.lastPageUrl) {
            lastPageUrl = String(value)
        }else if let value = try? container.decode(String.self, forKey:.lastPageUrl) {
            lastPageUrl = value
        }
        if let value = try? container.decode(String.self, forKey:.total) {
            total = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.total) {
            total = value
        }
        if let value = try? container.decode(String.self, forKey:.currentPage) {
            currentPage = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.currentPage) {
            currentPage = value
        }
        if let value = try? container.decode(Int.self, forKey:.currentPageUrl) {
            currentPageUrl = String(value)
        }else if let value = try? container.decode(String.self, forKey:.currentPageUrl) {
            currentPageUrl = value
        }
        if let value = try? container.decode(String.self, forKey:.perPage) {
            perPage = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.perPage) {
            perPage = value
        }
        if let value = try? container.decode(String.self, forKey:.lastPage) {
            lastPage = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.lastPage) {
            lastPage = value
        }
    }
    
}
