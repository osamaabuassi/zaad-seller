//
//  BaseClass.swift
//
//  Created by  Ahmed’s MacBook Pro on 10/27/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ShippingObject: Codable {

  enum CodingKeys: String, CodingKey {
    case data
    case message
    case code
    case status
    case locale
  }

  var data: ShippingData?
  var message: String?
  var code: Int?
  var status: Int?
  var locale: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    data = try container.decodeIfPresent(ShippingData.self, forKey: .data)
    if let value = try? container.decode(Int.self, forKey:.message) {                       
message = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.message) {
 message = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.code) {
 code = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.code) {
code = value 
}
    if let value = try? container.decode(String.self, forKey:.status) {
 status = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.status) {
status = value 
}
    if let value = try? container.decode(Int.self, forKey:.locale) {                       
locale = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.locale) {
 locale = value                                                                                     
}
  }

}
