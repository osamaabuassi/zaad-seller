//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 10/27/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ShippingData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case carriers
        case sellerShippingService
        case selectedShippingServiceCount
        
    }
    
    var carriers: [Carriers]?
    var sellerShippingService: SellerShippingService?
    var selectedShippingServiceCount: Int?
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        carriers = try container.decodeIfPresent([Carriers].self, forKey: .carriers)
        sellerShippingService = try container.decodeIfPresent(SellerShippingService.self, forKey: .sellerShippingService)
        if let value = try? container.decode(String.self, forKey:.selectedShippingServiceCount) {
            selectedShippingServiceCount = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.selectedShippingServiceCount) {
            selectedShippingServiceCount = value
        }
    }
    
}
