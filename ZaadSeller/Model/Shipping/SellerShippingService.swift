//
//  SellerShippingService.swift
//
//  Created by  Ahmed’s MacBook Pro on 10/27/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct SellerShippingService: Codable {

  enum CodingKeys: String, CodingKey {
    case active
    case fullName = "full_name"
    case sellerId = "seller_id"
  }

  var active: Bool?
  var fullName: String?
  var sellerId: Int?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    active = try container.decodeIfPresent(Bool.self, forKey: .active)
    if let value = try? container.decode(Int.self, forKey:.fullName) {                       
fullName = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.fullName) {
 fullName = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.sellerId) {
 sellerId = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.sellerId) {
sellerId = value 
}
  }

}
