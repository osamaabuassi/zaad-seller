//
//  Features.swift
//
//  Created by  Ahmed’s MacBook Pro on 10/29/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct FeaturesShipping: Codable {
    
    enum CodingKeys: String, CodingKey {
        case value
        case price
        case id
        case valueThreshold = "value_threshold"
    }
    
    var value: Int?
    var price: Int?
    var id: Int?
    var valueThreshold: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let values = try? container.decode(String.self, forKey:.value) {
            value = Int(values)
        } else if let values = try? container.decode(Int.self, forKey:.value) {
            value = values
        }
        if let value = try? container.decode(String.self, forKey:.price) {
            price = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.price) {
            price = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(String.self, forKey:.valueThreshold) {
            valueThreshold = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.valueThreshold) {
            valueThreshold = value
        }
    }
    
}
