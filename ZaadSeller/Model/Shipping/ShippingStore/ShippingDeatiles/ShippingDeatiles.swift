//
//  Data.swift
//
//  Created by  Ahmed’s MacBook Pro on 10/29/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ShippingDeatiles: Codable {
    
    enum CodingKeys: String, CodingKey {
        case prices
        case features
        case id
        case name
        case fullName = "full_name"
    }
    
    var prices: [PricesShipping]?
    var features: [FeaturesShipping]?
    var id: Int?
    var name: String?
    var fullName: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        prices = try container.decodeIfPresent([PricesShipping].self, forKey: .prices)
        features = try container.decodeIfPresent([FeaturesShipping].self, forKey: .features)
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)                                                                                     
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value 
        }
        if let value = try? container.decode(Int.self, forKey:.name) {                       
            name = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.name) {
            name = value                                                                                     
        }
        if let value = try? container.decode(Int.self, forKey:.fullName) {                       
            fullName = String(value)                                                                                     
        }else if let value = try? container.decode(String.self, forKey:.fullName) {
            fullName = value                                                                                     
        }
    }
    
}
