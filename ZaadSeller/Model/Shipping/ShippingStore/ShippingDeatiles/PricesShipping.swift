//
//  Prices.swift
//
//  Created by  Ahmed’s MacBook Pro on 10/29/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct PricesShipping: Codable {
    
    enum CodingKeys: String, CodingKey {
        case toCityId = "to_city_id"
        case price
        case maxQuantity = "max_quantity"
        case minQuantity = "min_quantity"
        case id
        case fromCityId = "from_city_id"
        case from
        case to

    }
    
    var toCityId: String?
    var price: Int?
    var maxQuantity: Int?
    var minQuantity: Int?
    var id: Int?
    var fromCityId: String?
    var from: String?
    var to: String?

    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.toCityId) {
            toCityId = String(value)
        }else if let value = try? container.decode(String.self, forKey:.toCityId) {
            toCityId = value
        }
        if let value = try? container.decode(String.self, forKey:.price) {
            price = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.price) {
            price = value
        }
        if let value = try? container.decode(String.self, forKey:.maxQuantity) {
            maxQuantity = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.maxQuantity) {
            maxQuantity = value
        }
        if let value = try? container.decode(String.self, forKey:.minQuantity) {
            minQuantity = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.minQuantity) {
            minQuantity = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.fromCityId) {
            fromCityId = String(value)
        }else if let value = try? container.decode(String.self, forKey:.fromCityId) {
            fromCityId = value
        }
        if let value = try? container.decode(Int.self, forKey:.from) {
            from = String(value)
        }else if let value = try? container.decode(String.self, forKey:.from) {
            from = value
        }
        if let value = try? container.decode(Int.self, forKey:.to) {
            to = String(value)
        }else if let value = try? container.decode(String.self, forKey:.to) {
            to = value
        }
    }
    
}
