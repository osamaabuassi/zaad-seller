//
//  Carriers.swift
//
//  Created by  Ahmed’s MacBook Pro on 10/27/20
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Carriers: Codable {

  enum CodingKeys: String, CodingKey {
    case sellerId = "seller_id"
    case id
    case fullName = "full_name"
    case active
  }

  var sellerId: Int?
  var id: Int?
  var fullName: String?
  var active: Bool?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(String.self, forKey:.sellerId) {
 sellerId = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.sellerId) {
sellerId = value 
}
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
    if let value = try? container.decode(Int.self, forKey:.fullName) {                       
fullName = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.fullName) {
 fullName = value                                                                                     
}
    active = try container.decodeIfPresent(Bool.self, forKey: .active)
  }

}
