//
//  Data.swift
//
//  Created by osamaaassi on 1/24/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct DetailsS: Codable {
    
    enum CodingKeys: String, CodingKey {
        case categoryId = "category_id"
        case id
        case priceType = "price_type"
        case descriptionValue = "description"
        case features
        case shortDescription = "short_description"
        case image
        case prices
        case shortTitle = "short_title"
        case priceHint = "price_hint"
        case title
        case privat = "private"
    }
    
    var categoryId: Int?
    var id: Int?
    var title: String?
    var shortDescription: String?
    var descriptionValue: String?
    var priceType: String?
    var shortTitle: String?
    var priceHint: String?
    var image: String?
    
    var features: [Features]?
    var prices: [ServicePrices]?
    
    
    
    var privat: Int?
    
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.categoryId) {
            categoryId = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.categoryId) {
            categoryId = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.priceType) {
            priceType = String(value)
        }else if let value = try? container.decode(String.self, forKey:.priceType) {
            priceType = value
        }
        if let value = try? container.decode(Int.self, forKey:.descriptionValue) {
            descriptionValue = String(value)
        }else if let value = try? container.decode(String.self, forKey:.descriptionValue) {
            descriptionValue = value
        }
        features = try container.decodeIfPresent([Features].self, forKey: .features)
        if let value = try? container.decode(Int.self, forKey:.shortDescription) {
            shortDescription = String(value)
        }else if let value = try? container.decode(String.self, forKey:.shortDescription) {
            shortDescription = value
        }
        if let value = try? container.decode(Int.self, forKey:.image) {
            image = String(value)
        }else if let value = try? container.decode(String.self, forKey:.image) {
            image = value
        }
        prices = try container.decodeIfPresent([ServicePrices].self, forKey: .prices)
        if let value = try? container.decode(Int.self, forKey:.shortTitle) {
            shortTitle = String(value)
        }else if let value = try? container.decode(String.self, forKey:.shortTitle) {
            shortTitle = value
        }
        if let value = try? container.decode(Int.self, forKey:.priceHint) {
            priceHint = String(value)
        }else if let value = try? container.decode(String.self, forKey:.priceHint) {
            priceHint = value
        }
        if let value = try? container.decode(Int.self, forKey:.title) {
            title = String(value)
        }else if let value = try? container.decode(String.self, forKey:.title) {
            title = value
        }
        if let value = try? container.decode(String.self, forKey:.privat) {
            privat = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.privat) {
            privat = value
        }
    }
    
}
