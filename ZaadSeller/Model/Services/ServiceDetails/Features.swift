//
//  Features.swift
//
//  Created by osamaaassi on 1/24/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Features: Codable {
    
    enum CodingKeys: String, CodingKey {
        case descriptionValue = "description"
        case optional
        case free
        case regularPrice = "regular_price"
        case unit
        case id
        case valu = "value"
        case salePrice = "sale_price"
        case valueThreshold = "value_threshold"
    }
    
    var descriptionValue: String?
    var optional: Int?
    var free: Int?
    var regularPrice: Int?
    var unit: String?
    var id: Int?
    var valu: Int?
    var salePrice: Int?
    var valueThreshold: Int?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.descriptionValue) {
            descriptionValue = String(value)
        }else if let value = try? container.decode(String.self, forKey:.descriptionValue) {
            descriptionValue = value
        }
        if let value = try? container.decode(String.self, forKey:.optional) {
            optional = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.optional) {
            optional = value
        }
        if let value = try? container.decode(String.self, forKey:.free) {
            free = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.free) {
            free = value
        }
        if let value = try? container.decode(String.self, forKey:.regularPrice) {
            regularPrice = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.regularPrice) {
            regularPrice = value
        }
        if let value = try? container.decode(Int.self, forKey:.unit) {
            unit = String(value)
        }else if let value = try? container.decode(String.self, forKey:.unit) {
            unit = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(String.self, forKey:.valu) {
            valu = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.valu) {
            valu = value
        }
        if let value = try? container.decode(String.self, forKey:.salePrice) {
            salePrice = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.salePrice) {
            salePrice = value
        }
        if let value = try? container.decode(String.self, forKey:.valueThreshold) {
            valueThreshold = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.valueThreshold) {
            valueThreshold = value
        }
    }
    
}
