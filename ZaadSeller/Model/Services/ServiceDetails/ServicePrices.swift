//
//  Prices.swift
//
//  Created by osamaaassi on 1/24/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ServicePrices: Codable {
    
    enum CodingKeys: String, CodingKey {
        case meta
        case id
        case regularPrice = "regular_price"
        case descriptionValue = "description"
        case salePrice = "sale_price"
        case maxQuantity = "max_quantity"
        case minQuantity = "min_quantity"
        case unit
    }
    
    var meta: String?
    var id: Int?
    var regularPrice: Int?
    var descriptionValue: String?
    var salePrice: Int?
    var maxQuantity: Int?
    var minQuantity: Int?
    var unit: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey:.meta) {
            meta = String(value)
        }else if let value = try? container.decode(String.self, forKey:.meta) {
            meta = value
        }
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(String.self, forKey:.regularPrice) {
            regularPrice = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.regularPrice) {
            regularPrice = value
        }
        if let value = try? container.decode(Int.self, forKey:.descriptionValue) {
            descriptionValue = String(value)
        }else if let value = try? container.decode(String.self, forKey:.descriptionValue) {
            descriptionValue = value
        }
        if let value = try? container.decode(String.self, forKey:.salePrice) {
            salePrice = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.salePrice) {
            salePrice = value
        }
        if let value = try? container.decode(String.self, forKey:.maxQuantity) {
            maxQuantity = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.maxQuantity) {
            maxQuantity = value
        }
        if let value = try? container.decode(String.self, forKey:.minQuantity) {
            minQuantity = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.minQuantity) {
            minQuantity = value
        }
        if let value = try? container.decode(Int.self, forKey:.unit) {
            unit = String(value)
        }else if let value = try? container.decode(String.self, forKey:.unit) {
            unit = value
        }
    }
    
}
