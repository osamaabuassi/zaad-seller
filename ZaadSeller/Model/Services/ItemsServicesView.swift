//
//  Items.swift
//
//  Created by osamaaassi on 1/23/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ItemsServicesView: Codable {

  enum CodingKeys: String, CodingKey {
    case id
    case image
    case published
    case title
    case status
    case statusCode = "status_code"
    case publishedCode = "published_code"
    case categoryTitle = "category_title"
    case categoryId = "category_id"
    
  }

  var statusCode: Int?
  var publishedCode: Int?
  var image: String?
  var published: String?
  var id: Int?
  var categoryTitle: String?
  var status: String?
  var categoryId: Int?
  var title: String?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(String.self, forKey:.statusCode) {
 statusCode = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.statusCode) {
statusCode = value 
}
    if let value = try? container.decode(String.self, forKey:.publishedCode) {
 publishedCode = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.publishedCode) {
publishedCode = value 
}
    if let value = try? container.decode(Int.self, forKey:.image) {                       
image = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.image) {
 image = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.published) {                       
published = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.published) {
 published = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.id) {
 id = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.id) {
id = value 
}
    if let value = try? container.decode(Int.self, forKey:.categoryTitle) {                       
categoryTitle = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.categoryTitle) {
 categoryTitle = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.status) {                       
status = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.status) {
 status = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.categoryId) {
 categoryId = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.categoryId) {
categoryId = value 
}
    if let value = try? container.decode(Int.self, forKey:.title) {                       
title = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.title) {
 title = value                                                                                     
}
  }

}
