//
//  Data.swift
//
//  Created by osamaaassi on 1/23/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct DataServicesView: Codable {

  enum CodingKeys: String, CodingKey {
    case currentPageUrl = "current_page_url"
    case lastPageUrl = "last_page_url"
    case total
    case items
    case perPage = "per_page"
    case nextPageUrl = "next_page_url"
    case firstPageUrl = "first_page_url"
    case currentPage = "current_page"
    case prevPageUrl = "prev_page_url"
    case lastPage = "last_page"
  }

  var currentPageUrl: String?
  var lastPageUrl: String?
  var total: Int?
  var items: [ItemsServicesView]?
  var perPage: Int?
  var nextPageUrl: String?
  var firstPageUrl: String?
  var currentPage: Int?
  var prevPageUrl: String?
  var lastPage: Int?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    if let value = try? container.decode(Int.self, forKey:.currentPageUrl) {                       
currentPageUrl = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.currentPageUrl) {
 currentPageUrl = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.lastPageUrl) {                       
lastPageUrl = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.lastPageUrl) {
 lastPageUrl = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.total) {
 total = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.total) {
total = value 
}
    items = try container.decodeIfPresent([ItemsServicesView].self, forKey: .items)
    if let value = try? container.decode(String.self, forKey:.perPage) {
 perPage = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.perPage) {
perPage = value 
}
    if let value = try? container.decode(Int.self, forKey:.nextPageUrl) {                       
nextPageUrl = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.nextPageUrl) {
 nextPageUrl = value                                                                                     
}
    if let value = try? container.decode(Int.self, forKey:.firstPageUrl) {                       
firstPageUrl = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.firstPageUrl) {
 firstPageUrl = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.currentPage) {
 currentPage = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.currentPage) {
currentPage = value 
}
    if let value = try? container.decode(Int.self, forKey:.prevPageUrl) {                       
prevPageUrl = String(value)                                                                                     
}else if let value = try? container.decode(String.self, forKey:.prevPageUrl) {
 prevPageUrl = value                                                                                     
}
    if let value = try? container.decode(String.self, forKey:.lastPage) {
 lastPage = Int(value)                                                                                     
} else if let value = try? container.decode(Int.self, forKey:.lastPage) {
lastPage = value 
}
  }

}
