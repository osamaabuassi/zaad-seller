//
//  Data.swift
//
//  Created by osamaaassi on 1/26/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct PricingData: Codable {
    
    enum CodingKeys: String, CodingKey {
        case items
    }
    
    var items: [PricingItems]?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        items = try container.decodeIfPresent([PricingItems].self, forKey: .items)
    }
    
}
