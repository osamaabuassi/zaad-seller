//
//  Items.swift
//
//  Created by osamaaassi on 1/26/21
//  Copyright (c) . All rights reserved.
//

import Foundation

struct PricingItems: Codable {
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
    }
    
    var id: Int?
    var name: String?
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey:.id) {
            id = Int(value)
        } else if let value = try? container.decode(Int.self, forKey:.id) {
            id = value
        }
        if let value = try? container.decode(Int.self, forKey:.name) {
            name = String(value)
        }else if let value = try? container.decode(String.self, forKey:.name) {
            name = value
        }
    }
    
}
