//
//  Helper.swift
//  HanDIYman
//
//  Created by Ahmad on 1/21/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import UIKit
import SystemConfiguration
import AVFoundation
import Alamofire
import ZVProgressHUD


class Helper: NSObject {
    
    class func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    
    //MARK: Check Internet Connection
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
    
    static func getName() -> String {
        let dateFormatter = DateFormatter()
        let dateFormat = "yyyyMMddHHmmss"
        dateFormatter.dateFormat = dateFormat
        let date = dateFormatter.string(from: Date())
        let name = date.appending(".mp4")
        return name
    }
    
 
    
    class func getThumbnailImage(forUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        
        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 1, timescale: 60) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print(error)
        }
        
        return nil
    }
    
    
    //MARK: Create group Chat Id
    static func createChatGroupId(userId:String,userId2:String) -> String {
        var chatRoomId = ""
        let comparison = userId.compare(userId2).rawValue
        
        if comparison < 0 {
            chatRoomId =  userId + "_" + userId2
        } else {
            chatRoomId = userId2 + "_" + userId
        }
        return chatRoomId
    }
    
//    class func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
//        URLSession.shared.dataTask(with: url) {
//            (data, response, error) in
//            completion(data, response, error)
//            }.resume()
//    }
    
    static func getRandomColor() -> UIColor{
        
        let randomRed:CGFloat = CGFloat(drand48())
        
        let randomGreen:CGFloat = CGFloat(drand48())
        
        let randomBlue:CGFloat = CGFloat(drand48())
        
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
        
    }
    
    
    
//    class func showIndicator(view: UIView)->Void{
//        if !ARSLineProgress.shown{
//            ARSLineProgress.show()
//            view.isUserInteractionEnabled = false
//        }
//    }
//
//    class func hideIndicator(view: UIView)->Void{
//        ARSLineProgress.hide()
//        view.isUserInteractionEnabled = true
//    }
//
//    class func hideIndicatorWithSuccess(view: UIView)->Void{
//        ARSLineProgress.showSuccess()
//        view.isUserInteractionEnabled = true
//    }
//
//    class func hideIndicatorWithFail(view: UIView)->Void{
//        ARSLineProgress.showFail()
//        view.isUserInteractionEnabled = true
//    }
    
    //    static func go_home(_ window: UIWindow){
    //        let vc = AppDelegate.sb_main.instantiateViewController(withIdentifier: "MainVC")
    //        let nav = UINavigationController.init(rootViewController: vc)
    //        window.rootViewController = nav
    //    }
    //
    //    static func set_Root(_ window: UIWindow, iden: String){
    //        let vc = AppDelegate.sb_main.instantiateViewController(withIdentifier: iden)
    //        let nav = UINavigationController.init(rootViewController: vc)
    //        window.rootViewController = nav
    //    }
    //
    //    static func saveUserData(usr: UserStruct){
    //
    //        Helper.save_user_id             = usr.id
    //        Helper.save_user_name           = usr.name
    //        Helper.save_user_email          = usr.email
    //        Helper.save_user_image          = usr.profile_image
    //        Helper.save_user_mobile         = usr.mobile
    //        Helper.user_token               = usr.access_token
    //        Helper.save_user_city_name      = usr.location
    //        Helper.is_driver                = usr.admin
    //        Helper.save_longitude           = usr.lan
    //        Helper.save_latitude            = usr.lat
    //    }
    //
    //
    
  
    
    static func isValidJson(text: String) -> Bool{
        if let data = text.data(using: .utf8) {
            do {
                let data =  try JSONSerialization.jsonObject(with: data, options: [])
                //print("JSONSerializationJSONSerializationJSONSerialization")
                print(data)
                return true
            } catch {
                print(error.localizedDescription)
            }
        }
        return false
    }
    
    static func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth,height: newHeight))
        image.draw(in: CGRect(x: 0,y: 0,width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    
    static func getLocationImageURL(lat: Double, long: Double) -> String{
        let fromLocation = "\(lat), \(long)"
        let url = "https://maps.googleapis.com/maps/api/staticmap?center=\(fromLocation)&zoom=18&size=800x410&markers=color:red|\(fromLocation)"
        return url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
    }
    
    
    static func openURL(urlStr: String){
        if let url = URL(string : urlStr){
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options:  [:], completionHandler: { (status) in
                    if status {
                        print("fooo/ success")
                    }
                })
            } else {
                
                // Fallback on earlier versions
            }
        }
    }
    
}





// Helper function inserted by Swift 4.2 migrator.
//fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
//    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
//}
