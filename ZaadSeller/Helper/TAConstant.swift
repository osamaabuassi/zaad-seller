//
//  TAConstant.swift
//


import Foundation
import UIKit


struct TAConstant
{
    static let APIBaseURL = "https://api.zaad.om/merchant-app-api/"
    
    //   live     "https://api.zaad.om/merchant-app-api/"
    //   test    "https://api.zaadoman.com/merchant-app-api/"

    //
    //    static let login                                       = "login"
    //    static let signUp                                      = "signUp"
    //    static let change_password                             =     "changePassword"
    //
    //    static let editProfile                                 = "editProfile"
    //    static let settings                                    = "settings"
    
    //static let refreshSideMenu = NSNotification.Name(rawValue: "refreshSideMenu")
    
    
}

public class IOSVersion {
    func iOS_VERSION_EQUAL_TO(version: String) -> Bool {
        return UIDevice.current.systemVersion.compare(version, options: NSString.CompareOptions.numeric) == ComparisonResult.orderedSame
    }
    
    func iOS_VERSION_GREATER_THAN(version: String) -> Bool {
        return UIDevice.current.systemVersion.compare(version, options: NSString.CompareOptions.numeric) == ComparisonResult.orderedDescending
    }
    
    func iOS_VERSION_GREATER_THAN_OR_EQUAL_TO(version: String) -> Bool {
        return UIDevice.current.systemVersion.compare(version, options: NSString.CompareOptions.numeric) != ComparisonResult.orderedAscending
    }
    
    class func iOS_VERSION_LESS_THAN(version: String) -> Bool {
        return UIDevice.current.systemVersion.compare(version, options: NSString.CompareOptions.numeric) == ComparisonResult.orderedAscending
    }
    
    func iOS_VERSION_LESS_THAN_OR_EQUAL_TO(version: String) -> Bool {
        return UIDevice.current.systemVersion.compare(version, options: NSString.CompareOptions.numeric) != ComparisonResult.orderedDescending
    }
}
