//
//  RadioButton.swift
//  BO
//
//  Created by  Ahmed’s MacBook Pro on 5/14/19.
//  Copyright © 2019  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class RadioButton: UIButton {
    var alternateButton:Array<RadioButton>?
    
    override func awakeFromNib() {
//        self.layer.cornerRadius = 14
//        self.layer.borderWidth = 1
        self.layer.masksToBounds = true
    }
    
    func unselectAlternateButtons(){
        if alternateButton != nil {
            self.isSelected = true
            
            for aButton:RadioButton in alternateButton! {
                aButton.isSelected = false
            }
        }else{
            toggleButton()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        unselectAlternateButtons()
        super.touchesBegan(touches, with: event)
    }
    
    func toggleButton(){
        self.isSelected = !isSelected
    }
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                self.setImage(UIImage(named: "Check"), for: .normal)
                self.setTitleColor("B1AF00".color, for: .normal)
            } else {
                self.setImage(UIImage(named: "unCheck"), for: .normal)
                self.setTitleColor("003B87".color, for: .normal)
            }
        }
    }
}


