//
//  Struct.swift
//  BASIT
//
//  Created by ahmed on 2/4/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit
import AVFoundation
struct CurrentUser  {

    static var userInfo : UserData?  {
        set {
            guard newValue != nil else {
                UserDefaults.standard.removeObject(forKey: "CurrentUser");
                return;
            }
            let encodedData = try? PropertyListEncoder().encode(newValue)
            UserDefaults.standard.set(encodedData, forKey:"CurrentUser")
            UserDefaults.standard.synchronize();
        }
        get {
            if let data = UserDefaults.standard.value(forKey:"CurrentUser") as? Data {
                return try? PropertyListDecoder().decode(UserData.self, from:data)

            }
            return nil
        }

    }
}

//
//struct CurrentAddress  {
//
//    static var addressInfo : Addresses?  {
//        set {
//            guard newValue != nil else {
//                UserDefaults.standard.removeObject(forKey: "Address");
//                return;
//            }
//            let encodedData = try? PropertyListEncoder().encode(newValue)
//            UserDefaults.standard.set(encodedData, forKey:"Address")
//            UserDefaults.standard.synchronize();
//        }
//        get {
//            if let data = UserDefaults.standard.value(forKey:"Address") as? Data {
//                return try? PropertyListDecoder().decode(Addresses.self, from:data)
////return nil
//            }
//            return nil
//        }
//
//    }


struct featuresShipping  {
    
    var id:String?
    var value:String?
    var valuethreshold:String?
    var regularprice:String?

    
}


struct pricessShipping  {
    
    var id:String?
    var fromcityid:String?
    var tocityid:String?
    var regularprice:String?
    var minquantity:String?
    var maxquantity:String?
    var fromcitynames:String?
    var tocitynames:String?

    
}



struct SocialNetworkUrl {
    let scheme: String
    let page: String

    func openPage() {
        let schemeUrl = NSURL(string: scheme)!
        if UIApplication.shared.canOpenURL(schemeUrl as URL) {
            UIApplication.shared.open(schemeUrl as URL)
        } else {
            UIApplication.shared.open(NSURL(string: page)! as URL)
        }
    }
}




