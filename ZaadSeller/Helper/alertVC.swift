//
//  alertVC.swift
//  dukan
//
//  Created by  Ahmed’s MacBook Pro on 7/28/19.
//  Copyright © 2019  Ahmed’s MacBook Pro. All rights reserved.
//
import UIKit

protocol alertDelgate : class {
    func selectedDone(data: Any , sender : UIViewController)
    func selectedOther(data: Any , sender : UIViewController)
    
}
class alertVC: SuperViewController {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var btnOther: UIButton!
    
    var delegate:alertDelgate?
    var txtTitle = ""
    var txtDescription = ""
    var txtBtnDone = ""
    var txtBtnOther = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = txtTitle
        lblDesc.text = txtDescription
        btnDone.setTitle(txtBtnDone, for: UIControl.State.normal)
        btnOther.setTitle(txtBtnOther, for: UIControl.State.normal)
        
        // Do any additional setup after loading the view.
    }
    @IBAction func doneAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
//        self.delegate?.selectedDone(data: "", sender: self)
    }
    @IBAction func otherAction(_ sender: Any) {
        self.delegate?.selectedOther(data: "" ,sender: self)
        
    }
    
}
