//
//  AppDelegate.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/3/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import FirebaseMessaging
import UserNotifications
import GoogleMaps
import GooglePlaces
import SwiftyStoreKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate,MessagingDelegate,MOLHResetable{
    
   
    
    var window: UIWindow?
    static let sb_main = UIStoryboard.init(name: "Main", bundle: nil)
    
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        FirebaseApp.configure()
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.toolbarTintColor = UIColor.white
        IQKeyboardManager.shared.toolbarBarTintColor = "FF6A2A".color
        IQKeyboardManager.shared.shouldToolbarUsesTextFieldTintColor = false
        IQKeyboardManager.shared.placeholderColor = "FFFFFF".color
        IQKeyboardManager.shared.placeholderFont = UIFont.init(name: "FFShamelFamilySemiRoundBook-S", size: 12)
        GMSServices.provideAPIKey("AIzaSyAgLfq54h5DuzBwBYYg0V5QkVXq4m36yYQ")
        GMSPlacesClient.provideAPIKey("AIzaSyAgLfq54h5DuzBwBYYg0V5QkVXq4m36yYQ")

        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            UIApplication.shared.applicationIconBadgeNumber = 0
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        
        // MOLH Language
        //MOLHLanguage.setDefaultLanguage("en")
       // MOLH.setLanguageTo("en")
        MOLH.shared.activate(true)
        
        connectToFcm()
        SwiftyStoreKit.completeTransactions(atomically: true) { purchases in
                for purchase in purchases {
                    switch purchase.transaction.transactionState {
                    case .purchased, .restored:
                        if purchase.needsFinishTransaction {
                            // Deliver content from server, then:
                            SwiftyStoreKit.finishTransaction(purchase.transaction)
                        }
                        // Unlock content
                    case .failed, .purchasing, .deferred:
                        break // do nothing
                    }
                }
            }
        return true
    }
    
    func reset() {
        let rootViewController: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        let story = UIStoryboard(name: "Main", bundle: nil)
        rootViewController.rootViewController = story.instantiateViewController(withIdentifier: "StartVC")
    }
    
    
    func connectToFcm() {
        
        Messaging.messaging().shouldEstablishDirectChannel = true
        Messaging.messaging().delegate = self
        
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                print("InstanceID token12: \(String(describing: result.token))")
                UserDefaults.standard.set(result.token, forKey: "token") //setObject
            }
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("Hooray! I'm registered!")
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("APNs device token: \(deviceTokenString)")
        connectToFcm()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let notfication = response.notification.request.content.userInfo
        print(notfication)
        if let orderId = notfication["order_id"] as? String{
            let type = notfication["type"] as? String
            let status = Int.init(type ?? "0")
            if status == 1{
                let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                if let chatViewController = storyboard.instantiateViewController(withIdentifier: "MainOrderDeatiles") as? MainOrderDeatiles
//                    let tabBar = storyboard.instantiateViewController(withIdentifier: "TTabBarViewController") as? TTabBarViewController
                    {
                    let navigationController = CustomNavigationBar(rootViewController: chatViewController)
                    chatViewController.hidesBottomBarWhenPushed = true
                    navigationController.viewControllers = [chatViewController]
                    chatViewController.ID = Int.init(orderId)
                    //tabBar.viewControllers = [navigationController]
                    window?.rootViewController = navigationController
                    UserDefaults.standard.set(true, forKey: "isNotification")
                }
                print(orderId)
            }
        }
        if  let type = notfication["type"] as? String{
            let status = Int.init(type)
            if status == 3{
                 let payViewController:SubscriptionDeatilesVC = SubscriptionDeatilesVC.loadFromNib()
//                   if let tabBar = storyboard.instantiateViewController(withIdentifier: "TTabBarViewController") as? TTabBarViewController {
                    let navigationController = CustomNavigationBar(rootViewController: payViewController)
                    payViewController.hidesBottomBarWhenPushed = true
                    navigationController.viewControllers = [payViewController]
                    //tabBar.viewControllers = [navigationController]
                    window?.rootViewController = navigationController
                    UserDefaults.standard.set(true, forKey: "isNotification")
//                }
            }
        }
        
                if  let type = notfication["type"] as? String{
                    let disputeID = notfication["dispute_id"] as? String
                    let status = Int.init(type)
                    if status == 2{
                         let payViewController:DisputesDeatilesVC = DisputesDeatilesVC.loadFromNib()
        //                   if let tabBar = storyboard.instantiateViewController(withIdentifier: "TTabBarViewController") as? TTabBarViewController {
                            let navigationController = CustomNavigationBar(rootViewController: payViewController)
                            payViewController.hidesBottomBarWhenPushed = true
                            navigationController.viewControllers = [payViewController]
                            payViewController.idDisputes = Int.init(disputeID ?? "0")
                            //tabBar.viewControllers = [navigationController]
                            window?.rootViewController = navigationController
                            UserDefaults.standard.set(true, forKey: "isNotification")
        //                }
                    }
                }
        
        completionHandler()
        
    }
    
   
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let userInfo = notification.request.content.userInfo
        let state = UIApplication.shared.applicationState
        if state == .active {
            if let orderId = userInfo["order_id"] as? String{
                let type = userInfo["type"] as? String
                let status = Int.init(type ?? "0")
                if status == 1{
                    let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                    if let orderViewController = storyboard.instantiateViewController(withIdentifier: "MainOrderDeatiles") as? MainOrderDeatiles
                       
                    {
                        let navigationController = CustomNavigationBar(rootViewController: orderViewController)
                        orderViewController.hidesBottomBarWhenPushed = true
                        navigationController.viewControllers = [orderViewController]
                        orderViewController.ID = Int.init(orderId)
                        //tabBar.viewControllers = [navigationController]
                        window?.rootViewController = navigationController
                        UserDefaults.standard.set(true, forKey: "isNotification")
                    }
                    print(orderId)
                }
            }
            
            if  let type = userInfo["type"] as? String{
                let status = Int.init(type)
                if status == 3{
                     let payViewController:SubscriptionDeatilesVC = SubscriptionDeatilesVC.loadFromNib()
                        let navigationController = CustomNavigationBar(rootViewController: payViewController)
                        payViewController.hidesBottomBarWhenPushed = true
                        navigationController.viewControllers = [payViewController]
                        //tabBar.viewControllers = [navigationController]
                        window?.rootViewController = navigationController
                        UserDefaults.standard.set(true, forKey: "isNotification")
                }
            }
            
            if  let type = userInfo["type"] as? String{
                let disputeID = userInfo["dispute_id"] as? String
                 let status = Int.init(type)
                 if status == 2{
                      let payViewController:DisputesDeatilesVC = DisputesDeatilesVC.loadFromNib()
                         let navigationController = CustomNavigationBar(rootViewController: payViewController)
                         payViewController.hidesBottomBarWhenPushed = true
                         navigationController.viewControllers = [payViewController]
                         payViewController.idDisputes = Int.init(disputeID ?? "0")
                         //tabBar.viewControllers = [navigationController]
                         window?.rootViewController = navigationController
                         UserDefaults.standard.set(true, forKey: "isNotification")
                 }
             }
            
            print("Push notification received in foreground.")
            completionHandler([.alert, .sound, .badge])
        }
        
    }
    
    func configureNotification() {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            center.delegate = self
            let openAction = UNNotificationAction(identifier: "OpenNotification", title: NSLocalizedString("Abrir", comment: ""), options: UNNotificationActionOptions.foreground)
            let deafultCategory = UNNotificationCategory(identifier: "CustomSamplePush", actions: [openAction], intentIdentifiers: [], options: [])
            center.setNotificationCategories(Set([deafultCategory]))
            
        } else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
        }
        UIApplication.shared.registerForRemoteNotifications()
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("APNs registration failed: \(error)")
    }
    
    // MARK: UISceneSession Lifecycle
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    
}

