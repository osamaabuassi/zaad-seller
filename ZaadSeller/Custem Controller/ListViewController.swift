//
//  OpenOrders.swift
//from git
//  Created by ahmed on 6/25/19.
//  Copyright © 2019 ahmed. All rights reserved.
//

import UIKit
//import DZNEmptyDataSet
class ListViewController: SuperViewController {
    var SuperPageItem = SuperItem()
    @IBOutlet weak var tableView: UITableView!
    var listArray = [Any]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.SuperPageItem.tableView = self.tableView
        SuperPageItem.viewController = self
        self.SuperPageItem.withIdentifierCell = ["HomeCell","HomeFilterCell"]
        self.SuperPageItem.LoadItem()
        SuperPageItem.ItemSetupNavigation(forViewController: self, prepareNavigationBar: self.navigationController as! CustomNavigationBar, title: "Stores")

        // Do any additional setup after loading the view.
    }

   override func CheckTable() {
//    self.tableView.emptyDataSetSource = self
//    self.tableView.emptyDataSetDelegate = self
    tableView.tableFooterView = UIView()
    tableView.reloadData()
    }
    /*
    // MARK: - Navigation
     

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
// MARK: - UI TableView
extension ListViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
   return SuperPageItem.prepareCellForData(in: tableView, Data: listArray, indexPath: indexPath)
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        SuperPageItem.cellDidSelected(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SuperPageItem.prepareCellheight(indexPath: indexPath)
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return SuperPageItem.prepareHeader(in: tableView, Data: listArray, viewForHeaderInSection: section)
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return SuperPageItem.heightForHeaderInSection(section: section)
    }
}


//extension ListViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate{
//    func description(forEmptyDataSet scrollView: UIScrollView?) -> NSAttributedString? {
//        let text = SuperPageItem.emptyText
//        let paragraph = NSMutableParagraphStyle()
//        paragraph.lineBreakMode = .byWordWrapping
//        paragraph.alignment = .center
//
//        let attributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14.0), NSAttributedString.Key.foregroundColor: UIColor.lightGray, NSAttributedString.Key.paragraphStyle: paragraph]
//
//        return NSAttributedString(string: text, attributes: attributes)
//    }
//    func buttonTitle(forEmptyDataSet scrollView: UIScrollView?, for state: UIControl.State) -> NSAttributedString? {
//        let attributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14.0),NSAttributedString.Key.foregroundColor : "ffffff".color]
//
//        return NSAttributedString(string: SuperPageItem.emptyBtnText, attributes: attributes)
//    }
//
//    func image(forEmptyDataSet scrollView: UIScrollView?) -> UIImage? {
//        return UIImage(named: SuperPageItem.emptyImage)
//    }
//
//
//
//    func backgroundColor(forEmptyDataSet scrollView: UIScrollView?) -> UIColor? {
//        return UIColor.white
//    }
//
//
//    func emptyDataSetShouldAllowTouch(_ scrollView: UIScrollView?) -> Bool {
//        return true
//    }
//    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView?) -> Bool {
//        return false
//    }
//    func emptyDataSet(_ scrollView: UIScrollView?, didTap button: UIButton?) {
////        self.performSegue(withIdentifier: "hi", sender: self)
////        SVProgressHUD.showSuccess(withStatus: "GO")
//    }
//}
