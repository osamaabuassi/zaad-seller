//
//  TTabBarViewController.swift
//  Zadak
//
//  Created by  Ahmed’s MacBook Pro on 2/21/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class TTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appearance = UITabBarItem.appearance(whenContainedInInstancesOf: [TTabBarViewController.self])
        appearance.setTitleTextAttributes([NSAttributedString.Key.foregroundColor:"004A97".color,NSAttributedString.Key.font:UIFont(name: "FFShamelFamilySemiRoundBook-S", size: 12)!], for: .selected)
        appearance.setTitleTextAttributes([NSAttributedString.Key.foregroundColor:"004A97".color,NSAttributedString.Key.font:UIFont(name: "FFShamelFamilySemiRoundBook-S", size: 12)!], for: .normal)
            tabBar.shadowImage = UIImage()
       
       
        
       
            if CurrentUser.userInfo?.user?.store?.activityType?.id == 1 {
                //Prodect
                self.viewControllers?.remove(at: 3)
                self.viewControllers?.remove(at: 3)
            }
            else if CurrentUser.userInfo?.user?.store?.activityType?.id == 2 {
                //Service
               self.viewControllers?.remove(at: 1)
               self.viewControllers?.remove(at: 1)
               
                
            }
        
        

        if #available(iOS 13, *) {
            let appearance = UITabBarAppearance()
            appearance.stackedLayoutAppearance.selected.titleTextAttributes = [NSAttributedString.Key.foregroundColor: "004A97".color,NSAttributedString.Key.font : UIFont(name: "FFShamelFamilySemiRoundBook-S", size: 12)!]
            appearance.stackedLayoutAppearance.normal.titleTextAttributes = [NSAttributedString.Key.foregroundColor: "004A97".color,NSAttributedString.Key.font : UIFont(name: "FFShamelFamilySemiRoundBook-S", size: 12)!]
            appearance.shadowImage = nil
            appearance.shadowColor = nil
            tabBar.standardAppearance = appearance
        }

    }
    

   

}
