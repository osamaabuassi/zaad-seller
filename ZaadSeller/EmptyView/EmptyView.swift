//
//  EmptyView.swift
//  Apps
//
//  Created by Omar Al tawashi on 2/23/19.
//  Copyright © 2019 Omar Al tawashi. All rights reserved.
//

import UIKit

class EmptyView: UIView {
    
    @IBOutlet weak var firstLabel:UILabel!
    @IBOutlet weak var secondLael:UILabel!
    @IBOutlet weak var imgEmpty:UIImageView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    
    
}
extension UIViewController{
    
    private struct EmptyViewHolder {
        var shared: EmptyView? = {
            let emptyView =  EmptyView.instantiateFromNib()
            return emptyView
            
        }()
    }
    
    //var emptyView:EmptyView? {
    //    get {
    //        return EmptyViewHolder.init().shared
    //    }
    //}
    
    func showEmptyView(emptyView:EmptyView?,parentView:UIView,refershSelector:Selector)->EmptyView? {
        emptyView?.removeFromSuperview()
        
        if let emptyView = EmptyView.instantiateFromNib() {
            
            
            emptyView.addGestureRecognizer(UITapGestureRecognizer(target: self, action:refershSelector))
            
            if let tableView = parentView as? UITableView{
                emptyView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: tableView.frame.height / 1.5)
                tableView.tableFooterView = emptyView
                
            } else if let collectionView = parentView as? UICollectionView{
                collectionView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: collectionView.frame.height)
                collectionView.backgroundView = emptyView
                
            }else{
                view.addSubview(emptyView)
                NSLayoutConstraint.activate([
                    emptyView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                    emptyView.topAnchor.constraint(equalTo: view.topAnchor),
                    emptyView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                    emptyView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
                ])
                emptyView.translatesAutoresizingMaskIntoConstraints = false
                
            }
            
            
            
            return emptyView
            
        }
        return emptyView
    }
}


extension UIView {
    
    static var nib: UINib {
        return UINib(nibName: "\(self)", bundle: nil)
    }
    
    static func instantiateFromNib() -> Self? {
        func instanceFromNib<T: UIView>() -> T? {
            return nib.instantiate() as? T
        }
        
        return instanceFromNib()
    }
    
}

extension UINib {
    
    func instantiate() -> Any? {
        return self.instantiate(withOwner: nil, options: nil).first
    }
    
}
