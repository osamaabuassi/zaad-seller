//
//  NotificationCell.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/12/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescrbtion: UILabel!
    @IBOutlet weak var lblTime: UILabel!



    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var notificationitems:Notifications?{
        didSet{
            self.lblTitle.text = notificationitems?.title ?? ""
            self.lblDescrbtion.text = notificationitems?.descriptionValue ?? ""
            self.lblTime.text = notificationitems?.createdAt ?? ""
        }
    }


    
    
}
