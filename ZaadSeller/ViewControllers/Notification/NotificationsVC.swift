//
//  NotificationsVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/12/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import ESPullToRefresh

class NotificationsVC: SuperViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var currentpage = 1
    
    var notifcationData:NotificationData?
    var notificationitems = [Notifications]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        tableView.registerCell(id: "NotificationCell")
        getNotification()
        self.tableView.es.addPullToRefresh {
            self.currentpage = 1
            self.notificationitems.removeAll()
            self.getNotification()
            self.hideIndicator()
        }
        
        self.tableView.es.addInfiniteScrolling {
            self.currentpage += 1
            self.getNotification() // next page
            self.tableView.estimatedRowHeight = UITableView.automaticDimension
            self.hideIndicator()
        }
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setRightButtons([navgtion.Settingsbtn!], sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    
    override func didClickRightButton(_sender: UIBarButtonItem) {
        let vc = SettingsControlVC.loadFromNib()
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func getNotification(){
        
        guard Helper.isConnectedToNetwork() else {
            self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
            self.emptyView?.imgEmpty.image = UIImage(named: "refreshment")
            self.emptyView?.firstLabel.text = "There is no internet connection".localized
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "notifications?page=\(currentpage)", method: HTTPMethod.get).start(){ (response, error) in
            
            self.tableView.es.stopLoadingMore()
            self.tableView.es.stopPullToRefresh()
            
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(NotificationObject.self, from: response.data!)
                self.notifcationData = Status.data
                self.notificationitems += self.notifcationData!.notifications!
                if self.notifcationData?.nextPageUrl == nil {
                    self.tableView.es.stopLoadingMore()
                    self.tableView.es.noticeNoMoreData()
                }
                if self.notificationitems.count == 0{
                    self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                    self.emptyView?.firstLabel.text = "No notifications to display".localized
                    self.emptyView?.imgEmpty.isHidden = true
                    self.notificationitems.removeAll()
                    self.tableView.reloadData()

                }else{
                    self.tableView.tableFooterView = nil
                }
                self.tableView.reloadData()
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
    }
    
    @IBAction func didRefersh(){
        notificationitems.removeAll()
        getNotification()
    }
}


extension NotificationsVC:UITableViewDataSource,UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationitems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
        cell.notificationitems = notificationitems[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
