//
//  StoresVC.swift
//  ZaadSeller
//
//  Created by OsamaAbuAssi on 2/2/21.
//  +972 598 14 62 70
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class StoresVC: SuperViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var store:Stores?
    var storesData = [StoresData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerCell(id: "StoreCell")
        getAllStores()
        
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateStore(notification:)), name: Notification.Name("UpdateStore"), object: nil)
    }
    
    @objc func UpdateStore(notification: NSNotification)  {
        storesData.removeAll()
        getAllStores()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    
}
extension StoresVC {
    func getAllStores(){
        _ = WebRequests.setup(controller: self).prepare(query: "stores", method:
            HTTPMethod.get).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "error".localized, message:Status.message!)
                        return
                    }
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
                do {
                    let Status =  try JSONDecoder().decode(Stores.self, from: response.data!)
                    
                    self.storesData += Status.data ?? []
                    self.tableView.reloadData()
                    
                } catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
        }
    }
    
}

extension StoresVC :UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return storesData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StoreCell", for: indexPath) as! StoreCell
        cell.obj = storesData[indexPath.row]
        
        cell.detailsStore = { [weak self] in
            let vc:DetailsStore = DetailsStore.loadFromNib()
            vc.id = self?.storesData[indexPath.row].id ?? 0
            vc.storesData = self?.storesData[indexPath.row]
            vc.hidesBottomBarWhenPushed = true
            vc.modalPresentationStyle = .fullScreen
            self?.navigationController?.pushViewController(vc, animated: true)
        }
        
        cell.editStore = { [weak self] in
            let vc:AddStoreVC = AddStoreVC.loadFromNib()
            vc.id = self?.storesData[indexPath.row].id ?? 0
            vc.storesData = self?.storesData[indexPath.row]
            vc.isFromEdit = true
            vc.hidesBottomBarWhenPushed = true
            vc.modalPresentationStyle = .fullScreen
            self?.navigationController?.pushViewController(vc, animated: true)
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc:DeleteAlert = DeleteAlert.loadFromNib()
        if self.storesData[indexPath.row].id != nil{
            vc.isFromChangeStore = true
            vc.id = self.storesData[indexPath.row].id!
            vc.hidesBottomBarWhenPushed = true
            vc.modalPresentationStyle = .overCurrentContext
           self.present(vc, animated: false, completion: nil)
            
        }
        
        
    }
}
