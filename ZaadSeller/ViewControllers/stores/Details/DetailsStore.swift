//
//  DetailsStore.swift
//  ZaadSeller
//
//  Created by osamaaassi on 2/2/21.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class DetailsStore: SuperViewController {
    
    @IBOutlet weak var tfStoreName: UILabel!
    @IBOutlet weak var tfStoreOwner: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfCreatedAt: UITextField!
    @IBOutlet weak var tfExpireDate: UITextField!
    @IBOutlet weak var tfPhone: UITextField!
    @IBOutlet weak var tfMobile: UITextField!
    @IBOutlet weak var tfSize: UITextField!
    @IBOutlet weak var tfaddress: UITextField!
    @IBOutlet weak var tfBusinessRecordNo: UITextField!
    @IBOutlet weak var tfBusinessName: UITextField!
    @IBOutlet weak var tfCountry: UITextField!
    @IBOutlet weak var tfCity: UITextField!
    @IBOutlet weak var tfActivityType: UITextField!
    
    @IBOutlet weak var imgStore: UIImageView!
    
    @IBOutlet weak var tableView: UITableView!
    var id:Int?
    var storesData : StoresData?
    
    
    
    var expandableStors :[ExpandableStore] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.registerCell(id: "detailsCell")
        tableView.registerCell(id: "ChildrenCell")
       // optimize()
        
        self.tfStoreName.text = self.storesData?.name ?? ""
        self.tfStoreOwner.text = self.storesData?.owner ?? ""
        self.tfEmail.text = self.storesData?.email ?? ""
        self.tfCreatedAt.text = self.storesData?.createdAt ?? ""
        self.tfExpireDate.text = self.storesData?.expireDate ?? ""
        self.tfMobile.text = self.storesData?.mobile ?? ""
        self.tfPhone.text = self.storesData?.phone ?? ""
        self.tfaddress.text = self.storesData?.address ?? ""
        self.tfBusinessName.text = self.storesData?.businessName ?? ""
        self.tfActivityType.text = self.storesData?.activity?.title ?? ""
        self.tfBusinessRecordNo.text = self.storesData?.businessRecordNo ?? ""
        self.imgStore.sd_custom(url: self.storesData?.logo ?? "")
        
        if self.storesData?.size?.description == "1"{
            self.tfSize.text = "Big".localized
        }else{
            self.tfSize.text = "Small".localized
        }
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
}
//extension DetailsStore: UITableViewDelegate,UITableViewDataSource {
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return expandableStors.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        if expandableStors[indexPath.row].type == .sub1{
//            let cell = tableView.dequeueReusableCell(withIdentifier: "detailsCell") as! detailsCell
//            cell.textLabel?.text = expandableStors[indexPath.row].activityTypeStore.title ?? ""
//            return cell
//
//        }else if expandableStors[indexPath.row].type == .sub2{
//            let cell = tableView.dequeueReusableCell(withIdentifier: "ChildrenCell") as! ChildrenCell
//            cell.lblTitle.text = expandableStors[indexPath.row].activityTypeStore.title ?? ""
//            return cell
//        }
//        else{
//            let cell = tableView.dequeueReusableCell(withIdentifier: "ChildrenCell") as! ChildrenCell
//            cell.lblTitle.text = expandableStors[indexPath.row].activityTypeStore.title ?? ""
//            return cell
//
//        }
//
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        tableView.deselectRow(at: indexPath, animated: true)
//        expandableStors.forEach { obj in
//            obj.activityTypeStore.isSelected = false
//        }
//        expandableStors[indexPath.row].activityTypeStore.isSelected = true
//        optimize()
//        tableView.reloadData()
//
//
//    }
//}
//extension DetailsStore {
//
//
//    func optimize(){
//        //        ExpandableStore
//        expandableStors.removeAll()
//        let sub1ExpandableStores = storesData!.activityType!.children?.map{
//            ExpandableStore(activityTypeStore: $0, type: .sub1)
//        }
//        if sub1ExpandableStores != nil{
//            expandableStors.append(contentsOf: sub1ExpandableStores!)
//
//            let sub2 = sub1ExpandableStores!.first(where: {$0.activityTypeStore.isSelected})?.activityTypeStore.children
//
//            let sub2ExpandableStores = sub2?.map{
//                ExpandableStore(activityTypeStore: $0, type: .sub2)
//            }
//
//            if sub2ExpandableStores != nil{
//                let index = sub1ExpandableStores?.firstIndex(where: {$0.activityTypeStore.isSelected})
//                expandableStors.insert(contentsOf: sub2ExpandableStores!, at: index! + 1)
//
//                let sub3 = sub2!.first(where: {$0.isSelected})?.children
//                let sub3ExpandableStores = sub3?.map{
//                    ExpandableStore(activityTypeStore: $0, type: .sub3)
//                }
//                if sub3ExpandableStores != nil{
//                    let index = sub2ExpandableStores?.firstIndex(where: {$0.activityTypeStore.isSelected})
//                    expandableStors.insert(contentsOf: sub3ExpandableStores!, at: index! + 1)
//                }
//            }
//        }
//
//
//
//    }
//
//}
