//
//  StoreCell.swift
//  ZaadSeller
//
//  Created by osamaaassi on 2/2/21.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class StoreCell: UITableViewCell {

    @IBOutlet weak var imgProducat: UIImageView!
    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var lblowner: UILabel!
    @IBOutlet weak var lblmobile: UILabel!
    @IBOutlet weak var lblexpiredate: UILabel!
 
    var detailsStore : (() -> ())?
    var editStore : (() -> ())?
    
    var obj: StoresData? {
        didSet{
            lblname.text = self.obj?.name ?? ""
            lblowner.text = self.obj?.owner ?? ""
            lblmobile.text = self.obj?.mobile ?? ""
            lblexpiredate.text = self.obj?.expireDate ?? ""
            imgProducat.sd_custom(url: self.obj?.logo ?? "")
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func showStores (_ sender:UIButton){
        detailsStore?()
    }
    @IBAction func editStores (_ sender:UIButton){
        editStore?()
    }
}
