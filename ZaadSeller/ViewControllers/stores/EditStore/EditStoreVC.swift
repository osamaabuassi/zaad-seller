//
//  EditStoreVC.swift
//  ZaadSeller
//
//  Created by osamaaassi on 2/3/21.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class EditStoreVC: SuperViewController {
    
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfOwner: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfCountry: UITextField!
    @IBOutlet weak var tfCity: UITextField!
    @IBOutlet weak var tfPhone: UITextField!
    @IBOutlet weak var tfMobile: UITextField!
    @IBOutlet weak var tfTypeActivity: UITextField!
    @IBOutlet weak var tfSize: UITextField!
    @IBOutlet weak var lbladdress: UILabel!
    @IBOutlet weak var tfBusinessRecordNo: UITextField!
    @IBOutlet weak var tfBusinessName: UITextField!
    @IBOutlet weak var tfActivity: UITextField!
    @IBOutlet weak var btLogo: UIButton!
    @IBOutlet weak var btMap: UIButton!
    @IBOutlet weak var imgStore: UIImageView!
    
    var id:Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    
}
