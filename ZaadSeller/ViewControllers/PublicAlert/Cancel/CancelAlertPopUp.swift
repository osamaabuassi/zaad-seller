//
//  CancelAlertPopUp.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/6/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class CancelAlertPopUp: UIViewController {
    
    @IBOutlet weak var ViewBackGround: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setValue()
        dimis()
    }
    var isFromOffers = false
    
    
    func setValue(){
        if isFromOffers{
            self.lblTitle.text = "Sorry, the coupon cannot be deleted because the coupon has been used by customers".localized
        }else{
            self.lblTitle.text = "Sorry, the offer cannot be deleted because the coupon has been used by customers".localized
        }
        
    }
    
    func dimis(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.dismiss(animated: true) {
                print("dimis")
            }
        }
    }
    
    
    
    
}
