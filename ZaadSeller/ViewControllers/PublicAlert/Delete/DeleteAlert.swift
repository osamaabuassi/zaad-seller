//
//  DeleteAlert.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/6/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class DeleteAlert: SuperViewController {
    
    
    @IBOutlet weak var lbltitle: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setValue()
        
    }
    var id:Int?
    var isFromOffers = false
    var isFromProducats = false
    var isFromCapoun = false
    var isFromService = false
    var isFromorderServices = false
    var isFromChangeStore = false
    
    
    
    @IBAction func didTab_Cancel(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        
    }
    
    func setValue(){
        if isFromOffers{
            self.lbltitle.text = "Are you sure you want to delete the offer?".localized
        }
        if isFromProducats{
            self.lbltitle.text = "Are you sure you want to delete the product?".localized
        }
        if isFromChangeStore{
            self.lbltitle.text = "Would you like to switch store?".localized
        }
        else{
            self.lbltitle.text = "Are you sure you want to delete the product?".localized
        }
        
    }
    
    
    @IBAction func didTab_Confirm(_ sender: Any) {
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        if isFromOffers{
            _ = WebRequests.setup(controller: self).prepare(query: "item-offers/\(id ?? 0)", method: HTTPMethod.delete).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self.showAlert(title: "Error".localized, message:Status.message!)
                        return
                    }else if Status.status == 200{
                        self.dismiss(animated: false) {
                            NotificationCenter.default.post(name: Notification.Name("Updateoffers"), object: nil)
                        }
                    }
                    
                }catch let jsonErr {
                    print("Error serializing respone json", jsonErr)
                }
            }
            
        }
        if isFromProducats{
            _ = WebRequests.setup(controller: self).prepare(query: "items/\(id ?? 0)", method: HTTPMethod.delete).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self.showAlert(title: "Error".localized, message:Status.message!)
                        return
                    }else if Status.status == 200{
                        self.dismiss(animated: false) {
                            NotificationCenter.default.post(name: Notification.Name("UpdateProducat"), object: nil)
                        }
                    }
                    
                }catch let jsonErr {
                    print("Error serializing respone json", jsonErr)
                }
            }
        }
        
        if isFromCapoun{
            
            _ = WebRequests.setup(controller: self).prepare(query: "coupons/\(id ?? 0)", method: HTTPMethod.delete).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self.showAlert(title: "Error".localized, message:Status.message!)
                        return
                    }else if Status.status == 200{
                        self.dismiss(animated: false) {
                            NotificationCenter.default.post(name: Notification.Name("UpdateCapoun"), object: nil)
                        }
                    }
                    
                }catch let jsonErr {
                    print("Error serializing respone json", jsonErr)
                }
            }
        }
        
        if isFromService{
            _ = WebRequests.setup(controller: self).prepare(query: "services/\(id ?? 0)", method: HTTPMethod.delete).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self.showAlert(title: "Error".localized, message:Status.message!)
                        return
                    }else if Status.status == 200{
                        self.dismiss(animated: false) {
                            NotificationCenter.default.post(name: Notification.Name("UpdateServices"), object: nil)
                        }
                    }
                }catch let jsonErr {
                    print("Error serializing respone json", jsonErr)
                }
            }
        }
        
        if isFromorderServices{
            _ = WebRequests.setup(controller: self).prepare(query: "service-orders/items/\(id ?? 0)", method: HTTPMethod.delete).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self.showAlert(title: "Error".localized, message:Status.message!)
                        return
                    }else if Status.status == 200{
                        self.dismiss(animated: false) {
                            NotificationCenter.default.post(name: Notification.Name("UpdateCellItemService"), object: nil)
                        }
                    }
                }catch let jsonErr {
                    print("Error serializing respone json", jsonErr)
                }
            }
        }
        
        if isFromChangeStore{
            var parameters = [String:Int]()
            parameters["id"] = id
            _ = WebRequests.setup(controller: self).prepare(query: "stores/set-active-store", method: HTTPMethod.post,parameters: parameters).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self.showAlert(title: "Error".localized, message:Status.message!)
                        return
                    }else if Status.status == 200{
                        let Status =  try JSONDecoder().decode(UserObject.self, from: response.data!)
                        CurrentUser.userInfo = Status.data
                        let vc:StartVC = AppDelegate.sb_main.instanceVC()
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)
                        
                        
                    }
                }catch let jsonErr {
                    print("Error serializing respone json", jsonErr)
                }
            }
        }
    }
    
}

