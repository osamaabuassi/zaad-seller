//
//  SuccessAlert.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/6/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class SuccessAlert: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dimis()
    }
    
    func dimis(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name: Notification.Name("UpdateCapoun"), object: nil)
            }
        }
    }
    
    
    
}
