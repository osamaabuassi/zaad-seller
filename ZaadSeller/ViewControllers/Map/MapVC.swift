//
//  MapVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 8/28/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation


protocol MapCoordinatesDelgate : class {
    
    func SelectedDone(Lat: Double, Lon: Double, address:String )
}

protocol MapCoordinatesImageDelgate : class {
    
    func SelectedDone(Lat: Double, Lon: Double, address:String,image:UIImage)
}

class MapVC: SuperViewController,GMSMapViewDelegate, CLLocationManagerDelegate,LocateOnTheMap {
    
    
    
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var searchView: UIView!

    
    
    
    var searchResults: [String]!
    var MapDelegate:MapCoordinatesDelgate?
    var MapImageDelegate:MapCoordinatesImageDelgate?

    
    var searchResultController: SearchResultsController!
    var gmsFetcher: GMSAutocompleteFetcher!
    var resultsArray = [String]()
    
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation!
    
    
    let geocoder = GMSGeocoder()
    
    
    var lon:Double = 0.0
    var lat:Double = 0.0
    var lating : Double = 0.0
    var lonting:Double = 0.0
    var latStore : Double = 0.0
    var lonStore:Double = 0.0
    
    
    var address:String = ""
    var marker = false
    var isFromOrder = false
    var imgmap:UIImage?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchTF.delegate = self
        
        mapView.delegate = self
      
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.searchResults = Array()
        
        enableLocationServices()
        checkLocationAuthorized()
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        searchResultController = SearchResultsController()
        searchResultController.delegate = self
        
        gmsFetcher = GMSAutocompleteFetcher()
        gmsFetcher.delegate = self
        
        searchView.isHidden = false
        
    }
    
   override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    
    @IBAction func btDone(_ sender: UIButton) {
        if marker{
            self.navigationController?.popViewController(animated: true)
            self.MapDelegate?.SelectedDone(Lat: self.lat, Lon: self.lon, address: self.address)
        }else{
            self.showAlert(title: "", message: "Please select the address on the map.".localized)
        }
    }

    @IBAction func disimisbutton(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
   
    
    func checkLocationAuthorized(){
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled(){
            locationManager.startUpdatingLocation()
        }else{
            self.showAlert(title: "Error".localized, message: "Enable Location //Services. Settings -> Privacy -> Location Services -> Enable") { _ in
            }
            
        }
    }
    
    
    // getCurrentLocation
    func getCurrentLocation() {
        if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() ==  .authorizedAlways){
            
            currentLocation = locationManager.location
            if currentLocation != nil{
                self.lon = currentLocation.coordinate.longitude
                self.lat = currentLocation.coordinate.latitude
                
                
                
            }
            mapView.camera = GMSCameraPosition.camera(withLatitude: self.lat, longitude: self.lon, zoom: 16)
            
            CATransaction.begin()
            CATransaction.setValue(2.0, forKey: kCATransactionAnimationDuration)
            let myLocationCamera = GMSCameraPosition.camera(withLatitude: self.lat, longitude: self.lon, zoom: 14)
            self.mapView.animate(to: myLocationCamera)
            CATransaction.commit()
            
            mapView.delegate = self
            mapView.isMyLocationEnabled = true
            
        }
        
        
        
    }
    
    
    func enableLocationServices() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            // Request when-in-use authorization initially
            locationManager.requestWhenInUseAuthorization()
            
            break
            
        case .restricted, .denied:
            // Disable location features
            
            break
            
        case .authorizedWhenInUse:
            // Enable basic location features
            getCurrentLocation()
            locationManager.startUpdatingLocation()
            
            break
            
        case .authorizedAlways:
            // Enable any of your app's location features
            getCurrentLocation()
            locationManager.startUpdatingLocation()
            
            break
            
        default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        enableLocationServices()
        
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
        self.marker = true
        self.lat = coordinate.latitude
        self.lon = coordinate.longitude
        getAddressFromLatLon(pdblLatitude: String(self.lat), withLongitude: String(self.lon))
        
        
        let position = CLLocationCoordinate2DMake(coordinate.latitude,coordinate.longitude)
        let marker = GMSMarker(position: position)
        mapView.clear()
        marker.map = mapView
        
    }
    
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        
        let lat: Double = Double("\(pdblLatitude)")!
        let lon: Double = Double("\(pdblLongitude)")!
        
        
        
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, preferredLocale: Locale(identifier: MOLHLanguage.currentAppleLanguage()), completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                if let pm = placemarks{
                    
                    if pm.count > 0 {
                        let pm = placemarks![0]
                        print(pm.country as Any)
                        print(pm.locality as Any)
                        print(pm.subLocality as Any)
                        print(pm.thoroughfare as Any)
                        print(pm.postalCode as Any)
                        print(pm.subThoroughfare as Any)
                        print(pm.isoCountryCode as Any)
                        
                        
                        var addressString : String = ""
                        
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality! + ", "
                        }
                        if pm.thoroughfare != nil {
                            addressString = addressString + pm.thoroughfare! + ", "
                        }
                        if pm.locality != nil {
                            addressString = addressString + pm.locality! + ", "
                            
                        }
                        if pm.country != nil {
                            addressString = addressString + pm.country! + ", "
                        }
                        if pm.postalCode != nil {
                            addressString = addressString + pm.postalCode! + " "
                        }
                        
                        
                        self.address = addressString
                        self.searchTF.text = self.address
                        
                        if addressString == ""{
                            self.address = "\(lat), \(lon)"
                            
                        }
                        
                    }
                    
                }else{
                    self.address = ""
                }
        })
        
    }
    
    
    
    
    
    
}


extension MapVC: UITextFieldDelegate, UISearchBarDelegate, GMSAutocompleteFetcherDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        searchView.isHidden = true
        
        let searchController = UISearchController(searchResultsController: searchResultController)
        
        searchController.searchBar.delegate = self
        
        self.present(searchController, animated:true, completion: nil)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let string = textField.text?.lowercased()
        
        if let keyword = string?.replacingOccurrences(of: " ", with: "+"){
            print(keyword)
            
            self.resultsArray.removeAll()
            gmsFetcher?.sourceTextHasChanged(keyword)
            
            
        }
        
        textField.resignFirstResponder()
        return true
    }
    
    
    
    func locateWithLongitude(_ lon: Double, andLatitude lat: Double, andTitle title: String) {
        DispatchQueue.main.async { () -> Void in
            
            let position = CLLocationCoordinate2DMake(lat, lon)
            self.lat = lat
            self.lon = lon
            
            self.getAddressFromLatLon(pdblLatitude: String(self.lat), withLongitude: String(self.lon) )
            self.mapView.clear()
            
            
            
            self.mapView.camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lon, zoom: 3)
            
            CATransaction.begin()
            CATransaction.setValue(3.0, forKey: kCATransactionAnimationDuration)
            let myLocationCamera = GMSCameraPosition.camera(withLatitude: lat, longitude: lon, zoom: 12)
            self.mapView.animate(to: myLocationCamera)
            let marker = GMSMarker(position: position)
            marker.map = self.mapView
            CATransaction.commit()
            
            self.searchTF.text = title
            self.searchView.isHidden = false
            
            
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.resultsArray.removeAll()
        gmsFetcher?.sourceTextHasChanged(searchText)
        
        
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        // Stop doing the search stuff
        // and clear the text in the search bar
        searchBar.text = ""
        self.searchTF.text = ""
        // Hide the cancel button
        searchBar.showsCancelButton = false
        searchView.isHidden = false
        
        // You could also change the position, frame etc of the searchBar
    }
    
    public func didFailAutocompleteWithError(_ error: Error) {
        self.searchTF.text = error.localizedDescription
        print(error.localizedDescription)
    }
    
    public func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
        //self.resultsArray.count + 1
        
        for prediction in predictions {
            
            if let prediction = prediction as GMSAutocompletePrediction?{
                self.resultsArray.append(prediction.attributedFullText.string)
            }
        }
        self.searchResultController.reloadDataWithArray(self.resultsArray)
        print(resultsArray)
    }
    
}


