//
//  HomeVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/9/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import Charts
import ESPullToRefresh

class HomeVC: SuperViewController{
    
    @IBOutlet var Scroll: UIScrollView!
    
    
    @IBOutlet var chartProductsView: PieChartView!
    @IBOutlet var chartFinancialView: PieChartView!
    @IBOutlet var chartOrdersView: PieChartView!
    @IBOutlet var chartIncomeView: PieChartView!
    @IBOutlet var chartOrderPayView: PieChartView!
    
    @IBOutlet var titleStore: UILabel!
    @IBOutlet var DecStore: UILabel!
    
    
    
    @IBOutlet var lblProducatAcitive: UILabel!
    @IBOutlet var lblProducatUnAcitive: UILabel!
    @IBOutlet var lblTotalProducat: UILabel!
    
    
    
    @IBOutlet var lblFinancialTyany: UILabel!
    @IBOutlet var lblFinancialBank: UILabel!
    @IBOutlet var lblTotalFinancial: UILabel!
    
    
    
    @IBOutlet var lblIncomTyany: UILabel!
    @IBOutlet var lblIncomBank: UILabel!
    @IBOutlet var lblTotalIncom: UILabel!
    
    
    
    
    @IBOutlet var tableViewOrder: UITableView!
    @IBOutlet var tableViewOrderPay: UITableView!
    
    
    @IBOutlet var lblTotal: UILabel!
    @IBOutlet var lblTotalOrderPay: UILabel!

    
    @IBOutlet var lblorders_count: UILabel!
    @IBOutlet var lblorders_sum: UILabel!
    
    
    
    
    var colors = ["003B87","05C8FF","01D5D8","FF501B","FFD800","B1AF00"]
    
    var totlaProducta:Int?
    var totlaFinancial:Int?
    var metricsData:HomeData?
    var totalOrders:Int?
    var totalIncome:Int?
    var totalOrdersPay:Int?
    var orderstatus = [OrderPerStatus]()
    var income = [Income]()
    var Ordersales = [Sales]()
    var firstProducat:Int!
    var lastProducat:Int!
    var firstFinancial:Int!
    var lastFinancial:Int!
    var firstIncom:Int!
    var lastIncom:Int!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if CurrentUser.userInfo?.user?.store?.activityType?.id == 1 {
            titleStore.text = "Products".localized
            DecStore.text =  "Statistics on the number of effective and ineffective products".localized
        }else{
            titleStore.text = "Services".localized
            DecStore.text =  "Statistics on the number of effective and ineffective services".localized
        }
        chartProductsView.delegate = self
        chartFinancialView.delegate = self
        chartOrdersView.delegate = self
        chartIncomeView.delegate = self
        chartOrderPayView.delegate = self
        tableViewOrder.registerCell(id: "HomeCell")
        tableViewOrderPay.registerCell(id: "HomeCell")
        self.Scroll.es.addPullToRefresh {
            guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
            self.setHome()
            self.hideIndicator()
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        setHome()
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setRightButtons([navgtion.Settingsbtn!], sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    
    override func didClickRightButton(_sender: UIBarButtonItem) {
        let vc = SettingsControlVC.loadFromNib()
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func setHome(){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "home/metrics", method: HTTPMethod.get).start(){ (response, error) in
            
            self.Scroll.es.stopLoadingMore()
            self.Scroll.es.stopPullToRefresh()
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(HomeObject.self, from: response.data!)
                self.metricsData = Status.data!
                self.orderstatus = self.metricsData!.orderPerStatus!
                self.Ordersales = self.metricsData!.sales!
                
                self.lblorders_count.text = "Sales Quantity".localized + " " + "\(self.metricsData?.ordersCount ?? 0)"
                self.lblorders_sum.text = "Total amount of sales".localized + " " + "\(self.metricsData?.ordersSum ?? 0)"
                
                let sum = self.metricsData?.orderPerStatus?.compactMap({$0.count})
                let total = sum?.reduce(0,+)
                self.lblTotal.text =  "\(total ?? 0)" + " " + "Total".localized
                
                //chart Products
                self.firstProducat = self.metricsData?.itemPerStatus?.compactMap({$0.count}).first ?? 0
                if self.metricsData?.itemPerStatus?.count == 1{
                    self.lastProducat = 0
                }else{
                    self.lastProducat =  self.metricsData?.itemPerStatus?.compactMap({$0.count}).last ?? 0
                }
                let totalproducta = Int(self.firstProducat) + Int(self.lastProducat)
                self.totlaProducta = totalproducta
                
                self.chartProductsView.animate(xAxisDuration: 1.4, easingOption: .easeOutBack)
                self.setupchartProductsDataCount((self.metricsData?.itemPerStatus?.count ?? 0), range: UInt32(360))
                self.setupchartProducts(pieChartView: self.chartProductsView)
                self.lblProducatUnAcitive.text = "\(self.firstProducat ?? 0)" + " " + "inactive".localized
                self.lblProducatAcitive.text = "\(self.lastProducat ?? 0)" + " " + "active".localized
                self.lblTotalProducat.text = "\(totalproducta)" + " " + "Total".localized
                if self.metricsData?.itemPerStatus?.count == 0{
                  // self.setupchartProductsDataCount(Int(1), range: UInt32(360))
                    self.chartProductsView.holeColor = "DEDEDE".color
                }
                
                //chart Financial
                
            self.firstFinancial = self.metricsData?.orderPerPaymentMethod?.compactMap({$0.count}).first
                    ?? 0
            if self.metricsData?.orderPerPaymentMethod?.count == 1{
                self.lastFinancial = 0
            }else{
                self.lastFinancial =  self.metricsData?.orderPerPaymentMethod?.compactMap({$0.count}).last ?? 0
                }
                let totalfinancial = Int(self.firstFinancial) + Int(self.lastFinancial)
                
                self.totlaFinancial = totalfinancial
                
                self.lblFinancialTyany.text = "\(self.firstFinancial ?? 0)" + " " + "Thawani".localized
                
                self.lblFinancialBank.text = "\(self.lastFinancial ?? 0)" + " " + "Bank account".localized
                
                
                self.lblTotalFinancial.text = "\(totalfinancial)" + " " + "Total".localized
                
                
                self.chartFinancialView.animate(xAxisDuration: 1.4, easingOption: .easeOutBack)
                
                self.setupchartFinancial(pieChartView: self.chartFinancialView)
                self.setupchartFinancialDataCount((self.metricsData?.orderPerPaymentMethod?.count ?? 0), range: UInt32(360))
                
                self.totlaFinancial = totalfinancial
                
                if self.metricsData?.orderPerPaymentMethod?.count == 0{
                    //self.setupchartFinancialDataCount(Int(1), range: UInt32(360))
                    self.chartFinancialView.holeColor = "DEDEDE".color
                   // self.chartFinancialView.alpha = 0
                }
                
                // chart Order
                let sumOrders = self.orderstatus.compactMap({$0.count})
                let totalOrders = sumOrders.reduce(0, +)
                self.totalOrders = totalOrders
                self.chartOrdersView.animate(xAxisDuration: 1.4, easingOption: .easeOutBack)
                
                self.setupchartOrders(pieChartView: self.chartOrdersView)
                self.setupchartOrdersDataCount((sumOrders.count), range: UInt32(360))
                if self.orderstatus.count == 0{
                    self.chartOrdersView.holeColor = "DEDEDE".color
                }
                
                // Income
                self.firstIncom = self.metricsData?.income?.compactMap({$0.sum}).first
                ?? 0
                if self.metricsData?.income?.count == 1{
                    self.lastFinancial = 0
                }else{
                    self.lastFinancial =  self.metricsData?.income?.compactMap({$0.sum}).last ?? 0
                }
                let totalincome = Int(self.firstIncom) + Int(self.lastFinancial)
                
                self.totalIncome = totalincome
                self.chartIncomeView.animate(xAxisDuration: 1.4, easingOption: .easeOutBack)
                self.setupchartIncomeDataCount((self.metricsData?.income?.count ?? 0), range: UInt32(360))
                self.setupchartIncome(pieChartView: self.chartIncomeView)
                
                self.lblIncomTyany.text = "\(self.firstIncom ?? 0)" + " " + "Thawani".localized
                self.lblIncomBank.text = "\(self.lastFinancial ?? 0)" + " " + "Bank account".localized
                self.lblTotalIncom.text = "\(totalincome)" + " " + "Total".localized
                
                if self.metricsData?.income?.count == 0{
                    self.chartIncomeView.holeColor = "DEDEDE".color
                    //self.setupchartIncomeDataCount(Int(1), range: UInt32(360))
                    //self.chartIncomeView.alpha = 0
                }
                
                // Order Pay
                
                let sumOrdersPay = self.Ordersales.compactMap({$0.sum})
                let totalOrdersPay = sumOrdersPay.reduce(0, +)
                self.totalOrdersPay = totalOrdersPay
                self.lblTotalOrderPay.text = "\(self.totalOrdersPay ?? 0)" + " " + "Total".localized
                self.chartOrderPayView.animate(xAxisDuration: 1.4, easingOption: .easeOutBack)
                
                self.setupchartOrdersPay(pieChartView: self.chartOrderPayView)
                self.setupchartOrdersPayDataCount((sumOrdersPay.count), range: UInt32(360))
                
                if self.metricsData?.sales?.count == 0{
                    self.chartOrderPayView.holeColor = "DEDEDE".color
                    //self.setupchartOrdersPayDataCount(Int(1), range: UInt32(360))
                    //self.chartOrderPayView.alpha = 0
                }
                
                
                self.tableViewOrder.reloadData()
                self.tableViewOrderPay.reloadData()
                
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
        }
    }
}

// MARK:- Charts,ChartViewDelegate

extension HomeVC:ChartViewDelegate{
    
    func setupchartProductsDataCount(_ count: Int, range: UInt32) {
        let entries = (0..<count).map { (i) -> PieChartDataEntry in
            // IMPORTANT: In a PieChart, no values (Entry) should have the same xIndex (even if from different DataSets), since no values can be drawn above each other.
            if self.metricsData?.itemPerStatus?.count != 0{
                let value = self.metricsData?.itemPerStatus?.compactMap({$0.count})
                return PieChartDataEntry(value: Double(value![i]),
                                         label: nil,
                                         icon: nil)
            }else{
                return PieChartDataEntry(value: Double(0),
                                         label: nil,
                                         icon: nil)
            }
            
        }
        
        let set = PieChartDataSet(entries: entries, label: nil)
        set.drawIconsEnabled = false
        set.sliceSpace = 0
        set.selectionShift = 0
        set.colors = ["FF501B".color,"003B87".color]

        
        
        let data = PieChartData(dataSet: set)
        
        
        let pFormatter = NumberFormatter()
        pFormatter.numberStyle = .percent
        pFormatter.maximumFractionDigits = 1
        pFormatter.multiplier = 1
        pFormatter.percentSymbol = " %"
        data.setValueTextColor(.white)
        data.setValueFont(.systemFont(ofSize: 12, weight: .light))
        chartProductsView.data = data
        chartProductsView.highlightValues(nil)
    }
    
    
    func setupchartProducts(pieChartView chartView: PieChartView) {
        
        chartView.usePercentValuesEnabled = true
        chartView.drawSlicesUnderHoleEnabled = false
        chartView.holeRadiusPercent = 0.50
        chartView.transparentCircleRadiusPercent = 0.61
        chartView.chartDescription?.enabled = false
        chartView.setExtraOffsets(left: 0, top: 0, right: 0, bottom: 0)
        
        chartView.drawCenterTextEnabled = true
        
        let paragraphStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        paragraphStyle.lineBreakMode = .byTruncatingTail
        paragraphStyle.alignment = .center
        
        let centerText = NSMutableAttributedString(string: "\(self.totlaProducta ?? 0)")
        centerText.setAttributes([.font : UIFont.FFShamelFamilySemiRoundBook(ofSize: 16),
                                  .paragraphStyle : paragraphStyle], range: NSRange(location: 0, length: centerText.length))
        centerText.addAttributes([.font : UIFont.FFShamelFamilySemiRoundBook(ofSize: 16),
                                  .foregroundColor : "003B87".color], range: NSRange(location: 0, length: centerText.length))
        chartView.centerAttributedText = centerText
        
        chartView.drawHoleEnabled = true
        chartView.rotationAngle = 0
        chartView.rotationEnabled = false
        chartView.legend.enabled = false
        chartView.highlightPerTapEnabled = true
    }
    
    
    func setupchartFinancialDataCount(_ count: Int, range: UInt32) {
        let entries = (0..<count).map { (i) -> PieChartDataEntry in
            // IMPORTANT: In a PieChart, no values (Entry) should have the same xIndex (even if from different DataSets), since no values can be drawn above each other.
            if self.metricsData?.orderPerPaymentMethod?.count != 0{
                let value = self.metricsData?.orderPerPaymentMethod?.compactMap({$0.count})
                return PieChartDataEntry(value: Double(value![i]),
                                         label: nil,
                                         icon: nil)
            }else{
                return PieChartDataEntry(value: Double(0),
                                         label: nil,
                                         icon: nil)
            }
            
        }
        let set = PieChartDataSet(entries: entries, label: nil)
        set.drawIconsEnabled = false
        set.sliceSpace = 0
        set.selectionShift = 0

        
        set.colors = ["FF501B".color,"FFCC00".color]
        
        let data = PieChartData(dataSet: set)
        
        let pFormatter = NumberFormatter()
        pFormatter.numberStyle = .percent
        pFormatter.maximumFractionDigits = 1
        pFormatter.multiplier = 1
        pFormatter.percentSymbol = "%"
        data.setValueTextColor(.white)
        data.setValueFont(.systemFont(ofSize: 12, weight: .light))
        chartFinancialView.data = data
        chartFinancialView.highlightValues(nil)
    }
    
    func setupchartFinancial(pieChartView chartView: PieChartView) {
        
        chartView.usePercentValuesEnabled = true
        chartView.drawSlicesUnderHoleEnabled = false
        chartView.holeRadiusPercent = 0.50
        chartView.transparentCircleRadiusPercent = 0.61
        chartView.chartDescription?.enabled = false
        chartView.setExtraOffsets(left: 0, top: 0, right: 0, bottom: 0)
        
        chartView.drawCenterTextEnabled = true
        
        let paragraphStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        paragraphStyle.lineBreakMode = .byTruncatingTail
        paragraphStyle.alignment = .center
        
        let centerText = NSMutableAttributedString(string: "\(self.totlaFinancial ?? 0)")
        centerText.setAttributes([.font : UIFont.FFShamelFamilySemiRoundBook(ofSize: 16),
                                  .paragraphStyle : paragraphStyle], range: NSRange(location: 0, length: centerText.length))
        centerText.addAttributes([.font : UIFont.FFShamelFamilySemiRoundBook(ofSize: 16),
                                  .foregroundColor : "003B87".color], range: NSRange(location: 0, length: centerText.length))
        chartView.centerAttributedText = centerText
        
        chartView.drawHoleEnabled = true
        chartView.rotationAngle = 0
        chartView.rotationEnabled = false
        chartView.legend.enabled = false
        chartView.highlightPerTapEnabled = true
    }
    
    
    
    
    // OrdersChart
    func setupchartOrdersDataCount(_ count: Int, range: UInt32) {
        let entries = (0..<count).map { (i) -> PieChartDataEntry in
            // IMPORTANT: In a PieChart, no values (Entry) should have the same xIndex (even if from different DataSets), since no values can be drawn above each other.
            if self.orderstatus.count != 0{
                let value = self.orderstatus.compactMap({$0.count})
                return PieChartDataEntry(value: Double(value[i]),
                                         label: nil,
                                         icon: nil)
            }else{
                return PieChartDataEntry(value: Double(0),
                                         label: nil,
                                         icon: nil)
            }
            
        }
        
        let set = PieChartDataSet(entries: entries, label: nil)
        set.drawIconsEnabled = false
        set.sliceSpace = 0
        set.selectionShift = 0
        
        
        set.colors = ["003B87".color,"05C8FF".color,"01D5D8".color,"FF501B".color,"FFD800".color,"B1AF00".color]
        
        let data = PieChartData(dataSet: set)
        
        
        let pFormatter = NumberFormatter()
        pFormatter.numberStyle = .percent
        pFormatter.maximumFractionDigits = 1
        pFormatter.multiplier = 1
        pFormatter.percentSymbol = " %"
        data.setValueTextColor(.white)
        data.setValueFont(.systemFont(ofSize: 12, weight: .light))
        chartOrdersView.data = data
        chartOrdersView.highlightValues(nil)
    }
    
    
    func setupchartOrders(pieChartView chartView: PieChartView) {
        
        chartView.usePercentValuesEnabled = true
        chartView.drawSlicesUnderHoleEnabled = false
        chartView.holeRadiusPercent = 0.50
        chartView.transparentCircleRadiusPercent = 0.61
        chartView.chartDescription?.enabled = false
        chartView.setExtraOffsets(left: 0, top: 0, right: 0, bottom: 0)
        
        chartView.drawCenterTextEnabled = true
        
        let paragraphStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        paragraphStyle.lineBreakMode = .byTruncatingTail
        paragraphStyle.alignment = .center
        
        let centerText = NSMutableAttributedString(string: "\(self.totalOrders ?? 0)")
        centerText.setAttributes([.font : UIFont.FFShamelFamilySemiRoundBook(ofSize: 16),
                                  .paragraphStyle : paragraphStyle], range: NSRange(location: 0, length: centerText.length))
        centerText.addAttributes([.font : UIFont.FFShamelFamilySemiRoundBook(ofSize: 16),
                                  .foregroundColor : "003B87".color], range: NSRange(location: 0, length: centerText.length))
        chartView.centerAttributedText = centerText
        
        chartView.drawHoleEnabled = true
        chartView.rotationAngle = 0
        chartView.rotationEnabled = false
        chartView.legend.enabled = false
        chartView.highlightPerTapEnabled = true
    }
    
    
    // Income
    
    func setupchartIncomeDataCount(_ count: Int, range: UInt32) {
        let entries = (0..<count).map { (i) -> PieChartDataEntry in
            // IMPORTANT: In a PieChart, no values (Entry) should have the same xIndex (even if from different DataSets), since no values can be drawn above each other.
            if self.metricsData?.income?.count != 0{
                let value = self.metricsData?.income?.compactMap({$0.sum})
                return PieChartDataEntry(value: Double(value![i]),
                                         label: nil,
                                         icon: nil)
            }else{
                return PieChartDataEntry(value: Double(0),
                                         label: nil,
                                         icon: nil)
            }
            
        }
        
        let set = PieChartDataSet(entries: entries, label: nil)
        set.drawIconsEnabled = false
        set.sliceSpace = 0
        set.selectionShift = 0

        
        set.colors = ["FF501B".color,"FFCC00".color]
        
        let data = PieChartData(dataSet: set)
        
        
        let pFormatter = NumberFormatter()
        pFormatter.numberStyle = .percent
        pFormatter.maximumFractionDigits = 1
        pFormatter.multiplier = 1
        pFormatter.percentSymbol = " %"
        data.setValueTextColor(.white)
        data.setValueFont(.systemFont(ofSize: 12, weight: .light))
        chartIncomeView.data = data
        chartIncomeView.highlightValues(nil)
    }
    
    
    func setupchartIncome(pieChartView chartView: PieChartView) {
        
        chartView.usePercentValuesEnabled = true
        chartView.drawSlicesUnderHoleEnabled = false
        chartView.holeRadiusPercent = 0.50
        chartView.transparentCircleRadiusPercent = 0.61
        chartView.chartDescription?.enabled = false
        chartView.setExtraOffsets(left: 0, top: 0, right: 0, bottom: 0)
        
        chartView.drawCenterTextEnabled = true
        
        let paragraphStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        paragraphStyle.lineBreakMode = .byTruncatingTail
        paragraphStyle.alignment = .center
        
        let centerText = NSMutableAttributedString(string: "\(self.totalIncome ?? 0)")
        centerText.setAttributes([.font : UIFont.FFShamelFamilySemiRoundBook(ofSize: 16),
                                  .paragraphStyle : paragraphStyle], range: NSRange(location: 0, length: centerText.length))
        centerText.addAttributes([.font : UIFont.FFShamelFamilySemiRoundBook(ofSize: 16),
                                  .foregroundColor : "003B87".color], range: NSRange(location: 0, length: centerText.length))
        chartView.centerAttributedText = centerText
        
        chartView.drawHoleEnabled = true
        chartView.rotationAngle = 0
        chartView.rotationEnabled = false
        chartView.legend.enabled = false
        chartView.highlightPerTapEnabled = true
    }
    
    
    // OrderPay
    
    func setupchartOrdersPayDataCount(_ count: Int, range: UInt32) {
        let entries = (0..<count).map { (i) -> PieChartDataEntry in
            // IMPORTANT: In a PieChart, no values (Entry) should have the same xIndex (even if from different DataSets), since no values can be drawn above each other.
            if self.orderstatus.count != 0{
                let value = self.Ordersales.compactMap({$0.sum})
                return PieChartDataEntry(value: Double(value[i]).rounded(),
                                         label: nil,
                                         icon: nil)
            }else{
                return PieChartDataEntry(value: Double(0),
                                         label: nil,
                                         icon: nil)
            }
            
        }
        
        let set = PieChartDataSet(entries: entries, label: nil)
        set.drawIconsEnabled = false
        set.sliceSpace = 0
        set.selectionShift = 0
        
        
        set.colors = ["003B87".color,"05C8FF".color,"01D5D8".color,"FF501B".color,"FFD800".color,"B1AF00".color]
        
        let data = PieChartData(dataSet: set)
        
        
        let pFormatter = NumberFormatter()
        pFormatter.numberStyle = .percent
        pFormatter.maximumFractionDigits = 1
        pFormatter.multiplier = 1
        pFormatter.percentSymbol = " %"
        data.setValueTextColor(.white)
        data.setValueFont(.systemFont(ofSize: 12, weight: .light))
        chartOrderPayView.data = data
        chartOrderPayView.highlightValues(nil)
    }
    
    
    func setupchartOrdersPay(pieChartView chartView: PieChartView) {
        
        chartView.usePercentValuesEnabled = true
        chartView.drawSlicesUnderHoleEnabled = false
        chartView.holeRadiusPercent = 0.50
        chartView.transparentCircleRadiusPercent = 0.61
        chartView.chartDescription?.enabled = false
        chartView.setExtraOffsets(left: 0, top: 0, right: 0, bottom: 0)
        
        chartView.drawCenterTextEnabled = true
        
        let paragraphStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        paragraphStyle.lineBreakMode = .byTruncatingTail
        paragraphStyle.alignment = .center
        
        let centerText = NSMutableAttributedString(string: "\(self.totalOrdersPay ?? 0)")
        centerText.setAttributes([.font : UIFont.FFShamelFamilySemiRoundBook(ofSize: 16),
                                  .paragraphStyle : paragraphStyle], range: NSRange(location: 0, length: centerText.length))
        centerText.addAttributes([.font : UIFont.FFShamelFamilySemiRoundBook(ofSize: 16),
                                  .foregroundColor : "003B87".color], range: NSRange(location: 0, length: centerText.length))
        chartView.centerAttributedText = centerText
        
        chartView.drawHoleEnabled = true
        chartView.rotationAngle = 0
        chartView.rotationEnabled = false
        chartView.legend.enabled = false
        chartView.highlightPerTapEnabled = true
    }
    
    
    
    
}


extension HomeVC:UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableViewOrder{
            return orderstatus.count
        }else if  tableView == tableViewOrderPay{
            return Ordersales.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tableViewOrder{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell", for: indexPath) as! HomeCell
            let obj = orderstatus[indexPath.row]
            print(obj.status ?? "")
            cell.lblStatus.text =  "\(obj.count ?? 0)" + " " + "\(obj.status?.localized ?? "" )"
            DispatchQueue.main.async{
                cell.ViewStatus.backgroundColor = self.colors[indexPath.row].color
            }
            return cell
        }else if tableView == tableViewOrderPay{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell", for: indexPath) as! HomeCell
            let obj = Ordersales[indexPath.row]
            cell.lblStatus.text = "\(obj.sum ?? 0)"  + " " + "\(obj.status?.localized ?? "" )"
            DispatchQueue.main.async{
                cell.ViewStatus.backgroundColor = self.colors[indexPath.row].color
            }
            return cell
        }
        return UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
