//
//  RegsiterVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/12/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class RegsiterVC: SuperViewController,cityDelgate{
    
    
    func SelectedDone(name: String, id: Int) {
        self.tfCity.text = name
        self.selectedCityID = id
    }
    
    
    @IBOutlet weak var tfCountry: UITextField!
    @IBOutlet weak var tfCity: UITextField!
    @IBOutlet weak var tfTypeActivity: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfPhone: UITextField!
    @IBOutlet weak var tfCommercialNumber: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    @IBOutlet weak var imgShow: UIImageView!
    @IBOutlet weak var imgShow2: UIImageView!
    @IBOutlet weak var CheckBox: CheckBox!
    @IBOutlet weak var btService: RadioButton!
    @IBOutlet weak var btProducat: RadioButton!
    
    
    
    
    var countryData:CountryData?
    var country = [Country]()
    var selectedCountryID:Int?
    var cityData:CityData?
    var city = [City]()
    var currentpage = 1
    var selectedCityID:Int?
    var Activity = [Category]()
    var ActivityData:CategroiesData?
    var selectedActivityID:Int?
    var selectedTypeID:Int?
    var planid:Int?
    var planpriceid:Int?
    var isShowPassword = true
    var isShow2Password = true

    
    override func viewDidLoad() {
        super.viewDidLoad()
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonForViewController(sender: self)
        navgtion.isDark = false
        btService.isSelected = false
        btProducat.isSelected = false
        btService?.alternateButton = [btProducat!]
        btProducat?.alternateButton = [btService!]
        tfPassword.isSecureTextEntry = true
        imgShow.image = UIImage(named: "unshow")
        tfConfirmPassword.isSecureTextEntry = true
        imgShow2.image = UIImage(named: "unshow")
        getCountry()
        getActivity()
  
    }
    
    
    func getToken(){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
          let token =  UserDefaults.standard.string(forKey: "token")
          var parameters: [String: Any] = [:]
          parameters["token"] = token
          
          _ = WebRequests.setup(controller: self).prepare(query: "notifications/token", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
             
              do {
                  let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                  if Status.status != 200 {
                      
                      self.showAlert(title: "Error".localized, message:Status.message!)
                      return
                  }
                  
              }catch let jsonErr {
                  print("Error serializing respone json", jsonErr)
              }
              
          }
      }
    
    @IBAction func didTab_SeePassword(_ sender: UIButton) {
        if sender.tag == 1{
            if(isShowPassword == true) {
                tfPassword.isSecureTextEntry = false
                imgShow.image = UIImage(named: "pass")
            } else {
                tfPassword.isSecureTextEntry = true
                imgShow.image = UIImage(named: "unshow")
            }
            
            isShowPassword = !isShowPassword
        }else{
            if(isShow2Password == true) {
                tfConfirmPassword.isSecureTextEntry = false
                imgShow2.image = UIImage(named: "pass")
            } else {
                tfConfirmPassword.isSecureTextEntry = true
                imgShow2.image = UIImage(named: "unshow")
            }
            isShow2Password = !isShow2Password
        }
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    @IBAction func didTab_Skip(_ sender: Any) {
        let mainVC = SettingsControlVC()
        UIApplication.shared.keyWindow?.rootViewController = mainVC.navigationController()
    }
    
    @IBAction func didTab_Type(_ sender: UIButton) {
        if sender.tag == 1{
            self.selectedTypeID = 1
        }else{
            self.selectedTypeID = 1
            //          self.selectedTypeID = 2
        }
    }
    @IBAction func didTab_Terms(_ sender: UIButton) {
        let vc:InfoVC = InfoVC.loadFromNib()
        vc.idInfo = 204
        vc.isFromTermsLogin = true
        vc.isDismis = true
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func getCountry(){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "countries", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(CountryObject.self, from: response.data!)
                self.countryData = Status.data!
                self.country = self.countryData!.resources!
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
        }
    }
    
    
    
    func getCity(){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        guard let CountryID = self.selectedCountryID, CountryID != 0 else{
            self.showAlert(title: "".localized, message: "Choose the country first".localized)
            return
        }
        
        _ = WebRequests.setup(controller: self).prepare(query: "cities?country_id=\(selectedCountryID ?? 0)", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(CityObject.self, from: response.data!)
                self.cityData = Status.data!
                self.city = self.cityData!.resources!
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
    }
    
    func getActivity(){
        
        guard Helper.isConnectedToNetwork() else {
                  self.showAlert(title: "".localized, message: "There is no internet connection".localized)
                  return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "categories/\(1)", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(CategroiesObject.self, from: response.data!)
                self.ActivityData = Status.data!
                self.Activity = self.ActivityData!.resources!
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
        
    }
    
    @IBAction func didTabActivity(_ sender: UIButton) {
        ActionSheetStringPicker.show(withTitle: "Activity type".localized, rows: self.Activity.map { $0.title as Any }
            , initialSelection: 0, doneBlock: {
                picker, value, index in
                if let Value = index {
                    self.tfTypeActivity.text = Value as? String
                    self.selectedActivityID = self.Activity[value].id ?? 0
                }
                return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    @IBAction func didTab_Country(_ sender: UIButton) {
        ActionSheetStringPicker.show(withTitle: "Country".localized, rows: self.country.map { $0.title as Any }
            , initialSelection: 0, doneBlock: {
                picker, value, index in
                if let Value = index {
                    self.tfCountry.text = Value as? String
                    self.selectedCountryID = self.country[value].id ?? 0
                    //self.getCity()
                }
                
                return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    
    @IBAction func didTab_City(_ sender: UIButton) {
        
        guard let CountryID = self.selectedCountryID, CountryID != 0 else{
            self.showAlert(title: "".localized, message: "Choose the country first".localized)
            return
        }
        
        let vc:CitysVC = CitysVC.loadFromNib()
        vc.Countryid = CountryID
        vc.citydelegte = self
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)

        
        
//        guard let CountryID = self.selectedCountryID, CountryID != 0 else{
//            self.showAlert(title: "".localized, message: "اختر الدولة اولا".localized)
//            return
//        }
//
//        ActionSheetStringPicker.show(withTitle: "المدينة".localized, rows: self.city.map { $0.name as Any }
//            , initialSelection: 0, doneBlock: {
//                picker, value, index in
//                if let Value = index {
//                    self.tfCity.text = Value as? String
//                    self.selectedCityID = self.city[value].id ?? 0
//                }
//
//                return
//        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
        
    }
    
    @IBAction func didTab_dismis(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func didTab_Register(_ sender: Any) {
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "error".localized, message: "There is no internet connection".localized)
            return }
        
        guard let email = self.tfEmail.text, !email.isEmpty else{
            self.showAlert(title: "error".localized, message: "Username cannot be empty".localized)
            return
        }
        
        guard let number = self.tfCommercialNumber.text, !number.isEmpty else{
            self.showAlert(title: "error".localized, message: "The CR number cannot be empty".localized)
            return
        }
        
        
        guard let password = self.tfPassword.text, !password.isEmpty else{
            self.showAlert(title: "error".localized, message: "Password cannot be empty".localized)
            return
        }
        
        if password.count < 6 {
            showAlert(title: "error".localized, message: "Password must be at least 6 characters long".localized)
            return
        }
        
        guard tfConfirmPassword.text == tfPassword.text  else {
            self.showAlert(title: "Error".localized, message: "Password does not match".localized)
            return
        }
        
        guard let passwordConfirm = self.tfConfirmPassword.text, !passwordConfirm.isEmpty else{
            self.showAlert(title: "error".localized, message: "Password cannot be empty".localized)
            return
        }
        
        guard let name = self.tfName.text, !name.isEmpty else{
            self.showAlert(title: "error".localized, message: "Store name cannot be empty".localized)
            return
        }
        
        guard let mobile = self.tfPhone.text, !mobile.isEmpty else{
            self.showAlert(title: "error".localized, message: "The mobile used cannot be empty".localized)
            return
        }
        
        guard self.selectedCountryID != nil  else{
            self.showAlert(title: "error".localized, message: "The state cannot be empty".localized)
            return
        }
        guard self.selectedCityID != nil  else{
            self.showAlert(title: "error".localized, message: "The city cannot be empty".localized)
            return
        }
        guard self.selectedActivityID != nil  else{
            self.showAlert(title: "error".localized, message: "Please select a type of activity".localized)
            return
        }
        
        guard CheckBox.isChecked == true else{
            showAlert(title: "error".localized, message: "You must agree to the terms and conditions".localized)
            return
        }
        
        var parameters: [String: Any] = [:]
        parameters["name"] = self.tfName.text ?? ""
        parameters["mobile"] = self.tfPhone.text ?? ""
        parameters["email"] = self.tfEmail.text ?? ""
        parameters["password"] = self.tfPassword.text ?? ""
        parameters["password_confirmation"] = self.tfConfirmPassword.text ?? ""
        parameters["business_record_no"] = self.tfCommercialNumber.text ?? ""
        parameters["country_id"] = self.selectedCountryID?.description
        parameters["city_id"] = self.selectedCityID?.description
        parameters["activity_id"] = self.selectedActivityID?.description
        parameters["type_activity"] = self.selectedTypeID?.description
//        parameters["plan_id"] = self.planid?.description
//        parameters["plan_price_id"] = self.planpriceid?.description

        
        _ = WebRequests.setup(controller: self).prepare(query: "auth/register", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }else if Status.status == 200{
                    let vc:VirifactionVC = VirifactionVC.loadFromNib()
                    vc.username = email
                    vc.modalPresentationStyle = .fullScreen
                    self.navigationController?.pushViewController(vc, animated: true)

                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
//            do {
////                let Status =  try JSONDecoder().decode(UserObject.self, from: response.data!)
////                CurrentUser.userInfo = Status.data! as UserData
////                if CurrentUser.userInfo != nil{
////                    self.getToken()
////                    let vc:VirifactionVC = VirifactionVC.loadFromNib()
////                    vc.username = email
////                    vc.modalPresentationStyle = .fullScreen
////                    self.present(vc, animated: true, completion: nil)
////                }
//
//            } catch let jsonErr {
//                print("Error serializing  respone json", jsonErr)
//            }
            
        }
    }
    
}
