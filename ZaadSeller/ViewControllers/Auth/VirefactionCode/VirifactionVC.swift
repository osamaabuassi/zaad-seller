//
//  VirifactionVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/3/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class VirifactionVC: SuperViewController {
    
    @IBOutlet weak var tf_virfiaction: UITextField!
    @IBOutlet weak var lb_time: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    
    var username:String?
    var timer = Timer()
    var seconds = 59
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonForViewController(sender: self)
        navgtion.isDark = false
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
        self.lblMobile.text  = "\(username ?? "") " + "Verify your account by entering the 4-digit code to the number".localized
    }
    
    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
         setNeedsStatusBarAppearanceUpdate()
     }
     
    
    @objc func updateTimer() {
        seconds -= 1     //This will decrement(count down)the seconds.
        if seconds <= 0 {
            lb_time.text = "00:00" //This will update the label.
            timer.invalidate()
            
        }else{
            lb_time.text = "00:\(seconds)" //This will update the label.
        }
    }
    
    
    @IBAction func bt_send(_ sender: Any) {
        guard Helper.isConnectedToNetwork() else {
                  self.showAlert(title: "".localized, message: "There is no internet connection".localized)
                  return }
        guard let code = self.tf_virfiaction.text, !code.isEmpty else{
            self.showAlert(title: "error".localized, message: "Code is empty".localized)
            return
        }
        
        var parameters: [String: Any] = [:]
        parameters["username"] = username
        parameters["code"] = self.tf_virfiaction.text ?? ""
        
        _ = WebRequests.setup(controller: self).prepare(query: "auth/verify-code", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }else if  Status.status == 200{
                    if Status.message == "كود التحقق خاطئ"{
                        self.showAlert(title: "error", message: "Verification code is wrong".localized)
                    }else{
                        let mainVC = LoginVC()
                        UIApplication.shared.keyWindow?.rootViewController = mainVC.navigationController()

                    }
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
        }
    }
    
    @IBAction func bt_resend(_ sender: Any) {
        
        guard Helper.isConnectedToNetwork() else {
                  self.showAlert(title: "".localized, message: "There is no internet connection".localized)
                  return }
        
        guard let code = self.tf_virfiaction.text, !code.isEmpty else{
            self.showAlert(title: "error".localized, message:"Code is empty".localized)
            return
        }
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)

        var parameters: [String: Any] = [:]
        parameters["username"] = username
        
        _ = WebRequests.setup(controller: self).prepare(query: "auth/verify-code", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }else if  Status.status == 200{
                    if Status.message == "كود التحقق خاطئ"{
                        self.showAlert(title: "error", message: "Verification code is wrong".localized)
                    }else{
                        let mainVC = LoginVC()
                        UIApplication.shared.keyWindow?.rootViewController = mainVC.navigationController()
                    }

                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
}
