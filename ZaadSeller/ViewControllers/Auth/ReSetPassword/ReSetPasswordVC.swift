//
//  ReSetPasswordVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/3/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class ReSetPasswordVC: SuperViewController {
    
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfNewPassword: UITextField!
    @IBOutlet weak var imgShow: UIImageView!
    @IBOutlet weak var imgShow2: UIImageView!

    
    var username:String?
    var code:String?
    var isShowPassword = true
    var isShow2Password = true

    
    override func viewDidLoad() {
        super.viewDidLoad()
        tfPassword.isSecureTextEntry = true
        imgShow.image = UIImage(named: "unshow")
        tfNewPassword.isSecureTextEntry = true
        imgShow2.image = UIImage(named: "unshow")

    }
    
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    
    @IBAction func didTab_SeePassword(_ sender: UIButton) {
           if sender.tag == 1{
               if(isShowPassword == true) {
                   tfPassword.isSecureTextEntry = false
                   imgShow.image = UIImage(named: "pass")
               } else {
                   tfPassword.isSecureTextEntry = true
                   imgShow.image = UIImage(named: "unshow")
               }
               
               isShowPassword = !isShowPassword
           }else{
               if(isShow2Password == true) {
                   tfNewPassword.isSecureTextEntry = false
                   imgShow2.image = UIImage(named: "pass")
               } else {
                   tfNewPassword.isSecureTextEntry = true
                   imgShow2.image = UIImage(named: "unshow")
               }
               isShow2Password = !isShow2Password
           }
           
           
       }
    
    @IBAction func didTab_Save(_ sender: Any) {
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        guard let password = self.tfPassword.text, !password.isEmpty else{
            self.showAlert(title: "error".localized, message: "Password cannot be empty".localized)
            return
        }
        
        if password.count < 6 {
            showAlert(title: "error".localized, message: "Password must be at least 6 characters long".localized)
            return
        }
        
        var parameters: [String: Any] = [:]
        parameters["username"] = self.username ?? ""
        parameters["code"] = self.code ?? ""
        parameters["password"] = self.tfPassword.text ?? ""
        parameters["password_confirmation"] = self.tfNewPassword.text ?? ""
        
        _ = WebRequests.setup(controller: self).prepare(query: "auth/reset-password", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }else if Status.status == 200{
                    self.showAlert(title: "Successfully".localized, message:"Password has been modified".localized)
                    let mainVC = LoginVC()
                    UIApplication.shared.keyWindow?.rootViewController = mainVC.navigationController()
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
        }
        
    }
    
    
}
