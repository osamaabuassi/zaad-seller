//
//  ForgatePasswordVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/3/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class ForgatePasswordVC: SuperViewController {
    
    @IBOutlet weak var tfEmail: UITextField!
    
    var forgatepassword:ForgtePasswoedStruct?
    var username:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tfEmail.text = self.username ?? ""
        
    }
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    
    @IBAction func didTab_Send(_ sender: Any) {
        
        guard Helper.isConnectedToNetwork() else {
                  self.showAlert(title: "".localized, message: "There is no internet connection".localized)
                  return }
        
        guard let email = self.tfEmail.text, !email.isEmpty else{
            self.showAlert(title: "error".localized, message: "Username cannot be empty".localized)
            return
        }
        
        var parameters: [String: Any] = [:]
        parameters["username"] = self.tfEmail.text ?? ""
        
        _ = WebRequests.setup(controller: self).prepare(query: "auth/forgot-password", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(ForgatePasswordObject.self, from: response.data!)
                self.forgatepassword = Status.data
                let vc:ReVirefactionCodeVC = ReVirefactionCodeVC.loadFromNib()
//                vc.username = self.forgatepassword?.email
                vc.username = self.tfEmail.text ?? ""
                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)

            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
            
            
        }
        
    }
    
    
    
}
