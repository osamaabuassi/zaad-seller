//
//  ReVirefactionCodeVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/3/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class ReVirefactionCodeVC: SuperViewController {
    
    @IBOutlet weak var tfCode: UITextField!
    
    var username:String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    
    @IBAction func didTab_Send(_ sender: Any) {
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        guard let code = self.tfCode.text, !code.isEmpty else{
            self.showAlert(title: "".localized, message: "Please type the code sent".localized)
            return
        }
        
        var parameters: [String: Any] = [:]
        parameters["username"] = self.username ?? ""
        parameters["code"] = self.tfCode.text ?? ""
        
        
        _ = WebRequests.setup(controller: self).prepare(query: "auth/verify-code", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }else if Status.status == 200  {
                    let vc:ReSetPasswordVC = ReSetPasswordVC.loadFromNib()
                    vc.code = self.tfCode.text
                    vc.username = self.username ?? ""
                    vc.modalPresentationStyle = .fullScreen
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
    }
    
}
