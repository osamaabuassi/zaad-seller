//
//  LoginVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/3/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class LoginVC: SuperViewController {
    
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var imgShow: UIImageView!    
    @IBOutlet weak var passButton: UIButton!
    
    var isShowPassword = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.isDark = false
        tfPassword.isSecureTextEntry = true
        imgShow.image = UIImage(named: "unshow")
        self.navigationController?.setNeedsStatusBarAppearanceUpdate()
        #if DEBUG
        //live
         self.tfEmail.text  = "hasan5_007@hotmail.com"
         self.tfPassword.text = "123123"
//        self.tfEmail.text  = "zaadtest2020@zaad.om"
//        self.tfPassword.text = "Zaadtest@2020"
         
//
        // test
//       self.tfEmail.text  = "hasan5_007@hotmail.com"
//       self.tfPassword.text = "987654321"

        #endif
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }

    func getToken(){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        let token =  UserDefaults.standard.string(forKey: "token")
        var parameters: [String: Any] = [:]
        parameters["token"] = token
        
        _ = WebRequests.setup(controller: self).prepare(query: "notifications/token", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
        }
    }
    
    @IBAction func didTab_SeePassword(_ sender: Any) {
        
        if(isShowPassword == true) {
            tfPassword.isSecureTextEntry = false
            imgShow.image = UIImage(named: "pass")
        } else {
            tfPassword.isSecureTextEntry = true
            imgShow.image = UIImage(named: "unshow")
        }
        
        isShowPassword = !isShowPassword
        
    }
    
    @IBAction func didTab_Skip(_ sender: Any) {
        let mainVC = SettingsControlVC()
        UIApplication.shared.keyWindow?.rootViewController = mainVC.navigationController()
    }
    
    @IBAction func didTab_login(_ sender: Any) {
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        guard let mobile = self.tfEmail.text, !mobile.isEmpty else{
            self.showAlert(title: "error".localized, message: "Username cannot be empty".localized)
            return
        }
        guard let password = self.tfPassword.text, !password.isEmpty else{
            self.showAlert(title: "error".localized, message: "Password cannot be empty".localized)
            return
        }
        
        if password.count < 6 {
            showAlert(title: "error".localized, message: "Password must be at least 6 characters long".localized)
            return
        }
        
        var parameters: [String: Any] = [:]
        parameters["username"] = self.tfEmail.text ?? ""
        parameters["password"] = self.tfPassword.text ?? ""
                
        _ = WebRequests.setup(controller: self).prepare(query: "auth/login", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(UserObject.self, from: response.data!)
                CurrentUser.userInfo = Status.data
                if CurrentUser.userInfo != nil{
                    self.getToken()
                    let vc:TTabBarViewController = AppDelegate.sb_main.instanceVC()
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                }
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
    }
    
    
    @IBAction func didTab_ForgatePassword(_ sender: Any){
        
        let vc:ForgatePasswordVC = ForgatePasswordVC.loadFromNib()
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func didTab_Register(_ sender: Any){
        
      let vc:SubscriptionsVC = SubscriptionsVC.loadFromNib()
        vc.isFromRegister = true
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)

//        let vc:RegsiterVC = RegsiterVC.loadFromNib()
//        vc.modalPresentationStyle = .fullScreen
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
