//
//  SettingsControlVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/4/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit


class SettingsControlVC: SuperViewController {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblStatusLog: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblversion: UILabel!
    
    
   // @IBOutlet weak var vService: UIView!
    @IBOutlet weak var vProudect: UIView!
    @IBOutlet weak var vDescount: UIView!
    @IBOutlet weak var vSettingService: UIView!
    
    enum SocialNetwork {
        case Facebook, Twitter, Instagram
        func url() -> SocialNetworkUrl {
            switch self {
            case .Facebook: return SocialNetworkUrl(scheme: "fb://profile?id=ZaadOman", page: "https://www.facebook.com/ZaadOman")
            case .Twitter: return SocialNetworkUrl(scheme: "twitter:///user?screen_name=ZaadOman", page: "https://twitter.com/ZaadOman")
            case .Instagram: return SocialNetworkUrl(scheme: "instagram://user?username=ZaadOman", page:"https://www.instagram.com/ZaadOman")
                
            }
        }
        func openPage() {
            self.url().openPage()
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if CurrentUser.userInfo != nil {
            if CurrentUser.userInfo?.user?.store?.activityType?.id == 1 {
                //Prodect
              //  vProudect.isHidden = true
                vDescount.isHidden = false
               // vService.isHidden = true
                vSettingService.isHidden = false
            }
            else{
                //Service
                vSettingService.isHidden = true
               // vService.isHidden = false
                vDescount.isHidden = true
              //  vProudect.isHidden = true
               

            }
        }
        if let text = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            print(text)
            self.lblversion.text =  "Application version".localized + " " + "(\(text))"
        }
        //        if let text = Bundle.main.infoDictionary?["NSHumanReadableCopyright"] as? String {
        //            print(text)
        //        }
        if CurrentUser.userInfo == nil{
            self.lblStatusLog.text = "Sign in".localized
            self.lblName.text =  "Visitor".localized
        }else{
            self.lblStatusLog.text = "Logoff".localized
            self.lblName.text =  "Welcome".localized + " " + (CurrentUser.userInfo?.user?.name ?? "")
            self.imgUser.sd_custom(url: CurrentUser.userInfo?.user?.store?.logo ?? "")
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    
    func getToken(){
        
        //        let token =  UserDefaults.standard.string(forKey: "token")
        var parameters: [String: Any] = [:]
        parameters["token"] = "token"
        
        _ = WebRequests.setup(controller: self).prepare(query: "notifications/token", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
        }
    }
    @IBAction func bts_SocialMedia(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            SocialNetwork.Facebook.openPage()
        case 1:
            SocialNetwork.Twitter.openPage()
        case 2:
            SocialNetwork.Instagram.openPage()
        default:
            break
        }
    }
    
    @IBAction func bts_Profile(_ sender: UIButton) {
        if sender.tag == 16{
            if CurrentUser.userInfo != nil{
                let vc:StoresVC = StoresVC.loadFromNib()
                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let mainVC = LoginVC()
                UIApplication.shared.keyWindow?.rootViewController = mainVC.navigationController()
            }
        }
        
        if sender.tag == 0{
            if CurrentUser.userInfo != nil{
                let vc:AccountSettingsVC = AccountSettingsVC.loadFromNib()
                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let mainVC = LoginVC()
                UIApplication.shared.keyWindow?.rootViewController = mainVC.navigationController()
            }
        }
        if sender.tag == 14{
            if CurrentUser.userInfo != nil{
                let vc:AddStoreVC = AddStoreVC.loadFromNib()
                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let mainVC = LoginVC()
                UIApplication.shared.keyWindow?.rootViewController = mainVC.navigationController()
            }
        }
//        if sender.tag == 15{
//            if CurrentUser.userInfo != nil{
//                let vc:ServicesVC = ServicesVC.loadFromNib()
//                vc.modalPresentationStyle = .fullScreen
//                self.navigationController?.pushViewController(vc, animated: true)
//            }else{
//                let mainVC = LoginVC()
//                UIApplication.shared.keyWindow?.rootViewController = mainVC.navigationController()
//            }
//        }
        
        if sender.tag == 1{
            if CurrentUser.userInfo != nil{
                let vc:ProfileVC = ProfileVC.loadFromNib()
                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let mainVC = LoginVC()
                UIApplication.shared.keyWindow?.rootViewController = mainVC.navigationController()
            }
        }
        if sender.tag == 2{
            if CurrentUser.userInfo != nil{
                let vc:MainPayVC = MainPayVC.loadFromNib()
                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let mainVC = LoginVC()
                UIApplication.shared.keyWindow?.rootViewController = mainVC.navigationController()
            }
        }
        if sender.tag == 3{
            if CurrentUser.userInfo != nil{
                let vc:ShippingSettingsVC = ShippingSettingsVC.loadFromNib()
                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let mainVC = LoginVC()
                UIApplication.shared.keyWindow?.rootViewController = mainVC.navigationController()
            }
        }
        if sender.tag == 4{
            if CurrentUser.userInfo != nil{
                let vc:FinancialPaymentsVC = FinancialPaymentsVC.loadFromNib()
                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let mainVC = LoginVC()
                UIApplication.shared.keyWindow?.rootViewController = mainVC.navigationController()
            }
        }
        
        if sender.tag == 5{
            if CurrentUser.userInfo != nil{
                let vc:DisputesVC = DisputesVC.loadFromNib()
                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let mainVC = LoginVC()
                UIApplication.shared.keyWindow?.rootViewController = mainVC.navigationController()
            }
        }
        
        if sender.tag == 12{
            if CurrentUser.userInfo != nil{
                let vc:SubscriptionDeatilesVC = SubscriptionDeatilesVC.loadFromNib()
                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let mainVC = LoginVC()
                UIApplication.shared.keyWindow?.rootViewController = mainVC.navigationController()
            }
        }
        
        if sender.tag == 6{
            if CurrentUser.userInfo != nil{
                let vc:CapounVC = CapounVC.loadFromNib()
                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let mainVC = LoginVC()
                UIApplication.shared.keyWindow?.rootViewController = mainVC.navigationController()
            }
        }
        if sender.tag == 7{
            if CurrentUser.userInfo != nil{
                let vc:OffersVC = OffersVC.loadFromNib()
                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let mainVC = LoginVC()
                UIApplication.shared.keyWindow?.rootViewController = mainVC.navigationController()
            }
        }
        if sender.tag == 8{
            if CurrentUser.userInfo != nil{
                let vc:CommonQuestionsVC = CommonQuestionsVC.loadFromNib()
                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let mainVC = LoginVC()
                UIApplication.shared.keyWindow?.rootViewController = mainVC.navigationController()
            }
        }
        if sender.tag == 9{
            if CurrentUser.userInfo != nil{
                let vc:HowUseVC = HowUseVC.loadFromNib()
                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let mainVC = LoginVC()
                UIApplication.shared.keyWindow?.rootViewController = mainVC.navigationController()
            }
        }
        if sender.tag == 10{
            if CurrentUser.userInfo != nil{
                let vc:CallUsVC = CallUsVC.loadFromNib()
                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let mainVC = LoginVC()
                UIApplication.shared.keyWindow?.rootViewController = mainVC.navigationController()
            }
        }
        if sender.tag == 11{
            if CurrentUser.userInfo == nil{
                let mainVC = LoginVC()
                UIApplication.shared.keyWindow?.rootViewController = mainVC.navigationController()
            }else{
                CurrentUser.userInfo = nil
                //                self.getToken()
                let mainVC = LoginVC()
                UIApplication.shared.keyWindow?.rootViewController = mainVC.navigationController()
            }
            
        }
        // MARK: Shipping
        if sender.tag == 13{
            if CurrentUser.userInfo != nil{
                let vc:ShippingVC = ShippingVC.loadFromNib()
                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                CurrentUser.userInfo = nil
                let mainVC = LoginVC()
                UIApplication.shared.keyWindow?.rootViewController = mainVC.navigationController()
            }
            
        }
        
        // MARK: Application language
        if sender.tag == 20{
            ChangeLanguage()
            
        }
        
    }
    
    func ChangeLanguage(){
        var actions: [(String, UIAlertAction.Style)] = []
        if(MOLHLanguage.currentAppleLanguage() == "ar"){
            actions.append(("English", UIAlertAction.Style.default))
        }
        if(MOLHLanguage.currentAppleLanguage() == "en"){
            actions.append(("العربية", UIAlertAction.Style.default))
        }
        
        actions.append(("Cancel", UIAlertAction.Style.cancel))
        
        showActionsheet(viewController: self, title: "Alert!".localized, message: "The language settings for the application will be changed.".localized, actions: actions) { (index) in
            print("call action \(index)")
            switch index{
            case 0:
                if(MOLHLanguage.currentAppleLanguage() == "ar"){
                    MOLH.setLanguageTo("en")
                    MOLH.reset(transition: .transitionCrossDissolve, duration: 0.25)
                }else{
                    MOLH.setLanguageTo("ar")
                    MOLH.reset(transition: .transitionCrossDissolve, duration: 0.25)
                }
            default:
                break
            }
            
        }
        
    }

    
    
    @IBAction func bt_terms(_ sender: Any) {
        let vc:InfoVC = InfoVC.loadFromNib()
        vc.idInfo = 204
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func bt_about(_ sender: Any) {
        let vc:InfoVC = InfoVC.loadFromNib()
        vc.idInfo = 200
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func bt_poalicy(_ sender: Any) {
        let vc:InfoVC = InfoVC.loadFromNib()
        vc.idInfo = 202
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func bt_poalicyreturn(_ sender: Any) {
        let vc:InfoVC = InfoVC.loadFromNib()
        vc.idInfo = 203
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func bt_addingproductTerms(_ sender: Any) {
        let vc:InfoVC = InfoVC.loadFromNib()
        vc.idInfo = 205
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}
