//
//  PaySeconds.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/4/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class PaySeconds: SuperViewController {
    
    
    @IBOutlet weak var tfIdentifier: UITextField!
    @IBOutlet weak var tfEncryptionkey: UITextField!
    
    var thawani:PaythawaniData?


    override func viewDidLoad() {
        super.viewDidLoad()
        getthawani()
    }
    
    func getthawani(){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "thawani", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(PaythawaniObject.self, from: response.data!)
                self.thawani = Status.data
                self.tfIdentifier.text = self.thawani?.apiKey ?? ""
                self.tfEncryptionkey.text = self.thawani?.publicKey ?? ""
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
        }
    }
    
    @IBAction func didTab_Save(_ sender: Any) {
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        var parameters: [String: Any] = [:]
            parameters["api_key"] = self.tfIdentifier.text ?? ""
            parameters["public_key"] = self.tfEncryptionkey.text ?? ""
            
            _ = WebRequests.setup(controller: self).prepare(query: "thawani", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self.showAlert(title: "Error".localized, message:Status.message!)
                        return
                    }else if Status.status == 200{
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                }catch let jsonErr {
                    print("Error serializing respone json", jsonErr)
                }
                
            }
    }
}


