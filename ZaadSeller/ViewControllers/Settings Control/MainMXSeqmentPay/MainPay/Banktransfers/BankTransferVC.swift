//
//  BankTransferVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/4/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class BankTransferVC: UIViewController {
    
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfBankName: UITextField!
    @IBOutlet weak var tfBranch: UITextField!
    @IBOutlet weak var tfNumberAccount: UITextField!

    var bankData:PayBankData?
    var bank:BankData?
    var bankitems = [Bank]()
    var selectedBankId:Int?


    override func viewDidLoad() {
        super.viewDidLoad()
        getBank()
        getBankitems()
   
    }
    
    
    func getBankitems(){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        _ = WebRequests.setup(controller: self).prepare(query: "banks", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(BankObject.self, from: response.data!)
                self.bank = Status.data
                self.bankitems = self.bank!.resources!
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            
        }

    }
    
    func getBank(){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "bank-account", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(PayBankObject.self, from: response.data!)
                self.bankData = Status.data
                self.tfName.text = self.bankData?.name ?? ""
                self.tfBankName.text = self.bankData?.bankName ?? ""
                self.tfBranch.text = self.bankData?.branch ?? ""
                self.tfNumberAccount.text = self.bankData?.accountNo ?? ""

            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
        }
    }
    
    @IBAction func didTab_BankName(_ sender: UIButton) {

        ActionSheetStringPicker.show(withTitle: "The bank".localized, rows: self.bankitems.map { $0.name as Any }
            , initialSelection: 0, doneBlock: {
                picker, value, index in
                if let Value = index {
                    self.tfBankName.text = Value as? String
                    self.selectedBankId = self.bankitems[value].id ?? 0
                }
                
                return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)

         }

    @IBAction func didTab_Save(_ sender: Any) {
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        var parameters: [String: Any] = [:]
        parameters["name"] = self.tfName.text ?? ""
        parameters["bank_name"] = self.tfBankName.text ?? ""
        parameters["branch"] = self.tfBranch.text ?? ""
        parameters["account_no"] = self.tfNumberAccount.text ?? ""
        
        _ = WebRequests.setup(controller: self).prepare(query: "bank-account", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }else if Status.status == 200{
                    self.navigationController?.popViewController(animated: true)
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
        }
      }
   

}
