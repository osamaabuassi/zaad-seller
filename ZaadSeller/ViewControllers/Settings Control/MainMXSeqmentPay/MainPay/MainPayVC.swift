//
//  MainPayVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/5/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import MXSegmentedPager

class MainPayVC: BaseViewController {
    
    
    var arrayView = [String]()
    var arrayViewController = [UIViewController]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if MOLHLanguage.isRTLLanguage() {
            arrayView = arrayView.reversed()
            arrayViewController = arrayViewController.reversed()
        }
        setUpSegmentation()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }

    
}

extension MainPayVC {
    func setUpSegmentation (){

    if MOLHLanguage.isRTLLanguage(){
        self.segmentedPager.pager.showPage(at:self.arrayView.count - 1, animated: false)
    }
    DispatchQueue.main.async {
        if MOLHLanguage.isRTLLanguage(){
            self.segmentedPager.pager.showPage(at:self.arrayView.count - 1 , animated: false)
//            self.segmentedPager.segmentedControl.selectedSegmentIndex = self.arrayView.count - 1
    // segmentedPager.pager.reloadData()
        }
    }
    
    segmentedPager.backgroundColor = UIColor.white
    segmentedPager.segmentedControl.backgroundColor = UIColor.white
    segmentedPager.parallaxHeader.view = nil
    segmentedPager.parallaxHeader.mode = .bottom
    segmentedPager.parallaxHeader.height = 0
    segmentedPager.parallaxHeader.minimumHeight = 0
        
        
    // Segmented Control customization
    self.segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocation.down
    segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)]
    
    segmentedPager.segmentedControl.selectionIndicatorColor = "FF6A2A".color
    segmentedPager.segmentedControl.selectionIndicatorHeight = 3
    
        self.segmentedPager.segmentedControl.titleTextAttributes = [NSAttributedString.Key.font: UIFont.FFShamelFamilySemiRoundBook(ofSize: 17), NSAttributedString.Key.foregroundColor :  #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)]
        
        
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {
        
        
        switch index {
        case 0:
            return "Bank transfers".localized

        case 1:
            return "Pay using Thawani".localized

        default:
            break
        }
        return "Bank transfers".localized

    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, didScrollWith parallaxHeader: MXParallaxHeader) {
    }
    
    
    override func numberOfPages(in segmentedPager: MXSegmentedPager) -> Int {
        return 2
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, viewControllerForPageAt index: Int) -> UIViewController {
        switch index {
            case 0:
                return BankTransfer()

            case 1:
                return PaySecound()

            default:
                break
            }
         return BankTransfer()

        
    }
  
    fileprivate  func BankTransfer() ->BankTransferVC{
        let vc:BankTransferVC = BankTransferVC.loadFromNib()
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    
    fileprivate  func PaySecound() ->PaySeconds{
         let vc:PaySeconds = PaySeconds.loadFromNib()
         addChild(vc)
         vc.didMove(toParent: self)
         return vc
     }
    
    
}


