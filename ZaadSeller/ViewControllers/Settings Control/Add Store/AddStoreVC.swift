//
//  AddStoreVC.swift
//  ZaadSeller
//
//  Created by osamaaassi on 1/22/21.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import CoreLocation

class AddStoreVC: SuperViewController, cityDelgate, MapCoordinatesDelgate {
    func SelectedDone(Lat: Double, Lon: Double, address: String) {
        self.lat = "\(Lat)"
        self.Lon = "\(Lon)"
        // self.lbladdress.text = address
        self.getAddressFromLatLon(pdblLatitude:self.lat , withLongitude: self.Lon)
        
        
    }
    
    func SelectedDone(name: String, id: Int) {
        self.tfCity.text = name
        self.selectedCityID = id
    }
    
    
    @IBOutlet weak var tfStore: UITextField!
    @IBOutlet weak var tfOwner: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfCountry: UITextField!
    @IBOutlet weak var tfCity: UITextField!
    @IBOutlet weak var tfPhone: UITextField!
    @IBOutlet weak var tfMobile: UITextField!
    @IBOutlet weak var tfActivity: UITextField!
    @IBOutlet weak var tfTypeActivity: UITextField!
    @IBOutlet weak var tfSize: UITextField!
    @IBOutlet weak var lbladdress: UILabel!
    @IBOutlet weak var tfBusinessRecordNo: UITextField!
    @IBOutlet weak var tfBusinessName: UITextField!
    
    @IBOutlet weak var btLogo: UIButton!
    @IBOutlet weak var btMap: UIButton!
    @IBOutlet weak var imgStore: UIImageView!
    
    @IBOutlet weak var serviceType: UILabel!
    @IBOutlet weak var TypeService: UIView!
    @IBOutlet weak var tfTypeService: UITextField!
    
    @IBOutlet weak var tfActivityType: UITextField!
    @IBOutlet weak var vActivityType: UIView!
    @IBOutlet weak var vType: UIView!
    @IBOutlet weak var vContry: UIView!
    
    
    //EditStore
    var id:Int?
    var isFromEdit:Bool = false
    var storesData : StoresData?
    
    //Country
    var country = [Country]()
    var countryData:CountryData?
    var selectedCountryID:Int?
    
    //City
    var city = [City]()
    var cityData:CityData?
    var selectedCityID:Int?
    
    //Size
    var size = ["Small","Big"]
    var selectedSize:Int?
    
    //Location
    var Lon:String = "0.0"
    var lat:String = "0.0"
    var address:String?
    
    
    //Image
    var selectImage:Bool? = false
    var imagePicker: ImagePicker!
    
    //Type Store
    var ActivitType = [ActivityType]()
    var selectedActivityID:Int?
    // var typeActivt : Int!
    
    
    var Activity = [Category]()
    var ActivityData:CategroiesData?
    var selectedTypeActivityID:Int?
    
    //children
    var typeServices = [Category]()
    var childre = [Children]()
    var selectedChildrenID:Int?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        TypeService.isHidden = true
        getCountry()
        getCity()
        getTypeActivity()
        
        if isFromEdit{
            showStoreData()
            vActivityType.isHidden = false
            vType.isHidden = true
            vContry.isHidden = true
        }
         
        
      
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        
        
        self.tfOwner.text = CurrentUser.userInfo?.user?.username ?? ""
        self.tfEmail.text = CurrentUser.userInfo?.user?.email ?? ""
        self.tfMobile.text = CurrentUser.userInfo?.user?.mobile ?? ""
        self.tfPhone.text = CurrentUser.userInfo?.user?.phone ?? ""
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    
  
    @IBAction func isEdit(_ sender:UIButton){
        vType.isHidden = false
    }
    func showStoreData(){
        self.tfStore.text = storesData?.name ?? ""
        self.tfOwner.text = self.storesData?.owner ?? ""
        self.tfEmail.text = self.storesData?.email ?? ""
        self.tfMobile.text = self.storesData?.mobile ?? ""
        self.tfPhone.text = self.storesData?.phone ?? ""
        self.lbladdress.text = self.storesData?.address ?? ""
        self.tfBusinessName.text = self.storesData?.businessName ?? ""
        self.tfBusinessRecordNo.text = self.storesData?.businessRecordNo ?? ""
        
        if self.storesData?.logo != nil {
        self.imgStore.sd_custom(url: self.storesData?.logo ?? "")
        }else{
            self.imgStore.image =  UIImage(named: "loginlogo")
        }
       
        self.selectImage = true
        
        self.tfBusinessName.text = self.storesData?.businessName ?? ""
        
        self.tfActivityType.text = self.storesData?.activity?.title ?? ""
        self.selectedActivityID = self.storesData?.activityType?.id ?? 0
        
        self.lat = self.storesData?.addressMap?.lat ?? "0"
        self.Lon = self.storesData?.addressMap?.lng ?? "0"
        
        if self.storesData?.size?.description == "1"{
            self.tfSize.text = "Big".localized
            self.selectedSize = 1
        }else{
            self.tfSize.text = "Small".localized
            self.selectedSize = 2
        }
        
        if self.storesData?.activity?.id  == 1 {
            self.selectedTypeActivityID = self.storesData?.activity?.id ?? 0
        }
        else{
            self.selectedChildrenID = self.storesData?.activity?.id ?? 0
        }
        
        
        
    }
    
    @IBAction func didTab_Country(_ sender: UIButton) {
        
        ActionSheetStringPicker.show(withTitle: "Country".localized, rows: self.country.map { $0.title as Any }
            , initialSelection: 0, doneBlock: {
                picker, value, index in
                if let Value = index {
                    self.tfCountry.text = Value as? String
                    self.selectedCountryID = self.country[value].id ?? 0
                    
                }
                
                return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    
    
    @IBAction func didTab_City(_ sender: UIButton) {
        
        guard let CountryID = self.selectedCountryID, CountryID != 0 else{
            self.showAlert(title: "".localized, message: "Choose the country first".localized)
            return
        }
        let vc:CitysVC = CitysVC.loadFromNib()
        vc.Countryid = CountryID
        vc.citydelegte = self
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
    @IBAction func didTab_taypActivity(_ sender: UIButton) {
        
        self.selectedTypeActivityID = 0
        self.tfTypeActivity.text = ""
        
        ActionSheetStringPicker.show(withTitle: "Activity type".localized, rows: self.ActivitType.map { $0.title as Any }
            , initialSelection: 0, doneBlock: {
                picker, value, index in
                if let Value = index {
                    self.tfActivity.text = Value as? String
                    self.selectedActivityID = self.ActivitType[value].id ?? 0
                    
                    if self.selectedActivityID == 1 {
                        self.serviceType.text = "Select activity".localized
                        self.getActivity(typeActivt: 1)
                        self.TypeService.isHidden = true
                    }
                    else{
                        self.serviceType.text = "Select service".localized
                        self.getActivity(typeActivt: 2)
                        self.getChildren(typeActivt : 2)
                    }
                }
                return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    
    @IBAction func didTab_Activity(_ sender: UIButton) {
        guard (selectedActivityID != nil) else {
            self.showAlert(title: "error".localized, message: "The type of activity must be select".localized)
            return
        }
        ActionSheetStringPicker.show(withTitle: self.serviceType.text, rows: self.Activity.map { $0.title as Any }
            , initialSelection: 0, doneBlock: {
                picker, value, index in
                if let Value = index {
                    self.tfTypeActivity.text = Value as? String
                    self.selectedTypeActivityID = self.Activity[value].id ?? 0
                    
                    if self.selectedActivityID == 2 {
                        self.TypeService.isHidden = false
                    }
                    
                    
                    if let object = self.typeServices.filter({ $0.id == self.selectedTypeActivityID }).first {
                        self.childre = object.children!
                        
                    }
                }
                return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    
    @IBAction func didTab_Services(_ sender: UIButton) {
        guard (selectedActivityID != nil) else {
            self.showAlert(title: "error".localized, message: "The service type must be specified".localized)
            return
        }
        ActionSheetStringPicker.show(withTitle: self.serviceType.text, rows: self.childre.map { $0.title as Any }
            , initialSelection: 0, doneBlock: {
                picker, value, index in
                if let Value = index {
                    self.tfTypeService.text = Value as? String
                    self.selectedChildrenID = self.childre[value].id ?? 0
                    
                }
                return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    
}

extension AddStoreVC {
    
    @IBAction func addStore (_ sender: UIButton) {
        var parameters: [String: String] = [:]

        guard let name = self.tfStore.text, !name.isEmpty else{
            self.showAlert(title: "error".localized, message: "Store name cannot be empty".localized)
            return
        }
        
        guard let owner = self.tfOwner.text, !owner.isEmpty else{
            self.showAlert(title: "error".localized, message: "Username cannot be empty".localized)
            return
        }
        guard let email = self.tfEmail.text, !email.isEmpty else{
            self.showAlert(title: "error".localized, message: "Email cannot be empty".localized)
            return
        }
        
        let phone = self.tfPhone.text
        
        guard let mobile = self.tfMobile.text, !mobile.isEmpty else{
            self.showAlert(title: "error".localized, message: "Mobile cannot be empty".localized)
            return
        }
        if !isFromEdit{
        guard let country = self.selectedCountryID?.description, !country.isEmpty else{
            self.showAlert(title: "error".localized, message: "The country must be selected".localized)
            return
        }
        
        guard let city = self.selectedCityID?.description, !country.isEmpty else{
            self.showAlert(title: "error".localized, message: "City must be selected".localized)
            return
        }
            parameters["country_id"]  = country
            parameters["city_id"]  = city
        }
        else{
            parameters["country_id"]  = CurrentUser.userInfo?.user?.country?.id?.description ?? "1"
            parameters["city_id"] = CurrentUser.userInfo?.user?.country?.id?.description ?? "1"
        }
        guard let size = self.selectedSize?.description, !size.isEmpty else{
            self.showAlert(title: "error".localized, message: "Store size must be selected".localized)
            return
        }
        guard let businessRecordNo = self.tfBusinessRecordNo.text, !businessRecordNo.isEmpty else{
            self.showAlert(title: "error".localized, message: "The CR number cannot be empty".localized)
            return
        }
        guard let businessName = self.tfBusinessName.text, !businessName.isEmpty else{
            self.showAlert(title: "error".localized, message: "Trade name cannot be empty".localized)
            return
        }
        
        guard let address = self.lbladdress.text, !address.isEmpty else{
            self.showAlert(title: "error".localized, message: "Location must be determined".localized)
            return
        }
        guard self.selectImage ?? false else {
            self.showAlert(title: "error".localized, message: "Store image must be selected".localized)
            return
        }
        
        let jsonString = "{\"lat\":\"\(self.lat)\",\"lng\":\"\(self.Lon)\"}"
        
        parameters["name"]  = name
        parameters["email"] = email
        parameters["mobile"]  = mobile
        parameters["phone"]  = phone ?? ""
        parameters["size"]  =  size
        parameters["business_name"]  = businessName
        parameters["business_record_no"]  =  businessRecordNo
        parameters["address"]  =  address
        parameters["address_map"]  = jsonString
        parameters["owner"]  =  owner
        parameters["type_activity"]  = self.selectedActivityID!.description
        
        

        
        
        if selectedActivityID == 1 {
            parameters["activity_id"]  = self.selectedTypeActivityID?.description ?? ""
        }
        if selectedActivityID == 2 {
            parameters["activity_id"]  = self.selectedChildrenID?.description ?? ""
        }
        
        
        //Fixed item
        parameters["inventory_no"]  =  "1"
        parameters["tax_no"]  =  "1"
        
        let imageData = (imgStore.image) ?? UIImage()
        if isFromEdit {
            _ = WebRequests.sendPostMultipartRequestWithImgParam(url: TAConstant.APIBaseURL + "stores/\(id ?? 0)", parameters:parameters,img: imageData, withName: "logo", completion: { (response, error) in
                self.hideIndicator()
                 do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self.showAlert(title: "error".localized, message:Status.message!)
                        return
                    }else{
                        self.showAlert(title: "".localized, message: Status.message ?? "")
                        NotificationCenter.default.post(name: Notification.Name("UpdateStore"), object: nil)
                        self.navigationController?.popViewController(animated: true)
                        
                        
                    }
                    
                }catch let jsonErr {
                    print("Error serializing respone json", jsonErr)
                }
            }
            )
            
        }
        else{
            _ = WebRequests.sendPostMultipartRequestWithImgParam(url: TAConstant.APIBaseURL + "stores", parameters:parameters,img: imageData, withName: "logo", completion: { (response, error) in
                self.hideIndicator()
                
                do {
                    
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self.showAlert(title: "error".localized, message:Status.message!)
                        return
                    }else{
                        self.showAlert(title: "".localized, message: Status.message ?? "")
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
            }
            )
        }
        
    }
}


extension AddStoreVC{
    func getCountry(){
        
        _ = WebRequests.setup(controller: self).prepare(query: "countries", method: HTTPMethod.get,isAuthRequired: false).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(CountryObject.self, from: response.data!)
                self.countryData = Status.data!
                self.country = self.countryData!.resources!
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
        }
    }
    
    
    func getCity(){
        
        _ = WebRequests.setup(controller: self).prepare(query: "cities?country_id=\(selectedCountryID ?? 0)", method: HTTPMethod.get,isAuthRequired: false).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(CityObject.self, from: response.data!)
                self.cityData = Status.data!
                self.city = self.cityData!.resources!
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
        }
    }
    
    
    @IBAction func didTab_Size(_ sender: UIButton) {
        
        ActionSheetStringPicker.show(withTitle: "The size".localized, rows: self.size.map { $0 as Any }
            , initialSelection: 0, doneBlock: {
                picker, value, index in
                if let Value = index {
                    self.tfSize.text = Value as? String
                    if self.tfSize.text == "Big".localized{
                        self.selectedSize = 1
                    }else{
                        self.selectedSize = 2
                    }
                }
                return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    
    @IBAction func didTab_Map(_ sender: Any) {
        let vc:MapVC = MapVC.loadFromNib()
        vc.MapDelegate = self
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        
        let lat: Double = Double("\(pdblLatitude)")!
        let lon: Double = Double("\(pdblLongitude)")!
        
        
        
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, preferredLocale: Locale(identifier: MOLHLanguage.currentAppleLanguage()), completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                if let pm = placemarks{
                    
                    if pm.count > 0 {
                        let pm = placemarks![0]
                        print(pm.country as Any)
                        print(pm.locality as Any)
                        print(pm.subLocality as Any)
                        print(pm.thoroughfare as Any)
                        print(pm.postalCode as Any)
                        print(pm.subThoroughfare as Any)
                        print(pm.isoCountryCode as Any)
                        
                        
                        var addressString : String = ""
                        
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality! + ", "
                        }
                        if pm.thoroughfare != nil {
                            addressString = addressString + pm.thoroughfare! + ", "
                        }
                        if pm.locality != nil {
                            addressString = addressString + pm.locality! + ", "
                            
                        }
                        if pm.country != nil {
                            addressString = addressString + pm.country! + ", "
                        }
                        if pm.postalCode != nil {
                            addressString = addressString + pm.postalCode! + " "
                        }
                        
                        
                        self.lbladdress.text = addressString
                        
                        if addressString == ""{
                            self.address = "\(lat), \(lon)"
                            
                        }
                        
                    }
                    
                }else{
                    self.address = ""
                }
        })
        
    }
    
    func getTypeActivity(){
        
        _ = WebRequests.setup(controller: self).prepare(query: "categories/\(0)/options", method: HTTPMethod.get,isAuthRequired: false).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(AcitivityObject.self, from: response.data!)
                self.ActivitType = Status.data!
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
        
    }
    
    func getActivity(typeActivt:Int){
        
        _ = WebRequests.setup(controller: self).prepare(query: "categories/\(typeActivt)", method: HTTPMethod.get,isAuthRequired: false).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(CategroiesObject.self, from: response.data!)
                self.ActivityData = Status.data!
                self.Activity = self.ActivityData!.resources!
                
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
        
    }
    
    func getChildren(typeActivt:Int){
        _ = WebRequests.setup(controller: self).prepare(query: "categories/\(typeActivt)", method: HTTPMethod.get,isAuthRequired: false).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(CategroiesObject.self, from: response.data!)
                
                self.ActivityData = Status.data!
                
                self.typeServices = self.ActivityData!.resources!
                
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
        
    }
    
    
}

extension AddStoreVC: ImagePickerDelegate {
    @IBAction func showImagePicker(_ sender: UIButton) {
        self.imagePicker.present(from: sender)
    }
    func didSelect(image: UIImage?) {
        
        self.imgStore.image = image
        self.selectImage = true
        
    }
    
}
