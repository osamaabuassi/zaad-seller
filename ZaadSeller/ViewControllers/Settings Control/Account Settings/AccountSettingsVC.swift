//
//  AccountSettingsVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/4/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import CoreLocation

class AccountSettingsVC: SuperViewController,MapCoordinatesDelgate,cityDelgate {
    
    
    func SelectedDone(Lat: Double, Lon: Double, address: String) {
        self.lat = "\(Lat)"
        self.Lon = "\(Lon)"
        self.lbladdress.text = address
    }
    
    func SelectedDone(name: String, id: Int) {
        self.tfCity.text = name
        self.selectedCityID = id
    }
    
    
    
    @IBOutlet weak var tfStore: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfCountry: UITextField!
    @IBOutlet weak var tfCity: UITextField!
    @IBOutlet weak var tfPhone: UITextField!
    @IBOutlet weak var tfMobile: UITextField!
    @IBOutlet weak var tfTypeActivity: UITextField!
    @IBOutlet weak var tfDate: UITextField!
    @IBOutlet weak var tfSize: UITextField!
    @IBOutlet weak var lbladdress: UILabel!
    @IBOutlet weak var tfCommercial: UITextField!
    @IBOutlet weak var tfTradeName: UITextField!
    @IBOutlet weak var tfActivity: UITextField!
    @IBOutlet weak var btLogo: UIButton!
    @IBOutlet weak var btMap: UIButton!
    @IBOutlet weak var imgStore: UIImageView!
    
    var settingStore:StoreSettingData?
    var countryData:CountryData?
    var country = [Country]()
    var cityData:CityData?
    var city = [City]()
    var Activity = [Category]()
    var ActivityData:CategroiesData?
    var ActivitType = [ActivityType]()
    var selectedTypeActivityID:Int?
    var selectedActivityID:Int?
    var selectedCountryID:Int?
    var selectedCityID:Int?
    var selectedSize:Int?
    var imagePicker: ImagePicker!
    var SelectImage = false
    var image:UIImageView?
    var isEdited = false
    var Lon:String = "0.0"
    var lat:String = "0.0"
    var address:String?
    var size = ["Small","Big"]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getStore()
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        getCountry()
        getActivity()
        getTypeActivity()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        
        let lat: Double = Double("\(pdblLatitude)")!
        let lon: Double = Double("\(pdblLongitude)")!
        
        
        
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, preferredLocale: Locale(identifier: MOLHLanguage.currentAppleLanguage()), completionHandler:
                                    {(placemarks, error) in
            if (error != nil)
            {
                print("reverse geodcode fail: \(error!.localizedDescription)")
            }
            if let pm = placemarks{
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    print(pm.country as Any)
                    print(pm.locality as Any)
                    print(pm.subLocality as Any)
                    print(pm.thoroughfare as Any)
                    print(pm.postalCode as Any)
                    print(pm.subThoroughfare as Any)
                    print(pm.isoCountryCode as Any)
                    
                    
                    var addressString : String = ""
                    
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                        
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    
                    
                    self.lbladdress.text = addressString
                    
                    if addressString == ""{
                        self.address = "\(lat), \(lon)"
                        
                    }
                    
                }
                
            }else{
                self.address = ""
            }
        })
        
    }
    
    
    func getStore(){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        let id  = CurrentUser.userInfo?.user?.store?.id
        _ = WebRequests.setup(controller: self).prepare(query: "stores/\(id ?? 0)", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(StoreSettingObject.self, from: response.data!)
                self.settingStore = Status.data!
                self.tfStore.text = self.settingStore?.name ?? ""
                self.tfTradeName.text = self.settingStore?.businessName ?? ""
                self.tfCommercial.text = self.settingStore?.businessRecordNo ?? ""
                self.tfActivity.text = self.settingStore?.activityType?.title ?? ""
                self.selectedTypeActivityID = self.settingStore?.activity?.id ?? 0
                self.tfTypeActivity.text = self.settingStore?.activity?.title ?? ""
                self.selectedActivityID = self.settingStore?.activityType?.id ?? 0
                self.tfEmail.text = self.settingStore?.email ?? ""
                self.tfPhone.text = self.settingStore?.phone ?? ""
                self.tfMobile.text = self.settingStore?.mobile ?? ""
                self.imgStore.sd_custom(url: self.settingStore?.logo ?? "")
                self.tfCity.text = CurrentUser.userInfo?.user?.city?.name ?? ""
                self.tfCountry.text = CurrentUser.userInfo?.user?.country?.title ?? ""
                self.lat = "\(self.settingStore?.addressMap?.lat ?? 0.0)"
                self.Lon = "\(self.settingStore?.addressMap?.lng ?? 0.0)"
                self.selectedCountryID = CurrentUser.userInfo?.user?.country?.id ?? 0
                self.selectedCityID = CurrentUser.userInfo?.user?.city?.id ?? 0
                
                if self.settingStore?.size == 1{
                    self.tfSize.text = "Big".localized
                    self.selectedSize = 1
                }else{
                    self.tfSize.text = "Small".localized
                    self.selectedSize = 2
                    
                }
                self.getCity()
                self.getAddressFromLatLon(pdblLatitude:self.lat , withLongitude: self.Lon)
                
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
    }
    
    
    func getCountry(){
        
        _ = WebRequests.setup(controller: self).prepare(query: "countries", method: HTTPMethod.get,isAuthRequired: false).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(CountryObject.self, from: response.data!)
                self.countryData = Status.data!
                self.country = self.countryData!.resources!
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
        }
    }
    
    func getCity(){
        
        _ = WebRequests.setup(controller: self).prepare(query: "cities?country_id=\(selectedCountryID ?? 0)", method: HTTPMethod.get,isAuthRequired: false).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(CityObject.self, from: response.data!)
                self.cityData = Status.data!
                self.city = self.cityData!.resources!
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
        }
    }
    
    func getActivity(){
        
        _ = WebRequests.setup(controller: self).prepare(query: "categories/\(1)", method: HTTPMethod.get,isAuthRequired: false).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(CategroiesObject.self, from: response.data!)
                self.ActivityData = Status.data!
                self.Activity = self.ActivityData!.resources!
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
        
    }
    
    func getTypeActivity(){
        
        _ = WebRequests.setup(controller: self).prepare(query: "categories/\(0)/options", method: HTTPMethod.get,isAuthRequired: false).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(AcitivityObject.self, from: response.data!)
                self.ActivitType = Status.data!
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
        
    }
    
    
    @IBAction func didTab_Country(_ sender: UIButton) {
        
        ActionSheetStringPicker.show(withTitle: "Country".localized, rows: self.country.map { $0.title as Any }
                                     , initialSelection: 0, doneBlock: {
            picker, value, index in
            if let Value = index {
                self.tfCountry.text = Value as? String
                self.selectedCountryID = self.country[value].id ?? 0
                // self.getCity()
            }
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    @IBAction func didTab_City(_ sender: UIButton) {
        
        guard let CountryID = self.selectedCountryID, CountryID != 0 else{
            self.showAlert(title: "".localized, message: "Choose the country first".localized)
            return
        }
        
        let vc:CitysVC = CitysVC.loadFromNib()
        vc.Countryid = CountryID
        vc.citydelegte = self
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
        
        
        
    }
    func jsonString(data : Any) -> String {
        
        var jsonString = "";
        
        do {
            
            let jsonData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
        } catch {
            print(error.localizedDescription)
        }
        
        return jsonString;
    }
    
    
    @IBAction func didTab_Map(_ sender: Any) {
        let vc:MapVC = MapVC.loadFromNib()
        vc.MapDelegate = self
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func didTab_Confirm(_ sender: UIButton) {
        
        guard let name = self.tfStore.text, !name.isEmpty else{
            self.showAlert(title: "error".localized, message: "Username cannot be empty".localized)
            return
        }
        
        guard let email = self.tfEmail.text, !email.isEmpty else{
            self.showAlert(title: "error".localized, message: "Email cannot be empty".localized)
            return
        }
        
        //        guard let phone = self.tfPhone.text, !phone.isEmpty else{
        //            self.showAlert(title: "error".localized, message: "لا يمكن أن يكون الهاتف فارغًا".localized)
        //            return
        //        }
        
        let phone = self.tfPhone.text
        
        guard let mobile = self.tfMobile.text, !mobile.isEmpty else{
            self.showAlert(title: "error".localized, message: "Mobile cannot be empty".localized)
            return
        }
        guard let tax_no = self.tfCommercial.text, !tax_no.isEmpty else{
            self.showAlert(title: "error".localized, message: "The CR number cannot be empty".localized)
            return
        }
        guard let TradeName = self.tfTradeName.text, !TradeName.isEmpty else{
            self.showAlert(title: "error".localized, message: "Trade name cannot be empty".localized)
            return
        }
        
        //        guard  self.lat != "0.0"  else{
        //            self.showAlert(title: "error".localized, message: "الرجاء اختيار عنوان".localized)
        //            return
        //        }
        //
        //        guard  self.Lon != "0.0"  else{
        //            self.showAlert(title: "error".localized, message: "الرجاء اختيار عنوان".localized)
        //            return
        //        }
        
        var parameters: [String: String] = [:]
        parameters["name"]  = name
        parameters["email"] = email
        parameters["phone"]  = phone ?? ""
        parameters["mobile"]  = mobile
        parameters["business_record_no"]  = tax_no
        parameters["business_name"]  = TradeName
        parameters["country_id"]  = self.selectedCountryID?.description
        parameters["city_id"]  = self.selectedCityID?.description
        parameters["size"]  = self.selectedSize?.description
        parameters["type_activity"]  = self.selectedActivityID?.description
        parameters["activity_id"]  = self.selectedTypeActivityID?.description
        var json: [String: Any]
        
        json = ["lat":self.lat.description,"lng":self.Lon.description]
        parameters["address_map"] =  jsonString(data: json)
        print(parameters)
        
        let imageData = (imgStore.image) ?? UIImage()
        
        let id  = CurrentUser.userInfo?.user?.store?.id
        
        _ = WebRequests.sendPostMultipartRequestWithImgParam(url: TAConstant.APIBaseURL + "stores/\(id ?? 0)", parameters:parameters,img: imageData, withName: "logo", completion: { (response, error) in
            self.hideIndicator()
            
            do {
                
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }else{
                    self.navigationController?.popViewController(animated: true)
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
        )
        
    }
    @IBAction func didTab_TypeActivity(_ sender: UIButton) {
        ActionSheetStringPicker.show(withTitle: "Select activity".localized, rows: self.Activity.map { $0.title as Any }
                                     , initialSelection: 0, doneBlock: {
            picker, value, index in
            if let Value = index {
                self.tfTypeActivity.text = Value as? String
                self.selectedTypeActivityID = self.Activity[value].id ?? 0
            }
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
        
    }
    
    @IBAction func didTab_Activity(_ sender: UIButton) {
        ActionSheetStringPicker.show(withTitle: "type of activity".localized, rows: self.ActivitType.map { $0.title as Any }
                                     , initialSelection: 0, doneBlock: {
            picker, value, index in
            if let Value = index {
                self.tfActivity.text = Value as? String
                self.selectedActivityID = self.ActivitType[value].id ?? 0
            }
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
        
    }
    @IBAction func didTab_Size(_ sender: UIButton) {
        
        ActionSheetStringPicker.show(withTitle: "The size".localized, rows: self.size.map { $0 as Any }
                                     , initialSelection: 0, doneBlock: {
            picker, value, index in
            if let Value = index {
                self.tfSize.text = Value as? String
                if self.tfSize.text == "Big".localized{
                    self.selectedSize = 1
                }else{
                    self.selectedSize = 2
                }
            }
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    
}

extension AccountSettingsVC: ImagePickerDelegate {
    @IBAction func showImagePicker(_ sender: UIButton) {
        self.imagePicker.present(from: sender)
    }
    func didSelect(image: UIImage?) {
        self.imgStore.image = image
        self.SelectImage = true
        
    }
    
}
