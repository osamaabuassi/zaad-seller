//
//  AddShippingPriceCell.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 10/28/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class AddShippingPriceCell: UITableViewCell {
    
    @IBOutlet weak var btDelete: UIButton!
    @IBOutlet weak var tfCityFrom: UITextField!
    @IBOutlet weak var tfCityTo: UITextField!
    @IBOutlet weak var tfWeightFrom: UITextField!
    @IBOutlet weak var tfWeightTo: UITextField!
    @IBOutlet weak var tfPrice: UITextField!


    var DeleteTapped: (() -> ())?


    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func Delete(_ sender: UIButton) {
        DeleteTapped?()
    }
  
    
}
