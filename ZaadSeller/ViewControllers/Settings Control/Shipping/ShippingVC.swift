//
//  ShippingVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 10/27/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class ShippingVC: SuperViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var shippingdata:ShippingData?
    var carriersitems = [Carriers]()
    var carriers:Carriers?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getShipping()
        tableView.registerCell(id: "ShippingCell")
        //        let name = shippingdata?.sellerShippingService?.fullName ?? ""
        //        carriersitems.insert(carriers!, at: 0)
        //        tableView.insertRows(at: [IndexPath.init(row: 0, section: 0)], with: .automatic)
        tableView.tableFooterView = UIView()
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateShipping(notification:)), name: Notification.Name("UpdateShipping"), object: nil)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    func getShipping(){
        guard Helper.isConnectedToNetwork() else {
            self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
            self.emptyView?.imgEmpty.image = UIImage(named: "refreshment")
            self.emptyView?.firstLabel.text = "There is no internet connection".localized
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "shipping/carriers", method: HTTPMethod.get).start(){ (response, error) in
            
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(ShippingObject.self, from: response.data!)
                self.shippingdata = Status.data
                self.carriersitems = self.shippingdata!.carriers!
                
                if self.carriersitems.count == 0{
                    self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                    self.emptyView?.firstLabel.text = "No services to display".localized
                    self.emptyView?.imgEmpty.isHidden = true
                    self.carriersitems.removeAll()
                    self.tableView.reloadData()
                    
                }else{
                    self.tableView.tableFooterView = UIView()
                }
                self.tableView.reloadData()
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
        }
        
    }
    
    
    
    @IBAction func didRefersh(){
        carriersitems.removeAll()
        getShipping()
    }
    @objc func UpdateShipping(notification: NSNotification)  {
        //carriersitems.removeAll()
        getShipping()
    }
    
    
    
    
}



extension ShippingVC:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if carriersitems.count != 0{
            return carriersitems.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShippingCell", for: indexPath) as! ShippingCell
        cell.carriersitems = carriersitems[indexPath.row]
        if indexPath.row == 0{
            cell.ViewShipping.isHidden = false
        }else{
            cell.ViewShipping.isHidden = true
        }
        
//        if carriersitems[indexPath.row].id == nil{
//            if indexPath.row == 0{
//                cell.StatusShipping.isUserInteractionEnabled = false
//                self.showAlert(title: "", message: "يجب إكمال إعدادات الشحن قبل التفعيل".localized)
//            }else{
//                cell.StatusShipping.isUserInteractionEnabled = true
//            }
//        }else{
//            if self.shippingdata?.selectedShippingServiceCount ?? 0 <= 1{
//                if carriersitems[indexPath.row].active == true{
//                    cell.StatusShipping.isUserInteractionEnabled = false
//                    //self.showAlert(title: "".localized, message: "يجب ان تكون على الاقل خدمة شحن فعالة!".localized)
//                }else{
//                    cell.StatusShipping.isUserInteractionEnabled = true
//                }
//            }else{
//                cell.StatusShipping.isUserInteractionEnabled = true
//            }
//        }
        
        
        cell.EditeTapped = { [weak self] in
            let vc:ShippingSellerVC = ShippingSellerVC.loadFromNib()
            vc.modalPresentationStyle = .fullScreen
            self?.navigationController?.pushViewController(vc, animated: true)
        }
        cell.statusShowTapped = { [weak self] in
            
            if self?.carriersitems[indexPath.row].id == nil{
                if indexPath.row == 0{
                    cell.StatusShipping.isUserInteractionEnabled = false
                    self?.showAlert(title: "Error", message: "Shipping settings must be completed before activation".localized)
                }else{
                    cell.StatusShipping.isUserInteractionEnabled = true
                }
            }else{
                if self?.shippingdata?.selectedShippingServiceCount ?? 0 <= 1{
                    if self?.carriersitems[indexPath.row].active == true{
                        cell.StatusShipping.setOn(true, animated: true)
                    }else{
                        cell.StatusShipping.isUserInteractionEnabled = true
                    }
                }else{
                    cell.StatusShipping.isUserInteractionEnabled = true
                }
            }
            
            guard Helper.isConnectedToNetwork() else {
                self?.showAlert(title: "".localized, message: "There is no internet connection".localized)
                return
            }
            
            let obj = self?.carriersitems[indexPath.row]
            
            let id = obj?.sellerId
            var parameters: [String: Any] = [:]
            parameters["seller_id"] = id?.description
            if obj?.active == true{
                parameters["active"] = "0"
            }else{
                parameters["active"] = "1"
            }
            
            
            _ = WebRequests.setup(controller: self).prepare(query: "shipping/carriers", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self?.showAlert(title: "Error".localized, message:Status.message!)
                        return
                    }else if Status.status == 200{
                        NotificationCenter.default.post(name: Notification.Name("UpdateShipping"), object: nil)
                    }
                    
                }catch let jsonErr {
                    print("Error serializing respone json", jsonErr)
                }
                
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 126
        }else{
            return 70
        }
    }
    
    
    
}
