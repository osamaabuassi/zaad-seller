//
//  ShippingSellerVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 10/27/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class ShippingSellerVC: SuperViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var shippingstoredata = [ShippingStoreData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerCell(id: "ShipingSellerCell")
        getShipping()
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateShippingDelete(notification:)), name: Notification.Name("UpdateShippingDelete"), object: nil)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    
    @objc func UpdateShippingDelete(notification: NSNotification)  {
        shippingstoredata.removeAll()
        getShipping()
    }
    
    
    
    func getShipping(){
        guard Helper.isConnectedToNetwork() else {
            self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
            self.emptyView?.imgEmpty.image = UIImage(named: "refreshment")
            self.emptyView?.firstLabel.text = "There is no internet connection".localized
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "shipping/services", method: HTTPMethod.get).start(){ (response, error) in
            
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(ShippingStoreObject.self, from: response.data!)
                self.shippingstoredata = Status.data ?? []
  
                if self.shippingstoredata.count == 0{
                    self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                    self.emptyView?.firstLabel.text = "No shipping services to display".localized
                    self.emptyView?.imgEmpty.isHidden = true
                    self.shippingstoredata.removeAll()
                    self.tableView.reloadData()
                    
                }else{
                    self.tableView.tableFooterView = UIView()
                }
                self.tableView.reloadData()
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
        }
        
    }
    
    
    @IBAction func didTab_Add(_ sender: Any) {
        let vc:AddShippingStoreVC = AddShippingStoreVC.loadFromNib()
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func didRefersh(){
        shippingstoredata.removeAll()
        getShipping()
    }
    @objc func UpdateShipping(notification: NSNotification)  {
        shippingstoredata.removeAll()
        getShipping()
    }
    
    
    
}


extension ShippingSellerVC:UITableViewDataSource,UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shippingstoredata.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShipingSellerCell", for: indexPath) as! ShipingSellerCell
        cell.shippingstoreitems = shippingstoredata[indexPath.row]
        cell.editeTapped = { [weak self] in
            let vc:AddShippingStoreVC = AddShippingStoreVC.loadFromNib()
            vc.modalPresentationStyle = .fullScreen
            vc.isFromEdite = true
            vc.idShipping = self?.shippingstoredata[indexPath.row].id
            self?.navigationController?.pushViewController(vc, animated: true)
        }
        cell.deleteTapped = { [weak self] in
            
            let obj = self?.shippingstoredata[indexPath.row]
            let id = obj?.id
            
            
            guard Helper.isConnectedToNetwork() else {
                self?.showAlert(title: "".localized, message: "There is no internet connection".localized)
                return }
            guard self?.shippingstoredata.count ?? 0 > 1 else {
                self?.showAlert(title: "".localized, message: "It must be at least a shipping service that has been added".localized)
                return
                
            }
            
            _ = WebRequests.setup(controller: self).prepare(query: "shipping/services/\(id ?? 0)", method: HTTPMethod.delete).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self?.showAlert(title: "Error".localized, message:Status.message!)
                        return
                    }else if Status.status == 200{
                        NotificationCenter.default.post(name: Notification.Name("UpdateShippingDelete"), object: nil)
                    }
                    
                }catch let jsonErr {
                    print("Error serializing respone json", jsonErr)
                }
                
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    
}
