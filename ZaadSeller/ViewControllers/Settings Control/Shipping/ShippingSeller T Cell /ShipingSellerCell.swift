//
//  ShipingSellerCell.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 10/27/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class ShipingSellerCell: UITableViewCell {

    @IBOutlet weak var lblname: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    var editeTapped: (() -> ())?
    var deleteTapped: (() -> ())?

    var shippingstoreitems:ShippingStoreData?{
        didSet{
            self.lblname.text = shippingstoreitems?.title ?? ""
        }
    }
    
    @IBAction func Edite(_ sender: UIButton) {
        editeTapped?()
    }
    
    @IBAction func Delete(_ sender: UIButton) {
        deleteTapped?()
    }
    
    
}
