//
//  AddShippingStoreVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 10/27/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class AddShippingStoreVC: SuperViewController,UITextFieldDelegate,AddsubAddtionDelgate,AddsubPricesDelgate,EditesubAddtionDelgate,EditePricesDelgate{
    
    
    func SelectedDone(id: Int, fromcityid: String, tocityid: String, regularprice: String, minquantity: String, maxquantity: String, fromcitynames: String, tocitynames: String) {
        if let row = self.Shippingprice.firstIndex(where: {$0.id == id}) {
            Shippingprice[row].id = id
            Shippingprice[row].fromCityId = fromcityid
            Shippingprice[row].toCityId = tocityid
            Shippingprice[row].from = fromcitynames
            Shippingprice[row].to = tocitynames
            Shippingprice[row].price = Int(regularprice)
            Shippingprice[row].minQuantity = Int(minquantity)
            Shippingprice[row].maxQuantity = Int(maxquantity)
            self.tableViewPrices.reloadData()
        }
    }
    
    
    func SelectedDone(id: Int, value: String, valuethreshold: String, regularprice: String) {
        if let row = self.Shippingfeatures.firstIndex(where: {$0.id == id}) {
            Shippingfeatures[row].id = id
            Shippingfeatures[row].value = Int(value)
            Shippingfeatures[row].valueThreshold = Int(valuethreshold)
            Shippingfeatures[row].price = Int(regularprice)
            self.tableViewFeatures.reloadData()
        }
    }
    
    
    
    func SelectedDone(id: String, fromcityid: String, tocityid: String, regularprice: String, minquantity: String, maxquantity: String, fromcitynames: String, tocitynames: String) {
        let prices = pricessShipping.init(id: id, fromcityid: fromcityid, tocityid: tocityid, regularprice: regularprice, minquantity: minquantity, maxquantity: maxquantity,fromcitynames:fromcitynames,tocitynames:tocitynames)
        
        
        pricesitems.append(prices)
        //heighttableViewPrices.constant = 410
        self.tableViewPrices.reloadData()
        self.heighttableViewPrices.constant = self.tableViewPrices.contentSize.height
        
    }
    
    
    func SelectedDone(id: String, value: String, valuethreshold: String, regularprice: String) {
        
        let features = featuresShipping.init(id: id, value: value, valuethreshold: valuethreshold, regularprice: regularprice)
        
        featuresitems.append(features)
        // heighttableViewFeatures.constant = 180
        self.tableViewFeatures.reloadData()
        self.heighttableViewFeatures.constant = self.tableViewFeatures.contentSize.height
        
    }
    
    
    @IBOutlet weak var tableViewPrices: UITableView!
    @IBOutlet weak var tableViewFeatures: UITableView!
    @IBOutlet weak var heighttableViewPrices: NSLayoutConstraint!
    @IBOutlet weak var heighttableViewFeatures: NSLayoutConstraint!
    @IBOutlet weak var tfnameservice: UITextField!
    @IBOutlet weak var viewAddprice: UIView!
    @IBOutlet weak var viewAddaddtion: UIView!
    @IBOutlet weak var btDone: UIButton!
    
    
    var items = [String]()
    var itemsAddtion = [String]()
    var features:featuresShipping?
    var featuresitems = [featuresShipping]()
    var pricesitems = [pricessShipping]()
    
    var tfforArray = [[String:String]]()
    var tfdistanceArray = [[String:String]]()
    var tfIncreaseArray = [[String:String]]()
    var isFromEdite = false
    var ShippingDeatiles:ShippingDeatiles?
    var Shippingprice = [PricesShipping]()
    var Shippingfeatures = [FeaturesShipping]()
    var idShipping:Int?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isFromEdite{
            self.viewAddprice.isHidden = true
            self.viewAddaddtion.isHidden = true
            self.btDone.setTitle("Save".localized, for: .normal)
            getShippingDeatiles()
        }
        self.featuresitems.removeAll()
        self.pricesitems.removeAll()
        tableViewPrices.registerCell(id: "AddShippingPriceCell")
        tableViewPrices.tableFooterView = UIView.init(frame: .zero)
        tableViewFeatures.registerCell(id: "AddShippingAdditions")
        tableViewFeatures.tableFooterView = UIView.init(frame: .zero)
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateCellPrice(notification:)), name: Notification.Name("UpdateCellPrice"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateCellAdditions(notification:)), name: Notification.Name("UpdateCellAdditions"), object: nil)
        
    }
    
    
    func getShippingDeatiles(){
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "shipping/services/\(idShipping ?? 0)", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(ShippingDeatilesObject.self, from: response.data!)
                
                self.ShippingDeatiles = Status.data
                self.tfnameservice.text = self.ShippingDeatiles?.name ?? ""
                self.Shippingprice = self.ShippingDeatiles?.prices ?? []
                self.Shippingfeatures = self.ShippingDeatiles?.features ?? []
                if self.Shippingprice.count != 0{
                    self.heighttableViewPrices.constant = 425
                }
                if self.Shippingfeatures.count != 0{
                    self.heighttableViewFeatures.constant = 220
                }
                
                self.tableViewPrices.reloadData()
                self.tableViewFeatures.reloadData()
                //                self.heighttableViewFeatures.constant = self.tableViewFeatures.contentSize.height
                //                self.heighttableViewPrices.constant = self.tableViewPrices.contentSize.height
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
    }
    
    
    @objc func UpdateCellPrice(notification: NSNotification)  {
        if pricesitems.count == 0{
            heighttableViewPrices.constant = 0
        }else{
            heighttableViewPrices.constant = 425
            
        }
        
    }
    
    
    @objc func UpdateCellAdditions(notification: NSNotification)  {
        if featuresitems.count == 0{
            heighttableViewFeatures.constant = 0
        }else{
            heighttableViewFeatures.constant = 165
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    @IBAction func didTab_AddPrice(_ sender: UIButton) {
        if sender.tag == 0{
            
            let vc:AddAddtionPopUp = AddAddtionPopUp.loadFromNib()
            vc.AddsubPrices = self
            vc.isFromPrice = true
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc, animated: false, completion: nil)
            
        }else{
            let vc:AddAddtionPopUp = AddAddtionPopUp.loadFromNib()
            vc.AddsubAddtion = self
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc, animated: false, completion: nil)
            
        }
        
    }
    
    
    @IBAction func didTab_AddEdite(_ sender: UIButton) {
        guard let name = self.tfnameservice.text, !name.isEmpty else{
            self.showAlert(title: "".localized, message: "The service name must be entered".localized)
            return
        }
        var parameters: [String: Any] = [:]
        parameters["name"] =  name
        if isFromEdite{
            var jsonpricesitems: [String: Any]
            var jsonAddtionitems: [String: Any]
            
            var jsonpricesitemsAll: [[String: Any]] = []
            var jsonAddtionitemsAll: [[String: Any]] = []
            if Shippingprice.count != 0{
                for item in Shippingprice{
                    jsonpricesitems = ["id":item.id ?? 0,"from_city_id":item.fromCityId ?? "0","to_city_id":item.toCityId ?? "0","regular_price":item.price ?? 0,"min_quantity":item.minQuantity ?? 0,"max_quantity":item.maxQuantity ?? 0]
                    print(jsonpricesitems)
                    jsonpricesitemsAll.append(jsonpricesitems)
                    
                    if Shippingprice.count != 0{
                        var i = 0
                        for item in jsonpricesitemsAll{
                            parameters["prices[\(i)]"] = jsonString(data: item)
                            i = i + 1
                            
                            print(item)
                            
                        }
                    }
                }
            }
            
            
            if Shippingfeatures.count != 0{
                for item in Shippingfeatures{
                    jsonAddtionitems = ["id":item.id ?? 0,"value":item.value ?? 0,"value_threshold":item.valueThreshold ?? 0,"regular_price":item.price ?? 0]
                    print(jsonAddtionitems)
                    jsonAddtionitemsAll.append(jsonAddtionitems)
                    
                    if Shippingfeatures.count != 0{
                        var i = 0
                        for item in jsonAddtionitemsAll{
                            parameters["features[\(i)]"] = jsonString(data: item)
                            i = i + 1
                            
                            print(item)
                            
                        }
                    }
                }
            }
            _ = WebRequests.setup(controller: self).prepare(query: "shipping/services/\(idShipping ?? 0)", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self.showAlert(title: "Error".localized, message:Status.message!)
                        return
                    }else if Status.status == 200 {
                        self.showAlert(title: "Successfully", message:  "Modified Successfully".localized)
                        self.navigationController?.popViewController(animated: true)
                        NotificationCenter.default.post(name: Notification.Name("UpdateShippingDelete"), object: nil)
                    }
                    
                }catch let jsonErr {
                    print("Error serializing respone json", jsonErr)
                }
            }
            
            
            
        }else{
            guard self.pricesitems.count != 0 else{
                self.showAlert(title: "".localized, message: "Please add prices".localized)
                return
            }
            
            var jsonprices: [String: Any]
            var jsonAddtion: [String: Any]
            
            var jsonpricesAll: [[String: Any]] = []
            var jsonAddtionAll: [[String: Any]] = []
            
            
            
            for item in pricesitems{
                jsonprices = ["id":item.id ?? 0,"from_city_id":item.fromcityid ?? "0","to_city_id":item.tocityid ?? "0","regular_price":item.regularprice ?? "0","min_quantity":item.minquantity ?? "0","max_quantity":item.maxquantity ?? "0"]
                print(jsonprices)
                jsonpricesAll.append(jsonprices)
                print(jsonpricesAll)
                if pricesitems.count != 0{
                    var i = 0
                    for item in jsonpricesAll{
                        parameters["prices[\(i)]"] = jsonString(data: item)
                        i = i + 1
                        
                        print(item)
                        
                    }
                }
            }
            
            for item in featuresitems{
                jsonAddtion = ["id":item.id ?? 0,"value":item.value ?? "0","value_threshold":item.valuethreshold ?? "0","regular_price":item.regularprice ?? "0"]
                print(jsonAddtion)
                jsonAddtionAll.append(jsonAddtion)
                if featuresitems.count != 0{
                    var i = 0
                    for item in jsonAddtionAll{
                        parameters["features[\(i)]"] = jsonString(data: item)
                        i = i + 1
                        
                        print(item)
                        
                    }
                }
            }
            _ = WebRequests.setup(controller: self).prepare(query: "shipping/services", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self.showAlert(title: "Error".localized, message:Status.message!)
                        return
                    }else if Status.status == 200 {
                        self.showAlert(title: "Successfully", message: "Added successfully".localized)
                        self.navigationController?.popViewController(animated: true)
                        NotificationCenter.default.post(name: Notification.Name("UpdateShippingDelete"), object: nil)
                    }
                    
                }catch let jsonErr {
                    print("Error serializing respone json", jsonErr)
                }
            }
            
        }
        
        
        
        
        
    }
    func jsonString(data : Any) -> String {
        
        var jsonString = "";
        
        do { let jsonData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
        } catch {
            print(error.localizedDescription)
        }
        return jsonString;
    }
}
extension AddShippingStoreVC:UITableViewDelegate,UITableViewDataSource{
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFromEdite{
            if tableView == tableViewPrices{
                return Shippingprice.count
            }else{
                return Shippingfeatures.count
            }
        }else{
            if tableView == tableViewPrices{
                return pricesitems.count
                
            }else{
                return featuresitems.count
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isFromEdite{
            if tableView == tableViewPrices{
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddShippingPriceCell", for: indexPath) as! AddShippingPriceCell
                let obj = Shippingprice[indexPath.row]
                cell.tfPrice.text = "\(obj.price ?? 0)"
                cell.tfWeightFrom.text = "\(obj.minQuantity ?? 0)"
                cell.tfWeightTo.text = "\(obj.maxQuantity ?? 0)"
                cell.tfCityFrom.text = obj.from ?? ""
                cell.tfCityTo.text = obj.to ?? ""
                cell.btDelete.setTitle("Edit".localized, for: .normal)
                cell.btDelete.backgroundColor = "B1AF00".color
                
                cell.tfPrice.keyboardType = .asciiCapable
                cell.tfWeightFrom.keyboardType = .asciiCapable
                cell.tfWeightTo.keyboardType = .asciiCapable
                
                cell.DeleteTapped = { [weak self] in
                    
                    let obj = self?.Shippingprice[indexPath.row]
                    let vc:AddAddtionPopUp = AddAddtionPopUp.loadFromNib()
                    vc.isFromEditePrice = true
                    vc.EditePrices = self
                    vc.Shippingprice = obj
                    vc.modalPresentationStyle = .overCurrentContext
                    self?.present(vc, animated: false, completion: nil)
                    
                }
                return cell
                
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddShippingAdditions", for: indexPath) as! AddShippingAdditions
                let obj = Shippingfeatures[indexPath.row]
                cell.tffor.text = "\(obj.value ?? 0)"
                cell.tfdistance.text = "\(obj.valueThreshold ?? 0)"
                cell.tfIncrease.text = "\(obj.price ?? 0)"
                cell.btDelete.setTitle("Edit".localized, for: .normal)
                cell.btDelete.backgroundColor = "B1AF00".color
                cell.DeleteTapped = { [weak self] in
                    let obj = self?.Shippingfeatures[indexPath.row]
                    let vc:AddAddtionPopUp = AddAddtionPopUp.loadFromNib()
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.Shippingfeatures = obj
                    vc.isFromEditeFeature = true
                    vc.EditesubAddtion = self
                    self?.present(vc, animated: false, completion: nil)
                    
                }
                
                return cell
            }
        }else{
            if tableView == tableViewPrices{
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddShippingPriceCell", for: indexPath) as! AddShippingPriceCell
                let obj = pricesitems[indexPath.row]
                cell.tfPrice.text = obj.regularprice ?? ""
                cell.tfWeightFrom.text = obj.minquantity ?? ""
                cell.tfWeightTo.text = obj.maxquantity ?? ""
                cell.tfCityFrom.text = obj.fromcitynames ?? ""
                cell.tfCityTo.text = obj.tocitynames ?? ""
                
                cell.DeleteTapped = { [weak self] in
                    self?.pricesitems.remove(at: indexPath.row)
                    self?.tableViewPrices.deleteRows(at: [indexPath], with: .left)
                    self?.tableViewPrices.reloadData()
                    NotificationCenter.default.post(name: Notification.Name("UpdateCellPrice"), object: nil)
                }
                return cell
                
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddShippingAdditions", for: indexPath) as! AddShippingAdditions
                let obj = featuresitems[indexPath.row]
                cell.tffor.text = obj.value
                cell.tfdistance.text = obj.valuethreshold
                cell.tfIncrease.text = obj.regularprice
                
                cell.DeleteTapped = { [weak self] in
                    self?.featuresitems.remove(at: indexPath.row)
                    self?.tableViewFeatures.deleteRows(at: [indexPath], with: .left)
                    self?.tableViewFeatures.reloadData()
                    NotificationCenter.default.post(name: Notification.Name("UpdateCellAdditions"), object: nil)
                }
                return cell
            }
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == tableViewPrices{
            self.heighttableViewPrices.constant = self.tableViewPrices.contentSize.height
        }else{
            self.heighttableViewFeatures.constant = self.tableViewFeatures.contentSize.height
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tableViewPrices{
            return 425
        }else{
            return 220
        }
    }
}





