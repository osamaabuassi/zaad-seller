//
//  ShippingCell.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 10/27/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class ShippingCell: UITableViewCell {
    
    @IBOutlet weak var lblShippingname: UILabel!
    @IBOutlet weak var StatusShipping: UISwitch!
    @IBOutlet weak var ViewShipping: UIView!
    @IBOutlet weak var btShippingSettings: UIButton!

    var EditeTapped: (() -> ())?
    var statusShowTapped: (() -> ())?

    override func awakeFromNib() {
        super.awakeFromNib()

    }
    var carriersitems:Carriers?{
        didSet{
            self.lblShippingname.text = carriersitems?.fullName ?? ""
                if carriersitems?.active == true{
                    self.StatusShipping.setOn(true, animated: true)
                    //self.lblstatusShow.text = producatitems?.published?.title ?? ""
                }else{
                    self.StatusShipping.setOn(false, animated: true)
                   // self.lblstatusShow.text = producatitems?.published?.title ?? ""
                }
        }
    }
    
    @IBAction func Edite(_ sender: UIButton) {
        EditeTapped?()
    }
    
    @IBAction func statusShow(_ sender: UISwitch) {
        statusShowTapped?()
    }
    
    
}
