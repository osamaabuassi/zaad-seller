//
//  AddShippingAdditions.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 10/28/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class AddShippingAdditions: UITableViewCell {
    
    @IBOutlet weak var tfIncrease: UITextField!
    @IBOutlet weak var tffor: UITextField!
    @IBOutlet weak var tfdistance: UITextField!
    @IBOutlet weak var btDelete: UIButton!


    var DeleteTapped: (() -> ())?

    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    
    @IBAction func Delete(_ sender: UIButton) {
          DeleteTapped?()
      }
 
    
}
