//
//  AddAddtionPopUp.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 10/28/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

protocol AddsubAddtionDelgate : class {
    func SelectedDone(id:String,value:String,valuethreshold:String,regularprice:String)
}

protocol EditesubAddtionDelgate : class {
    func SelectedDone(id:Int,value:String,valuethreshold:String,regularprice:String)
}

protocol AddsubPricesDelgate : class {
    func SelectedDone(id:String,fromcityid:String,tocityid:String,regularprice:String,minquantity:String,maxquantity:String,fromcitynames:String,tocitynames:String)
}

protocol EditePricesDelgate : class {
    func SelectedDone(id:Int,fromcityid:String,tocityid:String,regularprice:String,minquantity:String,maxquantity:String,fromcitynames:String,tocitynames:String)
}


class AddAddtionPopUp: SuperViewController {
    
    
    @IBOutlet weak var tfIncrease: UITextField!
    @IBOutlet weak var tffor: UITextField!
    @IBOutlet weak var tfdistance: UITextField!
    
    
    @IBOutlet weak var btCityFrom: UIButton!
    @IBOutlet weak var btCityTo: UIButton!
    @IBOutlet weak var tfCityFrom: UITextField!
    @IBOutlet weak var tfCityTo: UITextField!
    @IBOutlet weak var tfWeightFrom: UITextField!
    @IBOutlet weak var tfWeightTo: UITextField!
    @IBOutlet weak var tfPrice: UITextField!
    @IBOutlet weak var viewPrcie: UIView!
    @IBOutlet weak var viewAddtions: UIView!
    
    
    var isFromPrice = false
    var isFromEditePrice = false
    var isFromEditeFeature = false
    
    var city = [CitysAll]()
    var cityFrom = [CitysAll]()
    var cityTo = [CitysAll]()
    var AddsubAddtion:AddsubAddtionDelgate?
    var EditesubAddtion:EditesubAddtionDelgate?
    var AddsubPrices:AddsubPricesDelgate?
    var EditePrices:EditePricesDelgate?
    var SelectedFromCity : [CitysAll] = []
    var SelectedToCity : [CitysAll] = []
    var SelectedCityID:String?
    var SelectedCityToID:String?
    var Shippingprice:PricesShipping?
    var Shippingfeatures:FeaturesShipping?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tfIncrease.keyboardType = .asciiCapable
        self.tffor.keyboardType = .asciiCapable
        self.tfdistance.keyboardType = .asciiCapable
        self.tfCityFrom.keyboardType = .asciiCapable
        self.tfCityTo.keyboardType = .asciiCapable
        self.tfWeightFrom.keyboardType = .asciiCapable
        self.tfWeightTo.keyboardType = .asciiCapable
        self.tfPrice.keyboardType = .asciiCapable
        getCity()
        if isFromEditePrice{
            self.tfWeightFrom.text = "\(Shippingprice?.minQuantity ?? 0)"
            self.tfWeightTo.text = "\(Shippingprice?.maxQuantity ?? 0)"
            self.tfPrice.text = "\(Shippingprice?.price ?? 0)"
            self.tfCityFrom.text = Shippingprice?.from
            self.tfCityTo.text = Shippingprice?.to
            self.SelectedCityID = Shippingprice?.fromCityId
            self.SelectedCityToID = Shippingprice?.toCityId
            self.viewPrcie.isHidden = false
            self.viewAddtions.isHidden = true
            //            let stringNumbers =  Shippingprice?.fromCityId
            //            let arrayIntegers = stringNumbers?.components(separatedBy: ",").compactMap { Int($0) }
            
        }
        if isFromEditeFeature{
            self.tffor.text = "\(Shippingfeatures?.value ?? 0)"
            self.tfdistance.text = "\(Shippingfeatures?.valueThreshold ?? 0)"
            self.tfIncrease.text = "\(Shippingfeatures?.price ?? 0)"
            self.viewPrcie.isHidden = true
            self.viewAddtions.isHidden = false
            
        }
        if isFromPrice{
            self.viewPrcie.isHidden = false
            self.viewAddtions.isHidden = true
            
        }else if !isFromEditePrice,!isFromEditeFeature,!isFromPrice{
            self.viewPrcie.isHidden = true
            self.viewAddtions.isHidden = false
            
        }
        
    }
    
    
    func getCity(){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "cities/options?country_id=1", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(CitysAllObject.self, from: response.data!)
                self.cityFrom = Status.data ?? []
                self.cityTo = Status.data ?? []
                self.city = Status.data ?? []
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
    }
    
    @IBAction func Cancel(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func btCity(_ sender: UIButton) {
        
        let Appearance = YBTextPickerAppearanceManager.init(
            pickerTitle         : "Select cities".localized,
            titleFont           : UIFont.FFShamelFamilySemiRoundMedium(ofSize: 15),
            titleTextColor      : .black,
            titleBackground     : .clear,
            searchBarFont       : UIFont.FFShamelFamilySemiRoundBook(ofSize: 15),
            searchBarPlaceholder: "Search".localized,
            closeButtonTitle    : "Cancellation".localized,
            closeButtonColor    : "FF501B".color,
            closeButtonFont     : UIFont.FFShamelFamilySemiRoundMedium(ofSize: 15),
            doneButtonTitle     : "Add".localized,
            doneButtonColor     : "003B87".color,
            doneButtonFont      : UIFont.FFShamelFamilySemiRoundMedium(ofSize: 15),
            checkMarkPosition   : .Right,
            itemCheckedImage    : UIImage(named:"blue_ic_checked"),
            itemUncheckedImage  : UIImage(),
            itemColor           : .black,
            itemFont            : UIFont.FFShamelFamilySemiRoundBook(ofSize: 15)
        )
        
        
        
        
        
        if sender.tag == 0{
            let titlecity = self.cityFrom.compactMap({$0.title})
            
            let picker = YBTextPicker.init(
                with: titlecity, appearance: Appearance,
                onCompletion: { (selectedIndexes, selectedValues) in
                    
                    
                    if selectedValues.count > 0{
                        
                        var values = [String]()
                        self.SelectedFromCity.removeAll()
                        for index in selectedIndexes{
                            values.append(titlecity[index])
                            self.SelectedFromCity.append(self.cityFrom[index])
                            
                        }
                        
                        let id = self.SelectedFromCity.compactMap({$0.id})
                        
                        let formatter = NumberFormatter()
                        formatter.numberStyle = .none
                        formatter.maximumFractionDigits = 2
                        formatter.minimumFractionDigits = 0
                        formatter.locale = Locale(identifier: "en_US")
                        self.SelectedCityID = id.compactMap { formatter.string(for: $0) }
                        .joined(separator: ",")
                        print(self.SelectedCityID!)
                        
                        self.tfCityFrom.text = values.joined(separator: ", ")
                    }else{
                    }
                },
                onCancel: {
                    print("Cancelled")
                }
            )
            
            if let title = self.tfCityFrom.text{
                if title.contains(","){
                    picker.preSelectedValues = title.components(separatedBy: ", ")
                }
            }
            picker.allowMultipleSelection = true
            picker.show(withAnimation: .Fade)
        }else{
            let titlecity = self.cityTo.compactMap({$0.title})
            
            let picker = YBTextPicker.init(with: titlecity, appearance: Appearance,
                                           onCompletion: { (selectedIndexes, selectedValues) in
                
                
                if selectedValues.count > 0{
                    
                    var values = [String]()
                    self.SelectedToCity.removeAll()
                    for index in selectedIndexes{
                        values.append(titlecity[index])
                        self.SelectedToCity.append(self.cityTo[index])
                        
                    }
                    
                    let id = self.SelectedToCity.compactMap({$0.id})
                    
                    let formatter = NumberFormatter()
                    formatter.numberStyle = .none
                    formatter.maximumFractionDigits = 2
                    formatter.minimumFractionDigits = 0
                    formatter.locale = Locale(identifier: "en_US")
                    
                    self.SelectedCityToID = id.compactMap { formatter.string(for: $0) }
                    .joined(separator: ",")
                    print(self.SelectedCityToID!)
                    
                    self.tfCityTo.text = values.joined(separator: ", ")
                }else{
                }
            },
                                           onCancel: {
                print("Cancelled")
            }
            )
            
            if let title = self.tfCityTo.text{
                if title.contains(","){
                    picker.preSelectedValues = title.components(separatedBy: ", ")
                }
            }
            picker.allowMultipleSelection = true
            picker.show(withAnimation: .Fade)
        }
        
        
    }
    @IBAction func AddallCity(_ sender: UIButton) {
        if sender.tag == 0{
            let title = city.map({$0.title!}).joined(separator: ",")
            let id = self.city.compactMap({$0.id})
            let formatter = NumberFormatter()
            formatter.locale =  NSLocale(localeIdentifier: "EN") as Locale
            formatter.numberStyle = .none
            formatter.maximumFractionDigits = 2
            formatter.minimumFractionDigits = 0
            self.SelectedCityID = id.compactMap { formatter.string(for: $0) }
            .joined(separator: ",")
            self.tfCityFrom.text = title
            print(title)
            print(self.SelectedCityID ?? "")
            
        }else{
            let title = city.map({$0.title!}).joined(separator: ",")
            let id = self.city.compactMap({$0.id})
            let formatter = NumberFormatter()
            formatter.locale =  NSLocale(localeIdentifier: "EN") as Locale
            formatter.numberStyle = .none
            formatter.maximumFractionDigits = 2
            formatter.minimumFractionDigits = 0
            self.SelectedCityToID = id.compactMap { formatter.string(for: $0) }
            .joined(separator: ",")
            self.tfCityTo.text = title
            print(title)
            print(self.SelectedCityToID ?? "")
        }
    }
    
    
    @IBAction func Add(_ sender: UIButton) {
        
        
        if sender.tag == 0{
            
            guard tfIncrease.text != "" else{
                self.showAlert(title: "", message: "Please fill in all fields".localized)
                return
            }
            
            guard tffor.text != "" else{
                self.showAlert(title: "", message: "Please fill in all fields".localized)
                return
            }
            
            guard tfdistance.text != "" else{
                self.showAlert(title: "", message: "Please fill in all fields".localized)
                return
            }
            
            if isFromEditeFeature{
                self.dismiss(animated: false) {
                    self.EditesubAddtion?.SelectedDone(id: self.Shippingfeatures?.id ?? 0, value: self.tffor?.text ?? "", valuethreshold: self.tfdistance.text ?? "", regularprice: self.tfIncrease.text ?? "")
                }
                
            }else{
                
                self.dismiss(animated: false) {
                    self.AddsubAddtion?.SelectedDone(id: "", value: self.tffor.text ?? "", valuethreshold: self.tfdistance.text ?? "", regularprice: self.tfIncrease.text ?? "")
                }
            }
            
        }else{
            guard tfPrice.text != "" else{
                self.showAlert(title: "", message: "Please fill in all fields".localized)
                return
            }
            guard tfCityFrom.text != "" else{
                self.showAlert(title: "", message: "Please fill in all fields".localized)
                return
            }
            
            guard tfCityTo.text != "" else{
                self.showAlert(title: "", message: "Please fill in all fields".localized)
                return
            }
            
            
            guard tfWeightFrom.text != "" else{
                self.showAlert(title: "", message: "Please fill in all fields".localized)
                return
            }
            
            guard tfWeightTo.text != "" else{
                self.showAlert(title: "", message: "Please fill in all fields".localized)
                return
            }
            guard tfPrice.text != "" else{
                self.showAlert(title: "", message: "Please fill in all fields".localized)
                return
            }
            
            
            if isFromEditePrice{
                self.dismiss(animated: false) {
                    self.EditePrices?.SelectedDone(id: self.Shippingprice?.id ?? 0, fromcityid: self.SelectedCityID ?? "", tocityid: self.SelectedCityToID ?? "", regularprice: self.tfPrice.text ?? "", minquantity: self.tfWeightFrom.text ?? "", maxquantity: self.tfWeightTo.text ?? "", fromcitynames: self.tfCityFrom.text ?? "", tocitynames: self.tfCityTo.text ?? "")
                }
            }else{
                self.dismiss(animated: false) {
                    self.AddsubPrices?.SelectedDone(id: "", fromcityid: self.SelectedCityID ?? "", tocityid: self.SelectedCityToID ?? "", regularprice: self.tfPrice.text ?? "", minquantity: self.tfWeightFrom.text ?? "", maxquantity: self.tfWeightTo.text ?? "", fromcitynames: self.tfCityFrom.text ?? "", tocitynames: self.tfCityTo.text ?? "")
                }
            }
            
        }
        
    }
    
    
}
