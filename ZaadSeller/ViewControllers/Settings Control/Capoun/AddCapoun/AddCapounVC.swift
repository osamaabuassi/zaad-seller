//
//  AddCapounVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/6/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class AddCapounVC: SuperViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtDescrbtion: UITextView!
    @IBOutlet weak var tfCode: UITextField!
    @IBOutlet weak var tfdiscrbtion: UITextField!
    @IBOutlet weak var tfAllowednumber: UITextField!
    @IBOutlet weak var tflowestamount: UITextField!
    @IBOutlet weak var tfStartDate: UITextField!
    @IBOutlet weak var tfEndDate: UITextField!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var SwitchStatus: UISwitch!
    @IBOutlet weak var ViewDiscount: UIView!
    @IBOutlet weak var ViewRatio: UIView!
    @IBOutlet weak var ViewtwoDiscount: UIView!
    @IBOutlet weak var ViewtwoRatio: UIView!
    
    
    var isEdited = false
    var status:String?
    var CapounType:String?
    var id:Int?
    var capounDeatiles:CapounsDeatilesData?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getDeatile()
        if !isEdited{
        self.ViewRatio.borderColor = "B1AF00".color
        self.ViewRatio.borderWidth = 1
        self.ViewRatio.cornerRadius = 3.45
        self.ViewDiscount.borderColor = "AFAFAF".color
        self.ViewDiscount.borderWidth = 1
        self.ViewDiscount.cornerRadius = 3.45
        self.CapounType = "1"
        self.SwitchStatus.setOn(false, animated: true)
        self.lblStatus.text = "inactive".localized
        }
    }
    
    
    func getDeatile(){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        if isEdited{
            self.lblTitle.text = "Edit Coupon".localized
            _ = WebRequests.setup(controller: self).prepare(query: "coupons/\(id ?? 0)", method: HTTPMethod.get).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "error".localized, message:Status.message!)
                        return
                    }
                    
                }catch let jsonErr {
                    print("Error serializing respone json", jsonErr)
                }
                
                
                do {
                    let Status =  try JSONDecoder().decode(CapounsDeatilesObject.self, from: response.data!)
                    self.capounDeatiles = Status.data!
                    self.txtDescrbtion.text = self.capounDeatiles?.descriptionValue ?? ""
                    self.tfCode.text = self.capounDeatiles?.code ?? ""
                    self.tfdiscrbtion.text  = "\(self.capounDeatiles?.amountLabel ?? "0")"
                    self.tfAllowednumber.text = "\(self.capounDeatiles?.allowed ?? 0)"
                    self.tflowestamount.text = "\(self.capounDeatiles?.minAmount ?? 0)"
                    self.tfStartDate.text = self.capounDeatiles?.startDate ?? "0"
                    self.tfEndDate.text = self.capounDeatiles?.endDate ?? "0"
                    self.lblStatus.text = self.capounDeatiles?.status?.title ?? ""
                    if self.capounDeatiles?.discountType?.title == "نسبة"{
                        self.ViewRatio.borderColor = "B1AF00".color
                        self.ViewRatio.borderWidth = 1
                        self.ViewRatio.cornerRadius = 3.45
                        self.ViewDiscount.borderColor = "AFAFAF".color
                        self.ViewDiscount.borderWidth = 1
                        self.ViewDiscount.cornerRadius = 3.45
                        self.CapounType = "1"
                    }else{
                        self.ViewRatio.borderColor = "AFAFAF".color
                        self.ViewtwoRatio.backgroundColor = "AFAFAF".color
                        self.ViewRatio.borderWidth = 1
                        self.ViewRatio.cornerRadius = 3.45
                        self.ViewDiscount.borderColor = "B1AF00".color
                        self.ViewtwoDiscount.backgroundColor = "B1AF00".color
                        self.ViewDiscount.borderWidth = 1
                        self.ViewDiscount.cornerRadius = 3.45
                        self.CapounType = "2"
                    }
                    if self.capounDeatiles?.status?.title == "فعال"{
                        self.SwitchStatus.setOn(true, animated: true)
                        self.lblStatus.text = "active".localized
                        self.status = "1"
                    }else{
                        self.SwitchStatus.setOn(false, animated: true)
                        self.lblStatus.text = "inactive".localized
                        self.status = "0"
                    }
                    
                } catch let jsonErr {
                    print("Error serializing respone json", jsonErr)
                }
            }
        }
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    
    @IBAction func didTab_StartDate(_ sender: Any) {
        let datePicker = ActionSheetDatePicker(title: "Start Date".localized, datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "yyyy-MM-dd"
            let dateString = formatter.string(from: (value as? Date)!)
            
            self.tfStartDate.text = dateString
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: self.view)
        datePicker?.show()
        
    }
    @IBAction func didTab_EndDate(_ sender: Any) {
        let datePicker = ActionSheetDatePicker(title: "Expiry date".localized, datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "yyyy-MM-dd"
            let dateString = formatter.string(from: (value as? Date)!)
            
            self.tfEndDate.text = dateString
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: self.view)
        datePicker?.show()
        
    }
    @IBAction func didTab_Status(_ sender: UISwitch) {
        if sender.isOn{
            self.status = "1"
            self.lblStatus.text = "active".localized
        }else{
            self.status = "0"
            self.lblStatus.text = "inactive".localized
        }
        
    }
    @IBAction func didTab_Discounttype(_ sender: UIButton) {
        if sender.tag == 0{
            self.ViewRatio.borderColor = "B1AF00".color
            self.ViewRatio.borderWidth = 1
            self.ViewRatio.cornerRadius = 3.45
            self.ViewtwoRatio.backgroundColor = "B1AF00".color
            self.ViewDiscount.borderColor = "AFAFAF".color
            self.ViewDiscount.borderWidth = 1
            self.ViewDiscount.cornerRadius = 3.45
            self.ViewtwoDiscount.backgroundColor = "AFAFAF".color
            self.CapounType = "1"
            
        }else{
            self.ViewRatio.borderColor = "AFAFAF".color
            self.ViewtwoRatio.backgroundColor = "AFAFAF".color
            self.ViewRatio.borderWidth = 1
            self.ViewRatio.cornerRadius = 3.45
            self.ViewDiscount.borderColor = "B1AF00".color
            self.ViewtwoDiscount.backgroundColor = "B1AF00".color
            self.ViewDiscount.borderWidth = 1
            self.ViewDiscount.cornerRadius = 3.45
            self.CapounType = "2"
        }
        
    }
    
    @IBAction func didTab_Edite(_ sender: Any) {
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        guard let descrbtion = self.txtDescrbtion.text, !descrbtion.isEmpty else{
            self.showAlert(title: "error".localized, message: "Description cannot be empty".localized)
            return
        }
        guard let code = self.tfCode.text, !code.isEmpty else{
            self.showAlert(title: "error".localized, message: "Code cannot be empty".localized)
            return
        }
        
        guard let discount = self.tfdiscrbtion.text, !discount.isEmpty else{
            self.showAlert(title: "error".localized, message: "The discount cannot be empty".localized)
            return
        }
        guard let allowednumber = self.tfAllowednumber.text, !allowednumber.isEmpty else{
            self.showAlert(title: "error".localized, message: "The number cannot be empty".localized)
            return
        }
        guard let lowestnmber = self.tflowestamount.text, !lowestnmber.isEmpty else{
            self.showAlert(title: "error".localized, message: "The amount cannot be empty".localized)
            return
        }
        
        guard let startdate = self.tfStartDate.text, !startdate.isEmpty else{
            self.showAlert(title: "error".localized, message: "Added date cannot be empty".localized)
            return
        }
        
        guard let Enddate = self.tfEndDate.text, !Enddate.isEmpty else{
            self.showAlert(title: "error".localized, message: "Expiry date cannot be empty".localized)
            return
        }
        guard let status = self.status, !status.isEmpty else{
            self.showAlert(title: "error".localized, message: "Please select a status for the coupon".localized)
            return
        }
        
        
        var parameters: [String: Any] = [:]
        
        parameters["description"] = self.txtDescrbtion.text ?? ""
        parameters["code"] = self.tfCode.text ?? ""
        parameters["discount_type"] = self.CapounType ?? ""
        parameters["amount"] = self.tfdiscrbtion?.text ?? ""
        parameters["allowed"] = self.tfAllowednumber?.text ?? ""
        parameters["min_amount"] = self.tflowestamount.text ?? ""
        parameters["start_date"] = self.tfStartDate.text ?? ""
        parameters["end_date"] = self.tfEndDate.text ?? ""
        parameters["status"] = self.status ?? ""
        
        if isEdited{
            
            _ = WebRequests.setup(controller: self).prepare(query: "coupons/\(id ?? 0)", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self.showAlert(title: "Error".localized, message:Status.message!)
                        return
                    }else if Status.status == 200{
                        self.navigationController?.popViewController(animated: true)
                        
                        NotificationCenter.default.post(name: Notification.Name("UpdateCapoun"), object: nil)
                    }
                    
                }catch let jsonErr {
                    print("Error serializing respone json", jsonErr)
                }
            }
        }else{
            
            _ = WebRequests.setup(controller: self).prepare(query: "coupons", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self.showAlert(title: "Error".localized, message:Status.message!)
                        return
                    }else if Status.status == 200{
                        self.navigationController?.popViewController(animated: true)
                        
                        NotificationCenter.default.post(name: Notification.Name("UpdateCapoun"), object: nil)
                    }
                    
                }catch let jsonErr {
                    print("Error serializing respone json", jsonErr)
                }
            }
            
        }
    }
    
}
