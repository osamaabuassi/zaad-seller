//
//  CapounCell.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/5/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class CapounCell: UITableViewCell {
    
    
    @IBOutlet weak var lblDescrebtion: UILabel!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var lblDataAdd: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var BtStatus: UISwitch!
    @IBOutlet weak var btDeatiles: UIButton!
    @IBOutlet weak var btEdite: UIButton!
    @IBOutlet weak var btDelete: UIButton!
    
    var deatilesTapped: (() -> ())?
    var editeTapped: (() -> ())?
    var deleteTapped: (() -> ())?
    
    var capounsitems:CapounsResources?{
        didSet{
            self.lblDescrebtion.text = capounsitems?.descriptionValue ?? ""
            self.lblCode.text  =  capounsitems?.code ?? ""
            self.lblNumber.text  =  "\(capounsitems?.allowed ?? 0)"
            self.lblDataAdd.text = capounsitems?.createdAt ?? ""
            if capounsitems?.status?.title == "active"{
                self.BtStatus.setOn(true, animated: true)
                self.lblStatus.text = "active".localized
            }else{
                self.BtStatus.setOn(false, animated: true)
                self.lblStatus.text = "inactive".localized
            }
        }
    }

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    @IBAction func Deatiles(_ sender: UIButton) {
        deatilesTapped?()
    }
    
    @IBAction func Edite(_ sender: UIButton) {
        editeTapped?()
    }
    
    @IBAction func Delete(_ sender: UIButton) {
        deleteTapped?()
    }
    
    
    
    
}
