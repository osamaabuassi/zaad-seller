//
//  CapounsDeatilesVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/6/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class CapounsDeatilesVC: SuperViewController {
    
    
    @IBOutlet weak var lblDescrbtion: UILabel!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var lbldiscountValue: UILabel!
    @IBOutlet weak var lblDiscountType: UILabel!
    @IBOutlet weak var lblAllowednumber: UILabel!
    @IBOutlet weak var lbllowestamount: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblExpiryDate: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    var capounDeatiles:CapounsDeatilesData?
    var id:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getCapounDeaitles()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    
    func getCapounDeaitles(){
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "coupons/\(id ?? 0)", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(CapounsDeatilesObject.self, from: response.data!)
                self.capounDeatiles = Status.data!
                self.lblDescrbtion.text = self.capounDeatiles?.descriptionValue ?? ""
                self.lblCode.text = self.capounDeatiles?.code ?? ""
                self.lblDiscountType.text = self.capounDeatiles?.discountType?.title ?? ""
                self.lbldiscountValue.text  = "\(self.capounDeatiles?.amountLabel ?? "0")"
                self.lblAllowednumber.text = "\(self.capounDeatiles?.allowed ?? 0)"
                self.lbllowestamount.text = "\(self.capounDeatiles?.minAmount ?? 0)"
                self.lblStartDate.text = self.capounDeatiles?.startDate ?? "0"
                self.lblExpiryDate.text = self.capounDeatiles?.endDate ?? "0"
                self.lblStatus.text = self.capounDeatiles?.status?.title ?? ""

            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
        }
    }
    
    
}
