//
//  SubscriptionDeatilesVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 9/25/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import SwiftyStoreKit
class SubscriptionDeatilesVC: SuperViewController {
    
//    func SelectedDone(id: Int,nameplane: String, planID: Int,subscriptiontype: String) {
//        self.planpriceID = id
//        self.planID = planID
//        self.nameplan = nameplane
//        self.subscriptiontype = subscriptiontype
//        confirm()
//    }
    
    @IBOutlet weak var lblplane: UILabel!
    @IBOutlet weak var lblcreatedAtplane: UILabel!
    @IBOutlet weak var lblstartAtplane: UILabel!
    @IBOutlet weak var lblendAtplane: UILabel!
    @IBOutlet weak var lbltrialplane: UILabel!
    @IBOutlet weak var lblcanceledplane: UILabel!
    @IBOutlet weak var lblpaymentdateplane: UILabel!
    @IBOutlet weak var lblEndtrialplane: UILabel!
    @IBOutlet weak var btBuySubscription: UIButton!
    @IBOutlet weak var btCancelSubscription: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var heightcollectionView: NSLayoutConstraint!


    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.registerCell(id: "TypeSubscriptionsCell")
        NotificationCenter.default.addObserver(self, selector: #selector(UpdatedeatilesSubscription(notification:)), name: Notification.Name("UpdatedeatilesSubscription"), object: nil)
        
    }
    
    var deatilesSubscription:SubscriptionDeatiles?
    var subscriptiondata = [SubscriptionData]()
    var resourcePayment:PaymentRequest?
    var subscriptiontype:String?
    var planID:Int?
    var planpriceID:Int?
    var nameplan:String?

    
    @objc func UpdatedeatilesSubscription(notification: NSNotification)  {
        getSubscriptions()
        getAllSubscriptions()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getSubscriptions()
        getAllSubscriptions()
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    func confirm(){
        if planpriceID == self.deatilesSubscription?.plan?.planPriceId,self.deatilesSubscription?.trial == false{
            self.showAlert(title: "", message: "You are already subscribed to the package".localized)
        }else{
            self.showAlertWithCancel(title: "Confirmation".localized, message: "Confirmation".localized + "" + " " + "\(subscriptiontype ?? "")" + " "  + "\("at".localized)" + " \(nameplan ?? "")؟".localized, okAction: "Confirmation".localized) { (UIAlertAction) in
                self.UpdateSubscriptions()
                self.getSubscriptions()
                self.getAllSubscriptions()
            }
        }
    }
    
    @IBAction func didTab_BuySubscription(_ sender: Any) {
        self.BuySubscriptions()
    }
    
    @IBAction func didTab_ChangeSubscription(_ sender: Any) {
//        SwiftyStoreKit.purchaseProduct("com.zaad.seller.100year", quantity: 1, atomically: true) { result in
//            switch result {
//            case .success(let purchase):
//                print("Purchase Success: \(purchase.productId)")
//            case .error(let error):
//                switch error.code {
//                case .unknown: print("Unknown error. Please contact support")
//                case .clientInvalid: print("Not allowed to make the payment")
//                case .paymentCancelled: break
//                case .paymentInvalid: print("The purchase identifier was invalid")
//                case .paymentNotAllowed: print("The device is not allowed to make the payment")
//                case .storeProductNotAvailable: print("The product is not available in the current storefront")
//                case .cloudServicePermissionDenied: print("Access to cloud service information is not allowed")
//                case .cloudServiceNetworkConnectionFailed: print("Could not connect to the network")
//                case .cloudServiceRevoked: print("User has revoked permission to use this cloud service")
//                default: print((error as NSError).localizedDescription)
//                }
//            }
//        }

        let vc:ChangeSubscriptionVC = ChangeSubscriptionVC.loadFromNib()
        vc.deatiles = deatilesSubscription
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)

    }

    @IBAction func didTab_CancelBuySubscription(_ sender: Any) {
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "subscription/cancel", method: HTTPMethod.post).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    self.showAlert(title: "Error".localized, message:Status.message ?? "")
                    return
                }else{
                    self.showAlert(title: "", message: Status.message ?? "")
                    
                    SwiftyStoreKit.restorePurchases(atomically: true) { results in
                        if results.restoreFailedPurchases.count > 0 {
                            print("Restore Failed: \(results.restoreFailedPurchases)")
                        }
                        else if results.restoredPurchases.count > 0 {
                            print("Restore Success: \(results.restoredPurchases)")
                        }
                        else {
                            print("Nothing to Restore")
                        }
                    }
                    NotificationCenter.default.post(name: Notification.Name("UpdatedeatilesSubscription"), object: nil)
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    
    func UpdateSubscriptions(){
         
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
         var parameters: [String: Any] = [:]
         parameters["plan_id"] = self.planID?.description ?? "0"
         parameters["plan_price_id"] = self.planpriceID?.description ?? "0"
         
        _ = WebRequests.setup(controller: self).prepare(query: "subscription/subscribe", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(BuyResponse.self, from: response.data!)
                self.resourcePayment = Status.data?.paymentRequest
                let link = self.resourcePayment?.returnurl ?? ""
                if link != ""{
                    let vc:BuySubscriptionVC = BuySubscriptionVC.loadFromNib()
                    vc.paylink = link
                    vc.modalPresentationStyle = .fullScreen
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    self.showAlert(title: "", message: "Successfully".localized)
                }
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    
    
    func BuySubscriptions(){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
//        var parameters: [String: Any] = [:]
//        parameters["plan_id"] = self.deatilesSubscription?.plan?.planId?.description ?? "0"
//        parameters["plan_price_id"] = self.deatilesSubscription?.plan?.planPriceId?.description ?? "0"
//
//        _ = WebRequests.setup(controller: self).prepare(query: "subscription/subscribe", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
//
//            do {
//                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
//                if Status.status != 200 {
//                    self.showAlert(title: "Error".localized, message:Status.message!)
//                    return
//                }
//
//            }catch let jsonErr {
//                print("Error serializing  respone json", jsonErr)
//            }
//            do {
//                let Status =  try JSONDecoder().decode(BuyResponse.self, from: response.data!)
//                self.resourcePayment = Status.data?.paymentRequest
//                let link = self.resourcePayment?.returnurl ?? ""
//                if link != ""{
//                    let vc:BuySubscriptionVC = BuySubscriptionVC.loadFromNib()
//                    vc.paylink = link
//                    vc.modalPresentationStyle = .fullScreen
//                    self.navigationController?.pushViewController(vc, animated: true)
//                }else{
//                    self.showAlert(title: "", message: "تم بنجاح".localized)
//                }
//            } catch let jsonErr {
//                print("Error serializing  respone json", jsonErr)
//            }
//        }
    }
    
    func getSubscriptions(){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "subscription", method: HTTPMethod.get).start(){ (response, error) in
                    
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(SubscriptionDeatilesObject.self, from: response.data!)
                self.deatilesSubscription = Status.data
                self.lblplane.text = self.deatilesSubscription?.plan?.title ?? ""
                self.lblcreatedAtplane.text = self.deatilesSubscription?.createdAt?.suffix(11).description ?? ""
                self.lblstartAtplane.text = self.deatilesSubscription?.startsAt?.suffix(11).description ?? ""
                self.lblendAtplane.text = self.deatilesSubscription?.endsAt?.suffix(11).description ?? ""
                self.lblpaymentdateplane.text = self.deatilesSubscription?.paymentDate ?? ""
                self.lblEndtrialplane.text = self.deatilesSubscription?.cancelsAt ?? ""
                if self.deatilesSubscription?.trial == true{
                    self.lbltrialplane.font = UIFont.FFShamelFamilySemiRoundBook(ofSize: 25)
                    self.lbltrialplane.text = "✓"
                }else{
                    self.lbltrialplane.font = UIFont.FFShamelFamilySemiRoundBook(ofSize: 25)
                    self.lbltrialplane.text = "☓"
                }
                if self.deatilesSubscription?.canceled == true{
                    self.lblcanceledplane.font = UIFont.FFShamelFamilySemiRoundBook(ofSize: 25)
                    self.lblcanceledplane.text = "✓"
                }else{
                    self.lblcanceledplane.font = UIFont.FFShamelFamilySemiRoundBook(ofSize: 25)
                    self.lblcanceledplane.text = "☓"
                }
                if self.deatilesSubscription?.trial == true{
                    self.btBuySubscription.isHidden = true
                    self.btCancelSubscription.isHidden = true
                }else{
                    self.btBuySubscription.isHidden = true
                    self.btCancelSubscription.isHidden = true
                }

            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
        }

    }
    func getAllSubscriptions(){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
         _ = WebRequests.setup(controller: self).prepare(query: "plans", method: HTTPMethod.get).start(){ (response, error) in
                     
             do {
                 let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                 if Status.status != 200{
                     self.showAlert(title: "error".localized, message:Status.message!)
                     return
                 }
                 
             }catch let jsonErr {
                 print("Error serializing respone json", jsonErr)
             }
             
             
             do {
                 let Status =  try JSONDecoder().decode(SubscriptionObject.self, from: response.data!)
                 self.subscriptiondata = Status.data ?? []
                 self.collectionView.reloadData()
                let height = self.collectionView.collectionViewLayout.collectionViewContentSize.height
                self.heightcollectionView.constant = height
                
             } catch let jsonErr {
                 print("Error serializing respone json", jsonErr)
             }
         }
     }



}

extension SubscriptionDeatilesVC:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return subscriptiondata.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TypeSubscriptionsCell", for: indexPath) as! TypeSubscriptionsCell
        let sectionData = self.subscriptiondata[indexPath.section]
        cell.lblSubscriptionTitle.text = sectionData.title ?? ""
        cell.subscriptionfeatures = sectionData.features ?? []
        if sectionData.prices?.count != 0{
            cell.prices = sectionData.prices ?? []
            cell.collectionViewSubscriptionprice.isHidden = false
        }else{
            cell.collectionViewSubscriptionprice.isHidden = true
        }
        cell.subscriptiondata = sectionData
       // cell.subscriptionDelgate  = self
        cell.ViewSubscription.isHidden = true
        cell.ViewFree.isHidden = true
        cell.isFromDeatiles = true
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width - 80 , height: 400)

    }
    
    
}
