//
//  HeaderSubscriptionsCell.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 9/16/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

protocol SubscriptionDelgate : class {
    func SelectedDone(id:Int,appleID:String,nameplane:String,planID:Int,subscriptiontype:String)
}

class TypeSubscriptionsCell: UICollectionViewCell {
    
    @IBOutlet weak var HeightcollectionViewSubscription: NSLayoutConstraint!
    @IBOutlet weak var HeightViewSubscriptionprice: NSLayoutConstraint!
    @IBOutlet weak var ViewSubscription: UIView!
    @IBOutlet weak var ViewSub: UIView!
    @IBOutlet weak var ViewFree: UIView!
    @IBOutlet weak var ViewPrice: UIView!
    @IBOutlet weak var btSub: UIButton!
    @IBOutlet weak var btcontactus: UIButton!
    @IBOutlet weak var Viewcontactus: UIView!

    var planpriceID:Int?
    var subscriptionDelgate:SubscriptionDelgate?
    var isFromDeatiles = false
    var subscriptiondata:SubscriptionData?
    var selectedsubscription:Int?
    var addsubscription: (() -> ())?
    var contactus: (() -> ())?

    
    var prices : [Prices]?{
        didSet{
            self.collectionViewSubscriptionprice.reloadData()
        }
    }
    var subscriptionfeatures : [SubscriptionFeatures]?{
        didSet{
            self.collectionViewSubscription.reloadData()
        }
    }
    
    override func prepareForReuse() {
        collectionViewSubscription.reloadData()
        collectionViewSubscriptionprice.reloadData()
        super.prepareForReuse()
    }
    
    @IBOutlet weak var lblSubscriptionTitle: UILabel!
    @IBOutlet weak var collectionViewSubscription: UICollectionView!
    @IBOutlet weak var collectionViewSubscriptionprice: UICollectionView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionViewSubscription.registerCell(id: "SubscriptionsCell")
        collectionViewSubscriptionprice.registerCell(id: "FooterSubscriptionsCell")
        collectionViewSubscription.delegate = self
        collectionViewSubscription.dataSource = self
        collectionViewSubscriptionprice.delegate = self
        collectionViewSubscriptionprice.dataSource = self
    }
    @IBAction func btSub(_ sender: UIButton) {
        addsubscription?()
    }
    @IBAction func btcontactus(_ sender: UIButton) {
        contactus?()
    }
}

extension TypeSubscriptionsCell:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionViewSubscription{
            return subscriptionfeatures?.count ?? 0
        }else{
            return prices?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionViewSubscription{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubscriptionsCell", for: indexPath) as! SubscriptionsCell
        cell.subscriptions = subscriptionfeatures?[indexPath.row]
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FooterSubscriptionsCell", for: indexPath) as! FooterSubscriptionsCell
            let obj = prices?[indexPath.row]
            cell.prices = prices?[indexPath.row]
            if isFromDeatiles{
                cell.ViewSubscriptions.isHidden = false
            }else{
                cell.ViewSubscriptions.isHidden = true
            }
            if selectedsubscription == indexPath.row{
                cell.ViewSub.borderColor = "014A97".color
                cell.ViewSub.borderWidth = 1
            }else{
                cell.ViewSub.borderColor = UIColor.clear
                cell.ViewSub.borderWidth = 0
            }
            if obj?.mode == ""{
                cell.btMode.isHidden = true
            }else{
                cell.btMode.isHidden = false
            }
            
            if obj?.current == true{
                cell.lblcurrent.isHidden = false
            }else{
                cell.lblcurrent.isHidden = true
            }

            cell.ModeTapped = { [weak self] in
                if self!.isFromDeatiles{
                    self?.subscriptionDelgate?.SelectedDone(id: obj?.id ?? 0, appleID: obj?.appleProductId ?? "", nameplane: "\(self?.subscriptiondata?.title ?? "")", planID: self?.subscriptiondata?.id ?? 0, subscriptiontype: cell.btMode.titleLabel?.text ?? "")
                    
                }else{
                    //    self?.subscriptionDelgate?.SelectedDone(id: obj?.id ?? 0, nameplane: self?.subscriptiondata?.title ?? "", planID: self?.subscriptiondata?.id ?? 0, subscriptiontype: obj?.title ?? "")
                }
            }
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if !isFromDeatiles{
            let obj = prices?[indexPath.row]
            if collectionView == collectionViewSubscriptionprice{
                self.subscriptionDelgate?.SelectedDone(id: obj?.id ?? 0, appleID: obj?.appleProductId ?? "", nameplane: self.subscriptiondata?.title ?? "", planID: self.subscriptiondata?.id ?? 0, subscriptiontype: obj?.title ?? "")
            }
            self.selectedsubscription = indexPath.row
            self.collectionViewSubscriptionprice.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == collectionViewSubscriptionprice{
            let height = self.collectionViewSubscriptionprice.collectionViewLayout.collectionViewContentSize.height
            self.HeightViewSubscriptionprice.constant = height
        }else{
//            let height = self.collectionViewSubscription.collectionViewLayout.collectionViewContentSize.height
//            self.HeightViewSubscriptionprice.constant = height

        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = UIScreen.main.bounds.size.width
        if collectionView == collectionViewSubscription{
            return CGSize(width: width - 80, height: 50)
        }else{
            if isFromDeatiles{
                return CGSize(width: width - 80, height: 80)
            }else{
                return CGSize(width: width - 80, height: 55)
            }
        }
    }
    
    
}

