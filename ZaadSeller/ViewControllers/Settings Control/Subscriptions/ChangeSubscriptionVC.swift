//
//  ChangeSubscriptionVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 10/12/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import SwiftyStoreKit

class ChangeSubscriptionVC: SuperViewController,SubscriptionDelgate {
    
    
    func SelectedDone(id: Int, appleID: String, nameplane: String, planID: Int, subscriptiontype: String) {
        self.planpriceID = id
        self.planID = planID
        self.nameplan = nameplane
        self.appleID = appleID

        self.subscriptiontype = subscriptiontype
        confirm()
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var heightcollectionView: NSLayoutConstraint!
    var subscriptiondata = [SubscriptionData]()
    var subscriptiontype:String?
    var deatiles:SubscriptionDeatiles?
    var planID:Int?
    var planpriceID:Int?
    var nameplan:String?
    var appleID:String?
    var resourcePayment:PaymentRequest?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.registerCell(id: "TypeSubscriptionsCell")
        NotificationCenter.default.addObserver(self, selector: #selector(UpdatedeatilesSubscription(notification:)), name: Notification.Name("UpdatedeatilesSubscription"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
          getAllSubscriptions()
          let navgtion = self.navigationController as! CustomNavigationBar
          navgtion.setCustomBackButtonWhiteForViewController(sender: self)
          navgtion.setLogotitle(sender: self)
          navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
          
      }
      override func viewWillDisappear(_ animated: Bool) {
          let navgtion = self.navigationController as! CustomNavigationBar
          navgtion.setCustomBackButtonWhiteForViewController(sender: self)
          navgtion.setLogotitle(sender: self)
          navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
          
      }
      
    
    @objc func UpdatedeatilesSubscription(notification: NSNotification)  {
        getAllSubscriptions()
    }
    
    func confirm(){
        if  self.deatiles?.trial == false{
            self.showAlert(title: "", message: "You are already subscribed to the package".localized)
        }else{
            self.showAlertWithCancel(title: "Confirmation".localized, message: "Confirmation".localized + "" + " " + "\(subscriptiontype ?? "")" + " "  + "\("at".localized)" + " \(nameplan ?? "")؟".localized, okAction: "Confirmation".localized) { (UIAlertAction) in
                self.UpdateSubscriptions()
//                self.getAllSubscriptions()
            }
        }
    }
    
    func UpdateSubscriptions(){
        
        self.showIndicator()
        SwiftyStoreKit.purchaseProduct(self.appleID ?? "", quantity: 1, atomically: true) { result in
            self.hideIndicator()

            switch result {
           

            case .success(let purchase):
                self.bydDone()
                print("Purchase Success: \(purchase.productId)")
            case .error(let error):
                switch error.code {
                case .unknown:
                    self.showAlert(title: "Error".localized, message:"Unknown error. Please contact support".localized)
                    print("Unknown error. Please contact support")
                case .clientInvalid:
                    self.showAlert(title: "Error".localized, message:"Not allowed to make the payment".localized)

                    print("Not allowed to make the payment")
                case .paymentCancelled: break
                case .paymentInvalid:
                    self.showAlert(title: "Error".localized, message:"The purchase identifier was invalid".localized)

                    print("The purchase identifier was invalid")
                case .paymentNotAllowed:
                    self.showAlert(title: "Error".localized, message:"The device is not allowed to make the payment".localized)

                    print("The device is not allowed to make the payment")
                case .storeProductNotAvailable:
                    self.showAlert(title: "Error".localized, message:"The product is not available in the current storefront".localized)

                    print("The product is not available in the current storefront")
                case .cloudServicePermissionDenied:
                    self.showAlert(title: "Error".localized, message:"Access to cloud service information is not allowed".localized)

                    print("Access to cloud service information is not allowed")
                case .cloudServiceNetworkConnectionFailed:
                    self.showAlert(title: "Error".localized, message:"Could not connect to the network".localized)

                    print("Could not connect to the network")
                case .cloudServiceRevoked:
                    self.showAlert(title: "Error".localized, message:"User has revoked permission to use this cloud service".localized)

                    print("User has revoked permission to use this cloud service")
                default: print((error as NSError).localizedDescription)
                }
            }
        }

//        SwiftyStoreKit.retrieveProductsInfo([self.appleID ?? ""]) { result in
//            if let product = result.retrievedProducts.first {
//                let priceString = product.localizedPrice!
//                print("Product: \(product.localizedDescription), price: \(priceString)")
//            }
//            else if let invalidProductId = result.invalidProductIDs.first {
//                print("Invalid product identifier: \(invalidProductId)")
//            }
//            else {
//                print("Error: \(result.error)")
//            }
//        }
        
        
        
//        SwiftyStoreKit.fetchReceipt(forceRefresh: true) { result in
//            switch result {
//            case .success(let receiptData):
//                let encryptedReceipt = receiptData.base64EncodedString(options: [])
//                print("Fetch receipt success:\n\(encryptedReceipt)")
//            case .error(let error):
//                print("Fetch receipt failed: \(error)")
//            }
//
//        }
        return
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        var parameters: [String: Any] = [:]
        parameters["plan_id"] = self.planID?.description ?? "0"
        parameters["plan_price_id"] = self.planpriceID?.description ?? "0"
        
        _ = WebRequests.setup(controller: self).prepare(query: "subscription/subscribe", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(BuyResponse.self, from: response.data!)
                self.resourcePayment = Status.data?.paymentRequest
                let link = self.resourcePayment?.returnurl ?? ""
                if link != ""{
                    let vc:BuySubscriptionVC = BuySubscriptionVC.loadFromNib()
                    vc.paylink = link
                    vc.modalPresentationStyle = .fullScreen
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    self.showAlert(title: "", message: "Successfully".localized)
                }
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
    }
    
    
    func bydDone(){
        SwiftyStoreKit.fetchReceipt(forceRefresh: true) { result in
            switch result {
            case .success(let receiptData):
                let encryptedReceipt = receiptData.base64EncodedString(options: [])
                print("Fetch receipt success:\n\(encryptedReceipt)")
                
                guard Helper.isConnectedToNetwork() else {
                    self.showAlert(title: "".localized, message: "There is no internet connection".localized)
                    return }
                
                var parameters: [String: Any] = [:]
                parameters["receipt"] = encryptedReceipt
                parameters["seller_id"] = "\(CurrentUser.userInfo?.user?.id ?? 0)"
                parameters["store_id"] = "\(CurrentUser.userInfo?.user?.store?.id ?? 0)"
                
                _ = WebRequests.setup(controller: self).prepare(query: "subscription/payments/apple/validate-receipt", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
                    
                    do {
                        let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                        if Status.status != 200 {
                            self.showAlert(title: "Error".localized, message:Status.message!)
                            return
                        }
                        
                    }catch let jsonErr {
                        print("Error serializing respone json", jsonErr)
                    }
                    do {
                        self.showAlert(title: "", message: "Successfully".localized)
                    } catch let jsonErr {
                        print("Error serializing respone json", jsonErr)
                    }
                }
                
                
                
            case .error(let error):
                print("Fetch receipt failed: \(error)")
            }

        }

    }
    
    func getAllSubscriptions(){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "plans", method: HTTPMethod.get).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(SubscriptionObject.self, from: response.data!)
                self.subscriptiondata = Status.data ?? []
                self.collectionView.reloadData()
//                let height = self.collectionView.collectionViewLayout.collectionViewContentSize.height
//                self.heightcollectionView.constant = height
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
    }
    
    
    
}


extension ChangeSubscriptionVC:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return subscriptiondata.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TypeSubscriptionsCell", for: indexPath) as! TypeSubscriptionsCell
        let sectionData = self.subscriptiondata[indexPath.section]
        cell.lblSubscriptionTitle.text = sectionData.title ?? ""
        cell.subscriptionfeatures = sectionData.features ?? []
        if sectionData.prices?.count != 0{
            cell.prices = sectionData.prices ?? []
            cell.Viewcontactus.isHidden = true
            cell.ViewPrice.isHidden = false
            cell.collectionViewSubscriptionprice.isHidden = false
        }else{
            cell.Viewcontactus.isHidden = false
            cell.ViewPrice.isHidden = true
            cell.collectionViewSubscriptionprice.isHidden = true
        }
        cell.contactus = { [weak self] in
            let vc:CallUsVC = CallUsVC.loadFromNib()
            vc.modalPresentationStyle = .fullScreen
            self?.navigationController?.pushViewController(vc, animated: true)
        }
        cell.subscriptiondata = sectionData
        cell.subscriptionDelgate  = self
        cell.ViewSubscription.isHidden = true
        cell.ViewFree.isHidden = true
        cell.isFromDeatiles = true
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width - 80 , height: UIScreen.main.bounds.height - 150)
        
    }
    
    
}
