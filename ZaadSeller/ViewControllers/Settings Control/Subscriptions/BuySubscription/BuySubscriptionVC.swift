//
//  BuySubscriptionVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 10/4/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import WebKit

class BuySubscriptionVC: SuperViewController {
    
    @IBOutlet weak var webView:WKWebView!
    var paylink:String?


    override func viewDidLoad() {
        super.viewDidLoad()
        webView.load(URLRequest(url: URL(string: paylink ?? "")!))
    }

    override func viewWillAppear(_ animated: Bool) {
          let navgtion = self.navigationController as! CustomNavigationBar
          navgtion.setCustomBackButtonWhiteForViewController(sender: self)
          navgtion.setLogotitle(sender: self)
          navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
      }
      override func viewWillDisappear(_ animated: Bool) {
          let navgtion = self.navigationController as! CustomNavigationBar
          navgtion.setCustomBackButtonWhiteForViewController(sender: self)
          navgtion.setLogotitle(sender: self)
          navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
      }


}
