//
//  SubscriptionsCell.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 9/16/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class SubscriptionsCell: UICollectionViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblValue: UILabel!

    var subscriptions :SubscriptionFeatures?{
        didSet{
            self.lblTitle.text  = subscriptions?.title ?? ""
            if subscriptions?.value == "1",subscriptions?.type == "boolean"{
                self.lblValue.textColor = "B1AF00".color
                self.lblValue.font = UIFont.FFShamelFamilySemiRoundBook(ofSize: 25)
                if subscriptions?.value == "1"{
                    self.lblValue.text  = "✓"
                }else{
                    self.lblValue.text  = "☓"
                }
            }else{
                self.lblValue.font = UIFont.FFShamelFamilySemiRoundBook(ofSize: 15)
                self.lblValue.textColor = "FF501B".color
                self.lblValue.text  = subscriptions?.value ?? ""
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

}
