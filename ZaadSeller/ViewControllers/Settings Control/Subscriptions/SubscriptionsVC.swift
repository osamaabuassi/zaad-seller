//
//  SubscriptionsVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 9/16/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class SubscriptionsVC: SuperViewController,SubscriptionDelgate {
    
    
    func SelectedDone(id: Int, appleID: String, nameplane: String, planID: Int, subscriptiontype: String) {
        self.planpriceID = id
        self.nameplan = nameplane
        self.planID = planID
        self.subscriptiontype = subscriptiontype
        confirm()
    }
    

    @IBOutlet weak var collectionView: UICollectionView!

    var resourcePayment:PaymentRequest?
    var subscriptiondata = [SubscriptionData]()
    var subscriptiontype:String?
    var planID:Int?
    var planpriceID:Int?
    var nameplan:String?
    var isFromRegister = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.registerCell(id: "TypeSubscriptionsCell")
        getSubscriptions()
    }
    
    func confirm(){
//        if isFromRegister{
//            self.showAlertWithCancel(title: "تأكيد".localized, message: "تأكيد".localized + "\(subscriptiontype ?? "")" + " " + "في" + " \(nameplan ?? "")؟".localized, okAction: "تأكيد".localized) { (UIAlertAction) in
////                let vc:RegsiterVC = RegsiterVC.loadFromNib()
////                vc.modalPresentationStyle = .fullScreen
////                vc.planpriceid = self.planpriceID
////                vc.planid = self.planID
////                self.navigationController?.pushViewController(vc, animated: true)
//            }
//        }else{
//
////            self.showAlertWithCancel(title: "تأكيد".localized, message: "تأكيد الإشتراك في  \(nameplan ?? "")؟".localized, okAction: "تأكيد".localized) { (UIAlertAction) in
////                self.BuySubscriptions()
////            }
//        }
        
        
        let alertController = UIAlertController(title: "Confirmation".localized, message: "Confirmation".localized + "\(subscriptiontype ?? "")" + " " + "at" + " \(nameplan ?? "")؟".localized, preferredStyle: .alert)

            // Create the actions
        let okAction = UIAlertAction(title: "Confirmation".localized, style: UIAlertAction.Style.default) {
                UIAlertAction in
                NSLog("OK Pressed")
            }
        let cancelAction = UIAlertAction(title: "Cancellation".localized, style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            self.planpriceID = nil
            self.nameplan = nil
            self.planID = nil
            self.subscriptiontype = nil
        }

            // Add the actions
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)

            // Present the controller
        self.present(alertController, animated: true, completion: nil)


    }
    
    func BuySubscriptions(){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        var parameters: [String: Any] = [:]
        parameters["plan_id"] =   self.planID?.description ?? "0"
        parameters["plan_price_id"] =  self.planpriceID?.description ?? "0"
        
        _ = WebRequests.setup(controller: self).prepare(query: "subscription/subscribe", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(ResourceSubscrip.self, from: response.data!)
                self.resourcePayment = Status.paymentRequest
                let link = self.resourcePayment?.returnurl ?? ""
                if link != ""{
                    let vc:BuySubscriptionVC = BuySubscriptionVC.loadFromNib()
                    vc.paylink = link
                    vc.modalPresentationStyle = .fullScreen
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    self.showAlert(title: "", message: "Successfully".localized)
                }
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.planpriceID = nil
        self.nameplan = nil
        self.planID = nil
        self.subscriptiontype = nil
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    
    func getSubscriptions(){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        if isFromRegister{
            _ = WebRequests.setup(controller: self).prepare(query: "plans/public", method: HTTPMethod.get).start(){ (response, error) in
                        
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "error".localized, message:Status.message!)
                        return
                    }
                    
                }catch let jsonErr {
                    print("Error serializing respone json", jsonErr)
                }
                
                
                do {
                    let Status =  try JSONDecoder().decode(SubscriptionObject.self, from: response.data!)
                    self.subscriptiondata = Status.data ?? []
                    self.collectionView.reloadData()
                } catch let jsonErr {
                    print("Error serializing respone json", jsonErr)
                }
            }
        }else{
            _ = WebRequests.setup(controller: self).prepare(query: "plans", method: HTTPMethod.get).start(){ (response, error) in
                        
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "error".localized, message:Status.message!)
                        return
                    }
                    
                }catch let jsonErr {
                    print("Error serializing respone json", jsonErr)
                }
                
                do {
                    let Status =  try JSONDecoder().decode(SubscriptionObject.self, from: response.data!)
                    self.subscriptiondata = Status.data ?? []
                    self.collectionView.reloadData()
                } catch let jsonErr {
                    print("Error serializing respone json", jsonErr)
                }
            }
        }
    }

}

extension SubscriptionsVC:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return subscriptiondata.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TypeSubscriptionsCell", for: indexPath) as! TypeSubscriptionsCell
        let sectionData = self.subscriptiondata[indexPath.section]
        cell.lblSubscriptionTitle.text = sectionData.title ?? ""
        cell.subscriptionfeatures = sectionData.features ?? []
        cell.subscriptiondata = sectionData
        cell.prices = sectionData.prices ?? []
        //        cell.isFromDeatiles = true
        cell.subscriptionDelgate  = self
        cell.addsubscription = { [weak self] in
            
        guard (self?.planpriceID != nil) else{
            self?.showAlert(title: "".localized, message: "Please select a subscription type".localized)
            return
        }
            
        let vc:RegsiterVC = RegsiterVC.loadFromNib()
        vc.modalPresentationStyle = .fullScreen
        vc.planpriceid = self?.planpriceID
        vc.planid = self?.planID
        self?.navigationController?.pushViewController(vc, animated: true)
       }
        
        return cell
        
    }
//
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        guard let cell = collectionView.cellForItem(at: indexPath) as? TypeSubscriptionsCell
//        else{
//          return
//        }
//        cell.ViewSub.borderColor = "014A97".color
//        cell.ViewSub.borderWidth = 1
//       // self.collectionView.reloadData()
//        
//    }
     
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        guard let indexPath = collectionView.indexPathsForVisibleItems.last else {
//            return
//        }
//        let sectionData = self.subscriptiondata[indexPath.section]
//        self.planID = sectionData.id
//        self.nameplan = sectionData.title ?? ""
//
//    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width - 80 , height: UIScreen.main.bounds.height - 180)

    }
    
    
}
