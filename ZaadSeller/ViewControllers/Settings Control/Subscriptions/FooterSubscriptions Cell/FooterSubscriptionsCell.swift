//
//  FooterSubscriptionsCell.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 9/24/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class FooterSubscriptionsCell: UICollectionViewCell {
    
    @IBOutlet weak var lblplanname: UILabel!
    @IBOutlet weak var lblplanprice: UILabel!
    @IBOutlet weak var lbldiscountplanprice: UILabel!
    @IBOutlet weak var Viewdiscountplanprice: UIView!
    @IBOutlet weak var ViewSubscriptions: UIView!
    @IBOutlet weak var btMode: UIButton!
    @IBOutlet weak var btSubscription: UIButton!
    @IBOutlet weak var ViewSub: UIView!
    @IBOutlet weak var lblcurrent: UILabel!


    var ModeTapped: (() -> ())?
    var SubscriptionTapped: (() -> ())?

    
    var prices : Prices?{
        didSet{
            if prices?.salePrice != 0{
                self.lbldiscountplanprice.text =  "instead of".localized + "\(prices?.regularPrice ?? 0)" + "SAR".localized
                self.lbldiscountplanprice.isHidden = false
                self.Viewdiscountplanprice.isHidden = false
            }else{
                self.Viewdiscountplanprice.isHidden = true
            }
            self.btMode.setTitle(prices?.mode ?? "Subscription".localized, for: .normal)
            self.lblplanprice.text = "\(prices?.price ?? 0)" + "SAR".localized
            self.lblplanname.text = "\(prices?.title ?? "")"
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    @IBAction func Mode(_ sender: UIButton) {
        ModeTapped?()
    }
    
    
    @IBAction func Subscription(_ sender: UIButton) {
        SubscriptionTapped?()
    }
    
    
}
