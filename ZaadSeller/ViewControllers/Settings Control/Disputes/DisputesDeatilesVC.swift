//
//  DisputesDeatilesVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 9/5/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class DisputesDeatilesVC: SuperViewController {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var lblNameproducat: UILabel!
    @IBOutlet weak var lblproblame: UILabel!
    @IBOutlet weak var lblstatus: UILabel!
    @IBOutlet weak var lbldate: UILabel!
    @IBOutlet weak var imgdisputes: UIImageView!
    @IBOutlet weak var btSendAdmin: UIButton!
    @IBOutlet weak var btCloseDisputes: UIButton!
    @IBOutlet weak var lblvideo: UILabel!

    
    
    var disputesitemss:DisputesDeatilesData?
    var idDisputes:Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        getDisputestDeatiles()

    }
    


    
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    func getDisputestDeatiles(){
        
        
        _ = WebRequests.setup(controller: self).prepare(query: "disputes/\(idDisputes ?? 0)", method: HTTPMethod.get).start(){ (response, error) in
                    
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(DisputesDeatilesObject.self, from: response.data!)
                self.disputesitemss = Status.data

                self.lblNameproducat.text = self.disputesitemss?.item ?? ""
                self.lblstatus.text = self.disputesitemss?.status?.name ?? ""
                self.lblName.text = self.disputesitemss?.user?.name ?? ""
                self.lblCode.text = self.disputesitemss?.disputeCode ?? ""
                self.lblproblame.text = self.disputesitemss?.problem ?? ""
                self.lbldate.text = self.disputesitemss?.createdAt ?? ""
                self.lblvideo.text = self.disputesitemss?.videoUrl ?? ""
                self.imgdisputes.sd_custom(url: self.disputesitemss?.attachment ?? "")

                if self.disputesitemss?.sellerAskedForClose == false{
                    self.btCloseDisputes.isHidden = false
                }else{
                    self.btCloseDisputes.isHidden = true
                }
                if self.disputesitemss?.adminInformed == false{
                    self.btSendAdmin.isHidden = false
                }else{
                    self.btSendAdmin.isHidden = true
                }
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
        }
        
    }


    @IBAction func didTab_SendAdmin(_ sender: Any) {
               
               _ = WebRequests.setup(controller: self).prepare(query: "disputes/\(idDisputes ?? 0)/actions/inform-admin", method: HTTPMethod.post).start(){ (response, error) in
                   
                   do {
                       let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                       if Status.status != 200 {
                           
                           self.showAlert(title: "Error".localized, message:Status.message!)
                           return
                       }else{
                           self.navigationController?.popViewController(animated: true)
                           NotificationCenter.default.post(name: Notification.Name("UpdateDisputes"), object: nil)
                       }
                       
                   }catch let jsonErr {
                       print("Error serializing respone json", jsonErr)
                   }
                   
               }
           
           
       }
    @IBAction func didTab_Close(_ sender: Any) {
        
            
        _ = WebRequests.setup(controller: self).prepare(query: "disputes/\(idDisputes ?? 0))/actions/close", method: HTTPMethod.post).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }else{
                    self.navigationController?.popViewController(animated: true)
                    NotificationCenter.default.post(name: Notification.Name("UpdateDisputes"), object: nil)
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
        }
    
    
}
    @IBAction func didTab_Chat(_ sender: Any) {
        
        let vc:ChatVC = ChatVC.loadFromNib()
        vc.modalPresentationStyle = .fullScreen
        vc.name = self.disputesitemss?.user?.name ?? ""
        vc.idDisputes = self.disputesitemss?.id ?? 0
        vc.isFromDisputes = true
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)

        
    }
    

}
