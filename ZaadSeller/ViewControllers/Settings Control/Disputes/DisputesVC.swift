//
//  DisputesVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 9/5/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class DisputesVC: SuperViewController {

    @IBOutlet weak var tableView: UITableView!
    var currentpage = 1
    var disputesData:DisputesData?
    var disputesitems = [DisputesResources]()

    
     override func viewDidLoad() {
         super.viewDidLoad()
         self.tableView.tableFooterView = UIView()
         tableView.registerCell(id: "DisputesCell")
         getDisputest()
         self.tableView.es.addPullToRefresh {
             self.currentpage = 1
             self.disputesitems.removeAll()
             self.getDisputest()
             self.hideIndicator()
         }
         
         self.tableView.es.addInfiniteScrolling {
             self.currentpage += 1
             self.getDisputest() // next page
             self.tableView.estimatedRowHeight = 0
             self.hideIndicator()
         }
         NotificationCenter.default.addObserver(self, selector: #selector(UpdateDisputes(notification:)), name: Notification.Name("UpdateDisputes"), object: nil)
         
     }
    
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    func getDisputest(){
        guard Helper.isConnectedToNetwork() else {
            self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
            self.emptyView?.imgEmpty.image = UIImage(named: "refreshment")
            self.emptyView?.firstLabel.text = "There is no internet connection".localized
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "disputes?page=\(currentpage)", method: HTTPMethod.get).start(){ (response, error) in
            
            self.tableView.es.stopLoadingMore()
            self.tableView.es.stopPullToRefresh()
            
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(DisputesObject.self, from: response.data!)
                self.disputesData = Status.data
                self.disputesitems += self.disputesData!.resources!
                if self.disputesData?.nextPageUrl == nil {
                    self.tableView.es.stopLoadingMore()
                    self.tableView.es.noticeNoMoreData()
                }
                if self.disputesitems.count == 0{
                    self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                    self.emptyView?.firstLabel.text = "No Disputes to Show".localized
                    self.emptyView?.imgEmpty.isHidden = true
                    self.disputesitems.removeAll()
                    self.tableView.reloadData()
                    
                }else{
                    self.tableView.tableFooterView = nil
                }
                self.tableView.reloadData()
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
        }
        
    }
    
    @IBAction func didRefersh(){
        disputesitems.removeAll()
        getDisputest()
    }

    @objc func UpdateDisputes(notification: NSNotification)  {
           disputesitems.removeAll()
           getDisputest()
       }
  
}
extension DisputesVC:UITableViewDataSource,UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return disputesitems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DisputesCell", for: indexPath) as! DisputesCell
        cell.disputesitems =  disputesitems[indexPath.row]
        cell.DeatilesTapped = { [weak self] in
            let  obj  = self?.disputesitems[indexPath.row]
            let vc:DisputesDeatilesVC = DisputesDeatilesVC.loadFromNib()
            vc.idDisputes = obj?.id
            vc.modalPresentationStyle = .fullScreen
            self?.navigationController?.pushViewController(vc, animated: true)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let  obj  = disputesitems[indexPath.row]
        let vc:DisputesDeatilesVC = DisputesDeatilesVC.loadFromNib()
        vc.idDisputes = obj.id
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 188
    }
    
    
    
    
}
