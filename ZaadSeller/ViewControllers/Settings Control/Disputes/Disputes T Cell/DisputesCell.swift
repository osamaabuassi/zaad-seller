//
//  DisputesCell.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 9/5/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class DisputesCell: UITableViewCell {
    
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var lblNameproducat: UILabel!
    @IBOutlet weak var lblproblame: UILabel!
    @IBOutlet weak var lblstatus: UILabel!
    @IBOutlet weak var lbldate: UILabel!
    @IBOutlet weak var btDeatiles: UIButton!
    var DeatilesTapped: (() -> ())?
    
    
    var disputesitems:DisputesResources?{
        didSet{
            self.lblCode.text = disputesitems?.disputeCode ?? ""
            self.lblNameproducat.text = disputesitems?.item ?? ""
            self.lblproblame.text = disputesitems?.problem ?? ""
            self.lblstatus.text = disputesitems?.status?.name ?? ""
            self.lbldate.text = disputesitems?.createdAt ?? ""
        }
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func Deatiles(_ sender: UIButton) {
        DeatilesTapped?()
    }
    
    
}
