//
//  ShippingSettingsVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/4/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class ShippingSettingsVC: SuperViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!

    var tagsarray = ["Mandop".localized,"iTrans".localized,"Asyad Express".localized]

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.registerCell(id: "ShippngSettingsCell")

    }
    

    
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    
    @IBAction func didTab_Save(_ sender: Any) {
        print("test")
        
    }
    

}
extension ShippingSettingsVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tagsarray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShippngSettingsCell", for: indexPath) as! ShippngSettingsCell
        cell.lblName.text = tagsarray[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let string = tagsarray[indexPath.row]
        let font = UIFont.FFShamelFamilySemiRoundBook(ofSize: 14)
        let size = string.widthWithConstrainedHeight(30, font: font)
        return CGSize(width: size + 40, height: 50)

    }
    
    
    
}
