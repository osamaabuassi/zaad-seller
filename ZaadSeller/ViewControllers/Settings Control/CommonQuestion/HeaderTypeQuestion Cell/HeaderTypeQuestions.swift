//
//  HeaderTypeQuestions.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/15/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class HeaderTypeQuestions: UITableViewCell {
    
    @IBOutlet weak var lblType: UILabel!
    
    var questiondata:QuestionData?{
        didSet{
            self.lblType.text = questiondata?.title ?? ""
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()

    }

   
    
}
