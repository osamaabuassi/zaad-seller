//
//  CommonQuestionsVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/15/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class CommonQuestionsVC: SuperViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var questiondata = [QuestionData]()
    var question = [Questions]()
    var ExpandInt:Int?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerCell(id: "HeaderTypeQuestions")
        tableView.registerCell(id: "QuestionsCell")
        getQuestion()
    }
    override func viewWillAppear(_ animated: Bool) {
          let navgtion = self.navigationController as! CustomNavigationBar
          navgtion.setCustomBackButtonWhiteForViewController(sender: self)
          navgtion.setLogotitle(sender: self)
          navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
          
      }
      override func viewWillDisappear(_ animated: Bool) {
          let navgtion = self.navigationController as! CustomNavigationBar
          navgtion.setCustomBackButtonWhiteForViewController(sender: self)
          navgtion.setLogotitle(sender: self)
          navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
          
      }
    
    
    func getQuestion(){
        
        guard Helper.isConnectedToNetwork() else {
            self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
            self.emptyView?.imgEmpty.image = UIImage(named: "refreshment")
            self.emptyView?.firstLabel.text = "There is no internet connection".localized
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "common-questions", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(QuestionObject.self, from: response.data!)
                self.questiondata = Status.data!
                self.tableView.reloadData()
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
    }
    @objc func didRefersh(sender: UIButton) {
        getQuestion()
    }
    
}


extension CommonQuestionsVC:UITableViewDataSource,UITableViewDelegate{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return questiondata.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return (questiondata[section].questions?.count)!
       }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionsCell", for: indexPath) as! QuestionsCell
        let obj = questiondata[indexPath.section].questions![indexPath.row]
        cell.question = questiondata[indexPath.section].questions![indexPath.row]
        cell.lblNumber.text = "\(indexPath.row + 1)"
        if ExpandInt == obj.id{
            cell.ViewAnswer.isHidden = false
            cell.lblNumber.textColor = "FFFFFF".color
            cell.imgCheck.image = UIImage(named: "answer")
        }else{
            cell.ViewAnswer.isHidden = true
            cell.lblNumber.textColor = "003B87".color
            cell.imgCheck.image = UIImage(named: "unCheck")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableCell(withIdentifier: "HeaderTypeQuestions") as! HeaderTypeQuestions
        headerView.questiondata = questiondata[section]
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = questiondata[indexPath.section].questions![indexPath.row]

        if  ExpandInt == obj.id{
            ExpandInt =  -99
            tableView.reloadData()
        }else{
            ExpandInt = obj.id
            tableView.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
