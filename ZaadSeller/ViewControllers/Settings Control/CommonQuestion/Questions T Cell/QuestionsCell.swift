//
//  QuestionsCell.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/15/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class QuestionsCell: UITableViewCell {
    
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var lblAnswer: UILabel!
    @IBOutlet weak var ViewAnswer: UIView!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var imgCheck: UIImageView!


    
    var question:Questions?{
           didSet{
               self.lblQuestion.text = question?.question ?? ""
               self.lblAnswer.text = question?.answer ?? ""
           }
       }
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

  
    
}
