//
//  CallUsVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/5/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class CallUsVC: SuperViewController {
    
    @IBOutlet weak var lblName: UITextField!
    @IBOutlet weak var lblAddress: UITextField!
    @IBOutlet weak var tfemail: UITextField!
    @IBOutlet weak var txtNote: UITextView!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    
    @IBAction func didTab_Send(_ sender: Any) {
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        guard let name = self.lblName.text, !name.isEmpty else{
            self.showAlert(title: "".localized, message: "Please write the name".localized)
            return
        }
        guard let address = self.lblAddress.text, !address.isEmpty else{
            self.showAlert(title: "".localized, message: "Please write the address".localized)
            return
        }
        guard let note = self.txtNote.text, !note.isEmpty else{
            self.showAlert(title: "".localized, message: "Please write the message".localized)
            return
        }
        
        guard let email = self.tfemail.text, !email.isEmpty else{
                   self.showAlert(title: "".localized, message: "Please write an email".localized)
                   return
               }
        
        var parameters: [String: Any] = [:]
        parameters["message"] = self.txtNote.text ?? ""
        parameters["title"] = self.lblAddress.text ?? ""
        parameters["name"] = self.lblName.text ?? ""
        parameters["email"] = email
        
        
        _ = WebRequests.setup(controller: self).prepare(query: "contact-us", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }else if Status.status == 200{
                    self.navigationController?.popViewController(animated: true)
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
        }
        
        
        
    }
    
    
    
    
}
