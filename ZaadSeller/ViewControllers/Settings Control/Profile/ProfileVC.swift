//
//  ProfileVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/4/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class ProfileVC: SuperViewController,cityDelgate {
    
    @IBOutlet weak var tfStore: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfCountry: UITextField!
    @IBOutlet weak var tfCity: UITextField!
    @IBOutlet weak var tfGender: UITextField!
    @IBOutlet weak var tfMobile: UITextField!
    @IBOutlet weak var tfPhone: UITextField!
    @IBOutlet weak var tfAddress: UITextField!
    
    @IBOutlet weak var btCard: UIButton!
    @IBOutlet weak var btCommercial: UIButton!
    var isSelectImgCard: Bool = false
    var isSelectImgCommercial: Bool = false
    
    @IBOutlet weak var imgCard: UIImageView!
    @IBOutlet weak var imgCommercial: UIImageView!
    var imagePicker: ImagePicker!
    
    
    var profile:ProfileData?
    var countryData:CountryData?
    var country = [Country]()
    var selectedCountryID:Int?
    var cityData:CityData?
    var city = [City]()
    var selectedCityID:Int?
    var NationalityData:NationalityData?
    var nationality = [Nationality]()
    var selectedNationalityId:Int?
    
    
    func SelectedDone(name: String, id: Int) {
        self.tfCity.text = name
        self.selectedCityID = id
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getCountry()
        getProfile()
        getCity()
        getnationality()
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        
        
    }
    
    func getCountry(){
        _ = WebRequests.setup(controller: self).prepare(query: "countries", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(CountryObject.self, from: response.data!)
                self.countryData = Status.data!
                self.country = self.countryData!.resources!
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
        }
    }
    
    func getCity(){
        
        _ = WebRequests.setup(controller: self).prepare(query: "cities?country_id=\(selectedCountryID ?? 0)", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(CityObject.self, from: response.data!)
                self.cityData = Status.data!
                self.city = self.cityData!.resources!
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
        }
    }
    
    
    func getnationality(){
        
        _ = WebRequests.setup(controller: self).prepare(query: "nationalities", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(NationalityObject.self, from: response.data!)
                self.NationalityData = Status.data!
                self.nationality = self.NationalityData!.resources!
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
        }
    }
    
    
    
    
    
    
    
    func getProfile(){
        
        _ = WebRequests.setup(controller: self).prepare(query: "profile", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(ProfileObject.self, from: response.data!)
                self.profile = Status.data!
                self.tfEmail.text = self.profile?.email ?? ""
                self.tfStore.text = self.profile?.username ?? ""
                self.tfMobile.text = self.profile?.mobile ?? ""
                self.tfPhone.text = self.profile?.phone ?? ""
                self.tfCountry.text = self.profile?.country?.title ?? ""
                self.tfCity.text = self.profile?.city?.name ?? ""
                self.tfGender.text = self.profile?.nationality?.title ?? ""
                self.tfAddress.text = self.profile?.address ?? ""
                self.selectedCountryID = self.profile?.country?.id
                self.selectedCityID =  self.profile?.city?.id
                self.selectedNationalityId =  self.profile?.nationality?.id
                
                if self.profile?.idCard != nil {
                self.imgCard.sd_custom(url: self.profile?.idCard ?? "")
                }
                if self.profile?.commercialRegistration != nil {
                self.imgCommercial.sd_custom(url: self.profile?.commercialRegistration ?? "")
                }
                
                
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    
    
    @IBAction func didTab_Country(_ sender: UIButton) {
        
        ActionSheetStringPicker.show(withTitle: "Country".localized, rows: self.country.map { $0.title as Any }
                                     , initialSelection: 0, doneBlock: {
                                        picker, value, index in
                                        if let Value = index {
                                            self.tfCountry.text = Value as? String
                                            self.selectedCountryID = self.country[value].id ?? 0
                                            //self.getCity()
                                        }
                                        
                                        return
                                     }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    @IBAction func didTab_City(_ sender: UIButton) {
        guard let CountryID = self.selectedCountryID, CountryID != 0 else{
            self.showAlert(title: "".localized, message: "Choose the country first".localized)
            return
        }
        
        let vc:CitysVC = CitysVC.loadFromNib()
        vc.Countryid = CountryID
        vc.citydelegte = self
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    @IBAction func didTab_Gander(_ sender: UIButton) {
        
        ActionSheetStringPicker.show(withTitle: "Nationality".localized, rows: self.nationality.map { $0.title as Any }
                                     , initialSelection: 0, doneBlock: {
                                        picker, value, index in
                                        if let Value = index {
                                            self.tfGender.text = Value as? String
                                            self.selectedNationalityId = self.nationality[value].id ?? 0
                                            
                                        }
                                        
                                        return
                                     }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
        
    }
    
    
    @IBAction func didTab_Save(_ sender: Any) {
        
        guard let name = self.tfStore.text, !name.isEmpty else{
            self.showAlert(title: "error".localized, message: "Store name cannot be empty".localized)
            return
        }
        guard let email = self.tfEmail.text, !email.isEmpty else{
            self.showAlert(title: "error".localized, message: "Email cannot be empty".localized)
            return
        }
        guard let mobile = self.tfMobile.text, !mobile.isEmpty else{
            self.showAlert(title: "error".localized, message: "Mobile cannot be empty".localized)
            return
        }
        guard let phone = self.tfPhone.text, !phone.isEmpty else{
            self.showAlert(title: "error".localized, message: "Mobile cannot be empty".localized)
            return
        }
        guard let address = self.tfAddress.text, !address.isEmpty else{
            self.showAlert(title: "error".localized, message: "Title cannot be empty".localized)
            return
        }
        guard self.selectedCountryID != nil  else{
            self.showAlert(title: "error".localized, message: "The state cannot be empty".localized)
            return
        }
        guard self.selectedCityID != nil  else{
            self.showAlert(title: "error".localized, message: "The city cannot be empty".localized)
            return
        }
        guard self.selectedNationalityId != nil  else{
            self.showAlert(title: "error".localized, message: "Nationality cannot be empty".localized)
            return
        }
        
        
        var parameters: [String: Any] = [:]
        parameters["name"] = self.tfStore.text ?? ""
        parameters["email"] = self.tfEmail.text ?? ""
        parameters["country_id"] = self.selectedCountryID?.description
        parameters["city_id"] = self.selectedCityID?.description
        parameters["nationality_id"] = self.selectedNationalityId?.description
        parameters["mobile"] = self.tfMobile.text ?? ""
        parameters["phone"] = self.tfPhone.text ?? ""
        parameters["address"] = self.tfAddress.text ?? ""
        
        
        _ = WebRequests.setup(controller: self).prepare(query: "profile", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }else if Status.status == 200{
                    self.navigationController?.popViewController(animated: true)
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
        if imgCard.image != nil{
            let imageData = (imgCard.image) ?? UIImage()
            
            _ = WebRequests.sendPostMultipartRequestWithImgParam(url: TAConstant.APIBaseURL + "profile/identity-images", parameters: ["" : ""],img: imageData, withName: "id_card", completion: { (response, error) in
                self.hideIndicator()
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self.showAlert(title: "error".localized, message:Status.message!)
                        return
                    }else{
                        self.navigationController?.popViewController(animated: true)
                        
                    }
                    
                }catch let jsonErr {
                    print("Error serializing respone json", jsonErr)
                }
            }
            )
        }
        if imgCommercial.image != nil{
            let imageData = (imgCommercial.image) ?? UIImage()
            
            _ = WebRequests.sendPostMultipartRequestWithImgParam(url: TAConstant.APIBaseURL + "profile/identity-images", parameters: ["" : ""],img: imageData, withName: "commercial_registration", completion: { (response, error) in
                self.hideIndicator()
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self.showAlert(title: "error".localized, message:Status.message!)
                        return
                    }else{
                      
                        self.navigationController?.popViewController(animated: true)
                        
                    }
                    
                }catch let jsonErr {
                    print("Error serializing respone json", jsonErr)
                }
            }
            )
        }
        
        
        
    }
}

extension ProfileVC: ImagePickerDelegate {
    func didSelect(image: UIImage?) {
        if isSelectImgCard {
            self.imgCard.image = image
        }
        if isSelectImgCommercial {
            self.imgCommercial.image = image
        }
        
    }
    
    @IBAction func showImagePicker(_ sender: UIButton) {
        self.imagePicker.present(from: sender)
        if sender.tag == 0{
            isSelectImgCard = true
            isSelectImgCommercial = false
        }
        if sender.tag == 1{
            isSelectImgCommercial = true
            isSelectImgCard = false
        }
    }
    
}

