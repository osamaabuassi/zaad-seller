//
//  AddOffersVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/8/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class AddOffersVC: SuperViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tfnameProducta: UITextField!
    @IBOutlet weak var tfPrice: UITextField!
    @IBOutlet weak var tfDiscountValue: UITextField!
    @IBOutlet weak var tfOffersPrice: UITextField!
    @IBOutlet weak var tfStartDate: UITextField!
    @IBOutlet weak var tfEndDate: UITextField!
    @IBOutlet weak var llbStatus: UILabel!
    @IBOutlet weak var ViewDiscount: UIView!
    @IBOutlet weak var ViewRatio: UIView!
    @IBOutlet weak var ViewtwoDiscount: UIView!
    @IBOutlet weak var ViewtwoRatio: UIView!
    @IBOutlet weak var SwitchStatus: UISwitch!
    
    
    
    var isEdited = false
    var id:Int?
    var offersDeatiles:OffersDeatilesData?
    var status:String?
    var CapounType:String?
    var Value:String?
    var producats = [ProducatsItems]()
    var selectedProducatID:Int?
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getOffersDeaitles()
        getProducats()
        if !isEdited{
            self.ViewRatio.borderColor = "B1AF00".color
            self.ViewRatio.borderWidth = 1
            self.ViewRatio.cornerRadius = 3.45
            self.ViewDiscount.borderColor = "AFAFAF".color
            self.ViewDiscount.borderWidth = 1
            self.ViewDiscount.cornerRadius = 3.45
            self.CapounType = "1"
            self.SwitchStatus.setOn(false, animated: true)
            self.llbStatus.text = "inactive".localized
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    
    func getProducats(){
        _ = WebRequests.setup(controller: self).prepare(query: "items/options", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(ProducatsObject.self, from: response.data!)
                self.producats = Status.data!
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
        }
    }
    
    
    @IBAction func didTab_Producate(_ sender: UIButton) {
        ActionSheetStringPicker.show(withTitle: "Product".localized, rows: self.producats.map { $0.title as Any }
            , initialSelection: 0, doneBlock: {
                picker, value, index in
                if let Value = index {
                    self.tfnameProducta.text = Value as? String
                    self.selectedProducatID = self.producats[value].id ?? 0
                }

                return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    
    
    @IBAction func didTab_StartDate(_ sender: Any) {
        let datePicker = ActionSheetDatePicker(title: "Start Date".localized, datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "yyyy-MM-dd"
            
            let dateString = formatter.string(from: (value as? Date)!)
            self.tfStartDate.text = dateString
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: self.view)
        datePicker?.minimumDate = Date()
        datePicker?.show()
        
    }
    @IBAction func didTab_EndDate(_ sender: Any) {
        let datePicker = ActionSheetDatePicker(title: "Expiry date".localized, datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "yyyy-MM-dd"
            let dateString = formatter.string(from: (value as? Date)!)
            
            self.tfEndDate.text = dateString
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: self.view)
        datePicker?.minimumDate = Date()
        datePicker?.show()
        
    }
    @IBAction func didTab_Status(_ sender: UISwitch) {
        if sender.isOn{
            self.status = "1"
            self.llbStatus.text = "active".localized
        }else{
            self.status = "0"
            self.llbStatus.text = "inactive".localized
        }
        
    }
    @IBAction func didTab_Discounttype(_ sender: UIButton) {
        if sender.tag == 0{
            self.ViewRatio.borderColor = "B1AF00".color
            self.ViewRatio.borderWidth = 1
            self.ViewRatio.cornerRadius = 3.45
            self.ViewtwoRatio.backgroundColor = "B1AF00".color
            self.ViewDiscount.borderColor = "AFAFAF".color
            self.ViewDiscount.borderWidth = 1
            self.ViewDiscount.cornerRadius = 3.45
            self.ViewtwoDiscount.backgroundColor = "AFAFAF".color
            self.CapounType = "1"
            
        }else{
            self.ViewRatio.borderColor = "AFAFAF".color
            self.ViewtwoRatio.backgroundColor = "AFAFAF".color
            self.ViewRatio.borderWidth = 1
            self.ViewRatio.cornerRadius = 3.45
            self.ViewDiscount.borderColor = "B1AF00".color
            self.ViewtwoDiscount.backgroundColor = "B1AF00".color
            self.ViewDiscount.borderWidth = 1
            self.ViewDiscount.cornerRadius = 3.45
            self.CapounType = "2"
        }
        
    }
    
    
    
    
    func getOffersDeaitles(){
        if isEdited{
            self.lblTitle.text = "Offer edit".localized
            _ = WebRequests.setup(controller: self).prepare(query: "item-offers/\(id ?? 0)", method: HTTPMethod.get).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "error".localized, message:Status.message!)
                        return
                    }
                    
                }catch let jsonErr {
                    print("Error serializing respone json", jsonErr)
                }
                
                do {
                    let Status =  try JSONDecoder().decode(OffersDeatilesObject.self, from: response.data!)
                    self.offersDeatiles = Status.data!
                    self.tfnameProducta.text = self.offersDeatiles?.title ?? ""
                    self.tfPrice.text = "\(self.offersDeatiles?.regularPrice ?? 0)"
                    self.tfDiscountValue.text = self.offersDeatiles?.amountLabel ?? ""
                    self.tfOffersPrice.text = "\(self.offersDeatiles?.salePrice ?? 0)"
                    self.tfStartDate.text = self.offersDeatiles?.startDate ?? "0"
                    self.tfEndDate.text = self.offersDeatiles?.endDate ?? "0"
                    self.llbStatus.text = self.offersDeatiles?.status?.title ?? ""
                    self.selectedProducatID = self.offersDeatiles?.id
                    if self.offersDeatiles?.discountType?.title == "نسبة"{
                        self.ViewRatio.borderColor = "B1AF00".color
                        self.ViewRatio.borderWidth = 1
                        self.ViewRatio.cornerRadius = 3.45
                        self.ViewDiscount.borderColor = "AFAFAF".color
                        self.ViewDiscount.borderWidth = 1
                        self.ViewDiscount.cornerRadius = 3.45
                        self.CapounType = "1"
                    }else{
                        self.ViewRatio.borderColor = "AFAFAF".color
                        self.ViewtwoRatio.backgroundColor = "AFAFAF".color
                        self.ViewRatio.borderWidth = 1
                        self.ViewRatio.cornerRadius = 3.45
                        self.ViewDiscount.borderColor = "B1AF00".color
                        self.ViewtwoDiscount.backgroundColor = "B1AF00".color
                        self.ViewDiscount.borderWidth = 1
                        self.ViewDiscount.cornerRadius = 3.45
                        self.CapounType = "2"
                    }
                    if self.offersDeatiles?.status?.title == "فعال"{
                        self.SwitchStatus.setOn(true, animated: true)
                        self.llbStatus.text = "active".localized
                        self.status = "1"
                    }else{
                        self.SwitchStatus.setOn(false, animated: true)
                        self.llbStatus.text = "inactive".localized
                        self.status = "0"
                    }
                    
                } catch let jsonErr {
                    print("Error serializing respone json", jsonErr)
                }
            }
        }
    }
    
    
    
    
    @IBAction func didTab_Edite(_ sender: Any) {
        
        guard let nameproducat = self.tfnameProducta.text, !nameproducat.isEmpty else{
            self.showAlert(title: "error".localized, message: "Name cannot be empty".localized)
            return
        }
//
//        guard let price = self.tfPrice.text, !price.isEmpty else{
//            self.showAlert(title: "error".localized, message: "لا يمكن أن يكون السعر  فارغًا".localized)
//            return
//        }
        
        guard let DiscountValue = self.tfDiscountValue.text, !DiscountValue.isEmpty else{
            self.showAlert(title: "error".localized, message: "Discount value cannot be empty".localized)
            return
        }
        guard selectedProducatID != nil else{
              self.showAlert(title: "error".localized, message:"Please select a product".localized)
              return
          }
        
//        guard let OfferPrice = self.tfOffersPrice.text, !OfferPrice.isEmpty else{
//            self.showAlert(title: "error".localized, message: "لا يمكن أن يكون سعر العرض  فارغًا".localized)
//            return
//        }
        
        guard let startdate = self.tfStartDate.text, !startdate.isEmpty else{
            self.showAlert(title: "error".localized, message: "Added date cannot be empty".localized)
            return
        }
        
        guard let Enddate = self.tfEndDate.text, !Enddate.isEmpty else{
            self.showAlert(title: "error".localized, message: "Expiry date cannot be empty".localized)
            return
        }
        guard let status = self.status, !status.isEmpty else{
            self.showAlert(title: "error".localized, message: "Please select a status for the coupon".localized)
            return
        }
        
        if DiscountValue.contains("%"){
            self.Value = self.tfDiscountValue.text?.replacingOccurrences(of: "%", with: "")
        }else if DiscountValue.contains("‏ر.عُ"){
            self.Value = self.tfDiscountValue.text?.replacingOccurrences(of: "‏ر.عُ", with: "")
        }else{
            self.Value = DiscountValue
        }
        var parameters: [String: Any] = [:]

        parameters["title"] = self.tfnameProducta.text ?? ""
        parameters["offer_type"] = self.CapounType ?? ""
        parameters["offer_amount"] = Value ?? ""
//        parameters["regular_price"] = self.tfPrice.text ?? ""
//        parameters["sale_price"] = self.tfOffersPrice.text ?? ""
        parameters["offer_start_date"] = self.tfStartDate.text ?? ""
        parameters["offer_end_date"] = self.tfEndDate.text ?? ""
        parameters["offer_status"] = self.status ?? ""

        if isEdited{
            
            _ = WebRequests.setup(controller: self).prepare(query: "item-offers/\(id ?? 0)", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self.showAlert(title: "Error".localized, message:Status.message!)
                        return
                    }else if Status.status == 200{
                        self.navigationController?.popViewController(animated: true)
                        
                        NotificationCenter.default.post(name: Notification.Name("Updateoffers"), object: nil)
                    }
                    
                }catch let jsonErr {
                    print("Error serializing respone json", jsonErr)
                }
            }
        }else{
            
            _ = WebRequests.setup(controller: self).prepare(query: "item-offers/\(selectedProducatID ?? 0)", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self.showAlert(title: "Error".localized, message:Status.message!)
                        return
                    }else if Status.status == 200{
                        self.navigationController?.popViewController(animated: true)
                        
                        NotificationCenter.default.post(name: Notification.Name("Updateoffers"), object: nil)
                    }
                    
                }catch let jsonErr {
                    print("Error serializing respone json", jsonErr)
                }
            }
            
        }
    }
}
