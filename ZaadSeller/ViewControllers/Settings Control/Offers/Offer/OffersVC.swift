//
//  OffersVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/7/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import ESPullToRefresh
import ActionSheetPicker_3_0

class OffersVC: SuperViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblStatus: UILabel!

    var offersitems = [Offers]()
    var offersData:OffersData?
    var currentpage = 1
    var status = ["active","inactive"]
    var selectedStatus:Int?

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        self.lblStatus.text = "Select status".localized
        tableView.registerCell(id: "OffersCell")
        getOffers()
        self.tableView.es.addPullToRefresh {
            self.currentpage = 1
            self.offersitems.removeAll()
            self.getOffers()
            self.hideIndicator()
        }
        
        self.tableView.es.addInfiniteScrolling {
            self.currentpage += 1
            self.getOffers() // next page
            self.tableView.estimatedRowHeight = UITableView.automaticDimension
            self.hideIndicator()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateOffers(notification:)), name: Notification.Name("Updateoffers"), object: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    
    
    func getOffers(){
        
        guard Helper.isConnectedToNetwork() else {
            self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
            self.emptyView?.imgEmpty.image = UIImage(named: "refreshment")
            self.emptyView?.firstLabel.text = "There is no internet connection".localized
            return }
        
        var parameters: [String: Any] = [:]
        if selectedStatus != nil{
            parameters["offer_status"] = selectedStatus?.description
        }
        
        _ = WebRequests.setup(controller: self).prepare(query: "item-offers?page=\(currentpage)", method: HTTPMethod.get,parameters: parameters).start(){ (response, error) in
            
            self.tableView.es.stopLoadingMore()
            self.tableView.es.stopPullToRefresh()
            
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(OffersObject.self, from: response.data!)
                self.offersData = Status.data
                self.offersitems += self.offersData!.resources!
                if self.offersData?.nextPageUrl == nil {
                    self.tableView.es.stopLoadingMore()
                    self.tableView.es.noticeNoMoreData()
                }
                if self.offersitems.count == 0{
                    self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                    self.emptyView?.firstLabel.text = "No offers to display".localized
                    self.emptyView?.imgEmpty.isHidden = true
                    self.offersitems.removeAll()
                    self.tableView.reloadData()
                    
                }else{
                    self.tableView.tableFooterView = nil
                }
                self.tableView.reloadData()
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
        }
        
    }
    
    
    @IBAction func didRefersh(){
        offersitems.removeAll()
        getOffers()
    }
    
    
      @objc func UpdateOffers(notification: NSNotification)  {
            offersitems.removeAll()
            getOffers()
        }
      
    
    
    @IBAction func didTab_hours(_ sender: UIButton) {
        ActionSheetStringPicker.show(withTitle: "Status".localized, rows: self.status.map { $0 as Any }
            , initialSelection: 0, doneBlock: {
                picker, value, index in
                if let Value = index {
                    self.lblStatus.text = Value as? String
                    self.offersitems.removeAll()
                    if self.lblStatus.text == "active".localized{
                        self.selectedStatus = 1
                    }else{
                        self.selectedStatus = 0
                    }
                    self.getOffers()
                }
                
                return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)

    }
    
    @IBAction func didTab_Add(_ sender: Any) {
        let vc:AddOffersVC = AddOffersVC.loadFromNib()
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    
}

extension OffersVC:UITableViewDataSource,UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offersitems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OffersCell", for: indexPath) as! OffersCell
        cell.offers = offersitems[indexPath.row]
        cell.deatilesTapped = { [weak self] in
            let vc:OffersDeatilesVC = OffersDeatilesVC.loadFromNib()
            vc.id = self!.offersitems[indexPath.row].id
            vc.modalPresentationStyle = .fullScreen
            self!.navigationController?.pushViewController(vc, animated: true)
        }
        cell.editeTapped = { [weak self] in
            let vc:AddOffersVC = AddOffersVC.loadFromNib()
            vc.id = self!.offersitems[indexPath.row].id
            vc.isEdited = true
            vc.modalPresentationStyle = .fullScreen
            self!.navigationController?.pushViewController(vc, animated: true)
        }
        cell.deleteTapped = { [weak self] in
            let vc:DeleteAlert = DeleteAlert.loadFromNib()
            vc.isFromOffers = true
            vc.id = self!.offersitems[indexPath.row].id
            vc.modalPresentationStyle = .overCurrentContext
            self!.present(vc, animated: false, completion: nil)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
