//
//  OffersCell.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/7/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class OffersCell: UITableViewCell {

    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblPriceOffer: UILabel!
    @IBOutlet weak var Status: UISwitch!
    @IBOutlet weak var lblStatus: UILabel!


    var deatilesTapped: (() -> ())?
    var editeTapped: (() -> ())?
    var deleteTapped: (() -> ())?

    var offers:Offers?{
        didSet{
            self.lblname.text = offers?.title ?? ""
            self.lblPrice.text = "\(offers?.regularPrice ?? 0)" + " "  + "SAR".localized
            self.lblPriceOffer.text = "\(offers?.salePrice ?? 0)" + " "  + "SAR".localized
            if offers?.status?.title == "active"{
                self.Status.setOn(true, animated: true)
                self.lblStatus.text = "active".localized
            }else{
                self.Status.setOn(false, animated: true)
                self.lblStatus.text = "inactive".localized
            }
        }
    }

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    @IBAction func Deatiles(_ sender: UIButton) {
        deatilesTapped?()
    }
    
    @IBAction func Edite(_ sender: UIButton) {
        editeTapped?()
    }
    
    @IBAction func Delete(_ sender: UIButton) {
        deleteTapped?()
    }
    
    
    

}
