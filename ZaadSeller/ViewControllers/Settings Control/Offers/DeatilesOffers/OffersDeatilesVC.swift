//
//  OffersDeatilesVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/7/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class OffersDeatilesVC: SuperViewController {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblDiscountPrice: UILabel!
    @IBOutlet weak var lblDiscountType: UILabel!
    @IBOutlet weak var lblofferPrice: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblExpiryDate: UILabel!
    @IBOutlet weak var lblStatus: UILabel!


    var id:Int?
    var offersDeatiles:OffersDeatilesData?

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getOffersDeaitles()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
         let navgtion = self.navigationController as! CustomNavigationBar
         navgtion.setCustomBackButtonWhiteForViewController(sender: self)
         navgtion.setLogotitle(sender: self)
         navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
         
     }
     override func viewWillDisappear(_ animated: Bool) {
         let navgtion = self.navigationController as! CustomNavigationBar
         navgtion.setCustomBackButtonWhiteForViewController(sender: self)
         navgtion.setLogotitle(sender: self)
         navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
         
     }
    
    func getOffersDeaitles(){
        
        _ = WebRequests.setup(controller: self).prepare(query: "item-offers/\(id ?? 0)", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(OffersDeatilesObject.self, from: response.data!)
                self.offersDeatiles = Status.data!
                self.lblName.text = self.offersDeatiles?.title ?? ""
                self.lblPrice.text = "\(self.offersDeatiles?.regularPrice ?? 0)" + "SAR".localized
                self.lblDiscountPrice.text = self.offersDeatiles?.amountLabel ?? ""
                self.lblDiscountType.text = self.offersDeatiles?.discountType?.title ?? ""
                self.lblofferPrice.text = "\(self.offersDeatiles?.salePrice ?? 0)"
                self.lblStartDate.text = self.offersDeatiles?.startDate ?? "0"
                self.lblExpiryDate.text = self.offersDeatiles?.endDate ?? "0"
                self.lblStatus.text = self.offersDeatiles?.status?.title ?? ""

            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
    }
}
