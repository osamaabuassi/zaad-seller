//
//  InfoVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/5/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class InfoVC: SuperViewController {
    
    @IBOutlet weak var lblInfo: UILabel!
    @IBOutlet weak var lblTitleInfo: UILabel!
    
    var isFromAboutus = false
    var isFromTerms = false
    var isFromPoilcy = false
    var isFromPoilcyReturn = false
    var isFromTermsLogin = false
    var isDismis = false
    var idInfo:Int?
    var infoDeatiles :InfoDeatiles?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getInfo()
        if isDismis,isFromTermsLogin{
            let navgtion = self.navigationController as! CustomNavigationBar
            navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
         let navgtion = self.navigationController as! CustomNavigationBar
         navgtion.setCustomBackButtonWhiteForViewController(sender: self)
         navgtion.setLogotitle(sender: self)
         navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
         
     }
     override func viewWillDisappear(_ animated: Bool) {
         let navgtion = self.navigationController as! CustomNavigationBar
         navgtion.setCustomBackButtonWhiteForViewController(sender: self)
         navgtion.setLogotitle(sender: self)
         navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
         
     }
    func getInfo(){
        _ = WebRequests.setup(controller: self).prepare(query: "pages/\(idInfo ?? 0)", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(InfoDeatilesObject.self, from: response.data!)
                self.infoDeatiles = Status.data
                self.lblTitleInfo.text  = self.infoDeatiles?.title ?? ""
                self.lblInfo.text = self.infoDeatiles?.html?.htmlToString ?? ""
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
    }
    
    
    
    
}
