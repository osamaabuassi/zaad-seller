//
//  FinancialPaymentsVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/14/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import SKPhotoBrowser
import ESPullToRefresh


class FinancialPaymentsVC: SuperViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var payments:FinancialPaymentsData?
    var financialresources = [FinancialResources]()
    var images = [SKPhoto]()
    var currentpage = 1

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView  = UIView()
        tableView.registerCell(id: "FinancialPaymentsCell")
        getFinancialPayments()
        self.tableView.es.addPullToRefresh {
            self.currentpage = 1
            self.financialresources.removeAll()
            self.getFinancialPayments()
            self.hideIndicator()
        }
        
        self.tableView.es.addInfiniteScrolling {
            self.currentpage += 1
            self.getFinancialPayments() // next page
             self.tableView.estimatedRowHeight = 0
            self.hideIndicator()
        }
    }
    
    
      override func viewWillAppear(_ animated: Bool) {
          let navgtion = self.navigationController as! CustomNavigationBar
          navgtion.setCustomBackButtonWhiteForViewController(sender: self)
          navgtion.setLogotitle(sender: self)
          navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
          
      }
      override func viewWillDisappear(_ animated: Bool) {
          let navgtion = self.navigationController as! CustomNavigationBar
          navgtion.setCustomBackButtonWhiteForViewController(sender: self)
          navgtion.setLogotitle(sender: self)
          navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
          
      }
    
    func getFinancialPayments(){
        
        guard Helper.isConnectedToNetwork() else {
            self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
            self.emptyView?.imgEmpty.image = UIImage(named: "refreshment")
            self.emptyView?.firstLabel.text = "There is no internet connection".localized
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "bank-payments?page=\(currentpage)", method: HTTPMethod.get).start(){ (response, error) in
            
            self.tableView.es.stopLoadingMore()
            self.tableView.es.stopPullToRefresh()

            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(FinancialPaymentsObject.self, from: response.data!)
                self.payments = Status.data!
                self.financialresources += self.payments!.resources!
                if self.financialresources.count < 10 {
                    self.tableView.es.stopLoadingMore()
                    self.tableView.es.noticeNoMoreData()
                }
                if self.financialresources.count == 0{
                    self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                    self.emptyView?.firstLabel.text = "There are no remittances to display".localized
                    self.emptyView?.imgEmpty.isHidden = true
                    self.financialresources.removeAll()
                    self.tableView.reloadData()
                    
                }else{
                    self.tableView.tableFooterView = nil
                }
                self.tableView.reloadData()
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
        }
    }
    @IBAction func didRefersh(){
           financialresources.removeAll()
           getFinancialPayments()
       }
    
    
    
}

extension FinancialPaymentsVC:UITableViewDataSource,UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return financialresources.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FinancialPaymentsCell", for: indexPath) as! FinancialPaymentsCell
        cell.payment = financialresources[indexPath.row]
        cell.imageTapped = { [weak self] in
            let photo = SKPhoto.photoWithImageURL(self!.financialresources[indexPath.row].document ?? "")
            photo.shouldCachePhotoURLImage = true // you can use image cache by true(NSCache)
            self!.images.append(photo)
            let browser = SKPhotoBrowser(photos: self!.images)
            browser.initializePageIndex(indexPath.row)
            SKPhotoBrowserOptions.displayAction = false    // action button will be hidden
            self!.present(browser, animated: true, completion: nil)
            self?.images.removeAll()
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 275
    }
}
