//
//  FinancialPaymentsCell.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/14/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class FinancialPaymentsCell: UITableViewCell {
    
    @IBOutlet weak var lb_orderId: UILabel!
    @IBOutlet weak var lb_nameBuyer: UILabel!
    @IBOutlet weak var lb_orderNumber: UILabel!
    @IBOutlet weak var lb_stutse: UILabel!
    var imageTapped: (() -> ())?
    
    var payment:FinancialResources?{
        didSet{
            self.lb_orderId.text = "\(payment?.order?.orderNo ?? "0")"
            self.lb_nameBuyer.text = "\(payment?.customer?.name ?? "")"
            self.lb_orderNumber.text = "\(payment?.transactionNo ?? "")"
            self.lb_stutse.text = "\(payment?.order?.status?.title ?? "")"
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func ShowUrlTapped(_ sender: UIButton) {
            imageTapped?()
        }

 
}
