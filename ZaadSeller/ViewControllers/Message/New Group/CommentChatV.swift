//
//  CommentChatVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/14/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import ESPullToRefresh

class CommentChatVC: SuperViewController,UITextViewDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tfMessage: UITextView!
    @IBOutlet weak var btSend: UIButton!
    @IBOutlet weak var height: NSLayoutConstraint!
    
    
    var comments = [CommentChat]()
  
    var id:Int?
  

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btSend.isUserInteractionEnabled = false
        tfMessage.delegate = self
        tableView.registerCell(id: "MessageReciveCell")
        tableView.registerCell(id: "MessageSenderCell")
        if UIDevice.current.hasBottomNotch{
            self.height.constant = 100
        }else{
            self.height.constant = 60
        }
      

        
     
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setLogotitle(sender: self)
    //    navgtion.setTitle(name ?? "", sender: self)
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setLogotitle(sender: self)
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if tfMessage.text != ""{
            self.btSend.isUserInteractionEnabled = true
            self.btSend.setTitleColor(UIColor.white, for: .normal)
        }else{
            self.btSend.isUserInteractionEnabled = false
            self.btSend.setTitleColor(UIColor.darkGray, for: .normal)
        }
    }
      
    
    func getChat(){
          guard Helper.isConnectedToNetwork() else {
          self.showAlert(title: "".localized, message: "There is no internet connection".localized)
          return }
          
          
          _ = WebRequests.setup(controller: self).prepare(query: "service-orders/24/comments", method: HTTPMethod.get).start(){ (response, error) in
                     do {
                         let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                         if Status.status != 200{
                             self.showAlert(title: "خطأ".localized, message:Status.message!)
                             return
                         }
                         
                     }catch let jsonErr {
                         print("Error serializing  respone json", jsonErr)
                     }
                     
                     
                     do {
                         let Status =  try JSONDecoder().decode(CommentServes.self, from: response.data!)
                      
                      self.comments = Status.data.items
                  
                     
                     } catch let jsonErr {
                         print("Error serializing  respone json", jsonErr)
                     }
                 }
      }
  
}

extension CommentChatVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        <#code#>
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        <#code#>
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
}

