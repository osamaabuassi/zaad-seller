//
//  MessageCell.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/14/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {
    
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var imgMessage: UIImageView!

    var message:MessageData?{
        didSet{
            self.lblMessage.text = message?.customer?.name ?? ""
            self.imgMessage.sd_custom(url: message?.customer?.image ?? "")
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

  
    
}
