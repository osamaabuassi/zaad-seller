//
//  ChatVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/14/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import ESPullToRefresh

class ChatVC: SuperViewController,UITextViewDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tfMessage: UITextView!
    @IBOutlet weak var btSend: UIButton!
    @IBOutlet weak var height: NSLayoutConstraint!
    
    
    var messagesData:ChatData?
    var messages =  [Messages]()
    var id:Int?
    var name:String?
    var currentpage = 1
    var selectedMessgeID:Int?
    var disputesitems :DisputesDeatilesData?
    var isFromDisputes  = false
    var idDisputes:Int?
    var comments = [DisputesDeatilesComments]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btSend.isUserInteractionEnabled = false
        tfMessage.delegate = self
        tableView.registerCell(id: "MessageReciveCell")
        tableView.registerCell(id: "MessageSenderCell")
        if UIDevice.current.hasBottomNotch{
            self.height.constant = 100
        }else{
            self.height.constant = 60
        }
        setMessageRequest()

        if !isFromDisputes{
            self.tableView.es.addPullToRefresh {
                self.currentpage = 1
                self.messages.removeAll()
                self.setMessageRequest()
                self.hideIndicator()
            }
            
            self.tableView.es.addInfiniteScrolling {
                self.currentpage += 1
                self.setMessageRequest() // next page
                self.tableView.rowHeight = UITableView.automaticDimension
                self.tableView.estimatedRowHeight = 80
                self.hideIndicator()
                
            }
            tableView.transform = CGAffineTransform(scaleX: 1, y: -1)
        }
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateChat(notification:)), name: Notification.Name("UpdateChat"), object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setLogotitle(sender: self)
        navgtion.setTitle(name ?? "", sender: self)
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setLogotitle(sender: self)
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if tfMessage.text != ""{
            self.btSend.isUserInteractionEnabled = true
            self.btSend.setTitleColor(UIColor.white, for: .normal)
        }else{
            self.btSend.isUserInteractionEnabled = false
            self.btSend.setTitleColor(UIColor.darkGray, for: .normal)
        }
    }
    
    
   
    
    func setMessageRequest(){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        if isFromDisputes{
            _ = WebRequests.setup(controller: self).prepare(query: "disputes/\(idDisputes ?? 0)", method: HTTPMethod.get).start(){ (response, error) in
                           
                   
                   do {
                       let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                       if Status.status != 200{
                           self.showAlert(title: "error".localized, message:Status.message!)
                           return
                       }
                       
                   }catch let jsonErr {
                       print("Error serializing respone json", jsonErr)
                   }
                   
                do {
                    let Status =  try JSONDecoder().decode(DisputesDeatilesObject.self, from: response.data!)
                    self.disputesitems = Status.data
                    self.comments = self.disputesitems!.comments!
                    self.tableView.reloadData()
                } catch let jsonErr {
                    print("Error serializing respone json", jsonErr)
                }
                   
                   
               }
        }else{
            _ = WebRequests.setup(controller: self).prepare(query: "messages/\(id ?? 0)?page=\(currentpage)", method: HTTPMethod.get).start(){ (response, error) in
                
                self.tableView.es.stopLoadingMore()
                self.tableView.es.stopPullToRefresh()
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "error".localized, message:Status.message!)
                        return
                    }
                    
                }catch let jsonErr {
                    print("Error serializing respone json", jsonErr)
                }
                do {
                    let Status =  try JSONDecoder().decode(ChatObject.self, from: response.data!)
                    self.messagesData = Status.data!
                    self.messages += self.messagesData!.messages!
                    if self.messagesData?.nextPageUrl == nil {
                        self.tableView.es.stopLoadingMore()
                        self.tableView.es.noticeNoMoreData()
                    }
                    if self.messages.count == 0{
                        self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                        self.emptyView?.firstLabel.text = "No messages to display".localized
                        self.emptyView?.imgEmpty.isHidden = true
                        self.messages.removeAll()
                        
                    }else{
                        self.tableView.tableFooterView = nil
                    }
                    self.tableView.reloadData()
                } catch let jsonErr {
                    print("Error serializing respone json", jsonErr)
                }
            }
        }

    }
    
    @IBAction func bt_Send(_ sender: Any) {
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        if isFromDisputes{
            if tfMessage.text != ""{
                
                let id = disputesitems?.id ?? 0
                
                var parameters: [String: Any] = [:]
                parameters["comment"] = self.tfMessage.text
                
                _ = WebRequests.setup(controller: self).prepare(query: "disputes/\(id)/actions/comment", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
                    
                    do {
                        let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                        if Status.status != 200 {
                            
                            self.showAlert(title: "error".localized, message:Status.message!)
                            return
                        }else if Status.status == 200{
                            self.tfMessage.text = ""
                            NotificationCenter.default.post(name: Notification.Name("UpdateChat"), object: nil)

                        }
                        
                    }catch let jsonErr {
                        print("Error serializing respone json", jsonErr)
                    }
                    
                }
            }else{
                self.showAlert(title: "".localized, message:"The message is empty!".localized)
            }
        }else{
            if tfMessage.text != ""{
                var parameters: [String: Any] = [:]
                parameters["customer_id"] = id?.description
                parameters["message"] = self.tfMessage.text
                
                _ = WebRequests.setup(controller: self).prepare(query: "messages", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
                    
                    do {
                        let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                        if Status.status != 200 {
                            
                            self.showAlert(title: "error".localized, message:Status.message!)
                            return
                        }else if Status.status == 200{
                            self.tfMessage.text = ""
                            NotificationCenter.default.post(name: Notification.Name("UpdateChat"), object: nil)
                            self.tableView.scrollToTop()

                        }
                        
                    }catch let jsonErr {
                        print("Error serializing respone json", jsonErr)
                    }
                    
                }
            }else{
                self.showAlert(title: "".localized, message:"The message is empty!".localized)
            }
        }
    }
    
    @IBAction func didRefersh(){
        messages.removeAll()
        setMessageRequest()
    }    
    
    @objc func UpdateChat(notification: NSNotification)  {
        if isFromDisputes{
            setMessageRequest()
        }else{
            messages.removeAll()
            setMessageRequest()

        }
    }
}

extension ChatVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFromDisputes{
            return comments.count
        }else{
            return messages.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isFromDisputes{
            let obj = comments[indexPath.row]
            if obj.senderType == "user"{
                let cell = tableView.dequeueReusableCell(withIdentifier: "MessageReciveCell", for: indexPath) as! MessageReciveCell
               // cell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
                cell.lblMessage.text = obj.comment ?? ""
                cell.lblDate.text = obj.date ?? ""
                cell.imgUser.isHidden = false
                cell.lblname.isHidden = false
                cell.imgUser.sd_custom(url: disputesitems?.user?.image ?? "")
                cell.lblname.text = disputesitems?.user?.name ?? ""
                if selectedMessgeID == indexPath.row{
                    cell.lblDate.isHidden = false
                }else{
                    cell.lblDate.isHidden = true
                }
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "MessageSenderCell", for: indexPath) as! MessageSenderCell
              //  cell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
                cell.lblMessage.text = obj.comment ?? ""
                cell.lblDate.text = obj.date ?? ""
                cell.imgUser.isHidden = false
                cell.lblname.isHidden = false
                cell.imgUser.sd_custom(url: disputesitems?.seller?.image ?? "")
                cell.lblname.text = disputesitems?.user?.name ?? ""
                if selectedMessgeID == indexPath.row{
                    cell.lblDate.isHidden = false
                }else{
                    cell.lblDate.isHidden = true
                }
                return cell
                
            }
        }else{
            let obj = messages[indexPath.row]
            if obj.type == 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "MessageReciveCell", for: indexPath) as! MessageReciveCell
                cell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
                cell.lblMessage.text = obj.message
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let date = dateFormatter.date(from:obj.createdAt!)!
                cell.lblDate.text = date.getElapsedInterval()
                cell.imgUser.isHidden = true
                cell.lblname.isHidden = true
                if selectedMessgeID == indexPath.row{
                    cell.lblDate.isHidden = false
                }else{
                    cell.lblDate.isHidden = true
                }
                return cell
                
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "MessageSenderCell", for: indexPath) as! MessageSenderCell
                cell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
                cell.lblMessage.text = obj.message
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let date = dateFormatter.date(from:obj.createdAt!)!
                cell.lblDate.text = date.getElapsedInterval()
                cell.imgUser.isHidden = true
                cell.lblname.isHidden = true
                if selectedMessgeID == indexPath.row{
                    cell.lblDate.isHidden = false
                }else{
                    cell.lblDate.isHidden = true
                }
                return cell
                
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.selectedMessgeID == indexPath.row{
            self.selectedMessgeID = -99
            self.tableView.reloadData()
        }else{
            self.selectedMessgeID = indexPath.row
            self.tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if isFromDisputes{
            if self.selectedMessgeID != indexPath.row{
                UIView.animate(withDuration: 0.2) {
                    cell.transform = CGAffineTransform.identity
                }
            }
        }else{
            if self.selectedMessgeID != indexPath.row{
                cell.transform = CGAffineTransform(scaleX: 0.8, y: 0)
                UIView.animate(withDuration: 0.2) {
                    cell.transform = CGAffineTransform.identity
                }
            }
        }
        
    }
  
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
}

