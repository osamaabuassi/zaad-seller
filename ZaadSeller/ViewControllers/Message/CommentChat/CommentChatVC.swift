//
//  CommentChatVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/14/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit


class CommentChatVC : SuperViewController,UITextViewDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tfMessage: UITextView!
    @IBOutlet weak var btSend: UIButton!
    @IBOutlet weak var height: NSLayoutConstraint!
    
    
    var comments = [CommentChat]()
    var id:Int?
    var selectedMessgeID:Int?
    var name:String?

    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        getChat()
     
        
        self.btSend.isUserInteractionEnabled = false
        tfMessage.delegate = self
        tableView.registerCell(id: "MessageReciveCell")
        tableView.registerCell(id: "MessageSenderCell")
        if UIDevice.current.hasBottomNotch{
            self.height.constant = 100
        }else{
            self.height.constant = 60
        }
      

        
     
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setLogotitle(sender: self)
        //navgtion.setTitle(name ?? "", sender: self)
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setLogotitle(sender: self)
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)

    }
    
    func textViewDidChange(_ textView: UITextView) {
        if tfMessage.text != ""{
            self.btSend.isUserInteractionEnabled = true
            self.btSend.setTitleColor(UIColor.white, for: .normal)
        }else{
            self.btSend.isUserInteractionEnabled = false
            self.btSend.setTitleColor(UIColor.darkGray, for: .normal)
        }
    }
      
    
    func getChat(){
          guard Helper.isConnectedToNetwork() else {
          self.showAlert(title: "".localized, message: "There is no internet connection".localized)
          return }
          
          
          _ = WebRequests.setup(controller: self).prepare(query: "service-orders/\(id!)/comments", method: HTTPMethod.get).start(){ (response, error) in
                     do {
                         let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                         if Status.status != 200{
                             self.showAlert(title: "error".localized, message:Status.message!)
                             return
                         }
                         
                     }catch let jsonErr {
                         print("Error serializing respone json", jsonErr)
                     }
                     
                     
                     do {
                         let Status =  try JSONDecoder().decode(CommentServes.self, from: response.data!)
                        
                        self.comments = Status.data.items
                        
                        for i in self.comments{
                            if i.type == "User"{
                                let navgtion = self.navigationController as! CustomNavigationBar
                                navgtion.setTitle(self.comments[0].name ?? "", sender: self)
                                break
                            }
                        }
                        
                        
                      self.tableView.reloadData()
                     self.tableView.scrollToBottom()
                     } catch let jsonErr {
                         print("Error serializing respone json", jsonErr)
                     }
                 }
      }
    
    @IBAction func bt_Send(_ sender: Any) {
          
          guard Helper.isConnectedToNetwork() else {
              self.showAlert(title: "".localized, message: "There is no internet connection".localized)
              return }
          

              if tfMessage.text != ""{
                  
                 // let id = disputesitems?.id ?? 0
                  comments.removeAll()
                  var parameters: [String: Any] = [:]
                  parameters["comment"] = self.tfMessage.text
                  
                  _ = WebRequests.setup(controller: self).prepare(query: "service-orders/\(id!)/comments", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
                      
                      do {
                          let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                          if Status.status != 200 {
                              
                              self.showAlert(title: "error".localized, message:Status.message!)
                              return
                          }else if Status.status == 200{
                              self.tfMessage.text = ""
                              NotificationCenter.default.post(name: Notification.Name("UpdateChat"), object: nil)
                            self.getChat()
                           
                          }
                          
                      }catch let jsonErr {
                          print("Error serializing respone json", jsonErr)
                      }
                     //  self.tableView.reloadData()
                  }
              }else{
                  self.showAlert(title: "".localized, message:"The message is empty!".localized)
              
          }
      }
    @objc func UpdateChat(notification: NSNotification)  {
            
          
      
    }
  
}

extension CommentChatVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
         let obj = comments[indexPath.row]
        self.name =  obj.name
                   if obj.type == "User"{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "MessageReciveCell", for: indexPath) as! MessageReciveCell
                    cell.contentView.transform = CGAffineTransform(scaleX: 1, y: 1)
                    cell.lblMessage.text = obj.comment
                   cell.lblDate.text = obj.date ?? ""
                    cell.imgUser.isHidden = true
                    cell.lblname.isHidden = true
                    if selectedMessgeID == indexPath.row{
                        cell.lblDate.isHidden = false
                    }else{
                        cell.lblDate.isHidden = true
                    }
                    return cell
                    
                   }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "MessageSenderCell", for: indexPath) as!     MessageSenderCell
                    
                     cell.lblMessage.text = obj.comment
                     cell.lblDate.text = obj.date ?? ""
                     cell.imgUser.isHidden = true
                     cell.lblname.isHidden = true
                     if selectedMessgeID == indexPath.row{
                         cell.lblDate.isHidden = false
                     }else{
                         cell.lblDate.isHidden = true
                     }
                    return cell
                    
        }
    }
    
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
               if self.selectedMessgeID == indexPath.row{
                   self.selectedMessgeID = -99
                   self.tableView.reloadData()
               }else{
                   self.selectedMessgeID = indexPath.row
                   self.tableView.reloadData()
               }
           }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
}


