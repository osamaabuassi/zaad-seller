//
//  MessageVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/9/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class MessageVC: SuperViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var messageItems = [MessageData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        tableView.registerCell(id: "MessageCell")
        setMessageRequest()
        self.tableView.es.addPullToRefresh {
            self.setMessageRequest()
            self.hideIndicator()
        }
    }
    
    
    
    func setMessageRequest(){
        
        guard Helper.isConnectedToNetwork() else {
            self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
            self.emptyView?.imgEmpty.image = UIImage(named: "refreshment")
            self.emptyView?.firstLabel.text = "There is no internet connection".localized
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "messages", method: HTTPMethod.get).start(){ (response, error) in
            
            self.tableView.es.stopLoadingMore()
            self.tableView.es.stopPullToRefresh()
            
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(MessageObject.self, from: response.data!)
                self.messageItems = Status.data!
                if self.messageItems.count == 0{
                    self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                    self.emptyView?.firstLabel.text = "No messages to display".localized
                    self.emptyView?.imgEmpty.isHidden = true
                    self.messageItems.removeAll()
                    self.tableView.reloadData()
                    
                }
                self.tableView.reloadData()
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
    }
    
    @IBAction func didRefersh(){
        messageItems.removeAll()
        setMessageRequest()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setRightButtons([navgtion.Settingsbtn!], sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    
    override func didClickRightButton(_sender: UIBarButtonItem) {
        let vc = SettingsControlVC.loadFromNib()
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}




extension MessageVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return messageItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell", for: indexPath) as! MessageCell
        cell.message = messageItems[indexPath.row]
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc:ChatVC = ChatVC.loadFromNib()
        vc.id = self.messageItems[indexPath.row].customerId
        vc.name = self.messageItems[indexPath.row].customer?.name
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    
}
