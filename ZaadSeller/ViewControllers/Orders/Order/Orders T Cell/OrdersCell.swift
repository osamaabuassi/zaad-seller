//
//  OrdersCell.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/13/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class OrdersCell: UITableViewCell {
    
    @IBOutlet weak var lblCodeOrder: UILabel!
    @IBOutlet weak var lblDateOrder: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var btDeatiles: UIButton!
    @IBOutlet weak var btChangeStatus: UIButton!
    @IBOutlet weak var btDelete: UIButton!
    @IBOutlet weak var btShipping: UIButton!
    @IBOutlet weak var viewShipping: UIView!
    
    @IBOutlet weak var viewChangeStatus: UIView!
    
    var deatilesTapped: (() -> ())?
    var statusTapped: (() -> ())?
    var deleteTapped: (() -> ())?
    var ShippingTapped: (() -> ())?
    
    let isStatus = UserDefaults.standard.integer(forKey: "viewStatus")
    
    var order:Orders?{
        didSet{
            self.lblCodeOrder.text = order?.orderNo ?? ""
            self.lblDateOrder.text = order?.purchaseDate ?? ""
            self.lblName.text = order?.buyerName ?? ""
            self.lblStatus.text = order?.status?.title ?? ""
            if order?.status?.title == "تم الدفع"{
                self.viewShipping.isHidden = false
            }else{
                self.viewShipping.isHidden = true
            }
            
            if (isStatus == 5){
                self.viewChangeStatus.isHidden = true
            }else{
                self.viewChangeStatus.isHidden = false
            }
        }
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    @IBAction func Deatiles(_ sender: UIButton) {
        deatilesTapped?()
    }
    
    @IBAction func Status(_ sender: UIButton) {
        statusTapped?()
    }
    
    @IBAction func Delete(_ sender: UIButton) {
        deleteTapped?()
    }
    @IBAction func Shipping(_ sender: UIButton) {
        ShippingTapped?()
    }
  
}
