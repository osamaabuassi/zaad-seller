//
//  MainOrdsers.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/13/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import MXSegmentedPager

class MainOrdsers: BaseViewController {

   // @IBOutlet weak var HeadrView: UIView!
 

    var arrayView = [String]()
    var arrayViewController = [UIViewController]()
    let defaults = UserDefaults.standard


    override func viewDidLoad() {
        super.viewDidLoad()
  


        //arrayView = ["في انتظار الدفع","تم الدفع","قيد الشحن","تم الشحن","تم التسليم","ملغي" ]
        arrayView = ["Pending payment","Paid","Shipping Process","Shipping Done","Delivery finished","Canceled"]

        if MOLHLanguage.isRTLLanguage() {
            arrayView = arrayView.reversed()
            arrayViewController = arrayViewController.reversed()
        }
        setUpSegmentation()

    }

       override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setRightButtons([navgtion.Settingsbtn!], sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)

    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    override func didClickRightButton(_sender: UIBarButtonItem) {
            let vc = SettingsControlVC.loadFromNib()
            vc.hidesBottomBarWhenPushed = true
            vc.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(vc, animated: true)
            
        }



      }



extension MainOrdsers {


    func setUpSegmentation (){

        if MOLHLanguage.isRTLLanguage(){
            self.segmentedPager.pager.showPage(at:self.arrayView.count - 1, animated: false)
        }
        DispatchQueue.main.async {
            if MOLHLanguage.isRTLLanguage(){
                self.segmentedPager.pager.showPage(at:self.arrayView.count - 1, animated: false)
                self.segmentedPager.segmentedControl.selectedSegmentIndex = self.arrayView.count - 1
                // segmentedPager.pager.reloadData()
            }
        }

        segmentedPager.backgroundColor = UIColor.white
        segmentedPager.segmentedControl.backgroundColor = UIColor.white
       // segmentedPager.parallaxHeader.view = HeadrView
       segmentedPager.parallaxHeader.mode = .bottom
     //   segmentedPager.parallaxHeader.height = 50
        segmentedPager.parallaxHeader.minimumHeight = 44


        // Segmented Control customization
        self.segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocation.down
        segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSAttributedString.Key.foregroundColor: "014A97".color]

        segmentedPager.segmentedControl.selectionIndicatorColor = "FF6A2A".color
        segmentedPager.segmentedControl.selectionIndicatorHeight = 3

        self.segmentedPager.segmentedControl.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "FFShamelFamilySemiRoundBook-S", size: 17)!, NSAttributedString.Key.foregroundColor :  "014A97".color]


    }


    override func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {


        switch index {
        case 0:
            return "Canceled".localized

        case 1:
            return "Delivery finished".localized

        case 2:
            return "Shipping Done".localized
        case 3:
            return "Shipping Process".localized
        case 4:
            return "Paid".localized

        case 5:
            return "Pending payment".localized
        default:
            break
        }
        return "Delivery finished".localized

    }

    override func segmentedPager(_ segmentedPager: MXSegmentedPager, didScrollWith parallaxHeader: MXParallaxHeader) {
    }


    override func numberOfPages(in segmentedPager: MXSegmentedPager) -> Int {
        return arrayView.count
    }


    override func segmentedPager(_ segmentedPager: MXSegmentedPager, viewControllerForPageAt index: Int) -> UIViewController {
        
        switch index {
        case 5:
            return W0VC()
        case 4:
            return W1VC()
        case 3:
            return W2VC()
        case 2:
            return W3VC()
        case 1:
            return W4VC()
        case 0:
            return W5VC()
        default:
            break
        }
        return W0VC()


    }


    fileprivate  func W0VC() ->OrdersVC{
        let vc:OrdersVC = OrdersVC.loadFromNib()
        vc.Link = "status=0"
        defaults.set(0, forKey: "viewStatus")
        addChild(vc)
       vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func W1VC() ->OrdersVC{
        let vc:OrdersVC = OrdersVC.loadFromNib()
        vc.Link = "status=1"
        defaults.set(1, forKey: "viewStatus")
        addChild(vc)
        vc.didMove(toParent: self)
        return vc

    }
    fileprivate  func W2VC() ->OrdersVC{
        let vc:OrdersVC = OrdersVC.loadFromNib()
        vc.Link = "status=2"
        defaults.set(2, forKey: "viewStatus")
        addChild(vc)
        vc.didMove(toParent: self)
        return vc

    }

    fileprivate  func W3VC() ->OrdersVC{
        let vc:OrdersVC = OrdersVC.loadFromNib()
        vc.Link = "status=3"
        defaults.set(3, forKey: "viewStatus")
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func W4VC() ->OrdersVC{
        let vc:OrdersVC = OrdersVC.loadFromNib()
        vc.Link = "status=4"
        defaults.set(4, forKey: "viewStatus")
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func W5VC() ->OrdersVC{
        let vc:OrdersVC = OrdersVC.loadFromNib()
        vc.Link = "status=5"
        defaults.set(5, forKey: "viewStatus")
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
}
