//
//  OrdersVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/13/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class OrdersVC: SuperViewController {
    
    @IBOutlet weak var tableView: UITableView!
   
    var ordersitems = [Orders]()
    var ordersData:OrdersData?
    //var status =  [OrdersStatus]()
    //var status =  ["تم الدفع","قيد الشحن","تم الشحن","تم التسليم","ملغي"]
    //var status =  ["قيد الشحن","تم الشحن","تم التسليم","ملغي"]
    //var status =  ["تم الشحن","تم التسليم","ملغي"]
    //var status =  ["تم التسليم","ملغي"]
    //var status =  ["ملغي"]
   

    var status:Array = [String]()
//    var allStatus =  [["تم الدفع","قيد الشحن","تم الشحن","تم التسليم","ملغي"],["قيد الشحن","تم الشحن","تم التسليم","ملغي"],
//            ["تم الشحن","تم التسليم","ملغي"],["تم التسليم","ملغي"],["ملغي"]]

    var allStatus = [["Paid","Shipping Process","Shipping Done","Delivery finished","Canceled"],["Shipping Process","Shipping Done","Delivery finished","Canceled"],["Shipping Done","Delivery finished","Canceled"],["Delivery finished","Canceled"],["Canceled"]]
    
    var currentpage = 1
    var selectedStatus:Int?
    var Link = "status=0"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        tableView.registerCell(id: "OrdersCell")
        getOrders()
      //  getStatus()
        self.tableView.es.addPullToRefresh {
            self.currentpage = 1
            self.ordersitems.removeAll()
            self.getOrders()
            self.hideIndicator()
        }
        
        self.tableView.es.addInfiniteScrolling {
            self.currentpage += 1
            self.getOrders() // next page
            self.tableView.estimatedRowHeight = 0
            self.hideIndicator()
        }
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateOrder(notification:)), name: Notification.Name("UpdateOrder"), object: nil)
        
    }
        
    
//    func getStatus(){
//
//        guard Helper.isConnectedToNetwork() else {
//            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
//            return }
//
//        _ = WebRequests.setup(controller: self).prepare(query: "metrics/order-per-status", method: HTTPMethod.get).start(){ (response, error) in
//            do {
//                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
//                if Status.status != 200{
//                    self.showAlert(title: "error".localized, message:Status.message!)
//                    return
//                }
//
//            }catch let jsonErr {
//                print("Error serializing  respone json", jsonErr)
//            }
//
//
//            do {
//                let Status =  try JSONDecoder().decode(OrdersStatusObject.self, from: response.data!)
//                self.status = Status.data!
//            } catch let jsonErr {
//                print("Error serializing  respone json", jsonErr)
//            }
//
//
//        }
//    }
    
    func getOrders(){
        
        guard Helper.isConnectedToNetwork() else {
            self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
            self.emptyView?.imgEmpty.image = UIImage(named: "refreshment")
            self.emptyView?.firstLabel.text = "There is no internet connection".localized
            return }
        
        self.showIndicator()
            _ = WebRequests.setup(controller: self).prepare(query: "orders?page=\(self.currentpage)&" + Link, method: HTTPMethod.get).start(){ (response, error) in
                self.hideIndicator()
            
            self.tableView.es.stopLoadingMore()
            self.tableView.es.stopPullToRefresh()
            
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(OrdersObject.self, from: response.data!)
                self.ordersData = Status.data
                self.ordersitems += self.ordersData!.resources!
                if self.ordersData?.nextPageUrl == nil {
                    self.tableView.es.stopLoadingMore()
                    self.tableView.es.noticeNoMoreData()
                }
                if self.ordersitems.count == 0{
                    self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                    self.emptyView?.imgEmpty.isHidden = true
                    if self.Link == "status=0"{
                        self.emptyView?.firstLabel.text = "No orders waiting for payment".localized
                    }
                    if self.Link == "status=1"{
                        self.emptyView?.firstLabel.text = "No orders have been paid".localized
                    }
                    if self.Link == "status=2"{
                        self.emptyView?.firstLabel.text = "No orders are pending".localized
                    }
                    if self.Link == "status=3"{
                        self.emptyView?.firstLabel.text = "No orders shipped".localized
                    }
                    if self.Link == "status=4"{
                        self.emptyView?.firstLabel.text = "No orders delivered".localized
                    }
                    if self.Link == "status=5"{
                        self.emptyView?.firstLabel.text = "No canceled orders".localized
                    }
                    self.ordersitems.removeAll()
                    self.tableView.reloadData()
                    
                }else{
                    self.tableView.tableFooterView = nil
                }
                self.tableView.reloadData()
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
    }
    
    @IBAction func didRefersh(){
        ordersitems.removeAll()
        getOrders()
    }
    
    
    @objc func UpdateOrder(notification: NSNotification)  {
        ordersitems.removeAll()
        getOrders()
    }
}


extension OrdersVC:UITableViewDataSource,UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ordersitems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrdersCell", for: indexPath) as! OrdersCell
        cell.order = ordersitems[indexPath.row]
        
        cell.deatilesTapped = { [weak self] in
            let vc:MainOrderDeatiles = AppDelegate.sb_main.instanceVC()
            vc.ID = self!.ordersitems[indexPath.row].id
            vc.hidesBottomBarWhenPushed = true
            vc.modalPresentationStyle = .fullScreen
            self!.navigationController?.pushViewController(vc, animated: true)
        }
        
        cell.statusTapped = { [weak self] in
            if (self!.Link == "status=0"){
                self!.status = self!.allStatus[0]
            }
            if (self!.Link == "status=1"){
                self!.status = self!.allStatus[1]
            }
            if (self!.Link == "status=2"){
                self!.status = self!.allStatus[2]
            }
            if (self!.Link == "status=3"){
                self!.status = self!.allStatus[3]
            }
            if (self!.Link == "status=4"){
                self!.status = self!.allStatus[4]
            }
            
            ActionSheetStringPicker.show(withTitle: "Status".localized, rows: self!.status.map { $0.localized as Any }
                , initialSelection: 0, doneBlock: {
                    picker, value, index in
                    if let Value = index {
                        let statusValue = Value as? String
                        if statusValue == "Waiting payment".localized{
                            self!.selectedStatus = 0
                        }
                        if statusValue == "Paid".localized{
                            self!.selectedStatus = 1
                        }
                        if statusValue == "Shipping Process".localized{
                            self!.selectedStatus = 2
                        }
                        if statusValue == "Shipping Done".localized{
                            self!.selectedStatus = 3
                        }
                        if statusValue == "Delivery finished".localized{
                            self!.selectedStatus = 4
                        }
                        if statusValue == "Canceled".localized{
                            self!.selectedStatus = 5
                        }
                        var parameters: [String: Any] = [:]
                        parameters["status"] = self!.selectedStatus?.description
                        
                        guard Helper.isConnectedToNetwork() else {
                            self?.showAlert(title: "".localized, message: "There is no internet connection".localized)
                            return }
                        
                        let ID = self!.ordersitems[indexPath.row].id
                        _ = WebRequests.setup(controller: self).prepare(query: "orders/\(ID ?? 0)/status", method: HTTPMethod.post,parameters: parameters).start(){ (response, error) in
                            
                            do {
                                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                                if Status.status != 200 {
                                    self!.showAlert(title: "Error".localized, message:Status.message!)
                                    return
                                }else if Status.status == 200{
                                    NotificationCenter.default.post(name: Notification.Name("UpdateOrder"), object: nil)
                                }
                                
                            }catch let jsonErr {
                                print("Error serializing respone json", jsonErr)
                            }
                        }
                    }
                    
                    return
            }, cancel: { ActionStringCancelBlock in return }, origin: self?.tableView .cellForRow(at: indexPath))
        }
        
        cell.ShippingTapped = { [weak self] in
            guard Helper.isConnectedToNetwork() else {
                self?.showAlert(title: "".localized, message: "There is no internet connection".localized)
                return }
            let ID = self!.ordersitems[indexPath.row].id
            _ = WebRequests.setup(controller: self).prepare(query: "orders/\(ID ?? 0)/shipping", method: HTTPMethod.post).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        self!.showAlert(title: "Error".localized, message:Status.message!)
                        return
                    }else if Status.status == 200{
                        self!.showAlert(title: "", message:Status.message!)
                        NotificationCenter.default.post(name: Notification.Name("UpdateOrder"), object: nil)
                    }
                    
                }catch let jsonErr {
                    print("Error serializing respone json", jsonErr)
                }
            }
        }
        
        cell.deleteTapped = { [weak self] in
            let vc:DeleteAlert = DeleteAlert.loadFromNib()
            vc.isFromOffers = true
            vc.id = self!.ordersitems[indexPath.row].id
            vc.modalPresentationStyle = .overCurrentContext
            self!.present(vc, animated: false, completion: nil)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  160
    }
}
