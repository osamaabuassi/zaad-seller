//
//  AViewController.swift
//  ZaadSeller
//
//  Created by osamaaassi on 1/14/21.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class AViewController: SuperViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
         let navgtion = self.navigationController as! CustomNavigationBar
         navgtion.setRightButtons([navgtion.Settingsbtn!], sender: self)
         navgtion.setLogotitle(sender: self)
         navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
         
     }
     override func viewWillDisappear(_ animated: Bool) {
         let navgtion = self.navigationController as! CustomNavigationBar
         navgtion.setLogotitle(sender: self)
         navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
         
     }

 

}
