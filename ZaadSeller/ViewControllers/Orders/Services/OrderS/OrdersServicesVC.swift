//
//  OrdersServicesVC.swift
//  ZaadSeller
//
//  Created by osamaaassi on 1/14/21.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class OrdersServicesVC: SuperViewController,OfferPrices {
    func prices(price: String, note: String) {
        priceStatus = price
        if (!note.isEmpty){
            priceNote = note
        }
    }
    
  
    
    
    @IBOutlet weak var tableView: UITableView!
   
    
    
    var servicesOrders = [Resources]()
    var servicesData:ServicesData?
    var comments = [CommentChat]()
    var statusItem : ItemsServicesOrder?
    var serviceStatus = [PaymentStatus]()
    var  nextStatu = [StatusServices]()
    

    
    var currentpage = 1
    var selectedStatus:Int?
    var Link = "status=0"
    var priceStatus:String?
    var priceNote:String?
 
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       self.tableView.tableFooterView = UIView()
        tableView.registerCell(id: "ServicesCell")
        getOrdersServies()
        getStatus()
        self.tableView.es.addPullToRefresh {
            self.currentpage = 1
            self.servicesOrders.removeAll()
            self.getOrdersServies()
            self.hideIndicator()
        }
        
        self.tableView.es.addInfiniteScrolling {
            self.currentpage += 1
            self.getOrdersServies() // next page
            self.tableView.estimatedRowHeight = 0
            self.hideIndicator()
        }
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateOrder(notification:)), name: Notification.Name("UpdateOrder"), object: nil)
        
//        NotificationCenter.default.addObserver(self, selector: #selector(PriceStutes(notification:)), name: Notification.Name("PriceStutes"), object: nil)
//

        
    }

    
    
    func getStatus(){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "service-orders/status", method: HTTPMethod.get,isAuthRequired: false).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(StatusServicesOrder.self, from: response.data!)
                
                self.statusItem = Status.data!
                self.serviceStatus += self.statusItem!.items!
           
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
        
    }
    
    func getOrdersServies(){
        guard Helper.isConnectedToNetwork() else {
            self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
            self.emptyView?.imgEmpty.image = UIImage(named: "refreshment")
            self.emptyView?.firstLabel.text = "There is no internet connection".localized
            return }
        self.servicesOrders.removeAll()
        self.showIndicator()
        _ = WebRequests.setup(controller: self).prepare(query: "service-orders?page=\(self.currentpage)&" + Link, method: HTTPMethod.get).start(){ (response, error) in
            
            self.hideIndicator()
            
            self.tableView.es.stopLoadingMore()
            self.tableView.es.stopPullToRefresh()
            
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(ServicesOrders.self, from: response.data!)
                self.servicesData = Status.data
                self.servicesOrders += self.servicesData!.resources!
                
                if self.servicesOrders.count != 0 {
//                    if  let obj = self.servicesOrders.filter({$0.nextStatus?.count != 0}).first{
//
//                        self.nextStatus = obj.nextStatus?[0]
//                    }
                    
                    if let obj = self.servicesOrders.filter({$0.nextStatus?.count != 0}).map({$0.nextStatus}).first{
                        
                        self.nextStatu = obj!
                    }
                    

                }
//
                
                if self.servicesData?.nextPageUrl == nil {
                    self.tableView.es.stopLoadingMore()
                    self.tableView.es.noticeNoMoreData()
                }
                if self.servicesOrders.count == 0{
                    
                    self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                    self.emptyView?.firstLabel.text = "No orders to display".localized
                    self.emptyView?.imgEmpty.isHidden = true
                    
                    if self.Link == "status=0"{
                        self.emptyView?.firstLabel.text = "There are no applications awaiting acceptance".localized
                    }
                    if self.Link == "status=1"{
                        self.emptyView?.firstLabel.text = "No paid orders".localized
                    }
                    if self.Link == "status=2"{
                        self.emptyView?.firstLabel.text = "There are no priced orders".localized
                    }
                    if self.Link == "status=4"{
                        self.emptyView?.firstLabel.text = "No requests in progress".localized
                    }
                    if self.Link == "status=5"{
                        self.emptyView?.firstLabel.text = "No requests have been fulfilled".localized
                    }
                    if self.Link == "status=11"{
                        self.emptyView?.firstLabel.text = "There are no rejected price requests".localized
                    }
                    if self.Link == "status=12"{
                        self.emptyView?.firstLabel.text = "There are no rejected requests".localized
                    }
                    if self.Link == "status=13"{
                        self.emptyView?.firstLabel.text = "No canceled orders".localized
                    }
                    
                    self.servicesOrders.removeAll()
                    self.tableView.reloadData()
                    
                }else{
                    self.tableView.tableFooterView = nil
                }
                self.tableView.reloadData()
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
    }
    

    
    @IBAction func didRefersh(){
        servicesOrders.removeAll()
        getOrdersServies()
    }
    
//    @objc func PriceStutes(notification: NSNotification)  {
//        // print(notification.object)
//        offerPriceView(id: notification.object as! Int)
//    }
    
    
    @objc func UpdateOrder(notification: NSNotification)  {
        servicesOrders.removeAll()
        priceStatus = nil
        priceNote = nil
        getOrdersServies()
    }
   
    
    func offerPriceView(id:Int) {
        if (self.selectedStatus == 2 && ((self.priceStatus?.isEmpty) == nil)){
            let vc:OfferPrice = OfferPrice.loadFromNib()
            vc.id = id
            vc.selectedStatus = self.selectedStatus
            vc.delegetPrics = self
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc, animated: false, completion: nil)
        }
        else if (self.selectedStatus != nil){
            changeStatus(ID: id)
        }
        
       
    }
    func changeStatus(ID:Int)  {
        
        
        var parameters: [String: Any] = [:]
        parameters["status"] = self.selectedStatus?.description
       
        
        if (self.selectedStatus == 2){
            guard let price = self.priceStatus ,!price.isEmpty else{
                self.showAlert(title: "error".localized, message: "Please enter the price".localized)
                return
            }
            parameters["price"] = price
        }
        
        if(self.priceNote?.isEmpty != nil){
            parameters["note"] = priceNote!
        }
        
        
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }

//        let ID = self!.servicesOrders[indexPath.row].id
        _ = WebRequests.setup(controller: self).prepare(query: "service-orders/\(ID)/change-status", method: HTTPMethod.post,parameters: parameters).start(){ (response, error) in

            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }else if Status.status == 200{
                    NotificationCenter.default.post(name: Notification.Name("UpdateOrder"), object: nil)
                }

            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
    }
}


extension OrdersServicesVC:UITableViewDataSource,UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return servicesOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServicesCell", for: indexPath) as! ServicesCell
        let obj = servicesOrders[indexPath.row]
        cell.order = servicesOrders[indexPath.row]
        if(Link == "status=1"){
            cell.lblChangeStatus.text = "Add price".localized
        }
        else{
            cell.lblChangeStatus.text = "Change status".localized
        }

        
        cell.PaymentStatusTapped = { [weak self] in
            
            let vc:OfferPrice = OfferPrice.loadFromNib()
            vc.delegetPrics = self
            vc.modalPresentationStyle = .overCurrentContext
            self!.present(vc, animated: false, completion: nil)
            
        }
        cell.deatilesTapped = { [weak self] in
            let vc:MainServicesDeatiles = AppDelegate.sb_main.instanceVC()
            vc.ID = self!.servicesOrders[indexPath.row].id
            
            vc.hidesBottomBarWhenPushed = true
            vc.modalPresentationStyle = .fullScreen
            self!.navigationController?.pushViewController(vc, animated: true)
        }
        if servicesOrders[indexPath.row].nextStatus?.count == 0 {
            cell.viewChangeStatus.isHidden = true
            
        }
        
        cell.statusTapped = { [weak self] in
            
            if obj.nextStatus?.count != 0 {
                
                ActionSheetStringPicker.show(withTitle: "Status".localized, rows: obj.nextStatus?.map({$0.name ?? ""})
                                             , initialSelection: 0, doneBlock: {
                        picker, value, index in
                        if let Value = index {
                            let statusValue = Value as? String
                            
                            
//                                if let object = self!.serviceStatus.filter({ $0.name == statusValue }).first{
//                                    self!.selectedStatus =  object.id!
//
//                                }
                                
                                if let object2 = obj.nextStatus!.filter({ $0.name == statusValue }).first{
                                    self!.selectedStatus =  object2.id!
                                
                            }
                            
                            let id = self!.servicesOrders[indexPath.row].id ?? 0
                            self!.offerPriceView(id: id)
       
                        }

                        return
                }, cancel: { ActionStringCancelBlock in return }, origin: self?.tableView .cellForRow(at: indexPath))
            }
            
        }
        
        cell.AddTheOrderTapped = { [weak self] in
            let editVC:EditServicesVC = EditServicesVC.loadFromNib()
            editVC.servicesOrders = self!.servicesOrders[indexPath.row]
            self!.navigationController?.pushViewController(editVC, animated: true)
        }
        cell.messageTapped = { [weak self] in
            
            let vcChat:CommentChatVC = CommentChatVC.loadFromNib()
            vcChat.id = self!.servicesOrders[indexPath.row].id
            vcChat.hidesBottomBarWhenPushed = true
            self!.navigationController?.pushViewController(vcChat, animated: true)
            
        }
        cell.PaymentTapped = { [weak self] in
            let scriptionsVC:SubscriptionsVC = SubscriptionsVC.loadFromNib()
            self!.navigationController?.pushViewController(scriptionsVC, animated: true)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  222
    }
    
    
}
