//
//  ServicesCell.swift
//  ZaadSeller
//
//  Created by osamaaassi on 1/16/21.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class ServicesCell: UITableViewCell {
    
    @IBOutlet weak var lblCodeOrder: UILabel!
    @IBOutlet weak var lblDateOrder: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblPaymentStatus: UILabel!
    @IBOutlet weak var lblChangeStatus: UILabel!
    
    @IBOutlet weak var btDeatiles: UIButton!
    @IBOutlet weak var btChangeStatus: UIButton!
    @IBOutlet weak var btPayment: UIButton!
    @IBOutlet weak var btChangeTheOrder: UIButton!
    @IBOutlet weak var btMessage: UIButton!
    
    @IBOutlet weak var viewDeatiles: UIView!
    @IBOutlet weak var viewChangeStatus: UIView!
    @IBOutlet weak var viewPayment: UIView!
    @IBOutlet weak var viewChangeTheOrder: UIView!
    @IBOutlet weak var viewMessage: UIView!
    @IBOutlet weak var viewPaymentStatus: UIView!

   
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
    }
    
    var deatilesTapped: (() -> ())?
    var statusTapped: (() -> ())?
    var AddTheOrderTapped: (() -> ())?
    var messageTapped : (() -> ())?
    var PaymentTapped: (() -> ())?
    var PaymentStatusTapped: (() -> ())?
    
    
    
    let isStatus = UserDefaults.standard.integer(forKey: "status")
    var order:Resources?{
           didSet{
            self.lblCodeOrder.text = order?.orderNo ?? ""
            self.lblDateOrder.text = order?.createdAt ?? ""
            self.lblName.text = order?.orderNo ?? ""
            self.lblStatus.text = order?.status?.name ?? ""
            self.lblPaymentStatus.text = order?.paymentStatus?.name ?? ""
           
//            if (order?.nextStatus?.count == 0){
//                viewChangeStatus.isHidden = true
//                viewMessage.isHidden = true
//            }
            if (order?.status?.id == 1){
                viewPayment.isHidden = false
            }
            if (order?.status?.id == 0){
                viewPayment.isHidden = true
            }
            if (isStatus == 5){
//                viewChangeStatus.isHidden = true
                viewPayment.isHidden = true
              //  viewChangeTheOrder.isHidden = true
                viewMessage.isHidden = true
                
            }
            
            if (isStatus == 13){
                viewChangeTheOrder.isHidden = true
            }
             else{
                viewChangeTheOrder.isHidden = false
                viewMessage.isHidden = false
              //  viewChangeStatus.isHidden = false
             }
            
           }
           
       }
    
    
    @IBAction func Deatiles(_ sender: UIButton) {
        deatilesTapped?()
    }
    
    @IBAction func Status(_ sender: UIButton) {
        statusTapped?()
    }
    @IBAction func ChangeTheOrder(_ sender: UIButton) {
        AddTheOrderTapped?()
    }
    @IBAction func Message(_ sender: UIButton) {
        messageTapped?()
    }
    
    @IBAction func Payment(_ sender: UIButton) {
        PaymentTapped?()
    }
    @IBAction func PaymentStatus(_ sender: UIButton) {
        PaymentStatusTapped?()
    }
    
    
    
    
    
}
