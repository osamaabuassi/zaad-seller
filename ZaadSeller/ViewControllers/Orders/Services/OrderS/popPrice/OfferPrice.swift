//
//  OfferPrice.swift
//  ZaadSeller
//
//  Created by osamaaassi on 28/02/2021.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
protocol OfferPrices{
    func prices(price:String,note:String)
    
}

class OfferPrice: UIViewController {

    var price:Int?
    var id:Int?
    var selectedStatus:Int?
    var delegetPrics : OfferPrices?
    
    @IBOutlet weak var tfPrice: UITextField!
    @IBOutlet weak var tfNote: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func didSeave(_ sender:UIButton){
        
        
        guard let price = self.tfPrice.text, !price.isEmpty else{
            self.showAlert(title: "error".localized, message: "Please enter the price".localized)
            return
        }
//        guard let note = self.tfNote.text, !price.isEmpty else{
//            self.showAlert(title: "error".localized, message: "الرجاء ادخال ملاحظة".localized)
//            return
//        }
        
//        self.delegetPrics?.prices(price: price,note: self.tfNote.text ?? "" )
//        NotificationCenter.default.post(name: Notification.Name("PriceStutes"), object:id)
//
        var parameters: [String: Any] = [:]
        parameters["status"] = self.selectedStatus?.description ?? 0
       
            parameters["price"] = price
            parameters["note"] = tfNote.text ?? ""
        
      
            WebRequests.setup(controller: self).prepare(query: "service-orders/\(id ?? 0)/change-status", method: HTTPMethod.post,parameters: parameters).start(){ (response, error) in

            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }else if Status.status == 200{
                    self.dismiss(animated: false, completion: nil)
                    NotificationCenter.default.post(name: Notification.Name("UpdateOrder"), object: nil)
                }

            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            self.dismiss(animated: false, completion: nil)

    }

   

}
    @IBAction func Cancel(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    } 
    
}
