//
//  EditServicesVC.swift
//  ZaadSeller
//
//  Created by osamaaassi on 1/21/21.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
class EditServicesVC: SuperViewController {
    
    @IBOutlet weak var tfTitle: UITextField!
    @IBOutlet weak var tfPrice: UITextField!
    @IBOutlet weak var tfSellerDeliveryDate: UITextField!
    @IBOutlet weak var tfDescription: UITextField!
    @IBOutlet weak var tfFromTime: UITextField!
    @IBOutlet weak var tfToTime: UITextField!
    var idServece:Int!
    var date:String?
    var fromTime:String?
    var toTime:String?
    var servicesOrders:Resources?
    
    var servicesData:DataServicesView?
    var servicesitems = [ItemsServicesView]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getServices()
     
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setRightButtons([navgtion.Settingsbtn!], sender: self)
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    
    @IBAction func didTab_Activity(_ sender: UIButton) {
        
       
           
        ActionSheetStringPicker.show(withTitle: "Services".localized, rows: self.servicesitems.map { $0.title as Any }
               , initialSelection: 0, doneBlock: {
                   picker, value, index in
                   if let Value = index {
                       self.tfTitle.text = Value as? String
                  
                     self.idServece = self.servicesitems[value].id ?? 0
                    
                    
                    if let object = self.servicesitems.filter({$0.categoryTitle == Value as? String }).first{
                      
                        self.idServece =  object.id ?? 0
                    }
//
                   }
                   return
           }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    
    func getServices(){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "services", method:
            HTTPMethod.get).start(){ (response, error) in
          
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(ServicesView.self, from: response.data!)
                self.servicesData = Status.data
                self.servicesitems += self.servicesData!.items!
              
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
    }
   
  
    
    @IBAction func dp(_ sender: Any) {
        self.view.endEditing(true)
        let alert = UIAlertController(style: .actionSheet, title: "Please select date".localized)
        alert.addDatePicker(mode: .date, date: Date(), minimumDate: Date(), maximumDate: nil) {[weak self] date in
            
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "yyyy-M-dd"
            let myString = formatter.string(from: date)
            print(myString)
            self?.date = myString
            self?.tfSellerDeliveryDate.text = self?.date
            
//            self?.date = date.asString(style: .short)
//            self?.tfdatemeasurement.text = self?.date
        }
        alert.addAction(title: "Done".localized, style: .cancel)
        //alert.show()
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func timePicker(_ sender: Any) {
        self.view.endEditing(true)
        let alert = UIAlertController(style: .actionSheet, title: "Please select time".localized)
        alert.addDatePicker(mode: .time, date: Date(), minimumDate: Date(), maximumDate: nil) {[weak self] date in
            
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "hh:mm"
            let myString = formatter.string(from: date) // string purpose I add here
            
            print(myString)
            
            self?.fromTime = myString
            self?.tfFromTime.text = self?.fromTime
        }
        alert.addAction(title: "Done".localized, style: .cancel)
        //alert.show()
        self.present(alert, animated: true, completion: nil)
        
    }
    @IBAction func timePickerTo(_ sender: Any) {
        self.view.endEditing(true)
        let alert = UIAlertController(style: .actionSheet, title: "Please select time".localized)
        alert.addDatePicker(mode: .time, date: Date(), minimumDate: Date(), maximumDate: nil) {[weak self] date in
            
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "hh:mm"
            let myString = formatter.string(from: date) // string purpose I add here
            self?.toTime = myString
            self?.tfToTime.text = myString
        }
        alert.addAction(title: "Done".localized, style: .cancel)
        //alert.show()
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        tfSellerDeliveryDate.text = dateFormatter.string(from: sender.date)
    }
    
    
    @IBAction func didSeave(_ sender:UIButton){
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
       
        guard let price = self.tfPrice.text, !price.isEmpty else{
            self.showAlert(title: "error".localized, message: "You must enter the price".localized)
            return
        }
        guard let description = self.tfDescription.text, !description.isEmpty else{
            self.showAlert(title: "error".localized, message: "Description cannot be empty".localized)
            return
        }
        
        guard let fromTimes = self.fromTime, !fromTimes.isEmpty else{
            self.showAlert(title: "error".localized, message: "Please select time".localized)
            return
        }
        guard let time = self.toTime, !time.isEmpty else{
            self.showAlert(title: "error".localized, message: "Please select time".localized)
            return
        }
        guard let serveceID = self.idServece else{
            self.showAlert(title: "error".localized, message: "Service must be selected".localized)
            return
        }
        
        var parameters: [String: Any] = [:]
        parameters["order_id"] = servicesOrders?.id ?? 0
        parameters["service_id"] =  serveceID
        parameters["price"] = price
        parameters["address_id"] = 34
        parameters["description"] = description
        parameters["from_time"] = fromTimes
        parameters["to_time"] = time
        
        _ = WebRequests.setup(controller: self).prepare(query: "service-orders", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }
                else if Status.status == 200{
                NotificationCenter.default.post(name:Notification.Name("UpdateOrder"), object: nil)
                    self.navigationController?.popViewController(animated: true)
                }
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    
}
