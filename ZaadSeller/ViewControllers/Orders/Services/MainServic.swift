//
//  MainServic.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/13/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import MXSegmentedPager

class MainServic: BaseViewController {

    @IBOutlet weak var HeadrView: UIView!
  
    var arrayView = [String]()
    var arrayViewController = [UIViewController]()



    override func viewDidLoad() {
        super.viewDidLoad()
    

       
//arrayView = ["في انتظار الدفع","تم الدفع","قيد التنفيذ","تم التسليم","ملغي" ]

        if MOLHLanguage.isRTLLanguage() {
            arrayView = arrayView.reversed()
            arrayViewController = arrayViewController.reversed()
        }
       // setUpSegmentation()

    }
   
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)

    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }



      }



extension MainServic {


//    func setUpSegmentation (){
//
//        if MOLHLanguage.isRTLLanguage(){
//            self.segmentedPager.pager.showPage(at:self.arrayView.count - 1, animated: false)
//        }
//        DispatchQueue.main.async {
//            if MOLHLanguage.isRTLLanguage(){
//                self.segmentedPager.pager.showPage(at:self.arrayView.count - 1, animated: false)
//                self.segmentedPager.segmentedControl.selectedSegmentIndex = self.arrayView.count - 1
//                // segmentedPager.pager.reloadData()
//            }
//        }
//
//        segmentedPager.backgroundColor = UIColor.white
//        segmentedPager.segmentedControl.backgroundColor = UIColor.white
//        segmentedPager.parallaxHeader.view = HeadrView
//        segmentedPager.parallaxHeader.mode = .bottom
//        segmentedPager.parallaxHeader.minimumHeight = 44
//
//
//        // Segmented Control customization
//        self.segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocation.down
//        segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSAttributedString.Key.foregroundColor: "014A97".color]
//
//        segmentedPager.segmentedControl.selectionIndicatorColor = "FF6A2A".color
//        segmentedPager.segmentedControl.selectionIndicatorHeight = 3
//
//        self.segmentedPager.segmentedControl.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "FFShamelFamilySemiRoundBook-S", size: 17)!, NSAttributedString.Key.foregroundColor :  "014A97".color]
//
//
//    }


    override func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {


        switch index {
        case 0:
            return "Cancelled".localized

        case 1:
            return "Delivery finished".localized

        case 2:
            return "Underway".localized
        case 3:
            return "Paid".localized

        case 4:
            return "Pending payment".localized
        default:
            break
        }
        return "Delivery finished".localized

    }

    override func segmentedPager(_ segmentedPager: MXSegmentedPager, didScrollWith parallaxHeader: MXParallaxHeader) {
    }


    override func numberOfPages(in segmentedPager: MXSegmentedPager) -> Int {
        return arrayView.count
    }


    override func segmentedPager(_ segmentedPager: MXSegmentedPager, viewControllerForPageAt index: Int) -> UIViewController {
        switch index {
        case 4:
            return W0VC()
        case 3:
            return W1VC()
        case 2:
            return W2VC()
        case 1:
            return W3VC()
        case 0:
            return W4VC()
        default:
            break
        }
        return W0VC()


    }


    fileprivate  func W0VC() ->OrdersServicesVC{
        let vc:OrdersServicesVC = OrdersServicesVC.loadFromNib()
        vc.Link = "status=0"
        addChild(vc)
       vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func W1VC() ->OrdersServicesVC{
        let vc:OrdersServicesVC = OrdersServicesVC.loadFromNib()
        vc.Link = "status=1"
        addChild(vc)
        vc.didMove(toParent: self)
        return vc

    }
    fileprivate  func W2VC() ->OrdersServicesVC{
        let vc:OrdersServicesVC = OrdersServicesVC.loadFromNib()
        vc.Link = "status=2"
        addChild(vc)
        vc.didMove(toParent: self)
        return vc

    }

    fileprivate  func W3VC() ->OrdersServicesVC{
        let vc:OrdersServicesVC = OrdersServicesVC.loadFromNib()
        vc.Link = "status=3"
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func W4VC() ->OrdersServicesVC{
        let vc:OrdersServicesVC = OrdersServicesVC.loadFromNib()
        vc.Link = "status=4"
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
  
}
