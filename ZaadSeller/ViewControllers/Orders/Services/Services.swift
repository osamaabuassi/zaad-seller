//
//  Services.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/13/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import MXSegmentedPager

class Services: BaseViewController {
    
   // @IBOutlet weak var HeadrView: UIView!
    
    var arrayView = [String]()
    var arrayViewController = [UIViewController]()
    let defaults = UserDefaults.standard
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       //arrayView = ["الطلب مقبول","تم عرض سعر","السعر مقبول","قيد التنفيذ ","تم التنفيذ ","السعر مرفوض","مرفوضة","ملغي","انتظار القبول"]
        
        arrayView =  ["Waiting for acceptance",
          "The order is accepted",
          "Price offered",
          "Price Acceptable",
          "Underway",
          "Done",
          "Price Denied",
          "Rejected",
          "Cancelled"]
        
        if MOLHLanguage.isRTLLanguage() {
            arrayView = arrayView.reversed()
            arrayViewController = arrayViewController.reversed()
        }
        setUpSegmentation()
        
    }
    override func viewWillAppear(_ animated: Bool) {
     let navgtion = self.navigationController as! CustomNavigationBar
     navgtion.setRightButtons([navgtion.Settingsbtn!], sender: self)
     navgtion.setLogotitle(sender: self)
     navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)

 }
 override func viewWillDisappear(_ animated: Bool) {
     let navgtion = self.navigationController as! CustomNavigationBar
     navgtion.setLogotitle(sender: self)
     navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
 }
 override func didClickRightButton(_sender: UIBarButtonItem) {
         let vc = SettingsControlVC.loadFromNib()
         vc.hidesBottomBarWhenPushed = true
         vc.modalPresentationStyle = .fullScreen
         self.navigationController?.pushViewController(vc, animated: true)
         
     }
}


extension Services {
    func setUpSegmentation (){
        
        if MOLHLanguage.isRTLLanguage(){
            self.segmentedPager.pager.showPage(at:self.arrayView.count - 1, animated: false)
        }
        DispatchQueue.main.async {
            if MOLHLanguage.isRTLLanguage(){
                self.segmentedPager.pager.showPage(at:self.arrayView.count - 1, animated: false)
                self.segmentedPager.segmentedControl.selectedSegmentIndex = self.arrayView.count - 1
                // segmentedPager.pager.reloadData()
            }
        }
        
        segmentedPager.backgroundColor = UIColor.white
        segmentedPager.segmentedControl.backgroundColor = UIColor.white
        // segmentedPager.parallaxHeader.view = HeadrView
        segmentedPager.parallaxHeader.mode = .bottom
        //   segmentedPager.parallaxHeader.height = 50
        segmentedPager.parallaxHeader.minimumHeight = 44
        
        
        
        // Segmented Control customization
        self.segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocation.down
        segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSAttributedString.Key.foregroundColor: "014A97".color]
        
        segmentedPager.segmentedControl.selectionIndicatorColor = "FF6A2A".color
        segmentedPager.segmentedControl.selectionIndicatorHeight = 1
        
        self.segmentedPager.segmentedControl.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "FFShamelFamilySemiRoundBook-S", size: 17)!, NSAttributedString.Key.foregroundColor :  "014A97".color]
        
        
    }
    
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {
        
        
        switch index {
        case 8:
            return "Waiting for acceptance".localized
        case 7:
            return "The order is accepted".localized
        case 6:
            return "Price offered".localized
        case 5:
            return "Price Acceptable".localized
        case 4:
            return "Underway".localized
        case 3:
            return "Done".localized
        case 2:
            return "Price Denied".localized
        case 1:
            return "Rejected".localized
        case 0:
            return "Cancelled".localized
        default:
            break
        }
        return "Under discussion".localized
        
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, didScrollWith parallaxHeader: MXParallaxHeader) {
    }
    
    
    override func numberOfPages(in segmentedPager: MXSegmentedPager) -> Int {
        return arrayView.count
    }
    
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, viewControllerForPageAt index: Int) -> UIViewController {
        switch index {
        
        case 0:
            return W8VC()
        case 1:
            return W7VC()
        case 2:
            return W6VC()
        case 3:
            return W5VC()
        case 4:
            return W4VC()
        case 5:
            return W3VC()
        case 6:
            return W2VC()
        case 7:
            return W1VC()
        case 8:
            return W0VC()
       
            
        default:
            break
        }
        return W0VC()
        
        
    }
    
    
    fileprivate  func W0VC() -> OrdersServicesVC{
        let vc:OrdersServicesVC = OrdersServicesVC.loadFromNib()
        addChild(vc)
        vc.Link = "status=0"
        defaults.set(0, forKey: "status")
        vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func W1VC() ->OrdersServicesVC{
        let vc:OrdersServicesVC = OrdersServicesVC.loadFromNib()
        vc.Link = "status=1"
        defaults.set(1, forKey: "status")
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func W2VC() ->OrdersServicesVC{
        let vc:OrdersServicesVC = OrdersServicesVC.loadFromNib()
        vc.Link = "status=2"
        defaults.set(2, forKey: "status")
        NotificationCenter.default.post(name: Notification.Name("UpdateOrder"), object: nil)
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func W3VC() ->OrdersServicesVC{
        let vc:OrdersServicesVC = OrdersServicesVC.loadFromNib()
        vc.Link = "status=3"
        defaults.set(3, forKey: "status")
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func W4VC() ->OrdersServicesVC{

        let vc:OrdersServicesVC = OrdersServicesVC.loadFromNib()
        vc.Link = "status=4"
        defaults.set(4, forKey: "status")
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func W5VC() ->OrdersServicesVC{
        let vc:OrdersServicesVC = OrdersServicesVC.loadFromNib()
        vc.Link = "status=5"
        defaults.set(3, forKey: "status")
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func W6VC() ->OrdersServicesVC{
        let vc:OrdersServicesVC = OrdersServicesVC.loadFromNib()
        vc.Link = "status=11"
      //  vc.paymentStatus = "payment-status=11"
        defaults.set(3, forKey: "status")
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func W7VC() ->OrdersServicesVC{
        let vc:OrdersServicesVC = OrdersServicesVC.loadFromNib()
        vc.Link = "status=12"
    //    vc.paymentStatus = "payment-status=12"
        defaults.set(5, forKey: "status")
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func W8VC() ->OrdersServicesVC{
        let vc:OrdersServicesVC = OrdersServicesVC.loadFromNib()
        vc.Link = "status=13"
      //  vc.paymentStatus = "payment-status=13"
        defaults.set(13, forKey: "status")
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
}
