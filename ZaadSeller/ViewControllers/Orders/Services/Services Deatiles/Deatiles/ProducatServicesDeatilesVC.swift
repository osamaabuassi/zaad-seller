//
//  ProducatServicesDeatilesVC.swift
//  ZaadSeller
//
//  Created by osamaaassi on 1/19/21.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class ProducatServicesDeatilesVC: SuperViewController {
    
    
    @IBOutlet weak var OrderDetails: UIView!
    @IBOutlet weak var AddressOrder: UIView!
    @IBOutlet weak var ItemOrder: UIView!
    //  var orderDeatiles:OrderDeatilesData?
    
    @IBOutlet weak var tableViewServices: UITableView!
    @IBOutlet weak var heighttableViewServices: NSLayoutConstraint!
    
    var id:Int?
    var dataServices:DataServices?
    var orderServices = [ItemsServices]()
    
    var orderProductatems = [Items]()
    
    var isFromDeatileOrder = false
    var isFromAddressOrder = false
    var isFromItemOrder = false
    
    //Deatile Order
    @IBOutlet weak var lblNameService: UILabel!
    @IBOutlet weak var lblDescriptionService: UILabel!
    @IBOutlet weak var lblCodeService: UILabel!
    @IBOutlet weak var lblDateService: UILabel!
    @IBOutlet weak var lblStatusService: UILabel!
    @IBOutlet weak var lblTypePayService: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblDateStarting: UILabel!
    @IBOutlet weak var lblDateRequired: UILabel!
    @IBOutlet weak var lblfromTime: UILabel!
    @IBOutlet weak var lbltoTime: UILabel!
    @IBOutlet weak var lblNote: UILabel!
    @IBOutlet weak var viewNote: UIView!
    
    //Address Order
    @IBOutlet weak var lblnameBuy: UILabel!
    @IBOutlet weak var lbladdress1: UILabel!
    @IBOutlet weak var lbladdress2: UILabel!
    @IBOutlet weak var lbladdress3: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblPostalCode: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isFromDeatileOrder{
            self.OrderDetails.isHidden = false
            self.AddressOrder.isHidden = true
            self.ItemOrder.isHidden = true
        }
        if isFromAddressOrder{
            self.OrderDetails.isHidden = true
            self.AddressOrder.isHidden = false
            self.ItemOrder.isHidden = true
            
        }
        if isFromItemOrder{
            self.OrderDetails.isHidden = true
            self.AddressOrder.isHidden = true
            self.ItemOrder.isHidden = false
        }
        
        tableViewServices.registerCell(id: "ServiceCell")
        tableViewServices.tableFooterView = UIView.init(frame: .zero)
        
        NotificationCenter.default.addObserver(self, selector:#selector(UpdateCellItemService(notification:)), name: Notification.Name("UpdateCellItemService"), object: nil)
        
    }
    
    @objc func UpdateCellItemService(notification: NSNotification)  {
        getDeatiles()
        if orderProductatems.count == 0{
            heighttableViewServices.constant = 0
        }else{
            heighttableViewServices.constant = 740
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getDeatiles()
    }
    
    func getDeatiles(){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "service-orders/\(id ?? 0)", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(ServicesDeatiles.self, from: response.data!)
                self.dataServices = Status.data!
              //  self.orderServices = self.dataServices?.items ?? []
              //  self.tableViewServices.reloadData()
                //self.heighttableViewServices.constant = self.tableViewServices.contentSize.height
                
                //Main
                self.lblNameService.text = self.dataServices?.orderNo ?? ""
                self.lblDescriptionService.text = self.dataServices?.descriptionValue ?? ""
                self.lblCodeService.text = self.dataServices?.postalCode ?? ""
                self.lblDateService.text = self.dataServices?.requiredDate ?? ""
                self.lblStatusService.text = self.dataServices?.status?.name ?? ""
                self.lblTypePayService.text = self.dataServices?.paymentStatus?.name ?? ""
                self.lblName.text = self.dataServices?.user?.title ?? ""
                self.lblPrice.text = self.dataServices?.total?.description ?? "0"
                self.lblDateStarting.text = self.dataServices?.createdAt ?? ""
                self.lblDateRequired.text = self.dataServices?.requiredDate ?? ""
                self.lblfromTime.text = self.dataServices?.fromTime?.description ?? "0"
                self.lbltoTime.text = self.dataServices?.toTime?.description ?? "0"

                if (self.dataServices?.note != nil){
                    self.viewNote.isHidden = false
                    self.lblNote.text = self.dataServices?.note!
                }else{
                    self.viewNote.isHidden = true
                }
                
                
                
                //Address
                self.lblnameBuy.text = self.dataServices?.user?.title ?? ""
                self.lbladdress1.text = self.dataServices?.addressLine1 ?? ""
                self.lbladdress2.text = self.dataServices?.addressLine2 ?? ""
                self.lbladdress3.text = self.dataServices?.addressLine3 ?? ""
                self.lblCountry.text = self.dataServices?.county ?? ""
                self.lblCity.text = self.dataServices?.city ?? ""
                self.lblPostalCode.text = self.dataServices?.postalCode ?? ""
                self.lblPhone.text = self.dataServices?.phone ?? ""
                self.lblMobile.text = self.dataServices?.mobile ?? ""
                self.lblEmail.text = self.dataServices?.email ?? ""
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
    }
    
}


extension ProducatServicesDeatilesVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderServices.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableViewServices.dequeueReusableCell(withIdentifier: "ServiceCell", for: indexPath) as! ServiceCell
        cell.obj = orderServices[indexPath.row]
        cell.DeleteTapped = { [weak self] in

            let vc:DeleteAlert = DeleteAlert.loadFromNib()
            vc.id = self!.orderServices[indexPath.row].id
            vc.hidesBottomBarWhenPushed = true
            vc.isFromorderServices = true
            vc.modalPresentationStyle = .overCurrentContext
            self!.present(vc, animated: false, completion: nil)

        }
        cell.EditTapped = {[weak self] in

            let vc:EditPupVC = EditPupVC.loadFromNib()
            vc.id = self!.orderServices[indexPath.row].id
            vc.itemServices = self!.orderServices[indexPath.row]
            vc.modalPresentationStyle = .overCurrentContext
            self!.present(vc, animated: false, completion: nil)
        }

        return cell
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.heighttableViewServices.constant = self.tableViewServices.contentSize.height }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 740
    }

}
