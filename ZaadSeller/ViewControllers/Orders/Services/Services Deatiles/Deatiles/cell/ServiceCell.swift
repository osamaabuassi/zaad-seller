//
//  ServiceCell.swift
//  ZaadSeller
//
//  Created by osamaaassi on 1/31/21.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class ServiceCell: UITableViewCell {

    @IBOutlet weak var tfTitle: UITextField!
    @IBOutlet weak var tfQuantity: UITextField!
    @IBOutlet weak var tfStatus: UITextField!
    @IBOutlet weak var tfPaymentStatus: UITextField!
    @IBOutlet weak var tfSellerDeliveryDate: UITextField!
    @IBOutlet weak var tfRegularPrice: UITextField!
    @IBOutlet weak var tfDiscount: UITextField!
    @IBOutlet weak var tfPayable: UITextField!
    
    var DeleteTapped :(() -> ())?
    var EditTapped:(() -> ())?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    var obj : ItemsServices?{
        didSet{
            tfTitle.text = obj?.title ?? ""
            tfQuantity.text = obj?.quantity?.description ?? ""
            tfStatus.text = obj?.status?.name?.description ?? ""
            tfPaymentStatus.text = obj?.paymentStatus?.name?.description ?? ""
            tfSellerDeliveryDate.text = obj?.sellerDeliveryDate?.description ?? ""
            tfRegularPrice.text = obj?.regularPrice?.description ?? "0"
            tfDiscount.text = obj?.discount?.description ?? "0"
            tfPayable.text = obj?.payable?.description ?? "0"
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func Delete(_ sender: UIButton) {
                DeleteTapped?()
            }
    @IBAction func Edit(_ sender: UIButton) {
        EditTapped?()
    }
}
