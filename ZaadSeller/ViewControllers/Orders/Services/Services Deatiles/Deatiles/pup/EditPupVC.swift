//
//  EditPupVC.swift
//  ZaadSeller
//
//  Created by osamaaassi on 1/31/21.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class EditPupVC: SuperViewController {
    
    @IBOutlet weak var tfTitle: UITextField!
    @IBOutlet weak var tfQuantity: UITextField!
    @IBOutlet weak var tfSellerDeliveryDate: UITextField!
    @IBOutlet weak var tfRegularPrice: UITextField!
    @IBOutlet weak var tfNote: UITextField!
    
    
    var itemServices: ItemsServices?
    var id:Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tfTitle.text = itemServices?.title ?? ""
        tfQuantity.text = itemServices?.quantity?.description ?? "0"
        tfSellerDeliveryDate.text = itemServices?.sellerDeliveryDate ?? ""
        tfRegularPrice.text = itemServices?.regularPrice?.description ?? "0"
        tfNote.text = itemServices?.note ?? ""
        
    }
    
    
    @IBAction func didSeave(_ sender:UIButton){
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "error".localized, message: "There is no internet connection".localized)
            return }
        
        guard let title = self.tfTitle.text, !title.isEmpty else{
            self.showAlert(title: "error".localized, message: "You must enter the address".localized)
            return
        }
        guard let quantity = self.tfQuantity.text, !quantity.isEmpty else{
            self.showAlert(title: "error".localized, message: "Quantity cannot be empty".localized)
            return
        }
        guard let regularPrice = self.tfRegularPrice.text, !regularPrice.isEmpty else{
            self.showAlert(title: "error".localized, message: "You must enter the price".localized)
            return
        }
        
        var parameters: [String: Any] = [:]
        parameters["title"] = title
        parameters["quantity"] = quantity
        parameters["regular_price"] = regularPrice
        parameters["seller_delivery_date"] = tfSellerDeliveryDate.text?.description ?? ""
        parameters["note"] = tfNote.text?.description ?? ""
        
        _ = WebRequests.setup(controller: self).prepare(query: "service-orders/items/\(id ?? 0)", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }
                else if Status.status == 200{
                    self.dismiss(animated: false) {
                        NotificationCenter.default.post(name: Notification.Name("UpdateCellItemService"), object: nil)
                    }
                    self.dismiss(animated: false, completion: nil)
                }
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
    
    @IBAction func Cancel(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }  
}
