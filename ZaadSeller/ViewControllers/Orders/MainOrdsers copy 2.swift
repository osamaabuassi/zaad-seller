//
//  MainOrdsers.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/13/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import MXSegmentedPager

class MainOrdsers: BaseViewController {
    
    @IBOutlet weak var HeadrView: UIView!
    @IBOutlet weak var segmented: UISegmentedControl!
    
    var arrayView = [String]()
    var arrayViewController = [UIViewController]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arrayView = ["في انتظار الدفع","تم الدفع","قيد الشحن","تم الشحن","تم التسليم","ملغي" ]
        
        if MOLHLanguage.isRTLLanguage() {
            arrayView = arrayView.reversed()
            arrayViewController = arrayViewController.reversed()
        }
        setUpSegmentation()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    
}


extension MainOrdsers {
    
    
    func setUpSegmentation (){
        
        if MOLHLanguage.isRTLLanguage(){
            self.segmentedPager.pager.showPage(at:self.arrayView.count - 1, animated: false)
        }
        DispatchQueue.main.async {
            if MOLHLanguage.isRTLLanguage(){
                self.segmentedPager.pager.showPage(at:self.arrayView.count - 1, animated: false)
                self.segmentedPager.segmentedControl.selectedSegmentIndex = self.arrayView.count - 1
                // segmentedPager.pager.reloadData()
            }
        }
        
        segmentedPager.backgroundColor = UIColor.white
        segmentedPager.segmentedControl.backgroundColor = UIColor.white
        segmentedPager.parallaxHeader.view = HeadrView
        segmentedPager.parallaxHeader.mode = .bottom
        segmentedPager.parallaxHeader.height = 100
        segmentedPager.parallaxHeader.minimumHeight = 44
        
        
        // Segmented Control customization
        self.segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocation.down
        segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSAttributedString.Key.foregroundColor: "014A97".color]
        
        segmentedPager.segmentedControl.selectionIndicatorColor = "FF6A2A".color
        segmentedPager.segmentedControl.selectionIndicatorHeight = 3
        
        self.segmentedPager.segmentedControl.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "FFShamelFamilySemiRoundBook-S", size: 17)!, NSAttributedString.Key.foregroundColor :  "014A97".color]
        
        
    }
    
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {
        
        
        switch index {
        case 0:
            return "ملغي".localized
            
        case 1:
            return "تم التسليم".localized
            
        case 2:
            return "تم الشحن".localized
        case 3:
            return "قيد الشحن".localized
        case 4:
            return "تم الدفع".localized

        case 5:
            return "في انتظار الدفع".localized
        default:
            break
        }
        return "تم التسليم".localized
        
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, didScrollWith parallaxHeader: MXParallaxHeader) {
    }
    
    
    override func numberOfPages(in segmentedPager: MXSegmentedPager) -> Int {
        return 6
    }
    
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, viewControllerForPageAt index: Int) -> UIViewController {
        switch index {
        case 5:
            return W0VC()
        case 4:
            return W1VC()
        case 3:
            return W2VC()
        case 2:
            return W3VC()
        case 1:
            return W4VC()
        case 0:
            return W5VC()
        default:
            break
        }
        return W0VC()
        
        
    }
    
    
    fileprivate  func W0VC() ->OrdersVC{
        let vc:OrdersVC = OrdersVC.loadFromNib()
        vc.Link = "status=0"
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func W1VC() ->OrdersVC{
        let vc:OrdersVC = OrdersVC.loadFromNib()
        vc.Link = "status=1"
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
        
    }
    fileprivate  func W2VC() ->OrdersVC{
        let vc:OrdersVC = OrdersVC.loadFromNib()
        vc.Link = "status=2"
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
        
    }
    
    fileprivate  func W3VC() ->OrdersVC{
        let vc:OrdersVC = OrdersVC.loadFromNib()
        vc.Link = "status=3"
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func W4VC() ->OrdersVC{
        let vc:OrdersVC = OrdersVC.loadFromNib()
        vc.Link = "status=4"
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func W5VC() ->OrdersVC{
        let vc:OrdersVC = OrdersVC.loadFromNib()
        vc.Link = "status=5"
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
}
