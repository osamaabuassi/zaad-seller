//
//  MainOrderDeatiles.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/13/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import MXSegmentedPager

class MainOrderDeatiles: BaseViewController {
    
    @IBOutlet weak var HeadrView: UIView!
    
    
    var arrayView = [String]()
    var arrayViewController = [UIViewController]()
    var ID:Int?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arrayView = ["Products","Payment data","Shipping Address","Order data"]
        if MOLHLanguage.isRTLLanguage() {
            arrayView = arrayView.reversed()
            arrayViewController = arrayViewController.reversed()
        }
        setUpSegmentation()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        UserDefaults.standard.removeObject(forKey: "isNotification")
    }
    
   
}
extension MainOrderDeatiles {
    
    func setUpSegmentation (){
        
        if MOLHLanguage.isRTLLanguage(){
            self.segmentedPager.pager.showPage(at:self.arrayView.count - 1, animated: false)
        }
        DispatchQueue.main.async {
            if MOLHLanguage.isRTLLanguage(){
                self.segmentedPager.pager.showPage(at:self.arrayView.count - 1, animated: false)
                self.segmentedPager.segmentedControl.selectedSegmentIndex = self.arrayView.count - 1
                // segmentedPager.pager.reloadData()
            }
        }
        
        segmentedPager.backgroundColor = UIColor.white
        segmentedPager.segmentedControl.backgroundColor = UIColor.white
        segmentedPager.parallaxHeader.view = HeadrView
        segmentedPager.parallaxHeader.mode = .bottom
        segmentedPager.parallaxHeader.height = 50
        segmentedPager.parallaxHeader.minimumHeight = 44
        
        
        // Segmented Control customization
        self.segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocation.down
        segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSAttributedString.Key.foregroundColor: "014A97".color]
        
        segmentedPager.segmentedControl.selectionIndicatorColor = "FF6A2A".color
        segmentedPager.segmentedControl.selectionIndicatorHeight = 3
        
        self.segmentedPager.segmentedControl.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "FFShamelFamilySemiRoundBook-S", size: 17)!, NSAttributedString.Key.foregroundColor :  "014A97".color]
        
        
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {
        
        switch index {
        case 0:
            return "Products".localized
            
        case 1:
            return "Payment data".localized
            
        case 2:
            return "Shipping Address".localized
        case 3:
            return "Order data".localized
        default:
            break
        }
        return "Order data".localized
        
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, didScrollWith parallaxHeader: MXParallaxHeader) {
    }
    
    
    override func numberOfPages(in segmentedPager: MXSegmentedPager) -> Int {
        return 4
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, viewControllerForPageAt index: Int) -> UIViewController {
        switch index {
        case 3:
            return W0VC()
        case 2:
            return W1VC()
        case 1:
            return W2VC()
        case 0:
            return W3VC()
        default:
            break
        }
        return W0VC()

    }
    
    fileprivate  func W0VC() ->OrderDeatilesVC{
        let vc:OrderDeatilesVC = OrderDeatilesVC.loadFromNib()
        vc.isFromDeatileOrder = true
        vc.id = self.ID
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func W1VC() ->OrderDeatilesVC{
        let vc:OrderDeatilesVC = OrderDeatilesVC.loadFromNib()
        vc.isFromShippingOrder = true
        vc.id = self.ID
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func W2VC() ->OrderDeatilesVC{
        let vc:OrderDeatilesVC = OrderDeatilesVC.loadFromNib()
        vc.isFromFinanceOrder = true
        vc.id = self.ID
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    fileprivate  func W3VC() ->ProducatOrderDeatilesVC{
        let vc:ProducatOrderDeatilesVC = ProducatOrderDeatilesVC.loadFromNib()
        vc.id = self.ID
        addChild(vc)
        vc.didMove(toParent: self)
        return vc
    }
    
    
}
