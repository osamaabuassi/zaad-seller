//
//  OrderDeatilesVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/13/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class OrderDeatilesVC: SuperViewController {
    
    @IBOutlet weak var main: UIView!
    @IBOutlet weak var shipping: UIView!
    @IBOutlet weak var finance: UIView!
    
    //Main
    @IBOutlet weak var lblCodeOrder: UILabel!
    @IBOutlet weak var lblDateOrder: UILabel!
    @IBOutlet weak var lblStatusOrder: UILabel!
    @IBOutlet weak var lblTypePayOrder: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    
    //shipping
    @IBOutlet weak var lblnameBuy: UILabel!
    @IBOutlet weak var lbladdress1: UILabel!
    @IBOutlet weak var lbladdress2: UILabel!
    @IBOutlet weak var lbladdress3: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblPostalCode: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    
    //finance
    @IBOutlet weak var lblTotalOrder: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblDiscountShipping: UILabel!
    @IBOutlet weak var lblPay: UILabel!
    @IBOutlet weak var lblcapoun: UILabel!
    @IBOutlet weak var lblValueDiscountcapoun: UILabel!
    @IBOutlet weak var lblValueDiscountoffer: UILabel!
    
    
    
    
    
    var orderDeatiles:OrderDeatilesData?
    var mainData:Main?
    var financeData:Finance?
    var shippingData:Shipping?
    var id:Int?
    var link:String?
    
    var isFromDeatileOrder = false
    var isFromShippingOrder = false
    var isFromFinanceOrder = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getDeatiles()
        if isFromDeatileOrder{
            self.main.isHidden = false
            self.shipping.isHidden = true
            self.finance.isHidden = true
        }
        if isFromShippingOrder{
            self.main.isHidden = true
            self.shipping.isHidden = false
            self.finance.isHidden = true
        }
        if isFromFinanceOrder{
            self.main.isHidden = true
            self.shipping.isHidden = true
            self.finance.isHidden = false
        }

    }
 
    
    func getDeatiles(){
        
        guard Helper.isConnectedToNetwork() else {
                  self.showAlert(title: "".localized, message: "There is no internet connection".localized)
                  return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "orders/\(id ?? 0)", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(OrderDeatilesObject.self, from: response.data!)
                self.orderDeatiles = Status.data!
                self.mainData = self.orderDeatiles?.main
                self.shippingData = self.orderDeatiles?.shipping
                self.financeData = self.orderDeatiles?.finance
                
                //Main
                self.lblCodeOrder.text = self.orderDeatiles?.main?.orderNo ?? ""
                self.lblName.text = self.orderDeatiles?.main?.buyerName ?? ""
                self.lblEmail.text = self.orderDeatiles?.main?.buyerEmail ?? ""
                self.lblDateOrder.text = self.orderDeatiles?.main?.purchaseDate ?? ""
                self.lblStatusOrder.text = self.orderDeatiles?.main?.status?.title ?? ""
                self.lblTypePayOrder.text = self.orderDeatiles?.main?.paymentMethod?.title ?? ""
                
                //shipping
                self.lblnameBuy.text = self.orderDeatiles?.shipping?.name ?? ""
                self.lbladdress1.text = self.orderDeatiles?.shipping?.addressLine1 ?? ""
                self.lbladdress2.text = self.orderDeatiles?.shipping?.addressLine2 ?? ""
                self.lbladdress3.text = self.orderDeatiles?.shipping?.addressLine3 ?? ""
                self.lblCountry.text = self.orderDeatiles?.shipping?.country?.title ?? ""
                self.lblCity.text = self.orderDeatiles?.shipping?.city?.name ?? ""
                self.lblPostalCode.text = self.orderDeatiles?.shipping?.postalCode ?? ""
                self.lblPhone.text = self.orderDeatiles?.shipping?.phone ?? ""
                self.lblMobile.text = self.orderDeatiles?.shipping?.mobile ?? ""
                
                //finance
                self.lblTotalOrder.text = "\(self.orderDeatiles?.finance?.total ?? 0)"
                self.lblDiscount.text = "\(self.orderDeatiles?.finance?.discount ?? 0)"
                self.lblDiscountShipping.text = "\(self.orderDeatiles?.finance?.shippingDiscount ?? 0)"
                self.lblPay.text = "\(self.orderDeatiles?.finance?.net ?? 0)"
                self.lblcapoun.text = self.orderDeatiles?.finance?.coupon?.code ?? ""
                self.lblValueDiscountcapoun.text = self.orderDeatiles?.finance?.coupon?.couponDiscount ?? ""
                self.lblValueDiscountoffer.text = self.orderDeatiles?.finance?.coupon?.orderDiscount ?? ""
                
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            } 
        }
    }
    
    @IBAction func didTab_Print(_ sender: Any) {
        guard Helper.isConnectedToNetwork() else {
                  self.showAlert(title: "".localized, message: "There is no internet connection".localized)
                  return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "orders/\(id ?? 0)/invoice-link", method: HTTPMethod.get).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(LinkObject.self, from: response.data!)
                self.link = Status.data?.link

                if let encodedString = self.link?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
                   let aURL = URL(string: encodedString), UIApplication.shared.canOpenURL(aURL) {
                    UIApplication.shared.open(aURL)
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
    }
    
    
}
