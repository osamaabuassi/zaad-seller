//
//  ProducatOrderDeatilesVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/13/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class ProducatOrderDeatilesVC: SuperViewController {
    
    @IBOutlet weak var tableView: UITableView!


    var orderDeatiles:OrderDeatilesData?
    var orderProductatems = [Items]()
    var id:Int?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        tableView.registerCell(id: "ProducatOrderCell")
    }
    override func viewWillAppear(_ animated: Bool) {
        getDeatiles()
    }
    
    func getDeatiles(){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
         _ = WebRequests.setup(controller: self).prepare(query: "orders/\(id ?? 0)", method: HTTPMethod.get).start(){ (response, error) in
             do {
                 let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                 if Status.status != 200{
                     self.showAlert(title: "error".localized, message:Status.message!)
                     return
                 }
                 
             }catch let jsonErr {
                 print("Error serializing  respone json", jsonErr)
             }
             
             
             do {
                 let Status =  try JSONDecoder().decode(OrderDeatilesObject.self, from: response.data!)
                 self.orderDeatiles = Status.data!
                self.orderProductatems = self.orderDeatiles!.items!
                self.tableView.reloadData()
             } catch let jsonErr {
                 print("Error serializing  respone json", jsonErr)
             }
         }
     }


}

extension ProducatOrderDeatilesVC:UITableViewDataSource,UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderProductatems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProducatOrderCell", for: indexPath) as! ProducatOrderCell

            cell.producat = self.orderProductatems[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 351
    }
    
    
}
