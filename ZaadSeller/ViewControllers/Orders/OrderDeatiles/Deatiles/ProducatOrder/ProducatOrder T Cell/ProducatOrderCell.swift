//
//  ProducatOrderCell.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/13/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class ProducatOrderCell: UITableViewCell {
    
    @IBOutlet weak var lblnameproducat: UILabel!
    @IBOutlet weak var lbldeliveryname: UILabel!
    @IBOutlet weak var lblamount: UILabel!
    @IBOutlet weak var lblprice: UILabel!
    @IBOutlet weak var lblpriceShipping: UILabel!
    @IBOutlet weak var lbltotal: UILabel!


    
    var producat:Items?{
        didSet{
            self.lblnameproducat.text  = producat?.title ?? ""
            self.lbldeliveryname.text  = producat?.shippingService ?? ""
            self.lblamount.text  = "\(producat?.quantity ?? 0)"
            self.lblprice.text  = "\(producat?.unitPrice ?? 0)" + " " + "‏ر.عُ".localized
            self.lblpriceShipping.text  = "\(producat?.shippingPrice ?? 0)" + " " + "‏ر.عُ".localized
            self.lbltotal.text = "\(producat?.total ?? 0)" + " " + "‏ر.عُ".localized
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
