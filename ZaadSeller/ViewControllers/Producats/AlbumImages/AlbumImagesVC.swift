//
//  AlbumImagesVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/9/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import FSPagerView
import SKPhotoBrowser


class AlbumImagesVC: SuperViewController {
    
    var imagesdata:ImagesData?
    var images = [Images]()
    var imagesBrowser = [SKPhoto]()
    var id:Int?
    var IndexImage:Int?
    var imagePicker: ImagePicker!
    var SelectImage = false
    var image:UIImage?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        getImages()
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateImages(notification:)), name: Notification.Name("UpdateImages"), object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setRightButtons([navgtion.Settingsbtn!], sender: self)
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    
    func getImages(){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "error".localized, message: "There is no internet connection".localized)
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "item-images?item_id=\(id ?? 0)", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(ImagesObject.self, from: response.data!)
                self.imagesdata = Status.data!
                self.images = self.imagesdata!.resources!
                self.IndexImage = self.images.compactMap({$0.id}).first
                self.pagerView.reloadData()
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
        }
    }
    
    @objc func UpdateImages(notification: NSNotification)  {
        getImages()
    }
    
    @IBAction func didTab_Delete(_ sender: Any) {
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "item-images/\(IndexImage ?? 0)", method: HTTPMethod.delete).start(){ (response, error) in
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "Error".localized, message:Status.message!)
                    return
                }else if Status.status == 200{
                    NotificationCenter.default.post(name: Notification.Name("UpdateImages"), object: nil)
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
    }
    
    
    @IBOutlet weak var pagerView: FSPagerView!{
        didSet{
            self.pagerView.isScrollEnabled = true
            self.pagerView.register(UINib(nibName: "AlbumCell", bundle: Bundle.main), forCellWithReuseIdentifier: "AlbumCell")
            self.pagerView.itemSize = CGSize(width: UIScreen.main.bounds.width - 100, height: 250)
            self.pagerView.transformer = FSPagerViewTransformer(type: .linear)
        }
    }
    
}

extension AlbumImagesVC:FSPagerViewDelegate,FSPagerViewDataSource{
    
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return images.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "AlbumCell", at: index) as! AlbumCell
        let obj = images[index]
        cell.Albumimg.sd_custom(url: obj.src ?? "")
        return cell
    }
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        let obj = images[index]
        let photo = SKPhoto.photoWithImageURL(obj.src!)
        photo.shouldCachePhotoURLImage = true // you can use image cache by true(NSCache)
        self.imagesBrowser.append(photo)
        let browser = SKPhotoBrowser(photos: self.imagesBrowser)
        browser.initializePageIndex(index)
        SKPhotoBrowserOptions.displayAction = false    // action button will be hidden
        self.present(browser, animated: true, completion: nil)
        self.imagesBrowser.removeAll()

    }
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        let obj = images[targetIndex]
        self.IndexImage = obj.id
    }
    
}


extension AlbumImagesVC: ImagePickerDelegate {
    
    @IBAction func showImagePicker(_ sender: UIButton) {
        self.imagePicker.present(from: sender)
    }
    func didSelect(image: UIImage?) {
        self.image = image
        var parameters: [String: String] = [:]
        parameters["item_id"] = self.id?.description
        
        self.showIndicator()
        let imageData = (image) ?? UIImage()
        
        _ = WebRequests.sendPostMultipartRequestWithImgParam(url: TAConstant.APIBaseURL + "item-images", parameters:parameters ,img: imageData, withName: "src", completion: { (response, error) in
            self.hideIndicator()
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }else{
                    NotificationCenter.default.post(name: Notification.Name("UpdateImages"), object: nil)
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
        }
        )
    }
}
