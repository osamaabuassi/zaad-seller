//
//  ProducatsVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/9/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import ESPullToRefresh

class ProducatsVC: SuperViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblStatus: UILabel!
    
    
    var producatData:ProducatData?
    var producatitems = [Producat]()
    var currentpage = 1
    var status = ["active".localized,"inactive".localized]
    var selectedStatus:Int?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        self.lblStatus.text = "Select status".localized
        tableView.registerCell(id: "ProducatCell")
        getProductas()
        self.tableView.es.addPullToRefresh {
            
            self.currentpage = 1
            self.producatitems.removeAll()
            self.getProductas()
            self.hideIndicator()
        }
        
        self.tableView.es.addInfiniteScrolling {
            self.currentpage += 1
            self.getProductas() // next page
            self.tableView.estimatedRowHeight = UITableView.automaticDimension
            self.hideIndicator()
        }

        NotificationCenter.default.addObserver(self, selector: #selector(UpdateProducat(notification:)), name: Notification.Name("UpdateProducat"), object: nil)
                
    }
    
    
  
    
    
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setRightButtons([navgtion.Settingsbtn!], sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    func getProductas(){
        
        guard Helper.isConnectedToNetwork() else {
            self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
            self.emptyView?.imgEmpty.image = UIImage(named: "refreshment")
            self.emptyView?.firstLabel.text = "There is no internet connection".localized
            return }
        
        var parameters: [String: Any] = [:]
        if selectedStatus != nil{
            parameters["status"] = selectedStatus?.description
        }
        
        _ = WebRequests.setup(controller: self).prepare(query: "items?page=\(currentpage)", method: HTTPMethod.get,parameters: parameters).start(){ (response, error) in
            
            self.tableView.es.stopLoadingMore()
            self.tableView.es.stopPullToRefresh()
            
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(ProducatObject.self, from: response.data!)
                self.producatData = Status.data
                self.producatitems += self.producatData!.items!
                if self.producatData?.nextPageUrl == nil {
                    self.tableView.es.stopLoadingMore()
                    self.tableView.es.noticeNoMoreData()
                }
                if self.producatitems.count == 0{
                    self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                    self.emptyView?.firstLabel.text = "No products to display".localized
                    self.emptyView?.imgEmpty.isHidden = true
                    self.producatitems.removeAll()
                    self.tableView.reloadData()
                    
                }else{
                    self.tableView.tableFooterView = nil
                }
                self.tableView.reloadData()
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
    }
    
    @IBAction func didRefersh(){
        producatitems.removeAll()
        getProductas()
    }
    
    @objc func UpdateProducat(notification: NSNotification)  {
        producatitems.removeAll()
        getProductas()
    }
    
    
    override func didClickRightButton(_sender: UIBarButtonItem) {
        let vc = SettingsControlVC.loadFromNib()
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func didTab_Status(_ sender: UIButton) {
        ActionSheetStringPicker.show(withTitle: "Status".localized, rows: self.status.map { $0 as Any }
            , initialSelection: 0, doneBlock: {
                picker, value, index in
                if let Value = index {
                    self.lblStatus.text = Value as? String
                    self.producatitems.removeAll()
                    if self.lblStatus.text == "active".localized{
                        self.selectedStatus = 1
                    }else{
                        self.selectedStatus = 0
                    }
                    self.getProductas()
                }
                
                return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    
    @IBAction func didTab_Add(_ sender: Any) {
        let vc:AddProducatsVC = AddProducatsVC.loadFromNib()
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    

}


extension ProducatsVC:UITableViewDataSource,UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if producatitems.count != 0{
             return producatitems.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProducatCell", for: indexPath) as! ProducatCell
        let obj = producatitems[indexPath.row]
        cell.producatitems = producatitems[indexPath.row]
        cell.EditeTapped = { [weak self] in
            let vc:AddProducatsVC = AddProducatsVC.loadFromNib()
            vc.id = self!.producatitems[indexPath.row].id
            vc.hidesBottomBarWhenPushed = true
            vc.isEdited = true
            vc.modalPresentationStyle = .fullScreen
            self!.navigationController?.pushViewController(vc, animated: true)
        }
        cell.AlbumTapped = { [weak self] in
            let vc:AlbumImagesVC = AlbumImagesVC.loadFromNib()
            vc.id = self!.producatitems[indexPath.row].id
            vc.hidesBottomBarWhenPushed = true
            vc.modalPresentationStyle = .fullScreen
            self!.navigationController?.pushViewController(vc, animated: true)
        }
        cell.DeleteTapped = { [weak self] in
            let vc:DeleteAlert = DeleteAlert.loadFromNib()
            vc.id = self!.producatitems[indexPath.row].id
            vc.hidesBottomBarWhenPushed = true
            vc.isFromProducats = true
            vc.modalPresentationStyle = .overCurrentContext
            self!.present(vc, animated: false, completion: nil)
        }
        cell.statusShowTapped = { [weak self] in
            let id = obj.id
            var parameters: [String: Any] = [:]
            if obj.published?.id == 1{
                parameters["published"] = "0"
            }else{
                parameters["published"] = "1"
            }
            
            guard Helper.isConnectedToNetwork() else {
                self?.showAlert(title: "".localized, message: "There is no internet connection".localized)
                return }
            
            _ = WebRequests.setup(controller: self).prepare(query: "items/\(id ?? 0)/publishing", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self?.showAlert(title: "Error".localized, message:Status.message!)
                        return
                    }else if Status.status == 200{
                        NotificationCenter.default.post(name: Notification.Name("UpdateProducat"), object: nil)
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    
    
}
