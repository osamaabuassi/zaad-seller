//
//  AddProducatsVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/9/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class AddProducatsVC: SuperViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfShortDescrbtion: UITextField!
    @IBOutlet weak var tfTypeActivity: UITextField!
    @IBOutlet weak var tfPrice: UITextField!
    @IBOutlet weak var tfAmount: UITextField!
    @IBOutlet weak var tfWeight: UITextField!
    @IBOutlet weak var tfbrand: UITextField!
    @IBOutlet weak var tfdescription: UITextView!
    @IBOutlet weak var imgProducat: UIImageView!
    @IBOutlet weak var SwitchStatus: UISwitch!
    @IBOutlet weak var llbStatus: UILabel!
    
    
    var imagePicker: ImagePicker!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.status = "0"
        self.llbStatus.text = "inactive".localized
        self.SwitchStatus.setOn(false, animated: true)
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        getProduct()
        getActivity()
        
    }
    
    var id:Int?
    var isEdited = false
    var producatDeatiles:ProducatInfo?
    var status:String?
    var Activity = [Category]()
    var ActivityData:CategroiesData?
    var selectedActivityID:Int?
    var selectedimage = false
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setRightButtons([navgtion.Settingsbtn!], sender: self)
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    func getActivity(){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "categories/\(1)", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(CategroiesObject.self, from: response.data!)
                self.ActivityData = Status.data!
                self.Activity = self.ActivityData!.resources!
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
        
    }
    
    
    @IBAction func didTabActivity(_ sender: UIButton) {
        ActionSheetStringPicker.show(withTitle: "type of activity".localized, rows: self.Activity.map { $0.title as Any }
            , initialSelection: 0, doneBlock: {
                picker, value, index in
                if let Value = index {
                    self.tfTypeActivity.text = Value as? String
                    self.selectedActivityID = self.Activity[value].id ?? 0
                }
                return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    
    func getProduct(){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        if isEdited{
            self.lblTitle.text = "Edit Product".localized
            _ = WebRequests.setup(controller: self).prepare(query: "items/\(id ?? 0)", method: HTTPMethod.get).start(){ (response, error) in
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200{
                        self.showAlert(title: "error".localized, message:Status.message!)
                        return
                    }
                    
                }catch let jsonErr {
                    print("Error serializing respone json", jsonErr)
                }
                
                
                do {
                    let Status =  try JSONDecoder().decode(ProductaDeatilesObject.self, from: response.data!)
                    self.producatDeatiles = Status.data!
                    self.tfName.text = self.producatDeatiles?.title ?? ""
                    self.tfShortDescrbtion.text = self.producatDeatiles?.shortDescription ?? ""
                    self.tfTypeActivity.text = self.producatDeatiles?.category?.title ?? ""
                    self.tfPrice.text = "\(self.producatDeatiles?.regularPrice ?? 0)"
                    self.tfAmount.text = "\(self.producatDeatiles?.quantity ?? 0)"
                    self.tfWeight.text = "\(self.producatDeatiles?.weight ?? 0)"
                    self.tfbrand.text = "\(self.producatDeatiles?.brand ?? "")"
                    self.tfdescription.text = "\(self.producatDeatiles?.descriptionValue?.htmlToString ?? "")"
                    if self.producatDeatiles?.image != nil{
                        self.imgProducat.sd_custom(url: self.producatDeatiles?.image ?? "")
                        self.selectedimage = true
                    }else{
                        self.selectedimage = false
                    }
                    self.selectedActivityID = self.producatDeatiles?.category?.id
                    if self.producatDeatiles?.status?.title == "active".localized{
                        self.SwitchStatus.setOn(true, animated: true)
                        self.llbStatus.text = "active".localized
                        self.status = "1"
                    }else{
                        self.SwitchStatus.setOn(false, animated: true)
                        self.llbStatus.text = "inactive".localized
                        self.status = "0"
                    }
                } catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
            }
        }
    }
    
    
    @IBAction func didTab_Save(_ sender: Any) {
        
        guard let nameproducat = self.tfName.text, !nameproducat.isEmpty else{
            self.showAlert(title: "error".localized, message: "Name cannot be empty".localized)
            return
        }
        
        guard let smallDiscrbtionproducat = self.tfShortDescrbtion.text, !smallDiscrbtionproducat.isEmpty else{
            self.showAlert(title: "error".localized, message: "A short description cannot be empty".localized)
            return
        }
        guard let price = self.tfPrice.text, !price.isEmpty else{
            self.showAlert(title: "error".localized, message: "Price cannot be empty".localized)
            return
        }
        
        guard let amount = self.tfAmount.text, !amount.isEmpty else{
            self.showAlert(title: "error".localized, message: "Quantity cannot be empty".localized)
            return
        }
        guard let weight = self.tfWeight.text, !weight.isEmpty else{
            self.showAlert(title: "error".localized, message: "Weight cannot be empty".localized)
            return
        }
        
//        guard , !brand.isEmpty else{
//            self.showAlert(title: "error".localized, message: "لا يمكن أن يكون العلامة التجارية  فارغًا".localized)
//            return
//        }
        
        guard let Discrbtionproduca = self.tfdescription.text, !Discrbtionproduca.isEmpty else{
            self.showAlert(title: "error".localized, message: "Description cannot be empty".localized)
            return
        }
        guard  self.selectedActivityID != nil else{
            self.showAlert(title: "error".localized, message: "Activity cannot be empty".localized)
            return
        }
        
        var parameters: [String: Any] = [:]
        
        parameters["title"] = self.tfName.text ?? ""
        parameters["short_description"] = self.tfShortDescrbtion.text ?? ""
        parameters["description"] = self.tfdescription.text ?? ""
        parameters["regular_price"] = self.tfPrice.text ?? ""
        parameters["category_id"] = self.selectedActivityID?.description
        parameters["brand"] = self.tfbrand.text ?? ""
        parameters["quantity"] = self.tfAmount.text ?? ""
        parameters["weight"] = self.tfWeight.text ?? ""
        parameters["status"] = self.status ?? ""
        
        if self.selectedimage {
            
        if isEdited{
            self.showIndicator()
            let imageData = (imgProducat.image) ?? UIImage()
            
            _ = WebRequests.sendPostMultipartRequestWithImgParam(url: TAConstant.APIBaseURL + "items/\(id ?? 0)", parameters:parameters as! [String : String] ,img: imageData, withName: "image", completion: { (response, error) in
                self.hideIndicator()
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self.showAlert(title: "error".localized, message:Status.message!)
                        return
                    }else{
                        self.navigationController?.popViewController(animated: true)
                        NotificationCenter.default.post(name: Notification.Name("UpdateProducat"), object: nil)
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
            }
            )
        }else{
            self.showIndicator()
            let imageData = (imgProducat.image) ?? UIImage()
            
            _ = WebRequests.sendPostMultipartRequestWithImgParam(url: TAConstant.APIBaseURL + "items", parameters:parameters as! [String : String] ,img: imageData, withName: "image", completion: { (response, error) in
                self.hideIndicator()
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self.showAlert(title: "error".localized, message:Status.message!)
                        return
                    }else{
                        self.navigationController?.popViewController(animated: true)
                        NotificationCenter.default.post(name: Notification.Name("UpdateProducat"), object: nil)
                    }
                    
                }catch let jsonErr {
                    print("Error serializing respone json", jsonErr)
                }
            }
            )
        }
              }
    }
    @IBAction func didTab_Status(_ sender: UISwitch) {
          if sender.isOn{
              self.status = "1"
              self.llbStatus.text = "active".localized
          }else{
              self.status = "0"
              self.llbStatus.text = "inactive".localized
          }
          
      }
    
}



extension AddProducatsVC: ImagePickerDelegate {
    
    @IBAction func showImagePicker(_ sender: UIButton) {
        self.imagePicker.present(from: sender)
    }
    func didSelect(image: UIImage?) {
        self.imgProducat.image = image
        self.selectedimage = true
    }
}
