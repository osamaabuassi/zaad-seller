//
//  ProducatCell.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/9/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class ProducatCell: UITableViewCell {
    
    @IBOutlet weak var imgProducat: UIImageView!
    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var lbltype: UILabel!
    @IBOutlet weak var lblprice: UILabel!
    @IBOutlet weak var lbldate: UILabel!
    @IBOutlet weak var lblstatus: UILabel!
    @IBOutlet weak var btEdite: UIButton!
    @IBOutlet weak var btDelete: UIButton!
    @IBOutlet weak var btAlbum: UIButton!
    @IBOutlet weak var ViewShow: UIStackView!
    @IBOutlet weak var btstatusShow: UISwitch!
    @IBOutlet weak var lblstatusShow: UILabel!
    @IBOutlet weak var lblShowImag: UIView!
    @IBOutlet weak var Detailes: UIView!
    
    @IBOutlet weak var vprice: UIView!
    
    var EditeTapped: (() -> ())?
    var DeleteTapped: (() -> ())?
    var AlbumTapped: (() -> ())?
    var statusShowTapped: (() -> ())?
    var DetailesShowTapped: (() -> ())?
    
    var producatitems:Producat?{
        didSet{
            lblShowImag.isHidden = false
            Detailes.isHidden = true
            vprice.isHidden = false
            self.lblname.text = producatitems?.title ?? ""
            self.lbltype.text  = producatitems?.category?.title ?? ""
            self.lblprice.text  = "\(producatitems?.regularPrice ?? 0)" + " " + "SAR".localized
            self.lbldate.text = producatitems?.createdAt ?? ""
            self.imgProducat.sd_custom(url: producatitems?.image ?? "")
            
            if producatitems?.status?.title == "active".localized{
                self.lblstatus.text = "active".localized
                self.lblstatus.textColor = "AEAC09".color
                self.ViewShow.isHidden = false
            }else{
                self.lblstatus.text = "inactive".localized
                self.lblstatus.textColor = "F85223".color
                self.ViewShow.isHidden = true
            }
            if producatitems?.published?.id == 1{
                self.btstatusShow.setOn(true, animated: true)
                self.lblstatusShow.text = producatitems?.published?.title ?? ""
            }else{
                self.btstatusShow.setOn(false, animated: true)
                self.lblstatusShow.text = producatitems?.published?.title ?? ""
            }
        }
            }
    
    var servicestitems:ItemsServicesView?{
        didSet{
            lblShowImag.isHidden = true
            Detailes.isHidden = false
            vprice.isHidden = true
            self.lblname.text = servicestitems?.title ?? ""
            self.lbltype.text  = servicestitems?.categoryTitle ?? ""
            self.imgProducat.sd_custom(url: servicestitems?.image ?? "")
         //   self.lblprice.text = servicestitems?.categoryTitle
            if servicestitems?.statusCode == 1 {
                self.lblstatus.text = "active".localized
                self.lblstatus.textColor = "AEAC09".color
                self.ViewShow.isHidden = false
            }else{
                self.lblstatus.text = "inactive".localized
                self.lblstatus.textColor = "F85223".color
                self.ViewShow.isHidden = true
            }
            if servicestitems?.publishedCode == 1{
                self.btstatusShow.setOn(true, animated: true)
                self.lblstatusShow.text = servicestitems?.published ?? ""
            }else{
                self.btstatusShow.setOn(false, animated: true)
                self.lblstatusShow.text = servicestitems?.published ?? ""
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    
    @IBAction func Edite(_ sender: UIButton) {
        EditeTapped?()
    }
    
    @IBAction func Delete(_ sender: UIButton) {
        DeleteTapped?()
    }
    
    
    @IBAction func Album(_ sender: UIButton) {
        AlbumTapped?()
    }
    
    @IBAction func statusShow(_ sender: UISwitch) {
        statusShowTapped?()
    }
    @IBAction func detailesShow(_ sender: UISwitch) {
           DetailesShowTapped?()
       }
    
    
}
