//
//  CitysVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 10/6/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

protocol cityDelgate : class {
    func SelectedDone(name:String,id:Int)
}

class CitysVC: SuperViewController {
    
    @IBOutlet weak var tableView: UITableView!

    
    var citydelegte:cityDelgate?
    var cityData:CityData?
    var city = [City]()
    var currentpage = 1
    var Countryid:Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        tableView.registerCell(id: "CityCell")
        getCity()
        self.tableView.es.addPullToRefresh {
            self.currentpage = 1
            self.city.removeAll()
            self.getCity()
            self.hideIndicator()
        }
        
        self.tableView.es.addInfiniteScrolling {
            self.currentpage += 1
            self.getCity() // next page
            self.tableView.estimatedRowHeight = UITableView.automaticDimension
            self.hideIndicator()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }

    func getCity(){
        
        guard Helper.isConnectedToNetwork() else {
            self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
            self.emptyView?.imgEmpty.image = UIImage(named: "refreshment")
            self.emptyView?.firstLabel.text = "There is no internet connection".localized
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "cities?page=\(currentpage)&country_id=\(Countryid ?? 0)", method: HTTPMethod.get).start(){ (response, error) in
            
            self.tableView.es.stopLoadingMore()
            self.tableView.es.stopPullToRefresh()
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            do {
                let Status =  try JSONDecoder().decode(CityObject.self, from: response.data!)
                self.cityData = Status.data!
                self.city += self.cityData!.resources!
                if self.cityData?.nextPageUrl == nil {
                    self.tableView.es.stopLoadingMore()
                    self.tableView.es.noticeNoMoreData()
                }
                if self.city.count == 0{
                    self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                    self.emptyView?.firstLabel.text = "There are no cities to show".localized
                    self.emptyView?.imgEmpty.isHidden = true
                    self.city.removeAll()
                    self.tableView.reloadData()
                    
                }else{
                    self.tableView.tableFooterView = nil
                }
                self.tableView.reloadData()
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
    }
    
    @objc func didRefersh(sender: UIButton) {
        city.removeAll()
        getCity()
    }
}

extension CitysVC:UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return city.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let obj = city[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "CityCell", for: indexPath) as! CityCell
        cell.lblname.text = obj.name ?? ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = city[indexPath.row]
        CATransaction.begin()
        self.navigationController?.popViewController(animated: true)
        CATransaction.setCompletionBlock({ [weak self] in
            self?.citydelegte?.SelectedDone(name: obj.name ?? "", id: obj.id ?? 0)
        })
        CATransaction.commit()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
        
    }
}


