//
//  ViewController.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/3/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class StartVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startVC()
    }
    
    func startVC(){
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if !UserDefaults.standard.bool(forKey: "didSee") {
                UserDefaults.standard.set(true, forKey: "didSee")
                let vc:OnBordingVC = OnBordingVC.loadFromNib()
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
                
            }else{
                if CurrentUser.userInfo != nil{
                    let vc:TTabBarViewController = AppDelegate.sb_main.instanceVC()
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                }else{
                    let mainVC = LoginVC()
                    UIApplication.shared.keyWindow?.rootViewController = mainVC.navigationController()
                }
            }
        }
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
           return .lightContent
       }
    
    
}


