//
//  ServicesVC.swift
//  ZaadSeller
//
//  Created by osamaaassi on 1/23/21.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import ESPullToRefresh

class ServicesVC: SuperViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var addBut: UILabel!
    @IBOutlet weak var statustf: UILabel!
    
    
    var servicesData:DataServicesView?
    var servicesitems = [ItemsServicesView]()
    var currentpage = 1
    
    var status = ["all status".localized,"active".localized,"inactive".localized]
    var selectedStatus:Int?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.statustf.text = "Status".localized
        // self.addBut.text = "Add".localized
        
        self.tableView.tableFooterView = UIView()
        tableView.registerCell(id: "ProducatCell")
        self.getServices()
        self.lblStatus.text = "Select status".localized
        
        self.tableView.es.addPullToRefresh {
            
            self.currentpage = 1
            self.servicesitems.removeAll()
            self.getServices()
            self.hideIndicator()
        }
        
        self.tableView.es.addInfiniteScrolling {
            self.currentpage += 1
            self.getServices() // next page
            self.tableView.estimatedRowHeight = UITableView.automaticDimension
            self.hideIndicator()
        }
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateServices(notification:)), name: Notification.Name("UpdateServices"), object: nil)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setRightButtons([navgtion.Settingsbtn!], sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    
    override func didClickRightButton(_sender: UIBarButtonItem) {
        let vc = SettingsControlVC.loadFromNib()
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
}

extension ServicesVC {
    
    func getServices(){
        
        guard Helper.isConnectedToNetwork() else {
            self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
            self.emptyView?.imgEmpty.image = UIImage(named: "refreshment")
            self.emptyView?.firstLabel.text = "There is no internet connection".localized
            return }
        
        var parameters: [String: Any] = [:]
        if selectedStatus != nil{
            parameters["status"] = selectedStatus?.description
        }
        
        _ = WebRequests.setup(controller: self).prepare(query: "services?page=\(currentpage)", method:
                                                            HTTPMethod.get,parameters: parameters).start(){ (response, error) in
            
            self.tableView.es.stopLoadingMore()
            self.tableView.es.stopPullToRefresh()
            
            
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
            
            
            do {
                let Status =  try JSONDecoder().decode(ServicesView.self, from: response.data!)
                self.servicesData = Status.data
                self.servicesitems += self.servicesData!.items!
                
                if self.servicesData?.nextPageUrl == nil {
                    self.tableView.es.stopLoadingMore()
                    self.tableView.es.noticeNoMoreData()
                }
                if self.servicesitems.count == 0{
                    self.emptyView = self.showEmptyView(emptyView: self.emptyView, parentView: self.tableView, refershSelector: #selector(self.didRefersh))
                    self.emptyView?.firstLabel.text = "No services to display".localized
                    self.emptyView?.imgEmpty.isHidden = true
                    self.servicesitems.removeAll()
                    self.tableView.reloadData()
                    
                }else{
                    self.tableView.tableFooterView = nil
                }
                self.tableView.reloadData()
                
            } catch let jsonErr {
                print("Error serializing respone json", jsonErr)
            }
        }
    }
    
    @IBAction func didRefersh(){
        servicesitems.removeAll()
        getServices()
    }
    
    @objc func UpdateServices(notification: NSNotification)  {
        servicesitems.removeAll()
        getServices()
    }
    
    @IBAction func didTab_Status(_ sender: UIButton) {
        ActionSheetStringPicker.show(withTitle: "Status".localized, rows: self.status.map { $0 as Any }
                                     , initialSelection: 0, doneBlock: {
            picker, value, index in
            if let Value = index {
                self.lblStatus.text = Value as? String
                self.servicesitems.removeAll()
                if self.lblStatus.text == "active".localized{
                    self.selectedStatus = 1
                }else if  self.lblStatus.text == "inactive".localized{
                    self.selectedStatus = 0
                }
                else {
                    self.selectedStatus = nil
                }
                
                self.getServices()
            }
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    @IBAction func didTab_Add(_ sender: Any) {
        let vc:AddServicesVC = AddServicesVC.loadFromNib()
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}



extension ServicesVC :UITableViewDataSource,UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if servicesitems.count != 0{
            return servicesitems.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProducatCell", for: indexPath) as! ProducatCell
        let obj = servicesitems[indexPath.row]
        cell.servicestitems = servicesitems[indexPath.row]
        
        //        cell.EditeTapped = { [weak self] in
        //            let vc:AddProducatsVC = AddProducatsVC.loadFromNib()
        //            vc.id = self!.servicesitems[indexPath.row].id
        //            vc.hidesBottomBarWhenPushed = true
        //            vc.isEdited = true
        //            vc.modalPresentationStyle = .fullScreen
        //            self!.navigationController?.pushViewController(vc, animated: true)
        //        }
        //
        //        cell.AlbumTapped = { [weak self] in
        //            let vc:AlbumImagesVC = AlbumImagesVC.loadFromNib()
        //            vc.id = self!.servicesitems[indexPath.row].id
        //            vc.hidesBottomBarWhenPushed = true
        //            vc.modalPresentationStyle = .fullScreen
        //            self!.navigationController?.pushViewController(vc, animated: true)
        //        }
        
        cell.DetailesShowTapped = { [weak self] in
            let vc:DetailsServiceVC = DetailsServiceVC.loadFromNib()
            vc.id = self!.servicesitems[indexPath.row].id
            vc.isFromDetails = true
            vc.hidesBottomBarWhenPushed = true
            vc.modalPresentationStyle = .fullScreen
            self!.navigationController?.pushViewController(vc, animated: true)
        }
        
        cell.EditeTapped = { [weak self] in
            let vc:DetailsServiceVC = DetailsServiceVC.loadFromNib()
            vc.id = self!.servicesitems[indexPath.row].id
            vc.isFromEdit = true
            vc.hidesBottomBarWhenPushed = true
            vc.modalPresentationStyle = .fullScreen
            self!.navigationController?.pushViewController(vc, animated: true)
            
        }
        cell.DeleteTapped = { [weak self] in
            let vc:DeleteAlert = DeleteAlert.loadFromNib()
            vc.id = self!.servicesitems[indexPath.row].id
            
            vc.hidesBottomBarWhenPushed = true
            vc.isFromService = true
            vc.modalPresentationStyle = .overCurrentContext
            self!.present(vc, animated: false, completion: nil)
        }
        cell.statusShowTapped = { [weak self] in
            let id = obj.id
            var parameters: [String: Any] = [:]
            if obj.publishedCode == 1{
                parameters["published"] = "0"
            }else{
                parameters["published"] = "1"
            }
            
            guard Helper.isConnectedToNetwork() else {
                self?.showAlert(title: "".localized, message: "There is no internet connection".localized)
                return }
            
            _ = WebRequests.setup(controller: self).prepare(query: "services/\(id ?? 0)/publishing", method: HTTPMethod.post, parameters: parameters).start(){ (response, error) in
                
                do {
                    let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                    if Status.status != 200 {
                        
                        self?.showAlert(title: "Error".localized, message:Status.message!)
                        return
                    }else if Status.status == 200{
                        NotificationCenter.default.post(name: Notification.Name("UpdateServices"), object: nil)
                    }
                    
                }catch let jsonErr {
                    print("Error serializing  respone json", jsonErr)
                }
                
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    
    
}
