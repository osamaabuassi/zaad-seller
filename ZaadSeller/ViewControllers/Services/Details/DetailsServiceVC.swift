//
//  DetailsServiceVC.swift
//  ZaadSeller
//
//  Created by osamaaassi on 1/24/21.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class DetailsServiceVC: SuperViewController, ImagePickerDelegate {
    
    
    @IBOutlet weak var tableViewServices: UITableView!
    @IBOutlet weak var heighttableViewServices: NSLayoutConstraint!
    @IBOutlet weak var lblTitle: UITextField!
    @IBOutlet weak var tfShortTitle: UITextField!
    @IBOutlet weak var tfshortDescription: UITextField!
    @IBOutlet weak var imgServicer: UIImageView!
    @IBOutlet weak var tfDescription: UITextView!
    
    //Prices
    @IBOutlet weak var tfPriceType: UITextField!
    @IBOutlet weak var tfPriceHint: UITextField!
    @IBOutlet weak var tfMaxPrice: UITextField!
    @IBOutlet weak var tfMinPrice: UITextField!
    @IBOutlet weak var vMaxPrice: UIView!
    @IBOutlet weak var vMinPrice: UIView!
    @IBOutlet weak var vAllPrice: UIView!
    @IBOutlet weak var lbMPrice: UILabel!
    @IBOutlet weak var lbMinPrice: UILabel!
    
    
     @IBOutlet weak var tfCategoryId: UITextField!
     @IBOutlet weak var tfTypeCategory: UITextField!
    
    
    //Edit
    @IBOutlet weak var vSava: UIView!
    @IBOutlet weak var vfutcher: UIView!
    @IBOutlet weak var vtypeImg: UIView!
    @IBOutlet weak var vTypeService: UIView!
    @IBOutlet weak var vCatogreService: UIView!
    
    var Activity = [Category]()
    var ActivityData:CategroiesData?
    var selectedTypeActivityID:Int?
        
    //children
    var childre = [Children]()
    var selectedChildrenID:Int?
    
    var pricingItems = [PricingItems]()
    var pricingData : PricingData?
    var selectedpricingID:Int?
    
    var id: Int!
    var detailsS:DetailsS?
    var features = [Features]()
    var featuress = Features?.self
    
    var servicePrices = [ServicePrices]()
    var servicePricess = ServicePrices?.self
    var idPrices:Int?
    
    var isFromEdit = false
    var isFromDetails = false
    
    var jsonPriecingAll: [[String: Any]] = []
    var jsonServicesAll: [[String: Any]] = []
    //Image
    var imagePicker: ImagePicker!
    var selectImage:Bool? = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getPricingTypes()
        getActivity(2)
       
    
        
        if isFromEdit{
            vSava.isHidden = false
            vtypeImg.isHidden = false
            vCatogreService.isHidden = false
            vTypeService.isHidden = false
        }
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        tableViewServices.registerCell(id: "AddFeaturesTV")
        tableViewServices.tableFooterView = UIView.init(frame: .zero)
        
        vAllPrice.isHidden = true
        vfutcher.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {

        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    
    @IBAction func didTab_Activity(_ sender: UIButton) {
        guard isFromEdit else {
            return
        }
            self.tfTypeCategory.text = ""
           
           ActionSheetStringPicker.show(withTitle: self.tfCategoryId.text, rows: self.Activity.map { $0.title as Any }
               , initialSelection: 0, doneBlock: {
                   picker, value, index in
                   if let Value = index {
                       self.tfCategoryId.text = Value as? String
                       self.selectedTypeActivityID = self.Activity[value].id ?? 0
                    
                       if let object = self.Activity.filter({ $0.id == self.selectedTypeActivityID }).first{
                        if object.children != nil{
                           self.childre = object.children!
                          self.vTypeService.isHidden = false
                        }
                       }
                   }
                   return
           }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    
  
    @IBAction func didTab_Services(_ sender: UIButton) {
        guard isFromEdit else {
            return
        }
        guard (selectedTypeActivityID != nil ) else {
            self.showAlert(title: "error".localized, message: "Service rating must be specified.".localized)
            
            return
        }
        guard (self.childre.count != 0) else {
            self.showAlert(title: "error".localized, message: "There are no types of service.".localized)
            
            return
        }
        
        ActionSheetStringPicker.show(withTitle: self.tfTypeCategory.text, rows: self.childre.map { $0.title as Any }
            , initialSelection: 0, doneBlock: {
                picker, value, index in
                if let Value = index {
                    self.tfTypeCategory.text = Value as? String
                    self.selectedChildrenID = self.childre[value].id ?? 0
                      }
                return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    
    
    }
}

extension DetailsServiceVC {
    
    func getDetails() {
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "services/\(id ?? 0)", method: HTTPMethod.get).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
                
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(ServiceDetails.self, from: response.data!)
                
                self.detailsS = Status.data
                self.features = self.detailsS!.features ?? []
                self.servicePrices = Status.data!.prices ?? []
               
                
                self.lblTitle.text = self.detailsS?.title ?? ""
                self.tfShortTitle.text = self.detailsS?.shortTitle ?? ""
                self.tfshortDescription.text = self.detailsS?.shortDescription ?? ""
                self.tfDescription.text = self.detailsS?.descriptionValue ?? ""
                self.selectedpricingID =  Int(self.detailsS!.priceType!)
                self.selectedChildrenID =  Int(self.detailsS!.categoryId!)
              
                
                
                let obj = self.Activity.first(where: {$0.children?.first(where: {$0.id == self.detailsS?.categoryId}) != nil})
                self.tfCategoryId.text =  obj?.title

                
                self.tfTypeCategory.text = obj?.children?.first(where: {$0.id == self.detailsS?.categoryId})?.title

                
                if self.servicePrices.count != 0 {
                if let object = self.servicePrices.filter({ $0.id != nil }).first{
                    self.idPrices =  object.id!
                    
                }
                }
                
               if let object = self.childre.filter({ $0.id == self.detailsS?.categoryId }).first{
                                self.tfCategoryId.text =  object.title
                }
                

                
                if let object = self.pricingItems.filter({ $0.id == self.selectedpricingID }).first{
                    self.tfPriceType.text =  object.name
                }
                
                if Int(self.detailsS!.priceType!) == 1 {
                    self.tfMinPrice.text =  self.servicePrices.map({$0.minQuantity!.description as String}).first
                    self.vMaxPrice.isHidden = true
                    self.vAllPrice.isHidden = false
                    self.lbMPrice.isHidden = true
                    self.lbMinPrice.isHidden = false
                    
                }else if Int(self.detailsS!.priceType!) == 2 {
                    
                    self.tfMinPrice.text =  self.servicePrices.map({$0.minQuantity!.description as String}).first
                    self.tfMaxPrice.text = self.servicePrices.map({$0.maxQuantity!.description as String}).first
                    
                    self.vMaxPrice.isHidden = false
                    self.vMinPrice.isHidden = false
                    self.vAllPrice.isHidden = false
                    self.lbMPrice.isHidden = false
                    self.lbMinPrice.isHidden = false
                }else{
                    self.vAllPrice.isHidden = true
                }
                
                self.tfPriceHint.text = self.detailsS?.priceHint ?? ""
                self.imgServicer.sd_custom(url: self.detailsS?.image ?? "")
                
                self.tableViewServices.reloadData()
                self.heighttableViewServices.constant = self.tableViewServices.contentSize.height
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
    }
}


extension DetailsServiceVC{
    
    @IBAction func didTab_PriceingType(_ sender: UIButton) {
        guard isFromEdit  else {
          return
        }
        ActionSheetStringPicker.show(withTitle: self.tfPriceType.text, rows: self.pricingItems.map { $0.name as Any }
            , initialSelection: 0, doneBlock: {
                picker, value, index in
                if let Value = index {
                    self.tfPriceType.text = Value as? String
                    self.selectedpricingID = self.pricingItems[value].id ?? 0
                    
                    if self.selectedpricingID == 2 {
                        self.vMaxPrice.isHidden = false
                        self.vMinPrice.isHidden = false
                        self.vAllPrice.isHidden = false
                        self.lbMPrice.isHidden = false
                        self.lbMinPrice.isHidden = false
                        self.tfMaxPrice.text = ""
                        self.tfMinPrice.text = ""
                        
                    }else if self.selectedpricingID == 1 {
                        self.vMinPrice.isHidden = false
                        self.vMaxPrice.isHidden = true
                        self.vAllPrice.isHidden = false
                        self.lbMPrice.isHidden = true
                        self.lbMinPrice.isHidden = false
                        self.tfMaxPrice.text = ""
                        self.tfMinPrice.text = ""
                    }
                    else if self.selectedpricingID == 3 {
                        self.vMaxPrice.isHidden = true
                        self.vMinPrice.isHidden = true
                        self.vAllPrice.isHidden = true
                        self.lbMPrice.isHidden = true
                        self.lbMinPrice.isHidden = true
                        self.tfMaxPrice.text = ""
                        self.tfMinPrice.text = ""
                    }
                }
                return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    func getPricingTypes(){
       
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "services/pricing-types", method: HTTPMethod.get,isAuthRequired: false).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(PricingTypes.self, from: response.data!)
                self.pricingData = Status.data!
                self.pricingItems = self.pricingData!.items!
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        
    }
    
    
}
extension DetailsServiceVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if features.count != 0 {
            vfutcher.isHidden = false
            return features.count
        }else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableViewServices.dequeueReusableCell(withIdentifier: "AddFeaturesTV", for: indexPath) as! AddFeaturesTV
        let features = self.features[indexPath.row]
        cell.tfFeaturesDescription.text  = features.descriptionValue ?? ""
        cell.tfRegularThreshold.text  = features.valueThreshold?.description ?? ""
        cell.featuresRegularPrice.text = features.regularPrice?.description ?? ""
        
        if (features.free  == 1){
            cell.tfFree.text  = "Yes".localized
        }else{
            cell.tfFree.text  = "No".localized
        }
        if (features.optional  == 1){
            cell.tfoptional.text  = "Yes".localized
        }else{
            cell.tfoptional.text  = "No".localized
        }
        cell.vDelete.isHidden = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.heighttableViewServices.constant = self.tableViewServices.contentSize.height }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 556
    }
    
}


extension DetailsServiceVC {
    
    @IBAction func editServices (_ sender: UIButton) {
       
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        guard let title = self.lblTitle.text, !title.isEmpty else{
            self.showAlert(title: "error".localized, message: "The service name must be entered".localized)
            return }
        guard let description = self.tfDescription.text, !description.isEmpty else{
            self.showAlert(title: "error".localized, message: "Service description must be entered.".localized)
            return
        }
        
        guard let priceType = self.selectedpricingID?.description, priceType != "0" else{
            self.showAlert(title: "error".localized, message: "Pricing method must be determined".localized)
            return
        }
        
        
        if priceType == "1" {
            guard let minPrice = self.tfMinPrice.text, !minPrice.isEmpty else{
                self.showAlert(title: "error".localized, message: "You must enter the lowest price".localized)
                return
            }
            jsonPriecingAll.removeAll()
            let jsonAddPrice = ["id": idPrices ?? "","description":"","min_quantity": minPrice ,"max_quantity":"", "regular_price":0] as [String : Any]
            jsonPriecingAll.append(jsonAddPrice)
            
        }
        else if priceType == "2"{
            guard let maxPrice = self.tfMaxPrice.text, !maxPrice.isEmpty else{
                self.showAlert(title: "error".localized, message: "You must enter the highest price".localized)
                return
            }
            guard let minPrice = self.tfMinPrice.text, !minPrice.isEmpty else{
                self.showAlert(title: "error".localized, message: "You must enter the lowest price".localized)
                return
            }
            jsonPriecingAll.removeAll()
            let jsonAddPrice = ["id": idPrices ?? "","description":"","min_quantity": minPrice ,"max_quantity":maxPrice, "regular_price":0] as [String : Any]
            jsonPriecingAll.append(jsonAddPrice)
        }
        else if priceType == "3"{
            let jsonAddPrice = ["id": idPrices ?? "" ,"description": "","min_quantity":"" ,"max_quantity": "", "regular_price":0] as [String : Any]
            jsonPriecingAll.removeAll()
            jsonPriecingAll.append(jsonAddPrice)
        }
        
        var parameters: [String: Any] = [:]
        parameters["title"]  = title
        parameters["description"]  = description
        parameters["short_description"]  = self.tfshortDescription.text ?? ""
        parameters["category_id"] = self.selectedChildrenID?.description
        parameters["price_hint"]  = tfPriceHint.text ?? ""
        parameters["price_type"]  = priceType
        if jsonPriecingAll.count != 0{
               parameters["prices"] = jsonPriecingAll.toJSONString()
           }
        if features.count != 0 {
        for item in features{
            let jsonpricesitems = ["id":item.id ?? 0,"description":item.descriptionValue ?? "","optional":item.optional ?? "0","regular_price":item.regularPrice ?? 0,"value_threshold":item.valueThreshold ?? 0,"free":item.free ?? 0] as [String : Any]
        self.jsonServicesAll.append(jsonpricesitems)
            }
        if jsonServicesAll.count != 0{
               parameters["features"] = jsonServicesAll.toJSONString()
          }
        
        let imageData = (imgServicer.image) ?? UIImage()
        
        _ = WebRequests.sendPostMultipartRequestWithImgParam(url: TAConstant.APIBaseURL + "services/\(id!))", parameters: parameters,img: imageData, withName: "image", completion: { (response, error) in
            self.hideIndicator()
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }else{
                    self.showAlert(title: "".localized, message: Status.message ?? "")
                    
                    NotificationCenter.default.post(name: Notification.Name("UpdateServices"), object: nil)
                    
                    self.navigationController?.popViewController(animated: true)
                    
                    
                }
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        })
    }
}
    
    @IBAction func showImagePicker(_ sender: UIButton) {
        guard isFromEdit else {
            return
        }
           self.imagePicker.present(from: sender)
       }
       func didSelect(image: UIImage?) {
           self.imgServicer.image = image
           self.selectImage = true
       }
    
    
    func getActivity(_ typeActivt:Int){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "categories/\(typeActivt)", method: HTTPMethod.get,isAuthRequired: false).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(CategroiesObject.self, from: response.data!)
                self.ActivityData = Status.data!
                self.Activity = self.ActivityData!.resources!
                 self.getDetails()
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        
    }
}
