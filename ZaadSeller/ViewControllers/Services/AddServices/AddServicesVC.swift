//
//  AddServicesVC.swift
//  ZaadSeller
//
//  Created by osamaaassi on 1/24/21.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class AddServicesVC: SuperViewController,AddFeaturesDelgate {
    
    func SelectedDone(description: String, optionaId: String, free: String, regularThreshold: String, regularPrice: String) {
        print(description,optionaId,free,regularThreshold,regularPrice)
        let jsonAddtion = ["description":description,"optional":optionaId,"free":free,"value_threshold":regularThreshold,"regular_price":regularPrice , "value":"1"]
        jsonServicesAll.append(jsonAddtion)
        
        tableViewServices.reloadData()
        self.heighttableViewServices.constant = self.tableViewServices.contentSize.height
        
    }
    
    
    
    
    
    @IBOutlet weak var tableViewServices: UITableView!
    @IBOutlet weak var heighttableViewServices: NSLayoutConstraint!
    //Services Description
    
    @IBOutlet weak var lblTitle: UITextField!
    @IBOutlet weak var tfdescription: UITextView!
    @IBOutlet weak var tfShortDescrbtion: UITextField!
    @IBOutlet weak var tfCategoryId: UITextField!
    @IBOutlet weak var tfTypeCategory: UITextField!
    
    
    
    @IBOutlet weak var TypeService: UIView!
    
    //Prices
    @IBOutlet weak var tfTypePrice: UITextField!
    @IBOutlet weak var tfPriceHint: UITextField!
    @IBOutlet weak var tfMaxPrice: UITextField!
    @IBOutlet weak var tfMinPrice: UITextField!
    @IBOutlet weak var vMaxPrice: UIView!
    @IBOutlet weak var vMinPrice: UIView!
    @IBOutlet weak var vAllPrice: UIView!
    @IBOutlet weak var lbMPrice: UILabel!
    @IBOutlet weak var lbMinPrice: UILabel!
    
    //Image
    var selectImage:Bool? = false
    var imagePicker: ImagePicker!
    @IBOutlet weak var imgServices: UIImageView!
    @IBOutlet weak var btImage: UIButton!
    
    
    
    var Activity = [Category]()
    var ActivityData:CategroiesData?
    var selectedTypeActivityID:Int?
    
    //children
//    var typeServices = [Category]()
    var childre = [Children]()
    var selectedChildrenID:Int?
    
    var pricingItems = [PricingItems]()
    var pricingData : PricingData?
    var selectedpricingID:Int?
    
    
    var pricingOptional = ["No","Yes"]
    var PricingFree = ["No","Yes"]
    var isServices:Bool?
    var isOptional:Bool?
    var isFree:Bool?
    
    var jsonServicesAll: [[String: Any]] = []
    var jsonPriecingAll: [[String: Any]] = []
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getActivity(2)
        getPricingTypes()
        
        TypeService.isHidden = true
        vMaxPrice.isHidden = true
        vMinPrice.isHidden = true
        lbMPrice.isHidden = true
        lbMinPrice.isHidden = true
        vAllPrice.isHidden = true
        
        //tfdescription.placeholder = "Write here ...".localized
        
        tableViewServices.registerCell(id: "AddFeaturesTV")
        tableViewServices.tableFooterView = UIView.init(frame: .zero)
        
        NotificationCenter.default.addObserver(self, selector:#selector(UpdateCellService(notification:)), name: Notification.Name("UpdateCellService"), object: nil)
        
        
        
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        
        // Do any additional setup after loading the view.
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2901960784, blue: 0.5921568627, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        let navgtion = self.navigationController as! CustomNavigationBar
        navgtion.setCustomBackButtonWhiteForViewController(sender: self)
        navgtion.setLogotitle(sender: self)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    
    
    
    @objc func UpdateCellService(notification: NSNotification)  {
        if jsonServicesAll.count == 0{
            heighttableViewServices.constant = 0
        }else{
            heighttableViewServices.constant = 556
            
        }
    }
    
}



extension AddServicesVC{
    
    @IBAction func didTab_Activity(_ sender: UIButton) {
        self.tfTypeCategory.text = ""
        self.selectedChildrenID = 0
        if selectedChildrenID  == 0 {
            self.TypeService.isHidden = true
        }
        
        ActionSheetStringPicker.show(withTitle: self.tfCategoryId.text, rows: self.Activity.map { $0.title as Any }
            , initialSelection: 0, doneBlock: {
                picker, value, index in
                if let Value = index {
                    self.tfCategoryId.text = Value as? String
                    self.selectedTypeActivityID = self.Activity[value].id ?? 0
                    
                    if let object = self.Activity.filter({ $0.id == self.selectedTypeActivityID }).first{
                        if object.children != nil {
                            self.childre = object.children!
                            self.TypeService.isHidden = false
                        }
                        
                        
                    }
                }
                return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    
    @IBAction func didTab_Services(_ sender: UIButton) {
        
        guard (selectedTypeActivityID != nil) else {
            self.showAlert(title: "error".localized, message: "Service rating must be specified.".localized)
            return
        }
        guard (self.childre.count != 0) else {
            self.showAlert(title: "error".localized, message: "There are no types of service.".localized)
            
            return
        }
        
        ActionSheetStringPicker.show(withTitle: self.tfTypeCategory.text, rows: self.childre.map { $0.title as Any }
            , initialSelection: 0, doneBlock: {
                picker, value, index in
                if let Value = index {
                    self.tfTypeCategory.text = Value as? String
                    self.selectedChildrenID = self.childre[value].id ?? 0
                    
                }
                return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    
    @IBAction func didTab_PriceingType(_ sender: UIButton) {
        ActionSheetStringPicker.show(withTitle: self.tfTypePrice.text, rows: self.pricingItems.map { $0.name as Any }
            , initialSelection: 0, doneBlock: {
                picker, value, index in
                if let Value = index {
                    self.tfTypePrice.text = Value as? String
                    self.selectedpricingID = self.pricingItems[value].id ?? 0
                    
                    if self.selectedpricingID == 2 {
                        self.vMaxPrice.isHidden = false
                        self.vMinPrice.isHidden = false
                        self.vAllPrice.isHidden = false
                        self.lbMPrice.isHidden = false
                        self.lbMinPrice.isHidden = false
                        self.tfMaxPrice.text = ""
                        self.tfMinPrice.text = ""
                        
                    }else if self.selectedpricingID == 1 {
                        self.vMinPrice.isHidden = false
                        self.vMaxPrice.isHidden = true
                        self.vAllPrice.isHidden = false
                        self.lbMPrice.isHidden = true
                        self.lbMinPrice.isHidden = false
                        self.tfMaxPrice.text = ""
                        self.tfMinPrice.text = ""
                    }
                    else if self.selectedpricingID == 3 {
                        self.vMaxPrice.isHidden = true
                        self.vMinPrice.isHidden = true
                        self.vAllPrice.isHidden = true
                        self.lbMPrice.isHidden = true
                        self.lbMinPrice.isHidden = true
                        self.tfMaxPrice.text = ""
                        self.tfMinPrice.text = ""
                    }
                }
                return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    
    
    
    
    func getActivity(_ typeActivt:Int){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "categories/\(typeActivt)", method: HTTPMethod.get,isAuthRequired: false).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(CategroiesObject.self, from: response.data!)
                self.ActivityData = Status.data!
                self.Activity = self.ActivityData!.resources!
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        
    }
    
    
    func getPricingTypes(){
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        _ = WebRequests.setup(controller: self).prepare(query: "services/pricing-types", method: HTTPMethod.get,isAuthRequired: false).start(){ (response, error) in
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200{
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
            do {
                let Status =  try JSONDecoder().decode(PricingTypes.self, from: response.data!)
                self.pricingData = Status.data!
                self.pricingItems = self.pricingData!.items!
                
            } catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        }
        
    }
    
}


extension AddServicesVC {
    
    @IBAction func didTab_AddPrice(_ sender: UIButton) {
        
        
        let vc:AddFeaturesPupVC = AddFeaturesPupVC.loadFromNib()
        vc.addFeaturesDelgate = self
        
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: false, completion: nil)
        
        
    }
    
    @IBAction func addStore (_ sender: UIButton) {
        
        guard Helper.isConnectedToNetwork() else {
            self.showAlert(title: "".localized, message: "There is no internet connection".localized)
            return }
        
        
        
        guard let title = self.lblTitle.text, !title.isEmpty else{
            self.showAlert(title: "error".localized, message: "The service name must be entered".localized)
            return
        }
        guard let description = self.tfdescription.text, !description.isEmpty else{
            self.showAlert(title: "error".localized, message: "Service description must be entered.".localized)
            return
        }
        guard let selectedChildren = self.selectedChildrenID?.description, selectedChildren != "0" else{
            self.showAlert(title: "error".localized, message: "Classification must be specified".localized)
            return
        }
        guard let priceType = self.selectedpricingID?.description, priceType != "0" else{
            self.showAlert(title: "error".localized, message: "Pricing method must be determined".localized)
            return
        }
        
        
        if priceType == "1" {
            guard let minPrice = self.tfMinPrice.text, !minPrice.isEmpty else{
                self.showAlert(title: "error".localized, message: "You must enter the lowest price".localized)
                return
            }
            jsonPriecingAll.removeAll()
            let jsonAddPrice = ["id": "","description":self.tfPriceHint.text ?? "","min_quantity": minPrice ,"max_quantity":"", "regular_price":0] as [String : Any]
            jsonPriecingAll.append(jsonAddPrice)
            
        }
        else if priceType == "2"{
            guard let maxPrice = self.tfMaxPrice.text, !maxPrice.isEmpty else{
                self.showAlert(title: "error".localized, message: "You must enter the highest price".localized)
                return
            }
            guard let minPrice = self.tfMinPrice.text, !minPrice.isEmpty else{
                self.showAlert(title: "error".localized, message: "You must enter the lowest price".localized)
                return
            }
            jsonPriecingAll.removeAll()
            let jsonAddPrice = ["id": "","description":self.tfPriceHint.text ?? "","min_quantity": minPrice ,"max_quantity":maxPrice, "regular_price":0] as [String : Any]
            jsonPriecingAll.append(jsonAddPrice)
        }
        else if priceType == "3"{
            let jsonAddPrice = ["id": "","description":self.tfPriceHint.text ?? "","min_quantity":"" ,"max_quantity": "", "regular_price":0] as [String : Any]
            jsonPriecingAll.removeAll()
            jsonPriecingAll.append(jsonAddPrice)
        }
        guard  self.selectImage ?? false else{
            self.showAlert(title: "error".localized, message: "Please select a picture".localized)
            return
        }
        
        
        var parameters: [String: Any] = [:]
        parameters["title"]  = title
        parameters["description"]  = description
        parameters["short_description"]  = tfShortDescrbtion.text ?? ""
        parameters["category_id"] = selectedChildren
        parameters["price_type"]  = priceType
        parameters["price_hint"]  = tfPriceHint.text ?? ""
        
        if jsonPriecingAll.count != 0{
            parameters["prices"] = jsonPriecingAll.toJSONString()
        }
        if jsonServicesAll.count != 0{
            parameters["features"] = jsonServicesAll.toJSONString()
        }
        
        let imageData = (imgServices.image) ?? UIImage()
        
        _ = WebRequests.sendPostMultipartRequestWithImgParam(url: TAConstant.APIBaseURL + "services", parameters: parameters,img: imageData, withName: "image", completion: { (response, error) in
            self.hideIndicator()
            do {
                let Status =  try JSONDecoder().decode(StatusStruct.self, from: response.data!)
                if Status.status != 200 {
                    
                    self.showAlert(title: "error".localized, message:Status.message!)
                    return
                }else{
                    self.showAlert(title: "".localized, message: Status.message ?? "")
                    
                    NotificationCenter.default.post(name: Notification.Name("UpdateServices"), object: nil)
                    
                    self.navigationController?.popViewController(animated: true)
                  
            
                }
            }catch let jsonErr {
                print("Error serializing  respone json", jsonErr)
            }
        })
    }
}

extension AddServicesVC: ImagePickerDelegate {
    @IBAction func showImagePicker(_ sender: UIButton) {
        self.imagePicker.present(from: sender)
    }
    func didSelect(image: UIImage?) {
        self.imgServices.image = image
        self.selectImage = true
    }
    
    
    
    func jsonString(data : Any) -> String {
        
        var jsonString = "";
        
        do {
             let jsonData = try JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
             
            jsonString = NSString(data: jsonData, encoding:String.Encoding.utf8.rawValue) as! String
        } catch {
            print(error.localizedDescription)
        }
        return jsonString;
    }
}


extension AddServicesVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return jsonServicesAll.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableViewServices.dequeueReusableCell(withIdentifier: "AddFeaturesTV", for: indexPath) as! AddFeaturesTV
        
        let object = jsonServicesAll[indexPath.row]
        cell.tfFeaturesDescription.text  = (object["description"] as! String)
        cell.tfRegularThreshold.text  = (object["value_threshold"] as! String)
        cell.featuresRegularPrice.text  = (object["regular_price"] as! String)
        if (object["free"] as! String == "true"){
            cell.tfFree.text  = "Yes".localized
        }else{
            cell.tfFree.text  = "No".localized
        }
        if ((object["optional"] as! String) == "true"){
            cell.tfoptional.text  = "Yes".localized
        }else{
            cell.tfoptional.text  = "No".localized
        }
        
        cell.DeleteTapped = { [weak self] in
            self?.jsonServicesAll.remove(at: indexPath.row)
            self?.tableViewServices.deleteRows(at: [indexPath], with: .left)
            self?.tableViewServices.reloadData()
            NotificationCenter.default.post(name: Notification.Name("UpdateCellService"), object: nil)
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.heighttableViewServices.constant = self.tableViewServices.contentSize.height }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 556
    }
    
}


extension Collection where Iterator.Element == [String:Any] {
    func toJSONString(options: JSONSerialization.WritingOptions = .prettyPrinted) -> String {
        if let arr = self as? [[String:Any]],
            let dat = try? JSONSerialization.data(withJSONObject: arr, options: options),
            let str = String(data: dat, encoding: String.Encoding.utf8) {
            return str
        }
        return "[]"
    }
}
