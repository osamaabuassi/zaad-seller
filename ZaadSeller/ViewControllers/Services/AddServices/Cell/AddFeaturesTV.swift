//
//  AddFeaturesTV.swift
//  ZaadSeller
//
//  Created by osamaaassi on 1/26/21.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit

class AddFeaturesTV: UITableViewCell {

      @IBOutlet weak var tfFeaturesDescription: UITextField!
      @IBOutlet weak var tfoptional: UITextField!
      @IBOutlet weak var tfFree: UITextField!
      @IBOutlet weak var tfRegularThreshold: UITextField!
      @IBOutlet weak var featuresRegularPrice: UITextField!
      @IBOutlet weak var vDelete: UIView!
    
    var DeleteTapped: (() -> ())?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func Delete(_ sender: UIButton) {
             DeleteTapped?()
         }
    
}
