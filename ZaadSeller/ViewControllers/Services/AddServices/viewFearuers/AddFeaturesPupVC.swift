//
//  AddFeaturesPupVC.swift
//  ZaadSeller
//
//  Created by osamaaassi on 1/26/21.
//  Copyright © 2021  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

protocol AddFeaturesDelgate : class {
    func SelectedDone(description:String,optionaId:String,free:String,regularThreshold:String,regularPrice:String)
  
}

class AddFeaturesPupVC: SuperViewController {
    
    //    //Features
    @IBOutlet weak var tfFeaturesDescription: UITextField!
    @IBOutlet weak var tfoptional: UITextField!
    @IBOutlet weak var tfFree: UITextField!
    @IBOutlet weak var tfRegularThreshold: UITextField!
    @IBOutlet weak var featuresRegularPrice: UITextField!
    
    
    
    var addFeaturesDelgate: AddFeaturesDelgate?
    var pricingOptional = ["No","Yes"]
    var PricingFree = ["No","Yes"]
    var isOptional:Bool?
    var isFree:Bool?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func didTab_PriceingOptional(_ sender: UIButton) {
        ActionSheetStringPicker.show(withTitle: self.tfoptional.text, rows: self.pricingOptional.map { $0.localized as Any }
            , initialSelection: 0, doneBlock: {
                picker, value, index in
                if let Value = index {
                    self.tfoptional.text = Value as? String
                    if self.tfoptional.text == "Yes".localized {
                        self.isOptional = true }else {
                        self.isOptional = false
                    }
                    
                }
                return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    @IBAction func didTab_PriceingFree(_ sender: UIButton) {
        ActionSheetStringPicker.show(withTitle: self.tfFree.text, rows: self.PricingFree.map { $0 as Any }
            , initialSelection: 0, doneBlock: {
                picker, value, index in
                if let Value = index {
                    self.tfFree.text = Value as? String
                    if self.tfFree.text == "No".localized {
                        self.isFree = true }else {
                        self.isFree = false
                    } 
                }
                return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    @IBAction func Cancel(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    
    @IBAction func Add(_ sender: UIButton) {
        
        guard let description = self.tfFeaturesDescription.text, !description.isEmpty else{
            self.showAlert(title: "error".localized, message: "Please fill in all fields".localized)
            return
        }
        guard let optional = self.tfoptional.text, !optional.isEmpty else{
            self.showAlert(title: "error".localized, message: "Please fill in all fields".localized)
            return
        }
        guard let free = self.tfFree.text, !free.isEmpty else{
            self.showAlert(title: "error".localized, message: "Please fill in all fields".localized)
            return
        }
        guard let regularThreshold = self.tfRegularThreshold.text, !regularThreshold.isEmpty else{
            self.showAlert(title: "error".localized, message: "Please fill in all fields".localized)
            return
        }
        guard let regularPrice = self.featuresRegularPrice.text, !regularPrice.isEmpty else{
            self.showAlert(title: "error".localized, message: "Please fill in all fields".localized)
            return
        }
        
        self.dismiss(animated: false) {
            self.addFeaturesDelgate?.SelectedDone(description: description ,optionaId:self.isOptional!.description,free:self.isFree!.description,regularThreshold:regularThreshold,regularPrice:regularPrice)
            
        }
        
    }
}
