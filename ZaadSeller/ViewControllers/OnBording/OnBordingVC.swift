//
//  OnBordingVC.swift
//  ZaadSeller
//
//  Created by  Ahmed’s MacBook Pro on 7/3/20.
//  Copyright © 2020  Ahmed’s MacBook Pro. All rights reserved.
//

import UIKit
import FSPagerView

class OnBordingVC: UIViewController {
    
    
    @IBOutlet weak var pageControlle: FSPageControl!{
        didSet{
            pageControlle.numberOfPages =  4
            pageControlle.contentHorizontalAlignment = .center
            pageControlle.setImage(UIImage(named: "unselect"), for: .normal)
            pageControlle.setImage(UIImage(named: "select"), for: .selected)
        }
        
    }
    
    @IBOutlet weak var pagerView: FSPagerView!{
        didSet{
            self.pagerView.isScrollEnabled = true
            self.pagerView.register(UINib(nibName: "OnBordingCell", bundle: Bundle.main), forCellWithReuseIdentifier: "OnBordingCell")
            
        }
    }
    
    
    var listArray = [String]()
    var listTextArray = [String]()
    var listdisArray = [String]()
    var indexnext:Int?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupdata()
    }
    
    
    func setupdata(){
        listArray = ["img1","img2","img4","img3"]
        listdisArray = ["Marketing and sales increase", "Safe Means", "Kits", "Delivery"]
    
        listTextArray = ["We help you in marketing your products to reach potential customers that you did not expect to reach.","Your customers can pay electronically and receive your sales amounts directly through our partner Thawani platform.","Follow the sales that take place in your store, know the most sold products you have, the most viewed products from store visitors, and other important reports that you can get directly from the control panel in your store","We provide you with options to deliver your products to your customers, and you can use the delivery services that suit you"]
    }
    
    @IBAction func bt_Next(_ sender: Any) {
        let nextIndex = pagerView.currentIndex + 1 < numberOfItems(in:self.pagerView) ? pagerView.currentIndex + 1 : 0
        indexnext = nextIndex
        pagerView.scrollToItem(at: nextIndex, animated: true)
        if nextIndex == 0{
            let mainVC = LoginVC()
            let window = UIApplication.shared.windows.first
            window?.rootViewController = mainVC.navigationController()
            window?.makeKeyAndVisible()
        }
    }
    
    
    
    
    
}

extension OnBordingVC:FSPagerViewDelegate,FSPagerViewDataSource{
    
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return listArray.count
    }
    
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "OnBordingCell", at: index) as! OnBordingCell
        
        cell.lbname.text = listTextArray[index].localized
        cell.lblDesc.text  = listdisArray[index].localized
        cell.img.image = UIImage(named: listArray[index])
        
        return cell
    }
    
    
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        self.pageControlle.currentPage = pagerView.currentIndex
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.pageControlle.currentPage = targetIndex
        if targetIndex == 0{
            let mainVC = LoginVC()
            UIApplication.shared.keyWindow?.rootViewController = mainVC.navigationController()

        }
        
    }
    
    
}
